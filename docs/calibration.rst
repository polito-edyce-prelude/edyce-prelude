Ensure that you have Python ^3.8, `Poetry <https://python-poetry.org/>`_ and you have followed the installation procedure.


Calibration
===========

With PREDYCE it is possible to perform semi-automatic calibration procedures on an IDF model in order to align its behaviour to real building over a specific period of time.
In particular, the calibration is made possible by manually tuning actions and parameters with the help of sensitivity analysis indicators and charts given by PREDYCE.

What you need
-------------

* `EnergyPlus <https://energyplus.net/>`_ installed on your system
* The IDF file of the same version of EnergyPlus
* An EPW file of real weather data
* The input JSON file for calibration
* A CSV file of monitored data for a specific time period. The file must  have datetime indeces as first column in order to be recognized.

.. tip::
    IDF version is found on the top of IDF file if you open it with a text editor::

        Version, 8.9.0.001;                               !- Version Identifier
    
    The IDF version must be the same as the one of EnergyPlus installed on the system;
    if not you can install the correct version of EnergyPlus or update the
    IDF file with the built-in IDFVersionUpdater of EnergyPlus under the PreProcess
    folder. For more information visit `IDF Version Updater <https://bigladdersoftware.com/epx/docs/8-9/auxiliary-programs/idf-version-updater.html>`_ page.

.. tip::
    You do not need to specify the IDD file but only its version, e.g. "8.9".

YAML configuration file
^^^^^^^^^^^^^^^^^^^^^^^
This file is called ``config.yml`` and it is stored into PREDYCE main directory.
It is used to specify some parameters that are usually carried among different experiments and they do not need to be changed very often.

You can create you own configuration file by renaming **config.example.yml** into **config.yml**.

Here you can specify different things:

:plot_dir: Directory where KPI plots are saved
:out_dir: Directory where CSV file of results is saved
:num_of_cpus: number of parallel simulations, number of parallel simulations, defaults to 0 (auto, i.e. match CPU cores)
:idd_version: Version of EnergyPlus and IDF, e.g. "8.9"
:data_true_dir: Directory that include the monitored CSV file

JSON file for calibration
^^^^^^^^^^^^^^^^^^^^^^^^^
This file is used to specify the actions to perform during calibration phase and the types of plot to be generated.
It should be manually edited to tune calibration parameters after a batch of simulation is executed.

JSON input file example:

.. code-block:: JSON

    {
        "scenario": "calibration",
        "building_name": "MainBlock",
        "preliminary_actions": {
            "change_runperiod": {
                "start": "08-04",
                "end": "28-04",
                "fmt": "%d-%m"
            }
        },
        "data_true_filename": "data_true_main_block_resampled.csv",
        "data_true_columns": ["zone1_tdb", "zone2_tdb"],
        "plot_dir_subfolder": "calibration1",
        "out_dir_subfolder": "calibration1",
        "calibration_objectives": {
            "Zone Mean Air Temperature": ["MainBlock:Zone1", "MainBlock:Zone2"]
        },
        "calibration_actions": {
            "change_ufactor_walls": {
                "range": 0.9,
                "step": 0.05,
                "args": {
                    "relative": true
                }
            }
        },
        "calibration": {
            "rmse": {},
            "mbe": {},
            "rmse_mbe": {},
            "calibration_signature": {},
            "comparison": {},
            "plot_variables": {
                "variables":  [
                    {
                        "sim": ["MainBlock:Zone1"],
                        "meas": ["zone1_tdb"]
                    },
                    {
                        "sim": ["MainBlock:Zone2"],
                        "meas": ["zone2_tdb"]
                    }
                ]
            }
        }
    }


:building_name: The name of building which specifies zones that are included in the computations.
:preliminary_actions: Actions that are executed once before creating the pool of simulations. All building will share the same preliminary actions.
:data_true_filename: Name of the CSV file (without path) containing monitored data for the building.
:data_true_columns: List of columns to be considered when reading the file specified in "data_true_filename".
:calibration_objectives: Variable with associated list of IDF zones from which calibration indicators are evaluated.
:calibration_actions: Set of actions that will generate the pool of simulations.

  - range: range of variations (e.g. 0.9 means from :math:`-0.9` to :math:`+0.9`)
  - step: step inside range, e.g., *range* equal to 0.3 and *step* equal to 0.5 will generate a set of values :math:`[-0.3, -0.25, -0.20, ..., +0.25, +0.3]`.

:calibration: Set of indicators (RMSE, MBE, RSME_MBE, calibration signature) to be computed.

  - plot_variables: List of set of variables (simulated, measured) to be plotted as a comparison.

:plot_dir_subfolder:  Used to specify a subfolder for plot directory.
:out_dir_subfolder:  Used to specify a subfolder for out directory.

Running calibration from command line 
-------------------------------------
Arguments
^^^^^^^^^

-d, --output-directory            output folder
-t, --temp-directory              temp folder
-p, --plot-directory              plot folder
-m, --measured-data-directory     directory that include the monitored CSV file
-f, --input-file                  input JSON file
-w, --weather                     EPW file or EPW directory
-i, --idd                         IDD version
-j, --jobs                        number of parallel simulations, defaults to 0 (auto)
-o, --original                    include original model in dataframe, defaults to ``True``

.. warning::
    Command line options will override YAML configuration file options.

Running
^^^^^^^

From a terminal, activate the virtual environment::

    poetry shell

Run calibration::

    python run_calibrations.py ...

Outputs
-------
The outputs are organized as following:

* In the output folder you get a data_res.csv file with the list of all executed simulations and the respective results.
* In the plot folder you get a list of numbered subfolders which follow the indexing of data_res.csv. Inside each folder you can find the plots for the corresponding simulation.

Suggested procedure
-------------------
In the following it is proposed a generic way to perform the model calibration:

1. Choose an initial small set of actions with a relatively small amount of parameters in order to limit the pool of simulations.
2. Order the data_res CSV file based on the **rsme_mbe** column in ascending order.
3. Identify which actions and parameters mostly improve the building behaviour. The plots *predicted_vs_true.png* and individual rooms graphs can help in choosing the best condition.
4. Set the found values for corresponding actions by adding ``forced_value: [<value>]`` in the json file.
  
Example:

.. code-block:: JSON

    "change_infiltration": {
        "forced_value": [0.2],
        "args": {
            "relative": true,
            "filter_by": "Zone2"
    }

5. Add or modify actions and corresponding parameters in the json file and repeat until the best compromise is found and all actions have forced values.

Examples
--------

data_res.csv
^^^^^^^^^^^^

The rows are sorted by **rmse_mbe** in ascending order to find out the "best" building version.

.. csv-table::
   :file: images/calibration_data_res.csv
   :header-rows: 1


Calibration signature plot
^^^^^^^^^^^^^^^^^^^^^^^^^^

Before calibration:

.. figure:: images/cs_start.png
   :height: 350
   :alt: Calibration signature (start)
   :align: center

After calibration:

.. figure:: images/cs_final.png
   :height: 350
   :alt: Calibration signature (final)
   :align: center

Temperature timeseries plot
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Predicted vs Monitored:

.. figure:: images/predicted_vs_true_time.png
   :height: 350
   :alt: Predicted vs monitored (timeseries)
   :align: center
