****************************
PREDYCE Online Documentation
****************************
-----------------------------------------
Politecnico di Torino --- E-DYCE--PRELUDE
-----------------------------------------

.. figure:: images/Polito_Logo_2021_BLU.png
   :height: 80
   :alt: Polito logo
   :align: left

This software has been created by Politecnico di Torino under different
implementation actions: i. *DYCE*, part of the EU H2020 project **E-DYCE**
(https://edyce.eu/) co-funded under the GA 893945, and ii. *PRE*, part of the
EU H2020 project **PRELUDE** (https://prelude-project.eu/) co-funded under the GA
958345.

.. predyce documentation master file, created by
   sphinx-quickstart on Thu Feb 11 16:08:47 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Complete code can be found at https://gitlab.com/polito-edyce-prelude/predyce

.. note::
   Predyce currently supports **EnergyPlus Version 8.9** only.

.. toctree::
   :maxdepth: 1
   :caption: Scenarios
   :glob:

   sensitivity-analysis
   calibration
   performance-gap
   24h-forecast

.. toctree::
   :maxdepth: 2
   :caption: Modules
   :glob:

   modules
   

.. autosummary::
   :toctree: generated

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
