Ensure that you have Python ^3.8, `Poetry <https://python-poetry.org/>`_ and you have followed the installation procedure.


24h Forecast
============

The 24-hour forecasting technology is a scenario of the PREDYCE dynamic simulation platform that aims at improving thermal comfort in confined spaces by exploiting the untapped potential of free-running and bioclimatic technologies – e.g. heat gain prevention through the activation of shading systems and heat gain dissipation via ventilative cooling. \
The approach is based on the combination of weather forecasted data and building energy dynamic simulations to provide ad hoc suggestions for tenants/occupants and building management systems (BMS).
In the current implementation, these suggestions are available for managing shading, controlled natural ventilation, and mechanical ventilation systems.
Suggested actions can be integrated within automatic actuators for a more straightforward execution or to drive users’ self-actuation.

.. important::
    The script is optimised for specific demo cases with custom schedules, variable associations and strategies; it is not intended for general usage.
    The code must be carefully adapted in order to support other building models.

What you need
-------------

* `EnergyPlus <https://energyplus.net/>`_ installed on your system
* A CSV file of monitored data for a specific time period. The file must have datetime indeces as first column in order to be recognized.
* The IDF file of your model which should feature:
  
  * HVAC setpoint schedules as *Schedule:File* objects pointing to monitored data.
  * HVAC availability schedules as *Schedule:File* objects pointing to a CSV file.
  * Controllable 24hf elements (e.g. shading, windows, mechanical ventilation) whose schedules are *Schedule:File* objects pointing to a CSV file.
* An EPW file of real weather data and forecast data combined together
* The input JSON file for 24h forecasting

YAML configuration file
^^^^^^^^^^^^^^^^^^^^^^^
This file is called ``config.yml`` and it is stored into PREDYCE main directory.
It is used to specify some parameters that are usually carried among different experiments and they do not need to be changed very often.

You can create you own configuration file by renaming **config.example.yml** into **config.yml**.

Here you can specify different things:

:plot_dir: Directory where KPI plots are saved
:out_dir: Directory where CSV file of results is saved
:num_of_cpus: number of parallel simulations, defaults to 0 (auto, i.e. match CPU cores)
:idd_version: Version of EnergyPlus and IDF, e.g. "8.9"
:data_true_dir: Directory that include the monitored CSV file

JSON file for 24h forecast
^^^^^^^^^^^^^^^^^^^^^^^^^^

JSON input file example:

.. code-block:: JSON

    {
        "scenario": "24hf",
        "demo_case": "demo1",
        "building_name": "demo1",
        "actions": {},
        "day": "09/10",
        "hour_aggregations": [
            [0, 1, 2, 3, 4, 5, 6],
            [7],[8, 9, 10, 11, 12, 13, 14],
            [15], [16], [17], [18], [19], [20], [21], [22, 23, 24]
        ],
        "forecast_actions": {
            "blind": [0, 1],
            "stack_and_wind_ventilation": [0, 0.3, 0.6, 1]
        },
        "outputs": [],
        "preliminary_actions": {},
        "kpi": {
            "Q_c": {},
            "Q_h": {},
            "adaptive_comfort_model": {},
            "adaptive_residuals": {}
        },
        "optimization_algorithm": "NSGA2",
        "D-D_partitions_number": 10,
        "population_size": 20,
        "num_generations": 3,
        "optimization_objectives": ["adaptive_residuals", "stack_score", "blind_score"],
        "optimization_parameters": {}
    }

:demo_case: Name of demo case to be accessed in 24h scripts for differentiation purposes.
:day: Current day. The 24h forecast will be computed on the following day.
:hour_aggregations: Aggregation of hours in blocks. Strategies do not change within the same block.
:forecast_actions: Set of actions whose strategies are to be predicted.
:optimization_algorithm: Algorithm used by the optimised in PREDYCE.
:D-D_partitions_number: Number of gaps between two consecutive points along an objective axis.
:population_size: Population size of NSGA algorithm.
:num_generations: Number of generations of NSGA algorithm.
:optimization_objectives: Values to be minimized/maximized by the optimizer. *adaptive_residuals* refers to thermal comfort; the other *score* variables are defined in 24h forecast scripts.

Running 24h forecast from command line
--------------------------------------
Arguments
^^^^^^^^^

-f, --input-file                  input JSON file
-w, --weather                     EPW file with historical and forecast data
-d, --output-directory            output folder
-t, --temp-directory              temp folder
-p, --plot-directory              plot folder
-o, --original                    include original model in dataframe, defaults to ``True``
-j, --jobs                        number of parallel simulations, defaults to 0 (auto)

.. warning::
    Command line options will override YAML configuration file options.

Running
^^^^^^^

From a terminal, activate the virtual environment::

    poetry shell

Run calibration::

    python run_24h_forecast.py my_model.idf -f my_input.json -w my_epw.epw -o -j 30

Outputs
-------
The main output of 24h forecast consists of:

* CSV file containing predictions for the selected day