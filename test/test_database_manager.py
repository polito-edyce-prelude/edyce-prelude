#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Test script for idf_editor"""
from predyce import database_manager
from pathlib import Path
import json as js

P = Path(__file__).parent.absolute()
DB = P / "JSON/test_database.json"


def test_load_file():
    db = database_manager.Database(DB)
    db.load_file()
    assert isinstance(db.database, dict)


def test_get_object():
    db = database_manager.Database(DB)
    db.load_file()
    plaster = {
        "Name": "plaster lime and gypsum 15 mm",
        "Roughness": "Rough",
        "Thickness": 0.015,
        "Conductivity": 0.7,
        "Density": 1400,
        "Specific Heat": 840,
        "Thermal Absorptance": 0.9,
        "Solar Absorptance": 0.5,
        "Visible Absorptance": 0.5,
    }
    obj = db.get_object("Material".upper(), "plaster lime and gypsum 15 mm")
    assert obj == plaster

    unknown = db.get_object("Material".upper(), "unknown")
    print(unknown)
    assert unknown == -1
