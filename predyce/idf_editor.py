#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Functions used to edit IDF file"""
from geomeppy import recipes
from numpy import sin, cos, deg2rad
import numpy as np
import random
import pandas as pd
from predyce.database_manager import Database
from pathlib import Path
import os
import datetime
import uuid
import re

P = Path(os.path.dirname(__file__))
PATH = P / "database.json"
CAVITY_AIR_RESISTANCE = {
    "thickness": [0, 0.005, 0.007, 0.010, 0.015, 0.025, 0.050, 0.100, 0.300],
    "upwards": [0.0, 0.11, 0.13, 0.15, 0.16, 0.16, 0.16, 0.16, 0.16],
    "horizontal": [0.0, 0.11, 0.13, 0.15, 0.17, 0.18, 0.18, 0.18, 0.18],
    "downwards": [0.0, 0.11, 0.13, 0.15, 0.17, 0.18, 0.21, 0.22, 0.23],
}
TABLE = {
    0: 0,
    16: 1,
    33: 2,
    50: 3,
    67: 4,
    83: 5,
    100: 6
}
# HOUR_SET = {
#     0: [],
#     1: [20],
#     2: [20, 30],
#     3: [10, 20, 30],
#     4: [10, 20, 30, 40],
#     5: [0, 10, 20, 30, 40],
#     6: [0, 10, 20, 30, 40, 50]
# }
# NOTE: This is a test for LLE shading.
HOUR_SET = {
    0: [0, 10, 20, 30, 40, 50],
    1: [0, 10, 20, 30, 40, 50],
    2: [0, 10, 20, 30, 40, 50],
    3: [0, 10, 20, 30, 40, 50],
    4: [0, 10, 20, 30, 40, 50],
    5: [0, 10, 20, 30, 40, 50],
    6: [0, 10, 20, 30, 40, 50],
}
def to_list(x):
    """Convert element to a list if it is not already a list."""
    if isinstance(x, list):
        return x
    else:
        return [x]


def set_fields(obj, data=None, **fields):
    """Set value for a specific field in an IDF object.
    A dictionary with all pairs of key: value can be passed. In this case all
    keys are converted by replacing spaces with underscores.

    :param obj: geomeppy object
    :type obj: class 'geomeppy.patches.EpBunch'
    :param data: dictionary where each key corresponds to a pararameter to be
        changed in the IDF and each value corresponds to the new value.
    :type data: dict, optional
    :param fields: Same as `data` parameter but fields are passed as keyword
        arguments.

    :Example:

    .. code-block:: python

        set_fields(material, data={"Conductivity": 0.3})
        set_fields(material, Conductivity=0.3)

    """
    if isinstance(data, dict):
        data = convert_keys(data)
        for k in data.keys():
            try:
                setattr(obj, k, data[k])
            except Exception:
                print("Field %s does not exist" % k)

    for key, value in fields.items():
        try:
            setattr(obj, key, value)
        except Exception:
            print("Field %s does not exist" % key)


def set_object_params(idf, obj_str, data=None, **fields):
    """Set parameters on IDF objects given a string representing the object.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param obj_str: String representing the object; the typology and the name
        are separated by a comma. For example "material,Timber Flooring"
        will edit the idfobjects["MATERIAL"] whose name is Timber Flooring;
        a string like "material" will edit all materials regardless of name.
    :type obj_str: str
    :param data: dictionary where each key corresponds to a pararameter to be
        changed in the IDF and each value corresponds to the new value.
    :type data: dict, optional
    :param fields: Same as `data` parameter but fields are passed as keyword
        arguments, e.g. Conductivity=0.3 which is equivalent to
        {"Conductivity": 0.3} for `data` field.

    :Example:

    .. code-block:: python

        set_object_params(idf, "material,Timber Flooring", data={"Conductivity": 0.3})
        set_object_params(idf, "material,Timber Flooring", Conductivity=0.3)
    """
    if isinstance(obj_str, str):
        obj_str_list = [obj_str]
    elif isinstance(obj_str, list):
        obj_str_list = obj_str
    else:
        raise Exception
    for obj_str in obj_str_list:
        typology = obj_str.split(",")[0]
        try:
            name = obj_str.split(",")[1]
        except:
            name = None

        if name is not None:
            for el in idf.idfobjects[typology.upper()]:
                if el.Name == name:
                    set_fields(el, data, **fields)
        else:
            for el in idf.idfobjects[typology.upper()]:
                set_fields(el, data, **fields)


def set_category_params(idf, category, data=None, filter_by="", filter_by_field="Zone_Name"):
    """Set fields to objects of a specific category.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param category: Category of the IDF object, for example "OtherEquipment"
    :type category: str
    :param data: dictionary of data to be set, defaults to None
    :type data: dict, optional
    :param filter_by: String to be used as a filter, defaults to ""
    :type filter_by: str, optional
    :param filter_by_field: Fields to which filter_by is applied, defaults to "Zone_Name"
    :type filter_by_field: str, optional

    :Example:

    .. code-block:: python

        set_category_params(
            idf1,
            category="people",
            filter_by="livingroom",
            filter_by_field="Zone or ZoneList Name",
            data={
                "Number of People": "5"
            }
        )
    """
    if not isinstance(filter_by, list):
        filter_by = [filter_by]
    for cat in idf.idfobjects[category.upper()]:
        if any([f.lower() in cat[filter_by_field.replace(" ", "_")].lower() for f in filter_by]):
            set_fields(cat, data=data)


def remove_objects(idf, obj_name):
    """Delete all objects of a specific type from the IDF.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param obj_name: Name of the object, e.g. "ZoneVentilation:DesignFlowRate"
    :type obj_name: str (case insensitive)
    """
    del idf.idfobjects[obj_name.upper()][:]


def get_layer(obj, name, exact=False):
    """Get layer of a material given the name.

    :param obj: geomeppy object
    :type obj: class 'geomeppy.patches.EpBunch'
    :param name: Name of the layer or first/last
    :type name: str
    :return: Layer key
    :rtype: str
    :param exact: If the compared strings have to perfectly match, defaults to
        `False`
    :type exact: bool, optional
    """
    if name == "first":
        return obj.objls[2]

    elif name == "last":
        for el in reversed(obj.objls):
            if getattr(obj, el) != "":
                return el

    else:
        for el in obj.objls:
            if exact:
                if name.lower() == getattr(obj, el).lower():
                    layer = el
                    return layer
            else:
                if name.lower() in getattr(obj, el).lower():
                    layer = el
                    return layer


def insert_layer(obj, layer, name, below=False):
    """Insert a new layer above or below the given layer.

    :param obj: geomeppy object
    :type obj: class 'geomeppy.patches.EpBunch'
    :param layer: Key of the level above which the new level is inserted
    :type layer: str
    :param name: Name of the new layer
    :type name: str
    :param below: `True` if new layer have to be inserted below the given layer
    :type below: bool, defaults to `False`
    """
    layer_pos = obj.objls.index(layer)

    for cnt, key in enumerate(obj.objls):
        value = getattr(obj, key)
        if value == "":
            max_key = cnt
            break

    if below == False:
        for a in range(layer_pos, max_key)[::-1]:
            setattr(obj, obj.objls[a + 1], getattr(obj, obj.objls[a]))

        setattr(obj, obj.objls[a], name)
    else:
        for a in range(layer_pos + 1, max_key)[::-1]:
            setattr(obj, obj.objls[a + 1], getattr(obj, obj.objls[a]))
        setattr(obj, obj.objls[layer_pos + 1], name)


def remove_layer(obj, layer_name, exact=False):
    """Remove a layer given the name.

    :param obj: geomeppy object
    :type obj: class 'geomeppy.patches.EpBunch'
    :param layer_name: Name of the layer which needs to be removed
    :type layer_name: str
    :param exact: If the compared strings have to perfectly match, defaults to
        `False`
    :type exact: bool, optional
    """
    layer = get_layer(obj, layer_name, exact=exact)
    layer_pos = obj.objls.index(layer)

    for cnt, key in enumerate(obj.objls):
        value = getattr(obj, key)
        if value == "":
            max_key = cnt
            break

    setattr(obj, obj.objls[layer_pos], "")
    for a in range(layer_pos, max_key):
        setattr(obj, obj.objls[a], getattr(obj, obj.objls[a + 1]))

    obj.obj = obj.obj[:-1]  # Remove last empty field


def check_schedule(idf, schedule_name):
    """Check if given schedule is already present in the IDF.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param schedule_name: Schedule name (compact or file)
    :type schedule_name: str
    :return: 1 if there is a match, 0 otherwise
    :rtype: int
    """
    for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
        if sc.Name.lower() == schedule_name.lower():
            return 1
    for sf in idf.idfobjects["SCHEDULE:FILE"]:
        if sf.Name.lower() == schedule_name.lower():
            return 1
    return 0


def simple_hvac_sizing(idf, zone_heat_dict, zone_cool_dict, factor=1, **fields):
    """HVAC sizing according to yearly heating and cooling zonal max values, in 
    sobstitution to autosize defined values. The function is supposed to work
    automatically together with the homonymous function in runner module. The
    user can also provide the known maximum plant capacity [W] with keys
    'Maximum_Sensible_Heating_Capacity' and 'Maximum_Total_Cooling_Capacity'
    and eventually the known maximum flow rate values [m3/s] with keys
    'Maximum_Heating_Air_Flow_Rate' and 'Maximum_Cooling_Air_Flow_Rate'. 

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param zone_heat_dict: dictionary containing maximum heating capacity for
        each zone. It's automatically generated inside the runner module.
    :type zone_heat_dict: dict
    :param zone_cool_dict: dictionary containing maximum cooling capacity for
        each zone. It's automatically generated inside the runner module.
    :type zone_cool_dict: dict
    :param factor: factor to multiply found max capacity zonal values, in order
        to be flexible to different climate conditions, defaults to 1
    :type factor: float, optional
    """
    # simulation control set fields
    sz_control_obj = idf.idfobjects["SIMULATIONCONTROL"][0]
    # Set fields
    sz_control_properties = {
        "Do_Zone_Sizing_Calculation": "No",
        "Do_System_Sizing_Calculation": "No",
    }
    set_fields(sz_control_obj, sz_control_properties)

    # delete auto-sizing related objects
    design_days = idf.idfobjects["SIZINGPERIOD:DESIGNDAY"]
    sz_zones = idf.idfobjects["SIZING:ZONE"]
    des_spec = idf.idfobjects["DesignSpecification:ZoneAirDistribution".upper()]
    sz_params = idf.idfobjects["SIZING:PARAMETERS"]
    del design_days[:]
    del sz_zones[:]
    del des_spec[:]
    del sz_params[:]

    # change hvac limit typology
    new_sz_objs = idf.idfobjects["ZoneHVAC:IdealLoadsAirSystem".upper()]
    if "Maximum_Heating_Air_Flow_Rate" not in fields.keys():
        for el in new_sz_objs:
            el.Heating_Limit = "LimitCapacity"
    else:
        for el in new_sz_objs:
            el.Heating_Limit = "LimitFlowRateAndCapacity"
    if "Maximum_Cooling_Air_Flow_Rate" not in fields.keys():
        for el in new_sz_objs:
            el.Cooling_Limit = "LimitCapacity"
    else:
        for el in new_sz_objs:
            el.Cooling_Limit = "LimitFlowRateAndCapacity"

    # Set HVAC capacity limits.
    for el in new_sz_objs:
        for k in zone_heat_dict.keys():
            if k in el.Name:
                el.Maximum_Sensible_Heating_Capacity = zone_heat_dict[k]*factor
        for k in zone_cool_dict.keys():
            if k in el.Name:
                el.Maximum_Total_Cooling_Capacity = zone_cool_dict[k]*factor

    # Set user defined extra parameters.
    for el in new_sz_objs:
        for key, value in fields.items():
            setattr(el, key.replace(" ", "_"), value)


def set_outdoor_co2(idf, co2_schedule=""):
    """Activate outdoor CO2 contaminant calculation according to given schedule.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param co2_schedule: Schedule which has to be assigned. It follows the rules of
        Schedule:Compact objects of EnergyPlus.
    :type co2_schedule: dict
    """
    # Find or create
    if len(idf.idfobjects["ZONEAIRCONTAMINANTBALANCE"]) > 0:
        co2_obj = idf.idfobjects["ZONEAIRCONTAMINANTBALANCE"][0]
    else:
        co2_obj = idf.newidfobject("ZONEAIRCONTAMINANTBALANCE")

    # Remove () in schedule name for output readibility
    co2_schedule["Name"] = (
        co2_schedule["Name"].replace("(", "_").replace(")", "_")
    )

    # Set fields
    co2_obj_properties = {
        "Carbon Dioxide Concentration": "Yes",
        "Outdoor Carbon Dioxide Schedule Name": co2_schedule["Name"],
        "Generic Contaminant Concentration": "Yes",
        "Outdoor Generic Contaminant Schedule Name": "Off 24/7",
    }
    set_fields(co2_obj, co2_obj_properties)

    # Add schedule
    sc = idf.newidfobject("Schedule:Compact".upper())
    set_fields(sc, data=co2_schedule)
    # Add needed outputs #TODO maybe better to add from kpi
    set_outputs(
        idf, ["Schedule Value"], co2_schedule["Name"], frequency="Timestep"
    )
    set_outputs(idf, ["Zone Air CO2 Concentration"], "*", frequency="Timestep")


def change_co2_prod_rate(idf, filter_by="", multiplier=0, value=0):
    """Change zone CO2 generation rate.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param multiplier: multiply current CO2 generation rate
    :type multiplier: float, optional
    :param value: sobstitute current CO2 generation rate
    :type value: float, optional
    """
    # Filter by zone name
    people_obj_list = [
        p
        for p in idf.idfobjects["PEOPLE"]
        if filter_by.lower() in p.Name.lower()
    ]

    # Set CO2 geeration rate
    for obj in people_obj_list:
        if multiplier:
            obj.Carbon_Dioxide_Generation_Rate = (
                obj.Carbon_Dioxide_Generation_Rate * multiplier
            )
        elif value:
            obj.Carbon_Dioxide_Generation_Rate = value


def add_internal_mass(idf, filter_by="", area=0, relative=False):
    """Add internal mass to the building.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name or a list of those, defaults to ""
    :type filter_by: str or list, optional
    :param area: Area of the internal mass, defaults to 0
    :type area: int, optional
    :param relative: Specify if new value will be asssigned as a percentage
        increment from the old value, defaults to `False`
    :type relative: bool, optional
    """
    # create internal mass material
    filter_by = to_list(filter_by)
    new_mat = idf.newidfobject("MATERIAL")
    rand = random.randint(0, 1000)
    mat_properties = {
        "Name": "internal mass material - " + str(rand),
        "Roughness": "Rough",
        "Thickness": 0.012,
        "Conductivity": 1.4,
        "Density": 2100,
        "Specific Heat": 840,
        "Thermal Absorptance": 0.9,
        "Solar Absorptance": 0.6,
        "Visible Absorptance": 0.6,
    }
    set_fields(new_mat, mat_properties)

    # create internal mass cons and cons_rev
    new_cons = idf.newidfobject("CONSTRUCTION")
    new_cons_rev = idf.newidfobject("CONSTRUCTION")
    cons_properties = {
        "Name": "Internal Mass Construction - " + str(rand),
        "Outside Layer": new_mat.Name,
    }
    cons_properties_rev = {
        "Name": "Internal Mass Construction - " + str(rand) + "_Rev",
        "Outside Layer": new_mat.Name,
    }
    set_fields(new_cons, cons_properties)
    set_fields(new_cons_rev, cons_properties_rev)

    # create an internal mass per zone according to filter_by
    zones = idf.idfobjects["ZONE"]
    where = [z for z in zones if any([f.lower() in z.Name.lower() for f in filter_by])]
    for i in range(0, len(where)):
        mass = idf.newidfobject("INTERNALMASS")
        if relative:
            final_area = area * where[i].Floor_Area
        else:
            final_area = area
        mass_properties = {
            "Name": where[i].Name + " internal mass - " + str(rand),
            "Construction Name": new_cons.Name,
            "Zone Name": where[i].Name,
            "Surface Area": round(final_area, 2),
        }
        set_fields(mass, mass_properties)


def change_internal_mass(idf, value, filter_by="", relative=False):
    """Change internal mass.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New internal mass area value
    :type value: float
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param relative: Specify if new value will be asssigned as a percentage
        increment from the old value, defaults to `False`
    :type relative: bool, optional
    """
    for im in idf.idfobjects["INTERNALMASS"]:
        if filter_by.lower() in im.Zone_Name.lower():
            if relative:
                im.Surface_Area = float(im.Surface_Area) + float(im.Surface_Area) * float(value)
            else:
                im.Surface_Area = value
            im.Surface_Area = round(im.Surface_Area, 2)


def set_simplified_windows(idf, win_type="Simplified window system", filter_by="", suffix="", remove_shading=False):
    """Sobstitute all Windows and GlazedDoor objects in an IDF with a simpler
    object characterized by only 3 fields: U-factor, SHGC and visible
    transmittance.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param win_type: Construction name, defaults to "Simplified window system"
    :type win_type: str, optional
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name or a list of those, defaults to ""
    :type filter_by: str or list, optional
    """
    filter_by = to_list(filter_by)
    if win_type == None:
        pass  # leggi default

    # prendi l'elemento construction dal database
    d = Database(PATH)
    d.load_file()
    win = d.get_object("CONSTRUCTION", win_type)
    new_win = idf.newidfobject("CONSTRUCTION")
    new_mat = idf.newidfobject("WINDOWMATERIAL:SIMPLEGLAZINGSYSTEM")

    set_fields(new_win, win)
    win_layer = new_win.obj[2]
    layer_obj = d.get_object("WINDOWMATERIAL:SIMPLEGLAZINGSYSTEM", win_layer)
    
    # Assign suffix.
    new_win["Name"] += suffix
    new_win["Outside_Layer"] += suffix
    layer_obj["Name"] += suffix

    set_fields(new_mat, layer_obj)

    # cambiare il nome del pacchetto finestra usato in tutti gli oggetti finestra
    # solo al type window o anche ad altri tipi va riassegnato?
    fsd = idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]
    filtered_fsd = []
    for f in fsd:
        for bsd in idf.idfobjects["BuildingSurface:Detailed".upper()]:
            # Check if base surface is the one the window is belongs to.
            if bsd.Name.lower() == f.Building_Surface_Name.lower():
                # Check filter_by zone name and fenestration construction name.
                # Standard.
                if any([f.lower() in bsd.Zone_Name.lower() for f in filter_by]):
                    filtered_fsd.append(f)

    for f in filtered_fsd:
        if f.Surface_Type == "Window" or f.Surface_Type == "GlazedDoor":
            if len(f.Shading_Control_Name) > 0:
                # If the window has a shading control, the shaded version of
                # the construction has to be analysed and modified in order to
                # make it match the new window construction.
                for wpsc in idf.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]:
                    shad_type = wpsc.Shading_Type.lower()
                    if wpsc.Name.lower() == f.Shading_Control_Name.lower():
                        for shad_con in idf.idfobjects["CONSTRUCTION"]:
                            if (shad_con.Name.lower()
                                == wpsc.Construction_with_Shading_Name.lower()
                                and remove_shading == True
                            ):
                                f.Shading_Control_Name = ""
                            elif (
                                shad_con.Name.lower()
                                == wpsc.Construction_with_Shading_Name.lower()
                                and remove_shading != True
                            ):
                                # At this point the construction object with
                                # shading has been found. Only the shading
                                # materials have to be kept.
                                for el in shad_con.obj[2:]:
                                    keep = 0
                                    for wms in idf.idfobjects[
                                        "WINDOWMATERIAL:SHADE"
                                    ]:
                                        if el == wms.Name:
                                            keep = 1
                                    for wmb in idf.idfobjects[
                                        "WINDOWMATERIAL:BLIND"
                                    ]:
                                        if el == wmb.Name:
                                            keep = 1
                                    if not keep:
                                        remove_layer(shad_con, el, True)

                                # Copy new window layer to the shaded version
                                # of the construction to make them match.
                                if remove_shading == True:
                                    wpsc.Name += "_deleteme"
                                    f.Shading_Control_Name = ""

                                elif shad_type in [
                                    "exteriorblind",
                                    "exteriorshade",
                                ]:
                                    for layer in new_win.obj[2:]:
                                        pos = get_layer(shad_con, "last")
                                        insert_layer(
                                            shad_con, pos, layer, below=True
                                        )
                                elif shad_type in [
                                    "interiorblind",
                                    "interiorshade",
                                ]:
                                    for layer in new_win.obj[2:]:
                                        pos = get_layer(shad_con, "first")
                                        insert_layer(
                                            shad_con, pos, layer, below=False
                                        )
                                elif shad_type in [
                                    "betweenglassshade",
                                    "betweenglassblind",
                                    "switchableglazing",
                                    "exteriorscreen",
                                ]:
                                    raise NotImplementedError
                                else:
                                    raise ValueError(wpsc.Shading_Type, "not recognized")

            f.Construction_Name = new_win["Name"]

    # Remove deletable shading control objects.
    while True:
        found = 0
        for wpsc in idf.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]:
            if "_deleteme" in wpsc.Name:
                print(wpsc)
                idf.removeidfobject(wpsc)
                found = 1
        if not found:
            break


def change_windows_system(idf, win_type=None):
    """Change all Windows and GlazedDoors system with a different
    construction object.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param win_type: Construction name, defaults to None
    :type win_type: str, optional
    """
    # TODO: could be done according to orientation passing a map like in wwr
    if win_type == None:
        # TODO: Take default element
        pass

    # Read from database
    d = Database(PATH)
    d.load_file()
    win = d.get_object("CONSTRUCTION", win_type)

    # Add Construction to IDF
    new_win = idf.newidfobject("CONSTRUCTION")
    set_fields(new_win, win)

    # Add all materials of the window to the IDF if they are not present.
    # Materials are taken from the database.
    win_layers = new_win.obj[2:]
    win_materials = list(idf.idfobjects["WINDOWMATERIAL:GAS"]) + list(
        idf.idfobjects["WINDOWMATERIAL:GLAZING"]
    ) + list(idf.idfobjects["WINDOWMATERIAL:GASMIXTURE"])
    for el in win_layers:
        found = 0
        for obj in win_materials:
            if obj.Name == el:
                found = 1
                break

        if not found:
            for el_type in [
                    "WINDOWMATERIAL:GLAZING",
                    "WINDOWMATERIAL:GAS",
                    "WINDOWMATERIAL:GASMIXTURE"
                ]:
                if d.get_object(el_type, el) != -1:
                    break

            el_obj = d.get_object(el_type, el)
            new_el = idf.newidfobject(el_type)
            win_materials.append(new_el)
            set_fields(new_el, el_obj)

    # Update Construction name in all windows object.
    fsd = idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]
    for f in fsd:
        if f.Surface_Type == "Window" or f.Surface_Type == "GlazedDoor":
            # Check if the window does not overlook another room.
            if len(f.Outside_Boundary_Condition_Object) == 0:
                if len(f.Shading_Control_Name) > 0:
                    # If the window has a shading control, the shaded version of
                    # the construction has to be analysed and modified in order to
                    # make it match the new window construction.
                    for sc in idf.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]:
                        if sc.Name == f.Shading_Control_Name:
                            for shad_con in idf.idfobjects["CONSTRUCTION"]:
                                if (
                                    shad_con.Name
                                    == sc.Construction_with_Shading_Name
                                ):
                                    # At this point the construction object with
                                    # shading has been found. Only the shading
                                    # materials have to be kept.
                                    for el in shad_con.obj[2:]:
                                        keep = 0
                                        for wms in idf.idfobjects[
                                            "WINDOWMATERIAL:SHADE"
                                        ]:
                                            if el == wms.Name:
                                                keep = 1
                                        if not keep:
                                            remove_layer(shad_con, el, True)

                                    # Copy new window layer to the shaded version
                                    # of the construction to make them match.
                                    # TODO: Controllare se lo shading deve essere
                                    #       interno o esterno e inserire i livelli
                                    #       finestra nell'ordine corretto.
                                    for layer in new_win.obj[2:]:
                                        pos = get_layer(shad_con, "last")
                                        insert_layer(
                                            shad_con, pos, layer, True
                                        )

                f.Construction_Name = win_type


def change_shgc(idf, value, filter_by=""):
    """Set an SHGC (Solar Heat Gain Coefficient) value to all windows of type
    SimpleGlazingSystem.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New SHGC value
    :type value: float
    :param filter_by: Filter material by name. Can be the a string included in
        the material name or a list of accepted strings included in the
        material name, defaults to ""
    :type filter_by: str or list, optional
    """
    filter_by = to_list(filter_by)
    for wmsgs in idf.idfobjects["WINDOWMATERIAL:SIMPLEGLAZINGSYSTEM"]:
        if any([f.lower() in wmsgs.Name.lower() for f in filter_by]):
            set_fields(wmsgs, {"Solar Heat Gain Coefficient": value})


def change_ufactor_windows(idf, value, filter_by=""):
    """Set an U-Factor value to all windows of type SimpleGlazingSystem.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New U-Factor value
    :type value: float
    :param filter_by: Filter material by name. Can be the a string included in
        the material name or a list of accepted strings included in the
        material name, defaults to ""
    :type filter_by: str or list, optional
    """
    filter_by = to_list(filter_by)
    for wmsgs in idf.idfobjects["WINDOWMATERIAL:SIMPLEGLAZINGSYSTEM"]:
        if any([f.lower() in wmsgs.Name.lower() for f in filter_by]):
            set_fields(wmsgs, {"UFactor": value})


def change_visible_transmittance(idf, value, filter_by=""):
    """Set Visible Transmittance value to all windows of type
    SimpleGlazingSystem.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New visible transmittante value
    :type value: float
    :param filter_by: Filter material by name. Can be the a string included in
        the material name or a list of accepted strings included in the
        material name, defaults to ""
    :type filter_by: str or list, optional
    """
    filter_by = to_list(filter_by)
    for wmsgs in idf.idfobjects["WINDOWMATERIAL:SIMPLEGLAZINGSYSTEM"]:
        if any([f.lower() in wmsgs.Name.lower() for f in filter_by]):
            set_fields(wmsgs, {"Visible Transmittance": value})


def assign_construction_copy(
    idf,
    con_name,
    filter_by="",
    suffix=None
):
    """Detect the specified construction object, make a copy and then assign it
    to all elements included in zones of filter_by.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param con_name: Name of construction that has to be duplicated.
    :type con_name: str
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    """
    if re.search("_rev$", con_name.lower()):
        raise NotImplementedError("Directly edit _rev constructions is not supported yet.")

    for obj in idf.idfobjects["CONSTRUCTION"]:

        # Normal construction.
        if obj.Name.lower() == con_name.lower():
            obj_copy = idf.copyidfobject(obj)
            obj_name = obj.Name.lower()

            if suffix is not None:
                suff = suffix
            else:
                suff = str(uuid.uuid4()).replace("-", "")[0:6]
            
            obj_copy.Name += suff
        
        # Reversed construction.
        elif obj.Name.lower() == (con_name + "_Rev").lower():
            obj_copy_rev = idf.copyidfobject(obj)
            obj_name_rev = obj.Name.lower()

            if suffix is not None:
                suff = suffix
            else:
                suff = str(uuid.uuid4()).replace("-", "")[0:6]
            
            obj_copy_rev.Name = re.sub("_rev$", suff + "_rev", obj_copy_rev.Name.lower())


    # BuildingSurface:Detailed.
    for bsd in idf.idfobjects["BuildingSurface:Detailed".upper()]:
        # Check filter_by zone name and building construction name.
        # Standard.
        if bsd.Construction_Name.lower() == obj_name.lower() and filter_by.lower() in bsd.Zone_Name.lower():
            bsd.Construction_Name = obj_copy.Name
        # Reversed.
        elif bsd.Construction_Name.lower() == obj_name_rev.lower() and filter_by.lower() in bsd.Outside_Boundary_Condition_Object.lower():
            bsd.Construction_Name = obj_copy_rev.Name

    # FenestrationSurface:Detailed.
    for fsd in idf.idfobjects["FenestrationSurface:Detailed".upper()]: 
        for bsd in idf.idfobjects["BuildingSurface:Detailed".upper()]:
            # Check if base surface is the one the window is belongs to.
            if bsd.Name.lower() == fsd.Building_Surface_Name.lower():
                # Check filter_by zone name and fenestration construction name.
                # Standard.
                if fsd.Construction_Name.lower() == obj_name and filter_by.lower() in bsd.Zone_Name.lower():
                    fsd.Construction_Name = obj_copy.Name
                # Reversed.
                elif fsd.Construction_Name.lower() == obj_name_rev and filter_by.lower() in bsd.Zone_Name.lower():
                    fsd.Construction_Name = obj_copy_rev.Name


def change_wwr(
    idf,
    wwr_value=0.2,
    construction=None,
    force=False,
    wwr_map_value={},
    orientation=None,
):
    """Change wwr value for all windows in an IDF object or specify in
    wwr_map_value dictionary wwr corresponding to specific windows orientation.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param wwr_value: wwr for all windows, if not specified it is 20%
    :type wwr_value: float in range [0,1]
    :param construction: Name of a window construction, defaults to None
    :type construction: str, optional
    :param force: True to remove all subsurfaces before setting the WWR,
        defaults to False
    :type force: bool, optional
    :param wwr_map_value: dictionary where each key corresponds to a
        orientation and value to wwr, defaults to {}
    :type wwr_map_value: dict, {int (0,90,180,270): float (in range [0,1])}
    :param orientation: One of “north”, “east”, “south”, “west”. Walls within 45
        degrees will be affected. Defaults to None
    :type orientation: str, optional
    """
    # if wwr_value == None and wwr_map_value == None:
    #     idf.set_wwr()
    # elif wwr_value != None and wwr_map_value == None:
    #     idf.set_wwr(wwr=wwr_value)
    # elif wwr_value == None and wwr_map_value != None:
    #     idf.set_wwr(wwr_map=wwr_map_value)
    # else:
    #     idf.set(wwr=wwr_value, wwr_map=wwr_map_value)
    idf.set_wwr(
        wwr=wwr_value,
        construction=construction,
        force=force,
        wwr_map=wwr_map_value,
        orientation=orientation,
    )


def add_ceiling_insulation_tilted_roof(
    idf, ins_data=None, filter_by="", **fields
):
    """Add insulation layer on tilted roof environment non occupied ceiling.
    Area and volume reductions of roof object are neglected.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ins_data: Construction name, defaults to None
    :type ins_data: str, optional
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str or list, optional
    :param fields: Additional fields to be set, passed as keyword arguments
    """
    if ins_data == None:
        # TODO: Take default element
        pass

    # Read from database
    d = Database(PATH)
    d.load_file()
    if isinstance(ins_data, str):
        mat = d.get_object("MATERIAL", ins_data)
    elif isinstance(ins_data, dict):
        mat = ins_data

    # Find which BuildingSurface:Detailed are ceilings/roof floor
    bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]

    if not isinstance(filter_by, list):
        filter_by = [filter_by]
    cons_list = [
        b.Construction_Name
        for b in bsd
        if b.Surface_Type == "Ceiling"  # Filter only ceilings
        and any([f.lower() in b.Zone_Name.lower() for f in filter_by])
    ]
    cons_list = list(dict.fromkeys(cons_list))  # Remove duplicates

    # Add insulation layer in Construction objects
    cons = idf.idfobjects["CONSTRUCTION"]

    # Add insulation material in Material list
    new_mat = idf.newidfobject("MATERIAL")
    set_fields(new_mat, mat)
    for key, value in fields.items():
        if key != "ins_data":
            setattr(new_mat, key, value)

    for w in cons_list:
        for c in cons:
            # Add as last layer
            if c.Name == w:
                last_layer = get_layer(c, "first")
                insert_layer(c, last_layer, mat["Name"], below=False)

            # Add as first layer (reversed construction)
            if c.Name == w + "_Rev":
                first_layer = get_layer(c, "last")
                insert_layer(c, first_layer, mat["Name"], below=True)


def add_roof_insulation_tilted_roof(idf, ins_data=None):
    """Add insulation layer on tilted roof environment non occupied as internal
    layer under tilted part. Area and volume reductions on roof object are
    neglected.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ins_data: Construction name, defaults to None
    :type ins_data: str, optional
    """
    if ins_data == None:
        # TODO: Take default element
        pass

    # Read from database
    d = Database(PATH)
    d.load_file()
    mat = d.get_object("MATERIAL", ins_data)

    # Find which BuildingSurface:Detailed represent tilted roof
    bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
    cons_list = [
        b.Construction_Name
        for b in bsd
        if b.Surface_Type == "Roof"  # Filter only tilted roof
    ]
    cons_list = list(dict.fromkeys(cons_list))  # Remove duplicates

    # Add insulation layer in Construction objects
    cons = idf.idfobjects["CONSTRUCTION"]

    # Add insulation material in Material list
    new_mat = idf.newidfobject("MATERIAL")
    set_fields(new_mat, mat)

    for w in cons_list:
        for c in cons:
            # Add as last layer (reversed construction)
            if c.Name == w + "_Rev":
                last_layer = get_layer(c, "first")
                insert_layer(c, last_layer, mat["Name"], below=False)

            # Add as first layer
            if c.Name == w:
                first_layer = get_layer(c, "last")
                insert_layer(c, first_layer, mat["Name"], below=True)


def add_external_insulation_walls(idf, ins_data=None, filter_by="", **fields):
    """Add external insulation layer and plaster layer on external walls.
    Since layers are added externally internal building area and volume are
    not impacted.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ins_data: Construction objects list (insulation material and
        plaster), defaults to None
    :type ins_data: list of two string elements, optional
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param fields: Additional fields to be set, passed as keyword arguments

    :Example:

    .. code-block:: python

        add_external_insulation_walls(
            idf,
            ins_data=[
                [
                    "extruded polystyrene panel XPS 35 kg/m3 15 mm",
                    "plaster lime and gypsum 15 mm",
                ]
            ],
            filter_by="kitchen",
            Conductivity=0.4,
        )

    .. note:: *ins_data* parameter takes only names; the respective entire
        objects are included in the JSON database.
    """
    if ins_data == None:
        # TODO: Take default element
        pass

    # Read from database
    d = Database(PATH)
    d.load_file()
    obj_list = [d.get_object("MATERIAL", name) for name in ins_data]

    # Find which BuildingSurface:Detailed walls are exposed
    bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
    cons_list = [
        b.Construction_Name
        for b in bsd
        if b.Surface_Type == "Wall"  # Filter only walls
        and b.Outside_Boundary_Condition == "Outdoors"  # Only exposed
        and filter_by.lower() in b.Zone_Name.lower()
    ]
    cons_list = list(dict.fromkeys(cons_list))  # Remove duplicates

    # Add insulation layer in Construction objects
    cons = idf.idfobjects["CONSTRUCTION"]

    for cnt, mat in enumerate(obj_list):
        # Add insulation material in Material list
        new_mat = idf.newidfobject("MATERIAL")
        set_fields(new_mat, mat)

        # Set more fields.
        if cnt == 0:
            for key, value in fields.items():
                if key != "ins_data":
                    setattr(new_mat, key, value)

        # if thickness is not None:
        #     set_fields(new_mat, Thickness=thickness)
        # if conductivity is not None:
        #     set_fields(new_mat, Conductivity=conductivity)

        for w in cons_list:
            for c in cons:
                # Add as last layer
                if c.Name == w:
                    last_layer = get_layer(c, "first")
                    insert_layer(c, last_layer, mat["Name"], below=False)

                # Add as first layer (reversed construction)
                if c.Name == w + "_Rev":
                    first_layer = get_layer(c, "last")
                    insert_layer(c, first_layer, mat["Name"], below=True)


def change_ufactor_walls(idf, ufactor, filter_by="", relative=False):
    """Change Ufactor of walls to desired value or increment it by a
    specified percentage.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ufactor: desired final ufactor value or incremental percentage if
        `relative` field is set to `True`
    :type ufactor: float
    :param relative: Specify if new value will be asssigned as a percentage
        increment from the old value, defaults to `False`
    :type relative: bool, optional
    """
    change_ufactor(idf, ufactor, "Walls", filter_by, relative)


def change_specific_heat_material(idf, material_name, value, relative=False):
    # TODO: Si può generalizzare a obj e parameter e non solo material e
    #       specific heat.
    """Change specific heat of a material.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param material_name: Name of the material
    :type material_name: str
    :param value: New value for specific heat.
    :type value: int
    :param relative: Specify if new value will be asssigned as a percentage
        increment from the old value, defaults to `False`
    :type relative: bool, optional
    """
    for mat in idf.idfobjects["MATERIAL"]:
        if mat.Name == material_name:
            if relative:
                mat.Specific_Heat += mat.Specific_Heat * value
            else:
                mat.Specific_Heat = value


def change_ufactor_roofs(idf, ufactor, filter_by="", relative=False):
    """Change Ufactor of roofs to desired value or increment it by a
    specified percentage.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ufactor: desired final ufactor value or incremental percentage if
        `relative` field is set to `True`
    :type ufactor: float
    :param relative: Specify if new value will be asssigned as a percentage
        increment from the old value, defaults to `False`
    :type relative: bool, optional
    """
    change_ufactor(idf, ufactor, "Roof", filter_by, relative)


def change_ufactor(idf, ufactor, where="Walls", filter_by="", relative=False):
    """Change Ufactor of specified Construction objects to desired value acting
    on thickness of most insulating layer in the construction object.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ufactor: desired final ufactor value
    :type ufactor: float
    :param where: where to compute Ufactor, could be "Pavements", "Walls",
        "Ceilings","Roof" or the name of a specific Construction object
    :type where: string, defaults to "Walls"
    """
    # if where not in ["Pavements", "Walls", "Ceilings", "Roof"]:
    #     raise Exception(
    #         "Specified element must be Pavements, Walls, Ceiling or Roof"
    #     )

    Re = 0.04
    cons_list = []
    filter_by = filter_by.lower()
    if where.lower() == "walls":
        bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
        names_list = [
            b.Construction_Name
            for b in bsd
            if b.Surface_Type == "Wall"  # Filter only walls
            and b.Outside_Boundary_Condition == "Outdoors"  # Only exposed wall
            and filter_by in b.Zone_Name.lower()
        ]
        Ri = 0.13

    elif where.lower() == "floors":
        bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
        names_list = [
            b.Construction_Name
            for b in bsd
            if b.Surface_Type == "Floor"  # Filter only pavaments
            and filter_by in b.Zone_Name.lower()
        ]
        Ri = 0.1

    elif where.lower() == "ceilings":
        bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
        names_list = [
            b.Construction_Name
            for b in bsd
            if b.Surface_Type == "Ceiling"  # Filter only ceilings
            and filter_by in b.Zone_Name.lower()
        ]
        Ri = 0.1

    elif where.lower() == "roof":
        bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
        names_list = [
            b.Construction_Name
            for b in bsd
            if b.Surface_Type == "Roof"  # Filter only roof
            and filter_by in b.Zone_Name.lower()
        ]
        Ri = 0.1

    else:
        try:
            # TODO forse where potrebbe essere una lista di materiali?
            names_list = []
            for cons in idf.idfobjects["CONSTRUCTION"]:
                if cons.Name == where:
                    names_list.append(cons.Name)
                    break
            for bsd in idf.idfobjects["BUILDINGSURFACE:DETAILED"]:
                if bsd.Construction_Name == where:
                    if bsd.tilt == 90:
                        Ri = 0.13
                    else:
                        Ri = 0.1
        except:
            print("Not present construction object")

    names_list = list(dict.fromkeys(names_list))
    for name in names_list:
        for cons in idf.idfobjects["CONSTRUCTION"]:
            if cons.Name == name:
                cons_list.append(cons)

    for cnt, the_list in enumerate(cons_list):
        Rl = []
        lower_lambda_mat = ""
        lower_lambda = 500
        mat_to_change = ""
        mats = list(the_list.obj[2:])
        # Find most insulating material in the construction layers
        for el in mats:
            for materials in idf.idfobjects["MATERIAL"]:
                if (
                    materials.Name == el
                    and materials.Conductivity < lower_lambda
                ):
                    lower_lambda = materials.Conductivity
                    lower_lambda_mat = materials.Name
                    mat_to_change = materials
                    break

        mats.remove(lower_lambda_mat)
        # Compute constant Ufactor part
        for el in mats:
            found = 0
            for materials in idf.idfobjects["MATERIAL"]:
                if materials.Name == el:
                    Rl.append(materials.Thickness / materials.Conductivity)
                    found = 1
                    break

            if not found:
                for materials in idf.idfobjects["MATERIAL:NOMASS"]:
                    if materials.Name == el:
                        Rl.append(materials.Thermal_Resistance)
                        break

        # create material copy not to impact on other construction objects
        new_mat = idf.copyidfobject(mat_to_change)
        new_mat.Name = mat_to_change.Name + "_._" + str(cnt) + str(uuid.uuid4()).replace("-", "")[0:4]
        layer = get_layer(the_list, mat_to_change.Name)
        setattr(the_list, layer, new_mat.Name)

        # Reversed constuction fix. # NOTE: may cause problems in some IDFs.
        for con in idf.idfobjects["CONSTRUCTION"]:
            if con.Name.lower() == (the_list.Name + "_Rev").lower():
                layer = get_layer(con, mat_to_change.Name)
                setattr(con, layer, new_mat.Name)

        if relative:
            old_value = compute_ufactor(idf, the_list.Name)
            ufactor = old_value + old_value * ufactor

        # new thickness needed in mat_to_change to obtained desired Ufactor
        Rn = sum(Rl)
        Rt = Ri + Re + Rn
        x = max((1 / ufactor - Rt) * lower_lambda, 0.001)
        if x == 0.001:
            lower_lambda = max(x * (ufactor - Rt), 0.001)
        set_fields(new_mat, {"Thickness": x, "Conductivity": lower_lambda})


def compute_ufactor(idf, where="Walls"):
    """Compute Ufactor of specified opaque Construction objects.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param where: where to compute Ufactor, could be "Pavaments", "Walls",
        "Ceilings","Roof" or the name of a specific Construction object
    :type where: string, defaults to "Walls"
    """

    U_value_opaque = []
    Re = 0.04
    cons_list = []

    if where == "Walls":
        bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
        names_list = [
            b.Construction_Name
            for b in bsd
            if b.Surface_Type == "Wall"  # Filter only walls
            and b.Outside_Boundary_Condition == "Outdoors"  # Only exposed wall
        ]
        Ri = 0.13

    elif where == "Floors":
        bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
        names_list = [
            b.Construction_Name
            for b in bsd
            if b.Surface_Type == "Floor"  # Filter only pavaments
        ]
        Ri = 0.1

    elif where == "Ceilings":
        bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
        names_list = [
            b.Construction_Name
            for b in bsd
            if b.Surface_Type == "Ceiling"  # Filter only ceilings
        ]
        Ri = 0.1

    elif where == "Roof":
        bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
        names_list = [
            b.Construction_Name
            for b in bsd
            if b.Surface_Type == "Roof"  # Filter only roof
        ]
        Ri = 0.1

    else:
        try:
            # TODO forse where potrebbe essere una lista di materiali?
            names_list = []
            for cons in idf.idfobjects["CONSTRUCTION"]:
                if cons.Name == where:
                    names_list.append(cons.Name)
                    break
            for bsd in idf.idfobjects["BUILDINGSURFACE:DETAILED"]:
                if bsd.Construction_Name == where:
                    if bsd.tilt == 90:
                        Ri = 0.13
                    else:
                        Ri = 0.1
        except:
            print("Not present construction object")

    names_list = list(dict.fromkeys(names_list))
    for name in names_list:
        for cons in idf.idfobjects["CONSTRUCTION"]:
            if cons.Name == name:
                cons_list.append(cons)

    for the_list in cons_list:

        Rl = []
        for el in the_list.obj[2:]:
            found = 0
            for materials in idf.idfobjects["MATERIAL"]:
                if materials.Name == el:
                    Rl.append(materials.Thickness / materials.Conductivity)
                    found = 1
                    break

            if not found:
                for materials in idf.idfobjects["MATERIAL:NOMASS"]:
                    if materials.Name == el:
                        Rl.append(materials.Thermal_Resistance)
                        break

        Rn = sum(Rl)
        Rt = Ri + Re + Rn
        U_value_opaque.append(1 / Rt)

    return np.mean(U_value_opaque)


def add_internal_insulation_walls(idf, ins_data=None):
    """Add internal insulation layer and plaster layer on external walls.

    Since layers are added internally building area and volume are minimally
    reduced. However since information about external perimeter are not
    available in IDF file, the impact of these reductions is not considered.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ins_data: Construction objects list (insulation material and
        plaster), defaults to None
    :type ins_data: list of two string elements, optional
    """
    if ins_data == None:
        # TODO: Take default element
        pass

    # Read from database
    d = Database(PATH)
    d.load_file()
    obj_list = [d.get_object("MATERIAL", name) for name in ins_data]

    # Find which BuildingSurface:Detailed walls are exposed
    bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
    cons_list = [
        b.Construction_Name
        for b in bsd
        if b.Surface_Type == "Wall"  # Filter only walls
        and b.Outside_Boundary_Condition == "Outdoors"  # Only exposed wall
    ]
    cons_list = list(dict.fromkeys(cons_list))  # Remove duplicates

    # Add insulation layer in Construction objects
    cons = idf.idfobjects["CONSTRUCTION"]

    for mat in obj_list:
        # Add insulation material in Material list
        new_mat = idf.newidfobject("MATERIAL")
        set_fields(new_mat, mat)

        for w in cons_list:
            for c in cons:
                # Add as last layer (reversed construction)
                if c.Name == w + "_Rev":
                    last_layer = get_layer(c, "first")
                    insert_layer(c, last_layer, mat["Name"], below=False)

                # Add as first layer
                if c.Name == w:
                    first_layer = get_layer(c, "last")
                    insert_layer(c, first_layer, mat["Name"], below=True)


def add_cavity_insulation_walls(idf, ins_data=None):
    """Add insulation layer in external walls cavity.

    Information about cavity thickness are not available in IDF file but are
    retrieved based on air resistance values. However, cavity thickness could
    be underestimated and insulation thickness could be increased acting on
    material object fields.

    Based on known air resistance values in cavities, cavity thickness is
    retrieved. If insulation material thickness is thinner than available
    space, air resistance is consequently reduced, otherwise cavity is removed
    and completely sobstituted by insulation.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ins_data: Construction name, defaults to None
    :type ins_data: string, optional
    """

    air_r = pd.DataFrame(CAVITY_AIR_RESISTANCE)
    air_r = air_r.set_index("thickness")

    if ins_data == None:
        # TODO: Take default element
        pass

    # leggi da database
    d = Database(PATH)
    d.load_file()
    mat = d.get_object("MATERIAL", ins_data)

    # Find which BuildingSurface:Detailed walls are exposed
    bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
    cons_list = [
        b.Construction_Name
        for b in bsd
        if b.Surface_Type == "Wall"  # Filter only walls
        and b.Outside_Boundary_Condition == "Outdoors"  # Only exposed wall
    ]
    cons_list = list(dict.fromkeys(cons_list))  # Remove duplicates
    # Add insulation layer in Construction objects
    cons = idf.idfobjects["CONSTRUCTION"]

    # Add insulation material in Material list
    new_mat = idf.newidfobject("MATERIAL")
    set_fields(new_mat, mat)

    for w in cons_list:
        for c in cons:
            # Add as last layer
            if c.Name == w:
                lista = [l for l in c.obj]
                for el in lista:
                    for nomass in idf.idfobjects["MATERIAL:NOMASS"]:
                        if nomass.Name == el:
                            # Convert air R to air thickness
                            air_thickness = air_r[
                                air_r["horizontal"]
                                == nomass.Thermal_Resistance
                            ].index[
                                0
                            ]  # Thickness values corresponding to R

                            # Insert material layer near cavity layer.
                            air_lay = get_layer(c, nomass.Name)
                            insert_layer(c, air_lay, mat["Name"], below=False)

                            # Compute new air thickness (difference)
                            new_air_thickness = (
                                air_thickness - mat["Thickness"]
                            )

                            # Find which air R is the best for new thickness
                            absolute_difference_function = (
                                lambda list_value: abs(
                                    list_value - new_air_thickness
                                )
                            )
                            new_air_thickness = min(
                                list(air_r.index.values),
                                key=absolute_difference_function,
                            )
                            new_air_r = air_r.loc[
                                new_air_thickness, "horizontal"
                            ]
                            nomass.Thermal_Resistance = new_air_r

                            if new_air_thickness <= 0:
                                setattr(
                                    new_mat,
                                    "Thickness",
                                    air_thickness,
                                )
                                remove_layer(c, "RVAL")  # Remove cavity

            # Add as first layer (reversed construction)
            if c.Name == w + "_Rev":
                lista = [l for l in c.obj]
                for el in lista:
                    for nomass in idf.idfobjects["MATERIAL:NOMASS"]:
                        if nomass.Name == el:
                            # Convert air R to air thickness
                            air_thickness = air_r[
                                air_r["horizontal"]
                                == nomass.Thermal_Resistance
                            ].index[
                                0
                            ]  # Thickness values corresponding to R

                            # Insert material layer near cavity layer.
                            air_lay = get_layer(c, nomass.Name)
                            insert_layer(c, air_lay, mat["Name"], below=True)

                            # Compute new air thickness (difference)
                            new_air_thickness = (
                                air_thickness - mat["Thickness"]
                            )

                            # Find which air R is the best for new thickness
                            absolute_difference_function = (
                                lambda list_value: abs(
                                    list_value - new_air_thickness
                                )
                            )
                            new_air_thickness = min(
                                list(air_r.index.values),
                                key=absolute_difference_function,
                            )
                            new_air_r = air_r.loc[
                                new_air_thickness, "horizontal"
                            ]
                            nomass.Thermal_Resistance = new_air_r

                            if new_air_thickness <= 0:
                                setattr(
                                    new_mat,
                                    "Thickness",
                                    air_thickness,
                                )
                                remove_layer(c, "RVAL")  # Remove cavity


def add_insulation(idf, ins_names, type, construction, **fields):
    """Add insulation to the specified construction. The _Rev construction is
    also modified.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ins_names: Insulation material(s) to be added. It must be present in
        the database.json
    :type ins_names: str or list
    :param type: interior or exterior
    :type type: str
    :param construction: Construction(s) object(s) where insulation is added
    :type construction: str or list
    """
    # Read from database.
    d = Database(PATH)
    d.load_file()

    ins_names = to_list(ins_names)
    construction = to_list(construction)
    mats = []
    for ins_name in ins_names:
        mat = d.get_object("MATERIAL", ins_name)
        mats.append(mat)
        duplicate_found = 0
        for material in idf.idfobjects["MATERIAL"]:
            if material.Name.lower() == mat["Name"].lower():
                duplicate_found = 1

        if duplicate_found:
            mat["Name"] = mat["Name"] + "_" + str(uuid.uuid4()).replace("-", "")[0:4]
            print("[!] Insulating material already exists in the IDF. Using %s instead" % (mat["Name"]))

        new_mat = idf.newidfobject("MATERIAL")
        set_fields(new_mat, mat)

        # Set additional fields.
        for key, value in fields.items():
            setattr(new_mat, key, value)

    for con in idf.idfobjects["CONSTRUCTION"]:
        for single_construction in construction:
            if  (single_construction.lower() == con.Name.lower()) or (single_construction.lower() + "_rev" == con.Name.lower()):
                print("[i] Adding insulation to", con.Name)
                for mat in mats:
                    if type in ["interior", "internal"]:
                        if "_rev" not in con.Name.lower():
                            # Add as INTERNAL layer (last element).
                            last_layer = get_layer(con, "last")
                            insert_layer(con, last_layer, mat["Name"], below=True)
                        else:
                            # Add as external layer (reversed construction).
                            first_layer = get_layer(con, "first")
                            insert_layer(con, first_layer, mat["Name"], below=False)

                    elif type in ["exterior", "external"]:
                        if "_rev" not in con.Name.lower():
                            # Add as EXTERNAL layer (first/outside element).
                            first_layer = get_layer(con, "first")
                            insert_layer(con, first_layer, mat["Name"], below=False)
                        else:
                            # Add as internal layer (reversed construction).
                            last_layer = get_layer(con, "last")
                            insert_layer(con, last_layer, mat["Name"], below=True)


def add_insulation_flat_roof(
    idf, ins_name=None, sheathing_name=None, **fields
):
    """Add insulation to flat roof, under the sheating layer.

    If no insulation is provided, a default one would be used; if no sheathing
    layer is provided, the function will try to put the insulation under the
    fibreboard if present or under the asphalt.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ins_name: Name of the insulation which has to be added to the roof,
        defaults to None
    :type ins_name: str, optional
    :param sheathing_name: Name of the sheathing layer under which the
        insulation has to be added; if set to None, the function tries to
        insert the insulating layer under fibreboard or under asphalt; defaults
        to None
    :type sheathing_name: str, optional
    :param fields: Additional fields to be set, passed as keyword arguments
    """
    if ins_name == None:
        # TODO: Take default element
        pass

    # Read from database
    d = Database(PATH)
    d.load_file()
    mat = d.get_object("MATERIAL", ins_name)

    # Find BuildingSurface:Detailed floors
    bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
    cons_list = [
        b.Construction_Name
        for b in bsd
        if b.Surface_Type == "Roof"  # Filter only walls
    ]
    cons_list = list(dict.fromkeys(cons_list))  # Remove duplicates

    # Add insulation layer in Construction objects
    cons = idf.idfobjects["CONSTRUCTION"]

    # Add insulation material in Material list
    new_mat = idf.newidfobject("MATERIAL")
    set_fields(new_mat, mat)
    for key, value in fields.items():
        if key != "ins_data":
            setattr(new_mat, key, value)

    # Try to insert insulation under fibreboard layer.
    try:
        sheathing_name = "fibreboard"
        for w in cons_list:
            for c in cons:
                # Add as a layer above fibreboard
                if c.Name == w:
                    screed_layer = get_layer(c, sheathing_name)
                    insert_layer(c, screed_layer, mat["Name"], below=True)

                # Add as a layer below fibreboard
                if c.Name == w + "_Rev":
                    screed_layer = get_layer(c, sheathing_name)
                    insert_layer(c, screed_layer, mat["Name"], below=False)

    # Insert insulation under asphalt if there is no fibreboard layer.
    except Exception:
        sheathing_name = "asphalt"
        for w in cons_list:
            for c in cons:
                # Add as a layer above asphalt
                if c.Name == w:
                    screed_layer = get_layer(c, sheathing_name)
                    insert_layer(c, screed_layer, mat["Name"], below=True)

                # Add as a layer below asphalt
                if c.Name == w + "_Rev":
                    screed_layer = get_layer(c, sheathing_name)
                    insert_layer(c, screed_layer, mat["Name"], below=False)


def add_external_insulation_floor(idf, ins=None, filter_by="", check_ceilings=True, **fields):
    """Add insulation on the external layer of all floors in the building.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ins_name: Name of the insulation which has to be added to the floor,
        defaults to None
    :type ins_name: str, optional
    """
    if ins == None:
        # TODO: Take default element
        pass

    # Read from database
    d = Database(PATH)
    d.load_file()
    mat = d.get_object("MATERIAL", ins)

    # Find BuildingSurface:Detailed floors
    bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
    cons_list = [
        b.Construction_Name
        for b in bsd
        if b.Surface_Type.lower() == "floor"  # Filter only Floor type
        and filter_by.lower() in b.Zone_Name.lower()
    ]
    cons_list = list(dict.fromkeys(cons_list))  # Remove duplicates

    if check_ceilings:
        ceiling_cons_list = [
            b.Construction_Name
            for b in bsd
            if b.Surface_Type.lower() == "ceiling"  # Filter only Ceiling type
        ]
        ceiling_cons_list = list(dict.fromkeys(ceiling_cons_list))

    # Add insulation layer in Construction objects
    cons = idf.idfobjects["CONSTRUCTION"]

    # Add insulation material in Material list
    new_mat = idf.newidfobject("MATERIAL")
    set_fields(new_mat, mat)
    for key, value in fields.items():
        if key != "ins":
            setattr(new_mat, key, value)

    cnt = 0
    for con_name in cons_list:
        for c in cons:
            # Add as last layer
            # Case where original cons has no REV in name.
            if c.Name.lower() == con_name.lower() and "_rev" not in c.Name.lower():
                cnt += 1
                last_layer = get_layer(c, "first")
                insert_layer(c, last_layer, mat["Name"], below=False)

                if check_ceilings:
                    for ccon_name in ceiling_cons_list:
                        if c.Name.lower() == ccon_name.lower() + "_rev":
                            first_layer = get_layer(c, "last")
                            insert_layer(c, first_layer, mat["Name"], below=True)
                            # print(f"{cnt}c. Ceiling {filter_by}, Construction after:", c, "_____________________")

            # Case where original cons has REV in name.
            # Add as first layer (reversed construction)
            elif c.Name.lower() == con_name.lower() + "_rev":
                first_layer = get_layer(c, "last")
                insert_layer(c, first_layer, mat["Name"], below=True)

                if check_ceilings:
                    for ccon_name in ceiling_cons_list:
                        if c.Name.lower() == ccon_name.lower() + "_rev":
                            last_layer = get_layer(c, "first")
                            insert_layer(c, last_layer, mat["Name"], below=True)


def add_internal_insulation_floor(idf, ins_name=None, screed_name="screed"):
    """Add insulation under the screed layer of all floors in the building.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ins_name: Name of the insulation which has to be added to the floor,
        defaults to None
    :type ins_name: str, optional
    :param screed_name: Name of the screed layer used in the floor of the
        building, defaults to "screed"
    :type screed_name: str, optional
    """
    if ins_name == None:
        # TODO: Take default element
        pass

    # Read from database
    d = Database(PATH)
    d.load_file()
    mat = d.get_object("MATERIAL", ins_name)

    # Find BuildingSurface:Detailed floors
    bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
    cons_list = [
        b.Construction_Name
        for b in bsd
        if b.Surface_Type == "Floor"  # Filter only walls
    ]
    cons_list = list(dict.fromkeys(cons_list))  # Remove duplicates

    # Add insulation layer in Construction objects
    cons = idf.idfobjects["CONSTRUCTION"]

    # Add insulation material in Material list
    new_mat = idf.newidfobject("MATERIAL")
    set_fields(new_mat, mat)

    for w in cons_list:
        for c in cons:
            # TODO: What to do if there is no screed layer?
            # Add as a layer above the screed
            if c.Name == w:
                screed_layer = get_layer(c, screed_name)
                insert_layer(c, screed_layer, mat["Name"], below=False)

            # Add as a layer below the screed
            if c.Name == w + "_Rev":
                screed_layer = get_layer(c, screed_name)
                insert_layer(c, screed_layer, mat["Name"], below=True)


def add_internal_heat_gain(idf, filter_by="", schedule=None, **fields):
    """Add internal heat gain object.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param fields: Additional fields to be set, passed as keyword arguments
    :Example:

    .. code-block:: python

        add_internal_heat_gain(idf)

    """
    zones = [
        z
        for z in idf.idfobjects["ZONE"]
        if filter_by.lower() in z.Name.lower()
    ]
    for z in zones:
        oe = idf.newidfobject("OTHEREQUIPMENT")
        oe.Zone_or_ZoneList_Name = z.Name
        for key, value in fields.items():
            setattr(oe, key.replace(" ", "_"), value)

        # If name has not been set:
        if oe.Name == "":
            oe.Name = "internal_heat_gain_" + z.Name

    if schedule is not None:
        if isinstance(schedule, str):
            d = Database(PATH)
            d.load_file()
            schedule = d.get_object("SCHEDULE:COMPACT", schedule)
        elif isinstance(schedule, dict):
            pass
        else:
            raise (TypeError)

        for sch in idf.idfobjects["SCHEDULE:COMPACT"]:
            if sch.Name == schedule["Name"]:
                idf.removeidfobject(sch)

        sc = idf.newidfobject("SCHEDULE:COMPACT")
        set_fields(sc, data=schedule)


def change_internal_heat_gain(idf, value, filter_by="", relative=False):
    """Change internal heat gain.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New value for field "Power per Zone Floor Area" or a
        percentage if `relative` parameter is set to `True`
    :type value: float
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param relative: Specify if new value will be asssigned as a percentage
        increment from the old value, defaults to `False`
    :type relative: bool, optional
    """
    # TODO: Gestire altri tipi di equipment.
    for oe in idf.idfobjects["OTHEREQUIPMENT"]:
        if filter_by.lower() in oe.Name.lower():
            if relative:
                old_value = oe.Power_per_Zone_Floor_Area
                oe.Power_per_Zone_Floor_Area = old_value + old_value * value
            else:
                oe.Power_per_Zone_Floor_Area = value


def change_infiltration(idf, ach, filter_by="", relative=False):
    """Change infiltration ACH for all zones of a specific block of the
    building.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ach: New ACH (air changes per hour) value or dictionary of ACH
        values for each zone.
    :type ach: int or dict
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name or a list of those, defaults to "", ignored if ach is a `dict`
    :type filter_by: str, optional
    :param relative: Specify if new value will be asssigned as a percentage
        increment from the old value, defaults to `False`
    :type relative: bool, optional
    """
    change_ach(idf, ach, "infiltration", filter_by, relative)


def change_ach(
    idf,
    ach,
    ach_type="ventilation",
    filter_by="",
    relative=False,
    **fields,
):
    """Change ACH for all zones of a specific block of the building.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ach: New ACH (air changes per hour) value or dictionary of ACH
        values for each zone.
    :type ach: int or dict
    :param ach_type: ACH type, between `Infiltration` and `Ventilation`
    :type ach_type: str
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to "", ignored if ach is a `dict`
    :type filter_by: str or list, optional
    :param relative: Specify if new value will be asssigned as a percentage
        increment from the old value, defaults to `False`
    :type relative: bool, optional
    :param fields: Additional fields to be set to the DesignFlowRate object,
        passed as keyword arguments
    """
    zones = idf.idfobjects["ZONE"]
    if ach_type.lower() == "infiltration":
        z_dfr = idf.idfobjects["ZoneInfiltration:DesignFlowRate".upper()]
    elif ach_type.lower() == "ventilation":
        z_dfr = idf.idfobjects["ZoneVentilation:DesignFlowRate".upper()]
    else:
        raise Exception

    if isinstance(ach, dict):
        for zi in z_dfr:
            for z in zones:
                if z.Name in zi.Zone_or_ZoneList_Name and z.Name in ach.keys():
                    if (
                        zi.Design_Flow_Rate_Calculation_Method.lower()
                        == "airchanges/hour"
                    ):
                        zi.Air_Changes_per_Hour = ach[z.Name]
                    elif (
                        zi.Design_Flow_Rate_Calculation_Method.lower()
                        == "flow/zone"
                    ):
                        zi.Design_Flow_Rate = compute_dfr(z, ach[z.Name])
                    else:
                        raise NotImplementedError
    else:
        for zi in z_dfr:
            if not isinstance(filter_by, list):
                filter_by = [filter_by]
            if (
                any(
                    [
                        f.lower() in zi.Zone_or_ZoneList_Name.lower()
                        for f in filter_by
                    ]
                )
                or len(filter_by) == 0
            ):
                for z in zones:
                    if z.Name == zi.Zone_or_ZoneList_Name:
                        if relative:
                            if (
                                zi.Design_Flow_Rate_Calculation_Method.lower()
                                == "flow/zone"
                            ):
                                old_value = zi.Design_Flow_Rate
                                zi.Design_Flow_Rate = (
                                    old_value + old_value * ach
                                )
                            elif (
                                zi.Design_Flow_Rate_Calculation_Method.lower()
                                == "airchanges/hour"
                            ):
                                old_value = zi.Air_Changes_per_Hour
                                zi.Air_Changes_per_Hour = (
                                    old_value + old_value * ach
                                )
                            elif (
                                zi.Design_Flow_Rate_Calculation_Method.lower()
                                == "flow/area"
                            ):
                                old_value = zi.Flow_Rate_per_Zone_Floor_Area
                                zi.Flow_Rate_per_Zone_Floor_Area = (
                                    old_value + old_value * ach
                                )
                            else:
                                raise NotImplementedError
                        else:
                            zi.Design_Flow_Rate_Calculation_Method = (
                                "flow/zone"
                            )
                            zi.Design_Flow_Rate = compute_dfr(z, ach)

                        for key, value in fields.items():
                            setattr(zi, key, value)

    # TODO: Rivedere filter_by in get_mean_ach, forse necessario usare il nome
    #       del blocco contenuto nell'oggetto IDF.
    #idf.mean_ach = get_mean_ach(idf, filter_by)


def set_ach_schedule(
    idf, schedule, ach_type="ventilation", filter_by="", **fields
):
    """Set a schedule for ACH ventilation

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param schedule: Schedule which has to be assigned. It follows the rules of
        Schedule:Compact objects of EnergyPlus.
    :type schedule: dict
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param fields: Additional fields to be set, passed as keyword arguments
    """
    if ach_type == "ventilation":
        zv = idf.idfobjects["ZoneVentilation:DesignFlowRate".upper()]
    elif ach_type == "infiltration":
        zv = idf.idfobjects["ZoneInfiltration:DesignFlowRate".upper()]

    for zvdfr in zv:
        if filter_by in zvdfr.Zone_or_ZoneList_Name:
            zvdfr.Schedule_Name = schedule["Name"]

            # Set additional fields passed as parameters.
            for k, v in fields.items():
                setattr(zvdfr, k, v)

    sc = idf.newidfobject("Schedule:Compact".upper())
    set_fields(sc, data=schedule)


def compute_dfr(zone, ach):
    """Compute infiltration value given a zone and its ACH.

    :param zone: geomeppy object
    :type zone: class 'geomeppy.patches.EpBunch'
    :param ach: Air changes per hour
    :type ach: int
    :return: Design Flow Rate value
    :rtype: float
    """

    dfr = ach * zone.Volume / 3600
    try:
        if dfr < 0:
            raise Exception("Cannot set ACH lower than 0.")
    except Exception as msg:
        print(msg)
        dfr = 0

    return dfr


def get_ach(zone, dfr):
    """Return ACH from Design Flow Rate

    :param zone: Zone object
    :type zone: [type]
    :param dfr: Design Flow Rate Value
    :type dfr: float
    :return: ACH value for the zone
    :rtype: float
    """
    return dfr * 3600 / zone.Volume


def get_mean_ach(idf, filter_by="", ach_type="ventilation"):
    """Compute mean ACH value over all zones of the IDF.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name or a list of those, defaults to ""
    :type filter_by: str or list, optional
    :param ach_type: "ventilation" or "infiltration", defaults to "ventilation"
    :type ach_type: str, optional
    :return: Mean ACH value
    :rtype: float
    """
    ach_values = []
    if not isinstance(filter_by, list):
        filter_by = [filter_by]
    
    if ach_type.lower() == "ventilation":
        vent_obj = "ZoneVentilation:DesignFlowRate".upper()
    elif ach_type.lower() == "infiltration":
        vent_obj = "ZoneInfiltration:DesignFlowRate".upper()
    else:
        raise ValueError("Incorrect type of ventilation was requested.")
    
    for zvdfr in idf.idfobjects[vent_obj]:
        if any([f.lower() in zvdfr.Name.lower() for f in filter_by]):
            for z in idf.idfobjects["ZONE"]:
                if zvdfr.Zone_or_ZoneList_Name == z.Name:
                    if (
                        zvdfr.Design_Flow_Rate_Calculation_Method.lower()
                        == "airchanges/hour"
                    ):
                        ach_values.append(zvdfr.Air_Changes_per_Hour)
                    elif (
                        zvdfr.Design_Flow_Rate_Calculation_Method.lower()
                        == "flow/zone"
                    ):
                        ach_values.append(get_ach(z, zvdfr.Design_Flow_Rate))
                    elif (
                        zvdfr.Design_Flow_Rate_Calculation_Method.lower()
                        == "flow/area"
                    ):
                        ach_values.append(
                            get_ach(z, zvdfr.Flow_Rate_per_Zone_Floor_Area * z.Floor_Area)
                        )
                    elif(
                        zvdfr.Design_Flow_Rate_Calculation_Method.lower()
                        == "flow/person"
                    ):
                        ach_values.append(
                            get_ach(z, zvdfr.Flow_Rate_per_Person * get_number_of_people(idf, z))
                        )


                    else:
                        raise NotImplementedError

    return round(np.mean(ach_values), 2)


def get_mech_ach(idf, filter_by=""):
    """Return mean mechanical ventilation (outdoor air) ACH/flow value over
    all/some zones of the IDF.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name or a list of those, defaults to ""
    :type filter_by: str or list, optional
    :param method: Method used to compute the amount of air, defaults to "Flow/Zone"
    :type method: str, optional

    :return: Mean mechanical ventilation (outdoor air) ACH value.
    :rtype: float
    """
    # method = method.lower()
    # if method not in ["flow/zone"]:
    #     raise NotImplementedError

    mech_values = []
    for sz in idf.idfobjects["SIZING:ZONE"]:
        if any([f.lower() in sz.Zone_or_ZoneList_Name.lower() for f in filter_by]):
            for dsoa in idf.idfobjects["DESIGNSPECIFICATION:OUTDOORAIR"]:
                if sz.Design_Specification_Outdoor_Air_Object_Name.lower() == dsoa.Name.lower():
                    for z in idf.idfobjects["ZONE"]:
                        if z.Name.lower() == sz.Zone_or_ZoneList_Name.lower():
                            if (
                                dsoa.Outdoor_Air_Method.lower()
                                == "airchanges/hour"
                            ):
                                flow = dsoa.Outdoor_Air_Flow_Air_Changes_per_Hour * z.Volume / 3600
                                mech_values.append(flow)

                            elif (
                                dsoa.Outdoor_Air_Method.lower()
                                == "flow/zone"
                            ):
                                flow = dsoa.Outdoor_Air_Flow_per_Zone
                                mech_values.append(flow)

                            elif (
                                dsoa.Outdoor_Air_Method.lower()
                                == "flow/area"
                            ):
                                # print(f"dsoa.Outdoor_Air_Flow_per_Zone_Floor_Area: {dsoa.Outdoor_Air_Flow_per_Zone_Floor_Area}")
                                # print(type(dsoa.Outdoor_Air_Flow_per_Zone_Floor_Area))
                                # print(f"z.Floor_Area: {z.Floor_Area}")
                                # print(type(z.Floor_Area))
                                flow = float(dsoa.Outdoor_Air_Flow_per_Zone_Floor_Area) * z.Floor_Area # FIXME: Without float cast id does not work.
                                mech_values.append(flow)

                            else:
                                raise NotImplementedError

    return round(np.mean(mech_values), 2)


def get_mean_dt(idf, filter_by=""):
    """Compute mean Delta Temperature over all Zone Ventilations.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :return: Mean Delta Temperature
    :rtype: float
    """
    dt_values = []
    for zvdfr in idf.idfobjects["ZoneVentilation:DesignFlowRate".upper()]:
        if filter_by.lower() in zvdfr.Name.lower():
            if zvdfr.Delta_Temperature not in [-100, ""]:
                dt_values.append(zvdfr.Delta_Temperature)

    if dt_values:
        return round(np.mean(dt_values), 2)
    else:
        print("\n/!\ Warning: Mean delta temperature for natural ventilation is set to -100. Be careful in KPI computations!\n")
        return -100


def change_zone_ach(zone, zone_inf, ach):  # Deprecated
    """Change ACH in a zone by modifying the Design Flow Rate in the
    ZoneInfiltration:DesignFlowRate object.

    :param zone: geomeppy object
    :type zone: class 'geomeppy.patches.EpBunch'
    :param zone_inf: Collection of IDF objects
    :type zone_inf: class 'eppy.idf_msequence.Idf_MSequence'
    """
    zone_name = zone.Name
    for z in zone_inf:
        if z.Zone_or_ZoneList_Name == zone_name:
            z.Design_Flow_Rate = compute_dfr(zone, ach)


def filter_object(obj, key, value):
    """Return object given a field value from a list of objects.

    :param obj: Collection of IDF objects
    :type obj: class 'eppy.idf_msequence.Idf_MSequence'
    :param key: Field name
    :type key: str
    :param value: Field value
    :type value: int or str
    :return: Object from the list
    :rtype: class 'geomeppy.patches.EpBunch'
    """
    # print(key, value)
    retrieved = [o for o in obj if value in getattr(o, key)]
    return retrieved


def convert_keys(data):
    """Convert dictionary keys by replacing spaces with underscores.

    :param data: Dictionary
    :type data: dict
    :return: Dictionary with new keys
    :rtype: dict
    """
    return {x.replace(" ", "_"): y for x, y in data.items()}


def change_setpoint(idf, heat=None, cool=None, filter_by=""):
    """Change heating and cooling setpoints in the IDF.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param heat: New heating setpoint value, defaults to None
    :type heat: int, optional
    :param cool: New cooling setpoint value, defaults to None
    :type cool: int, optional
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name or a list of those, defaults to ""
    :type filter_by: str or list, optional
    """

    filter_by = to_list(filter_by)
    for zct in idf.idfobjects["ZoneControl:Thermostat".upper()]:
        if any([f.lower() in zct.Zone_or_ZoneList_Name.lower() for f in filter_by]):
            obj = zct.Control_1_Name

            for t in idf.idfobjects["THERMOSTATSETPOINT:DUALSETPOINT"]:
                if t.Name.lower() == obj.lower():
                    # Get Schedule:Compact names for heating and cooling setpoint schedules.
                    heat_sched = getattr(t, "Heating_Setpoint_Temperature_Schedule_Name")
                    cool_sched = getattr(t, "Cooling_Setpoint_Temperature_Schedule_Name")

                    # Find Schedule:Compact with right names and change setpoints of them.
                    schedules = idf.idfobjects["SCHEDULE:COMPACT"]
                    for s in schedules:
                        if heat_sched.lower() in s.Name.lower() and heat is not None:
                            _change_setpoint(s, heat, which="heating")

                        if cool_sched.lower() in s.Name.lower() and cool is not None:
                            _change_setpoint(s, cool, which="cooling")


def _change_setpoint(obj, value, which):
    """Change setpoint of a schedule.

    :param obj: Schedule:Compact object
    :type obj: class 'geomeppy.patches.EpBunch'
    :param value: New setpoint value
    :type value: float
    :param which: Which system, 'heating' or 'cooling'
    :type which: str
    """
    if which == "heating":
        number = 0
        found = 0
        for key in obj.objls:
            if found:
                new_value = float(getattr(obj, key))
                if new_value > number:
                    number = new_value
                found = 0
            if "Until" in str(getattr(obj, key)):
                found = 1

        found = 0
        for key in obj.objls:
            if found:
                if float(getattr(obj, key)) == number:
                    setattr(obj, key, value)
                found = 0
            if "Until" in str(getattr(obj, key)):
                found = 1

    elif which == "cooling":
        number = 100
        found = 0
        for key in obj.objls:
            if found:
                new_value = int(getattr(obj, key))
                if new_value < number:
                    number = new_value
                found = 0
            if "Until" in str(getattr(obj, key)):
                found = 1

        found = 0
        for key in obj.objls:
            if found:
                if int(getattr(obj, key)) == number:
                    setattr(obj, key, value)
                found = 0
            if "Until" in str(getattr(obj, key)):
                found = 1


def change_setback(idf, heat=None, cool=None, filter_by=""):
    """Change heating and cooling setbacks in the IDF.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param heat: New heating setback value, defaults to None
    :type heat: int, optional
    :param cool: New cooling setback value, defaults to None
    :type cool: int, optional
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name or a list of those, defaults to ""
    :type filter_by: str or list, optional
    """
    filter_by = to_list(filter_by)
    for zct in idf.idfobjects["ZoneControl:Thermostat".upper()]:
        if any([f.lower() in zct.Zone_or_ZoneList_Name.lower() for f in filter_by]):
            obj = zct.Control_1_Name

            for t in idf.idfobjects["THERMOSTATSETPOINT:DUALSETPOINT"]:
                if t.Name.lower() == obj.lower():
                    # Get Schedule:Compact names for heating and cooling setpoint schedules.
                    heat_sched = getattr(t, "Heating_Setpoint_Temperature_Schedule_Name")
                    cool_sched = getattr(t, "Cooling_Setpoint_Temperature_Schedule_Name")

                    # Find Schedule:Compact with right names and change setbacks of them.
                    schedules = idf.idfobjects["SCHEDULE:COMPACT"]
                    for s in schedules:
                        if heat_sched.lower() in s.Name.lower() and heat is not None:
                            _change_setback(s, heat, which="heating")

                        if cool_sched.lower() in s.Name.lower() and cool is not None:
                            _change_setback(s, cool, which="cooling")


def _change_setback(obj, value, which):
    """Change setback of a schedule.

    :param obj: Schedule:Compact object
    :type obj: class 'geomeppy.patches.EpBunch'
    :param value: New setback value
    :type value: float
    :param which: Which system, 'heating' or 'cooling'
    :type which: str
    """
    if which == "heating":
        number = 100
        found = 0
        for key in obj.objls:
            if found:
                new_value = float(getattr(obj, key))
                if 0 < new_value < number:
                    number = new_value
                found = 0
            if "Until" in str(getattr(obj, key)):
                found = 1

        found = 0
        for key in obj.objls:
            if found:
                if float(getattr(obj, key)) == number:
                    setattr(obj, key, value)
                found = 0
            if "Until" in str(getattr(obj, key)):
                found = 1

    elif which == "cooling":
        number = 0
        found = 0
        for key in obj.objls:
            if found:
                new_value = int(getattr(obj, key))
                if number < new_value < 100:
                    number = new_value
                found = 0
            if "Until" in str(getattr(obj, key)):
                found = 1

        found = 0
        for key in obj.objls:
            if found:
                if int(getattr(obj, key)) == number:
                    setattr(obj, key, value)
                found = 0
            if "Until" in str(getattr(obj, key)):
                found = 1


def add_eahx(idf,
    filter_by="",
    schedule=None,
    dfr=0.139,
    min_temp_cool=24,
    max_temp_heat=20,
    delta_temp=2,
    earthtube_type = "Intake",
    fan_pressure_rise = None,
    fan_total_efficiency = None,
    pipe_r = 0.125,
    pipe_t = 0.004,
    pipe_l = 75,
    pipe_cond = 0.4,
    pipe_p = 2.5,
    soil_condition = 2,
    soil_surf_cond = 2,
    av_soil_surf_temp = None,
    ampl_soil_surf_temp = None,
    phase_soil_surf_temp = None,
    params={},
):
    """Add Earth tubes objects in each zone allowed by filter_by parameter.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param filter_by: Filter zone by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param schedule: Name of the Schedule:Compact construction object to
        be added in IDF or the Schedule:Compact object in a dictionary format,
        defaults to None
    :type schedule: str or dict, optional
    :param dfr: design flow rate of earth tubes, defaults to None
    :type dfr: float, optional
    :param min_temp_cool: minimum indoor temperature for cooling activation, defaults to 24
    :type min_temp_cool: float, optional
    :param max_temp_heat: maximum indoor temperature for heating activation, defaults to 20
    :type max_temp_heat: float, optional
    :param delta_temp: delta temperature for system activation, defaults to 2
    :type delta_temp: float, optional
    :param earthtube_type: it can be Natural, Exhaust, or Intake; defaults to None
    :type earthtube_type: str, optional
    :param fan_pressure_rise: pressure rise experienced across the eventual fan in Pascals (N/m2), defaults to None
    :type fan_pressure_rise: float, optional
    :param fan_total_efficiency: total fan efficiency (a decimal number between 0.0 and 1.0), defaults to None
    :type fan_total_efficiency: float, optional
    :param pipe_r: pipe radius in meters, defaults to None
    :type pipe_r: float, optional
    :param pipe_t: pipe thickness in meters, defaults to None
    :type pipe_t: float, optional
    :param pipe_l: pipe length in meters, defaults to None
    :type pipe_l: float, optional
    :param pipe_cond: pipe thermal conductivity, defaults to None
    :type pipe_cond: float, optional
    :param pipe_p: pipe depth in meters, defaults to None
    :type pipe_p: float, optional
    :param soil_condition: it can be 1. HeavyAndSaturated, 2. HeavyAndDamp, 3. HeavyAndDry or 4. LightAndDry, defaults to None
    :type soil_condition: int, optional
    :param soil_surf_cond: it can be 1. BARE AND WET, 2. BARE AND MOIST, 3. BARE AND ARID, 4. BARE AND DRY, 
        5. COVERED AND WET, 6. COVERED AND MOIST, 7. COVERED AND ARID and 8. COVERED AND DRY, defaults to None
    :type soil_surf_cond: int, optional
    :param av_soil_surf_temp: average soil surface temperature, defaults to None
    :type av_soil_surf_temp: float, optional
    :param ampl_soil_surf_temp: amplitude of soil surface temperature, defaults to None
    :type ampl_soil_surf_temp: float, optional
    :param phase_soil_surf_temp: phase of soil surface temperature, defaults to None
    :type phase_soil_surf_temp: float, optional
    :param params: Additional scheduled natural ventilation parameters for the
        ZoneVentilation:DesignFlowRate object, defaults to {}
    :type params: dict, optional
    """
    if schedule == None:
        # TODO: Take default element
        raise NotImplementedError

    # Read from database.
    d = Database(PATH)
    d.load_file()
    if isinstance(schedule, str):
        sched_nat_vent_obj = d.get_object("SCHEDULE:COMPACT", schedule)
        schedule_name = schedule
    elif isinstance(schedule, dict):
        sched_nat_vent_obj = schedule
        schedule_name = schedule["Name"]

    found = 0
    try:
        for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
            if sc.Name.lower() == schedule_name.lower():
                found = 1
                raise Warning
    except Warning:
        print(
            "Schedule ",
            schedule_name,
            " already exists in the IDF.\
                Given schedule may not be applied.",
        )
    if not found:
        new_sc = idf.newidfobject("SCHEDULE:COMPACT")
        set_fields(new_sc, data=sched_nat_vent_obj)

    if soil_condition == 1:
        soil_condition = "HeavyAndSaturated"
    elif soil_condition == 2:
        soil_condition = "HeavyAndDamp"
    elif soil_condition == 3:
        soil_condition = "HeavyAndDry"
    elif soil_condition == 4:
        soil_condition = "LightAndDry"

    zones = idf.idfobjects["ZONE"]
    filtered_zones = [z.Name for z in zones if filter_by.lower() in z.Name.lower()]
    for z in filtered_zones:
        earth_tube = idf.newidfobject["ZoneEarthTube".upper()]
        set_fields(
            earth_tube,
            {
                "Zone_Name": z,
                "Schedule_Name": schedule_name,
                "Design_Flow_Rate": dfr,
                "Minimum_Zone_Temperature_when_Cooling": min_temp_cool,
                "Maximum_Zone_Temperature_when_Heating": max_temp_heat,
                "Delta_Temperature": delta_temp,
                'Earthtube_Type':  earthtube_type,
                'Fan_Pressure_Rise':fan_pressure_rise,
                'Fan_Total_Efficiency':fan_total_efficiency,
                'Pipe_Radius':pipe_r,
                'Pipe_Thickness':pipe_t,
                'Pipe_Length':pipe_l,
                'Pipe_Thermal_Conductivity':pipe_cond,
                'Pipe_Depth_Under_Ground_Surface':pipe_p,
                'Soil_Condition':soil_condition,
                'Average_Soil_Surface_Temperature': av_soil_surf_temp,
                'Amplitude_of_Soil_Surface_Temperature': ampl_soil_surf_temp,
                'Phase_Constant_of_Soil_Surface_Temperature': phase_soil_surf_temp,
                'Constant_Term_Flow_Coefficient':1,
                'Temperature_Term_Flow_Coefficient':0,
                'Velocity_Term_Flow_Coefficient':0,
                'Velocity_Squared_Term_Flow_Coefficient':0
            })
        set_fields(earth_tube, **params)

    # set outputs
    set_outputs(idf, ["Earth Tube Zone Sensible Cooling Energy"], "*", frequency="Hourly")
    set_outputs(idf, ["Earth Tube Zone Sensible Cooling Rate"], "*", frequency="Hourly")
    set_outputs(idf, ["Earth Tube Zone Sensible Heating Energy"], "*", frequency="Hourly")
    set_outputs(idf, ["Earth Tube Zone Sensible Heating Rate"], "*", frequency="Hourly")
    set_outputs(idf, ["Earth Tube Air Flow Volume"], "*", frequency="Hourly")
    set_outputs(idf, ["Earth Tube Air Volume Flow Rate"], "*", frequency="Hourly")
    set_outputs(idf, ["Earth Tube Air Flow Mass"], "*", frequency="Hourly")
    set_outputs(idf, ["Earth Tube Air Mass Flow Rate"], "*", frequency="Hourly")
    set_outputs(idf, ["Earth Tube Fan Electric Energy"], "*", frequency="Hourly")
    set_outputs(idf, ["Earth Tube Fan Electric Power"], "*", frequency="Hourly")
    set_outputs(idf, ["Earth Tube Zone Inlet Air Temperature"], "*", frequency="Hourly")
    set_outputs(idf, ["Earth Tube Ground Interface Temperature"], "*", frequency="Hourly")
    set_outputs(idf, ["Earth Tube Outdoor Air Heat Transfer Rate"], "*", frequency="Hourly")


def add_pdec(idf,
    filter_by="",
    schedule=None,
    dfr=None,
    ev_cool_typology = "EvaporativeCooler:Direct:CelDekPad",
    direct_pad_area=None,
    direct_pad_depth=None,
    water_pump_pc=None,
    fan_press_rise=None,
    fan_dfr = None,
    ZoneHVAC_EvaporativeCoolerUnit_params={},
    EvaporativeCooler_params={},
    Fan_ConstantVolume_params={}
):
    """Add Evaporative Cooler units and all related objects to all zones 
    allowed by the filter_by parameter.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param filter_by: Filter zone by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param schedule: Name of the Schedule:Compact construction object to
        be added in IDF or the Schedule:Compact object in a dictionary format,
        defaults to None
    :type schedule: str or dict, optional
    :param dfr: design flow rate of evaporative cooler units, defaults to None
    :type dfr: float, optional
    :param ev_cool_typology: typology of evaporative cooler unit object. It can be 
        EvaporativeCooler:Direct:CelDekPad or EvaporativeCooler:Direct:ResearchSpecial, 
        defaults to EvaporativeCooler:Direct:CelDekPad
    :type ev_cool_typology: str, optional
    :param direct_pad_area: face area of the evaporative pad in meters if ev_cool_typology 
        is set to EvaporativeCooler:Direct:CelDekPad, defaults to None
    :type direct_pad_area: float, optional
    :param direct_pad_depth: depth of the evaporative pad in meters if ev_cool_typology 
        is set to EvaporativeCooler:Direct:CelDekPad, defaults to None, defaults to None
    :type direct_pad_depth: float, optional
    :param water_pump_pc: power consumed by the evaporative cooler recirculating 
        pump in Watts, defaults to None
    :type water_pump_pc: float, optional
    :param fan_press_rise: ressure rise in Pascals at full flow and standard (sea level) 
        conditions (20°C and 101325 Pa), defaults to None
    :type fan_press_rise: float, optional
    :param fan_dfr: fan full load air volumetric flow rate (m3/sec) at 
        standard temperature and pressure (dry air at 20°C drybulb), defaults to None
    :type fan_dfr: float, optional
    :param ZoneHVAC_EvaporativeCoolerUnit_params: additional parameters for 
        ZoneHVAC:EvaporativeCoolerUnit object, defaults to {}
    :type ZoneHVAC_EvaporativeCoolerUnit_params: dict, optional
    :param EvaporativeCooler_params:  additional parameters for 
        EvaporativeCooler unit object, defaults to {}
    :type EvaporativeCooler_params: dict, optional
    :param Fan_ConstantVolume_params:  additional parameters for 
        Fan:ConstantVolume object, defaults to {}
    :type Fan_ConstantVolume_params: dict, optional
    """
    # TODO ha senso che funzioni in questo modo il passaggio dei parametri?
    if schedule == None:
        # TODO: Take default element
        raise NotImplementedError

    # Read from database.
    d = Database(PATH)
    d.load_file()
    if isinstance(schedule, str):
        sched_nat_vent_obj = d.get_object("SCHEDULE:COMPACT", schedule)
        schedule_name = schedule
    elif isinstance(schedule, dict):
        sched_nat_vent_obj = schedule
        schedule_name = schedule["Name"]

    found = 0
    try:
        for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
            if sc.Name.lower() == schedule_name.lower():
                found = 1
                raise Warning
    except Warning:
        print(
            "Schedule ",
            schedule_name,
            " already exists in the IDF.\
                Given schedule may not be applied.",
        )
    if not found:
        new_sc = idf.newidfobject("SCHEDULE:COMPACT")
        set_fields(new_sc, data=sched_nat_vent_obj)

    zones = idf.idfobjects["ZONE"]
    filtered_zones = [z.Name for z in zones if filter_by.lower() in z.Name.lower()]
    # for each zone add 1 evaporative cooler unit and all related objects
    for z in filtered_zones:
        
        # TODO fare magari funzione a parte che gestisca inserimento di paramettro e oggetti correlati a AvailabilityManagerAssignmentList	
        # add ZoneHVAC:EvaporativeCoolerUnit
        dec_unit = idf.newidfobject["ZoneHVAC:EvaporativeCoolerUnit".upper()]
        set_fields(
            dec_unit,
            {
                'Name': z + " Evap Unit",
                'Availability_Schedule_Name': schedule_name,
                'Outdoor_Air_Inlet_Node_Name': z + " Cooler Unit OA Inlet",
                'Cooler_Outlet_Node_Name': "Node " + z + " In",
                'Zone_Relief_Air_Node_Name': "Node " + z + " Relief",
                'Supply_Air_Fan_Object_Type': "Fan:ConstantVolume",
                'Supply_Air_Fan_Name': z + " Supply Fan",
                'Design_Supply_Air_Flow_Rate': dfr,
                'Fan_Placement': "DrawThrough",
                'Cooler_Unit_Control_Method': "ZoneCoolingLoadOnOffCycling", # TODO accettare in input anche altra opzione ZoneTemperatureDeadbandOnOffCycling
                'First_Evaporative_Cooler_Object_Type': "EvaporativeCooler:Direct:CelDekPad",
                'First_Evaporative_Cooler_Object_Name': z + " Evaporative Cooler",
            })
        set_fields(dec_unit, **ZoneHVAC_EvaporativeCoolerUnit_params)

        # add EvaporativeCooler unit depending on typology
        if ev_cool_typology == "EvaporativeCooler:Direct:CelDekPad":
        
            ev_cool = idf.newidfobject["EvaporativeCooler:Direct:CelDekPad".upper()]
            set_fields(
                ev_cool,
                {
                    'Name': z + " Evaporative Cooler",
                    'Availability_Schedule_Name': schedule_name,
                    'Direct_Pad_Area': direct_pad_area,
                    'Direct_Pad_Depth': direct_pad_depth,
                    'Recirculating_Water_Pump_Power_Consumption': water_pump_pc,
                    'Air_Inlet_Node_Name': z + " Cooler Unit OA Inlet",
                    'Air_Outlet_Node_Name': z + " Cooler Unit OA Inlet",
                })
            set_fields(ev_cool, **EvaporativeCooler_params)
        
        elif ev_cool_typology == "EvaporativeCooler:Direct:ResearchSpecial":
            
            # TODO sono settati per default pochissimi parametri, ci si aspettano vengano dati in input dall'utente, ok??
            ev_cool = idf.newidfobject["EvaporativeCooler:Direct:CelDekPad".upper()]
            set_fields(
                ev_cool,
                {
                    'Name': z + " Evaporative Cooler",
                    'Availability_Schedule_Name': schedule_name,
                    'Recirculating_Water_Pump_Design_Power': water_pump_pc,
                    'Air_Inlet_Node_Name': z + " Cooler Unit OA Inlet",
                    'Air_Outlet_Node_Name': z + " Cooler Unit OA Inlet",
                })
            set_fields(ev_cool, **EvaporativeCooler_params)

        # TODO solo questo tipo di fan è attualmente gestito. è necessario gestirne altri?
        # add Fan:ConstantVolume
        fan = idf.newidfobject["Fan:ConstantVolume".upper()]
        set_fields(
            fan,
            {
                'Name':  z + " Supply Fan",
                'Availability_Schedule_Name': schedule_name,
                'Pressure_Rise': fan_press_rise,
                'Maximum_Flow_Rate': fan_dfr,
                'Air_Inlet_Node_Name': z + " Cooler Unit OA Inlet",
                'Air_Outlet_Node_Name': "Node " + z + " In",
                'EndUse_Subcategory': "General" #vogliamo gestire altre opzioni?
            })
        set_fields(fan, **Fan_ConstantVolume_params)

        # add ZoneHVAC:EquipmentList
        equip_list = idf.newidfobject["ZoneHVAC:EquipmentList".upper()]
        set_fields(
            equip_list,
            {
                'Name': z+ " Equipment",
                'Zone_Equipment_1_Object_Type': "EvaporativeCooler:Direct:CelDekPad",
                'Zone_Equipment_1_Name': z + " Evap Unit",
                'Zone_Equipment_1_Cooling_Sequence': 1,
                'Zone_Equipment_1_Heating_or_NoLoad_Sequence': 1,
            })

        # add ZoneHVAC:EquipmentConnections
        equip_conn = idf.newidfobject["ZoneHVAC:EquipmentConnections".upper()]
        set_fields(
            equip_conn,
            {
                'Zone_Name': z,
                'Zone_Conditioning_Equipment_List_Name': z + " Equipment",
                'Zone_Air_Inlet_Node_or_NodeList_Name': z + " Inlets",
                'Zone_Air_Exhaust_Node_or_NodeList_Name': z + " Relief",
                'Zone_Air_Node_Name': "Node " + z + " Zone",
                'Zone_Return_Air_Node_or_NodeList_Name': "Node " + z + " Out",
            })

        # add OutdoorAir:Node 
        out_node = idf.newidfobject["OutdoorAir:Node".upper()]
        set_fields(
            out_node,
            {
                'Name': z + " Cooler Unit OA Inlet",
            })

        # add NodeList
        node_list = idf.newidfobject["NodeList".upper()]
        set_fields(
            node_list,
            {
                'Name': z + " Inlets",
                'Name_1_Name': "Node " + z + " In",
            })

        # add NodeList
        node_list = idf.newidfobject["NodeList".upper()]
        set_fields(
            node_list,
            {
                'Name': z + " Relief",
                'Name_1_Name': "Node " + z + " Relief",
            })
    
    # set outputs
    set_outputs(idf, ["Evaporative Cooler Wet Bulb Effectivenesse"], "*", frequency="Hourly")
    set_outputs(idf, ["Evaporative Cooler Electric Power"], "*", frequency="Hourly")
    set_outputs(idf, ["Evaporative Cooler Electric Energy"], "*", frequency="Hourly")
    set_outputs(idf, ["Evaporative Cooler Water Volume"], "*", frequency="Hourly")
    set_outputs(idf, ["Evaporative Cooler Mains Water Volume"], "*", frequency="Hourly")
    set_outputs(idf, ["Evaporative Cooler Storage Tank Water Volume"], "*", frequency="Hourly")
    set_outputs(idf, ["Evaporative Cooler Starved Water Volume"], "*", frequency="Hourly")
    set_outputs(idf, ["Evaporative Cooler Starved Mains Water Volume"], "*", frequency="Hourly")

def add_scheduled_nat_vent(
    idf,
    filter_by="",
    schedule=None,
    ach=None,
    min_ind_temp="",
    max_ind_temp="",
    delta_temp=-100,
    **params,
):
    """Add scheduled natural ventilation to desired zones.

    Calculation method is supposed to be the IDD default one, Flow/zone.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name or a list of those, defaults to ""
    :type filter_by: str or list, optional
    :param schedule: Name of the Schedule:Compact construction object to
        be added in IDF or the Schedule:Compact object in a dictionary format;
        if set to None, schedule name is not updated, defaults to None
    :type schedule: str or dict, optional
    :param ach: Value of air changes per hour, defaults to None
    :type ach: float, optional
    :param min_ind_temp: Minimum indoor temperature setpoint, defaults to None
    :type min_ind_temp: float, optional
    :param max_ind_temp: Maximum indoor temperature setpoint, defaults to None
    :type max_ind_temp: float, optional
    :param delta_temp: Delta temperature for activation, defaults to None
    :type delta_temp: float, optional
    :param params: Additional scheduled natural ventilation parameters for the
        ZoneVentilation:DesignFlowRate object.
    """
    filter_by = to_list(filter_by)
    if schedule == None:
        pass
        # # TODO: Take default element
        # raise NotImplementedError

    # Read from database.
    d = Database(PATH)
    d.load_file()
    if isinstance(schedule, str):
        sched_nat_vent_obj = d.get_object("SCHEDULE:COMPACT", schedule)
        schedule_name = schedule
    elif isinstance(schedule, dict):
        sched_nat_vent_obj = schedule
        schedule_name = schedule["Name"]
    elif schedule == None:
        schedule_name = None

    if schedule_name is not None:
        found = 0
        try:
            for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
                if sc.Name.lower() == schedule_name.lower():
                    found = 1
                    raise Warning
        except Warning:
            print(
                "Schedule ",
                schedule_name,
                " already exists in the IDF.\
                    Given schedule may not be applied.",
            )
        if not found:
            new_sc = idf.newidfobject("SCHEDULE:COMPACT")
            set_fields(new_sc, data=sched_nat_vent_obj)

    zones = idf.idfobjects["ZONE"]
    for zone in [z for z in zones if any([f.lower() in z.Name.lower().lower() for f in filter_by])]:
        vent_obj = idf.newidfobject("ZoneVentilation:DesignFlowRate".upper())

        if ach:
            design_flow_rate = round(compute_dfr(zone, ach), 4)
        else:
            design_flow_rate = ""
            print("Warning: Design_Flow_Rate field is not set.")

        set_fields(
            vent_obj,
            {
                "Name": zone.Name + " Nat Vent",
                "Zone_or_ZoneList_Name": zone.Name,
                "Design_Flow_Rate": design_flow_rate,
                "Minimum_Indoor_Temperature": min_ind_temp,
                "Maximum_Outdoor_Temperature": max_ind_temp,
                "Delta_Temperature": delta_temp,
            },
        )
        if schedule_name is not None:
            set_fields(
                vent_obj,
                {
                    "Schedule_Name": schedule_name,
                }
            )

        # Set additional fields
        set_fields(vent_obj, **params)


def add_windandstack_nat_vent(
    idf,
    value=None,
    filter_by="",
    schedule=None,
    schedule_type="compact",
    func=max,
    **params
):
    """Add scheduled natural ventilation exploiting stack and wind effects
    formulas to desired zones.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param filter_by: Filter zone by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param schedule: Name of the Schedule:Compact construction object to
        be added in IDF or the Schedule:Compact object in a dictionary format,
        defaults to None
    :type schedule: str or dict, optional
    :param schedule_type: EnergyPlus schedule tipe ("compact" or "file"),
        defaults to "compact"
    :type schedule_type: str, optional
    :param params: Additional scheduled natural ventilation parameters for the
        ZoneVentilation:WindandStackOpenArea object
    """
    if schedule == None:
        # TODO: Take default element
        raise NotImplementedError

    schedule_type = schedule_type.upper()

    # Read from database.
    d = Database(PATH)
    d.load_file()
    if isinstance(schedule, str):
        sched_nat_vent_obj = d.get_object("SCHEDULE:COMPACT", schedule)
        schedule_name = schedule
    elif isinstance(schedule, dict):
        sched_nat_vent_obj = schedule
        schedule_name = schedule["Name"]

    found = 0
    try:
        for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
            if sc.Name.lower() == schedule_name.lower():
                found = 1
                raise Warning
    except Warning:
        print(
            "Schedule ",
            schedule_name,
            " already exists in the IDF.\
                Given schedule may not be applied.",
        )
    if not found and schedule is not None:
        new_sc = idf.newidfobject("SCHEDULE:" + schedule_type)
        set_fields(new_sc, data=sched_nat_vent_obj)
        set_fields(new_sc, Name=schedule_name)

    zones = idf.idfobjects["ZONE"]
    fsd = [
        x
        for x in idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]
        if x.Surface_Type == "Window"
        or x.Surface_Type == "GlazedDoor"
        and len(x.Outside_Boundary_Condition_Object) == 0
    ]

    for zone in [z for z in zones if filter_by.lower() in z.Name.lower()]:
        # TODO: Maybe check if the window outlook another room and exclude it.
        zone_windows = [
            f
            for f in fsd
            if zone.Name.lower() in f.Building_Surface_Name.lower()
        ]
        if len(zone_windows) > 0:  # Check if the zone has windows
            if not value:
                value = func([x.area for x in zone_windows])
            vent_obj = idf.newidfobject(
                "ZoneVentilation:WindandStackOpenArea".upper()
            )
            set_fields(
                vent_obj,
                {
                    "Name": zone.Name
                    + " Wind & Stack Nat Vent_"
                    + str(uuid.uuid4()).replace("-", "")[0:4],
                    "Zone_Name": zone.Name,
                    "Opening_Area": round(value, 4),
                    "Opening_Area_Fraction_Schedule_Name": schedule_name,
                    "Height_Difference": round(zone_windows[0].height, 4),
                },
            )
            # Set additional fields
            set_fields(vent_obj, **params)


def change_stack_opening(idf, value, filter_by=""):
    """Change opening factor for WindandStackOpenArea ventilation.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New opening value (mq)
    :type value: float
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name or a list of those, defaults to ""
    :type filter_by: str or list, optional
    :raises Exception:  If no ZoneVentilation:WindAndStackOpenArea objects are
        found in the model.
    """
    found = 0
    for zvwsoa in idf.idfobjects[
        "ZoneVentilation:WindandStackOpenArea".upper()
    ]:
        if not isinstance(filter_by, list):
            filter_by = [filter_by]
        if (
            any([f.lower() in zvwsoa.Zone_Name.lower() for f in filter_by])
            or len(filter_by) == 0
        ):
            found = 1
            zvwsoa.Opening_Area = value

    if not found:
        raise Exception("No edit for change_stack_opening()")


def add_mechanical_ventilation(idf, filter_by="", schedule="", deactivate_natural_ventilation=False, mech_params={}, hvac_params={}):
    """Add mechanical ventilation.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param filter_by: Filter zone by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param schedule: Schedule to be assigned to mechanical ventilation, defaults to ""
    :type schedule: dict or str, optional
    :param deactivate_natural_ventilation: If true, natural ventilation is
        removed from the entire IDF, defaults to `False`
    :type deactivate_natural_ventilation: bool, optional
    :raises Warning: If the name of the given schedule is already present in the
        model. In such case, the old schedule is preserved
    :raises TypeError: If schedule type is not `dict` or `str`
    """
    if deactivate_natural_ventilation:
        change_ach(idf, ach=0, filter_by=filter_by)
        try:
            del idf.idfobjects["ZoneVentilation:DesignFlowRate".upper()][:]
            del idf.idfobjects["ZoneVentilation:WindandStackOpenArea".upper()][:]
        except:
            pass

    # Add Schedule:Compact object to the IDF if needed.
    found = 0
    if isinstance(schedule, dict):
        try:
            for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
                if sc.Name.lower() == schedule["Name"]:
                    found = 1
                    raise Warning
        except Warning:
            print(
                "Schedule ",
                schedule["Name"],
                " already exists in the IDF.\
                  Given schedule may not be applied.",
            )
        if not found:
            new_sc = idf.newidfobject("SCHEDULE:COMPACT")
            set_fields(new_sc, data=schedule)

    # Update zones
    for z in idf.idfobjects["ZONE"]:
        if filter_by.lower() in z.Name.lower():
            dsoa = idf.newidfobject("DESIGNSPECIFICATION:OUTDOORAIR")
            set_fields(
                dsoa,
                data={
                    "Name": z.Name,
                },
            )
            if isinstance(schedule, dict):
                schedule_name = schedule["Name"]

            elif isinstance(schedule, str):
                schedule_name = schedule

            else:
                raise TypeError("Schedule must be a dict or str")

            set_fields(dsoa, data={"Outdoor Air Schedule Name": schedule_name})
            # Set additional fields for outdoor air object
            set_fields(dsoa, **mech_params)

            # Edit HVAC system.
            # TODO: Deal with already existing supplied air.
            for zhec in idf.idfobjects[
                "ZoneHVAC:EquipmentConnections".upper()
            ]:
                if zhec.Zone_Name.lower() == z.Name.lower():
                    inlet = zhec.Zone_Air_Inlet_Node_or_NodeList_Name
                    break  # Found

            for nl in idf.idfobjects["NodeList".upper()]:
                if inlet.lower() == nl.Name.lower():
                    node = nl.Node_1_Name
                    break  # Found

            for zh in idf.idfobjects["ZoneHVAC:IdealLoadsAirSystem".upper()]:
                if zh.Zone_Supply_Air_Node_Name.lower() == node.lower():
                    zh.Design_Specification_Outdoor_Air_Object_Name = z.Name

                    # Set additional fields for economizer
                    set_fields(zh, **hvac_params)
                    break

            for sz in idf.idfobjects["SIZING:ZONE"]:
                if sz.Zone_or_ZoneList_Name.lower() == z.Name.lower():
                    zh.Design_Specification_Outdoor_Air_Object_Name = z.Name


def remove_mechanical_ventilation(idf):
    """Remove mechanical ventilation objects from the IDF.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    """
    del idf.idfobjects["DESIGNSPECIFICATION:OUTDOORAIR"][:]

    for zhilas in idf.idfobjects["ZoneHVAC:IdealLoadsAirSystem".upper()]:
        zhilas.Design_Specification_Outdoor_Air_Object_Name = ""
    
    for sz in idf.idfobjects["SIZING:ZONE"]:
        sz.Design_Specification_Outdoor_Air_Object_Name = ""


def add_overhangs_simple(idf, extension=1, tilt=90, shift=0.04, filter_by=""):
    """Add simple overhang.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param extension: Extension of the overhang, defaults to 1
    :type extension: int, optional
    :param tilt: Tilt of the overhang, defaults to 90
    :type tilt: int, optional
    :param shift: Shift of the overhang, defaults to 0.04
    :type shift: float, optional
    :param filter_by: Filter zone by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    """
    windows = idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]
    for w in windows:

        # Check type of window.
        if w.Surface_Type == "Window" or w.Surface_Type == "GlazedDoor":

            # Check if the window does not overlook another room.
            if len(w.Outside_Boundary_Condition_Object) == 0:
    
                # Check filter_by by looking at zone name.
                for bsd in idf.idfobjects["BUILDINGSURFACE:DETAILED"]:
                    if bsd.Name == w.Building_Surface_Name:
                        if filter_by.lower() not in bsd.Zone_Name.lower():
                            continue
                        else:
                            el = idf.newidfobject("SHADING:OVERHANG")
                            data = {
                                "Name": str(round(random.random() * 100000)),
                                "Window or Door Name": w.Name,
                                "Left extension from WindowDoor Width": shift,
                                "Right extension from WindowDoor Width": shift,
                                "Height above Window or Door": shift,
                                "Tilt Angle from WindowDoor": tilt,
                                "Depth": extension,
                            }
                            set_fields(el, data)


def add_overhangs_projection_simple(
    idf, extension_ratio=1, tilt=90, shift=0.04
):
    windows = idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]
    for w in windows:
        el = idf.newidfobject("SHADING:OVERHANG:PROJECTION")
        data = {
            "Name": str(round(random.random() * 100000)),
            "Window or Door Name": w.Name,
            "Height above Window or Door": shift,
            "Tilt Angle from WindowDoor": tilt,
            "Left extension from WindowDoor Width": shift,
            "Right extension from WindowDoor Width": shift,
            "Depth as Fraction of WindowDoor Height": extension_ratio,
        }
        set_fields(el, data)


def add_overhangs_complex(
    idf, depth=float, tilt=90, orientation=None, transmittance_schedule=""
):
    # Taken from BESOS (University of Victoria)
    # Potrebbe non funzionare se le finestre sono definite con coordinate
    # diverse.
    """Set the overhang shading on all external windows by creating a
    SHADING:ZONE:DETAILED object which can be viewed using geomeppy's
    view_model() function.

    Note:
    Depth must be greater than 0

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param depth: The depth in meters of the overhang.
    :type depth: float
    :param tilt: Tilt Angle from Window/Door {deg}.
    :type tilt: int
    :param orientation: One of "north", "east", "south", "west". Walls within
        45 degrees will be affected.
    :type orientation: int
    :param transmittance_schedule: Transmittance Schedule Name, defaults to ""
    :type transmittance_schedule: str, optional
    """
    try:
        ggr = idf.idfobjects["GLOBALGEOMETRYRULES"][0]
    except IndexError:
        ggr = None
    # Orientation to degrees.
    orientations = {
        "north": 0.0,
        "east": 90.0,
        "south": 180.0,
        "west": 270.0,
        None: None,
    }
    degrees = orientations.get(orientation, None)
    external_walls = filter(
        lambda x: x.Outside_Boundary_Condition.lower() == "outdoors",
        idf.getsurfaces("wall"),
    )
    external_walls = list(
        filter(
            lambda x: recipes._has_correct_orientation(x, degrees),
            external_walls,
        )
    )
    windows = idf.getsubsurfaces("window")
    for wall in external_walls:

        for window in windows:

            if window.Building_Surface_Name == wall.Name:
                coords = [window.coords[3]]
                x, y, _ = window.coords[0]
                coords.append(
                    (
                        x
                        + depth
                        * sin(deg2rad(window.azimuth))
                        * sin(deg2rad(tilt)),
                        y
                        + depth
                        * cos(deg2rad(window.azimuth))
                        * sin(deg2rad(tilt)),
                        window.coords[3][2] - depth * cos(deg2rad(tilt)),
                    )
                )
                x, y, _ = window.coords[1]
                coords.append(
                    (
                        x
                        + depth
                        * sin(deg2rad(window.azimuth))
                        * sin(deg2rad(tilt)),
                        y
                        + depth
                        * cos(deg2rad(window.azimuth))
                        * sin(deg2rad(tilt)),
                        window.coords[2][2] - depth * cos(deg2rad(tilt)),
                    )
                )
                coords.append(window.coords[2])
                Shade = idf.newidfobject(
                    "SHADING:ZONE:DETAILED",
                    Name="%s - Overhang" % window.Name,
                    Transmittance_Schedule_Name=transmittance_schedule,
                    Base_Surface_Name=window.Building_Surface_Name,
                )
                Shade.setcoords(coords, ggr)


def add_overhangs_detailed(idf, extension=1, shift=0.04):
    """Add shading to all windows in the IDF.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param extension: Extension in meter for the shading, defaults to 1
    :type extension: int, optional
    """
    windows = [w for w in idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]]
    for w in windows:
        _add_one_shading(idf, w, extension, shift)


def _add_one_shading(idf, window, extension, shift):
    """Add shading over a single window.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param window: Fenestrationsurface:Detailed object
    :type obj: class 'geomeppy.patches.EpBunch'
    :param extension: Extension in meter for the shading, defaults to 1
    :type extension: int, optional
    """
    # TODO: make sure that the global order of shadings is correct
    # TODO: make sure that the vertex order for each shading is correct
    name_list = [n.Name for n in idf.idfobjects["SHADING:BUILDING:DETAILED"]]
    name = "1"
    available = 0

    if len(name_list) > 0:
        while not available:
            name = str(int(name) + 1)
            if name not in name_list:
                available = 1
    shad = idf.newidfobject("SHADING:BUILDING:DETAILED")

    if max(
        window.Vertex_1_Xcoordinate,
        window.Vertex_2_Xcoordinate,
        window.Vertex_3_Xcoordinate,
        window.Vertex_4_Xcoordinate,
    ) == min(
        window.Vertex_1_Xcoordinate,
        window.Vertex_2_Xcoordinate,
        window.Vertex_3_Xcoordinate,
        window.Vertex_4_Xcoordinate,
    ):
        if window.Vertex_1_Xcoordinate > 0:
            x1 = window.Vertex_1_Xcoordinate
            x2 = window.Vertex_1_Xcoordinate + extension
        else:
            x1 = window.Vertex_1_Xcoordinate
            x2 = window.Vertex_1_Xcoordinate - extension

        y1 = (
            min(
                window.Vertex_1_Ycoordinate,
                window.Vertex_2_Ycoordinate,
                window.Vertex_3_Ycoordinate,
                window.Vertex_4_Ycoordinate,
            )
            - shift
        )
        y2 = (
            max(
                window.Vertex_1_Ycoordinate,
                window.Vertex_2_Ycoordinate,
                window.Vertex_3_Ycoordinate,
                window.Vertex_4_Ycoordinate,
            )
            + shift
        )

    elif max(
        window.Vertex_1_Ycoordinate,
        window.Vertex_2_Ycoordinate,
        window.Vertex_3_Ycoordinate,
        window.Vertex_4_Ycoordinate,
    ) == min(
        window.Vertex_1_Ycoordinate,
        window.Vertex_2_Ycoordinate,
        window.Vertex_3_Ycoordinate,
        window.Vertex_4_Ycoordinate,
    ):
        if window.Vertex_1_Ycoordinate > 0:
            y1 = window.Vertex_1_Ycoordinate
            y2 = window.Vertex_1_Ycoordinate + extension
        else:
            y1 = window.Vertex_1_Ycoordinate
            y2 = window.Vertex_1_Ycoordinate - extension

        x1 = (
            min(
                window.Vertex_1_Xcoordinate,
                window.Vertex_2_Xcoordinate,
                window.Vertex_3_Xcoordinate,
                window.Vertex_4_Xcoordinate,
            )
            - shift
        )
        x2 = (
            max(
                window.Vertex_1_Xcoordinate,
                window.Vertex_2_Xcoordinate,
                window.Vertex_3_Xcoordinate,
                window.Vertex_4_Xcoordinate,
            )
            + shift
        )

    z = (
        max(
            window.Vertex_1_Zcoordinate,
            window.Vertex_2_Zcoordinate,
            window.Vertex_3_Zcoordinate,
            window.Vertex_4_Zcoordinate,
        )
        + shift
    )

    data = {
        "Name": name,
        "Number of Vertices": "4",
        "Vertex 1 Xcoordinate": round(x1, 10),
        "Vertex 1 Ycoordinate": round(y2, 10),
        "Vertex 1 Zcoordinate": round(z, 10),
        "Vertex 2 Xcoordinate": round(x2, 10),
        "Vertex 2 Ycoordinate": round(y2, 10),
        "Vertex 2 Zcoordinate": round(z, 10),
        "Vertex 3 Xcoordinate": round(x2, 10),
        "Vertex 3 Ycoordinate": round(y1, 10),
        "Vertex 3 Zcoordinate": round(z, 10),
        "Vertex 4 Xcoordinate": round(x1, 10),
        "Vertex 4 Ycoordinate": round(y1, 10),
        "Vertex 4 Zcoordinate": round(z, 10),
    }

    set_fields(shad, data=data)


def remove_shading(
    idf,
    which=[
        "Shading:Building:Detailed",
        "Shading:Zone:Detailed",
        "Shading:Overhang",
    ],
):
    """Remove all or specified shading overhangs from the building.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param which: Specify which overhangs objects have to be removed, defaults
        to ["Shading:Building:Detailed", "Shading:Zone:Detailed",
        "Shading:Overhang"]
    :type which: str or list, optional
    """
    if isinstance(which, list):
        for s in which:
            shadings = idf.idfobjects[s.upper()]
            del shadings[:]

    if isinstance(which, str):
        shadings = idf.idfobjects[which.upper()]
        del shadings[:]


def add_fin_simple(idf, extension=1, shift=0.04):
    """Add simple fin.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param extension: Extension of the fin, defaults to 1
    :type extension: int, optional
    :param shift: Shift of the fin from the window edges, defaults to 0.04
    :type shift: float, optional
    """
    windows = idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]
    for w in windows:
        el = idf.newidfobject("SHADING:FIN")
        data = {
            "Name": str(round(random.random() * 100000)),
            "Window or Door Name": w.Name,
            "Left Extension from WindowDoor": shift,
            "Left Distance Above Top of Window": shift,
            "Left Distance Below Bottom of Window": shift,
            "Left Depth": extension,
            "Right Extension from WindowDoor": shift,
            "Right Distance Above Top of Window": shift,
            "Right Distance Below Bottom of Window": shift,
            "Right Depth": extension,
        }
        set_fields(el, data)


def add_blind(
    idf,
    blind_data,
    type="interior",
    filter_by="",
    filter_by_orientation=None,
    control_type="AlwaysOn",
    schedule=None,
    blind_type="WINDOWMATERIAL:BLIND",
    **fields
):
    # TODO Allow shading control strategy customization.
    """Add interior or exterior blind to all windows.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param blind_data: WINDOWMATERIAL:BLIND" JSON data
    :type blind_data: dict
    :param type: Type of blind (interior or exterior), defaults to "interior"
    :type type: str, optional
    :param filter_by: Filter zone by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param filter_by_orientation: Specify the orientation of the windows to
        which the blind has to be added. Possible values are 0, 45, 90, 135,
        180, 225, 270, 315, 360 (the windows orientation in the building are
        converted to the nearest of these values for simplicity), defaults to
        None
    :type filter_by_orientation: int, optional
    :param control_type: Shading control type, defaults to "AlwaysOn"
    :type control_type: str, optional
    :param schedule: If specified blinds will follow the given schedule,
        otherwise they are supposed to be always on, defaults to None
    :type schedule: dict or str, optional

    :Example:

    .. code-block:: python

        add_blind(idf,
                  blind_data={
                      "Name": "ext_blind",
                      "Slat Width": 0.025,
                      "Slat Separation": 0.01875,
                      "Slat Thickness": 0.001,
                      "Slat Conductivity": 0.9,
                      "Front Side Slat Beam Solar Reflectance": 0.8,
                      "Back Side Slat Beam Solar Reflectance": 0.8,
                      "Front Side Slat Diffuse Solar Reflectance": 0.8,
                      "Back Side Slat Diffuse Solar Reflectance": 0.8,
                      "Slat Beam Visible Transmittance": 0,
                      "Front Side Slat Beam Visible Reflectance": 0.8,
                      "Back Side Slat Beam Visible Reflectance": 0.8,
                      "Front Side Slat Diffuse Visible Reflectance": 0.8,
                      "Back Side Slat Diffuse Visible Reflectance": 0.8,
                      "Blind to Glass Distance": 0.015,
                      "Blind Bottom Opening Multiplier": 0.5
                  },
                  type="exterior",
                  filter_by="main_block"
                  )

    """
    # Possible values for orientation (azimuth) of windows.
    possible_values = [0, 45, 90, 135, 180, 225, 270, 315, 360]
    shaded = "_shaded_" + str(uuid.uuid4()).replace("-", "")[0:4]
    control = "_control_" + str(uuid.uuid4()).replace("-", "")[0:4]
    # Add shaded construction object by copying window constructions.
    construction_list = []
    for f in idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]:
        o = possible_values[
            min(
                range(len(possible_values)),
                key=lambda i: abs(possible_values[i] - f.azimuth),
            )
        ]
        if o == 360:
            o = 0
        if filter_by_orientation == 360:
            filter_by_orientation = 0
        # Check if:
        #   - the window type is Window or GlazedDoor
        #   - the window does not overlook another room
        #   - the zone name is included in filter_by
        if (
            (f.Surface_Type == "Window" or f.Surface_Type == "GlazedDoor")
            and len(f.Outside_Boundary_Condition_Object) == 0
            and filter_by.lower() in f.Name.lower()
        ):
            if filter_by_orientation == None or o == filter_by_orientation:
                construction_list.append(f.Construction_Name)

                # Change Shading_Control_Name according to ShadingControl name.
                setattr(
                    f,
                    "Shading_Control_Name",
                    f.Construction_Name + shaded + control,
                )

    construction_list = list(set(construction_list))  # Remove duplicates

    # Transform construction names into real construction objects.
    construction_list = [
        c
        for c in idf.idfobjects["CONSTRUCTION"]
        if c.Name in construction_list
    ]

    # Create WindowMaterial:Blind object.
    try:
        for wmb in idf.idfobjects[blind_type]:
            if wmb.Name == blind_data["Name"]:
                raise ValueError(
                    "Blind material with specified name is already present in the model"
                )
    except ValueError as e:
        print(e)
        print("Assigning a new name to the material")
        blind_data["Name"] += str(uuid.uuid4()).replace("-", "")[0:6]
    blind = idf.newidfobject(blind_type)
    set_fields(blind, blind_data)

    for con in construction_list:
        new_con = idf.copyidfobject(con)
        con_name = getattr(con, "Name")
        con_name += shaded
        setattr(new_con, "Name", con_name)

        # Interior: add layer as last (inner) layer.
        if type == "interior":
            shad_type = "InteriorBlind"
            last_layer = get_layer(new_con, "last")
            insert_layer(new_con, last_layer, blind_data["Name"], below=True)

        # Exterior: add layer as first (outer) layer.
        elif type == "exterior":
            shad_type = "ExteriorBlind"
            first_layer = get_layer(new_con, "first")
            insert_layer(new_con, first_layer, blind_data["Name"], below=False)

        # Create Windowproperty:ShadingControl object.
        wpsc = idf.newidfobject("WINDOWPROPERTY:SHADINGCONTROL")
        data = {
            "Name": con_name + control,
            "Shading Type": shad_type,  # InteriorBlind/ExteriorBlind
            "Construction with Shading Name": con_name,  # Automatically assigned
            "Shading Control Type": control_type,
        }
        set_fields(wpsc, data=data)

        # Set schedule to shading control if is given.
        if isinstance(schedule, str):
            set_fields(
                wpsc,
                data={
                    "Schedule Name": schedule,
                    "Shading Control Type": "OnIfScheduleAllows",
                    "Shading Control Is Scheduled": "Yes",
                },
            )
        elif isinstance(schedule, dict):   
            set_fields(
                wpsc,
                data={
                    "Schedule Name": schedule["Name"],
                    "Shading Control Type": "OnIfScheduleAllows",
                    "Shading Control Is Scheduled": "Yes",
                },
            )

    # Set additional fields.
    for key, value in fields.items():
        try:
            setattr(wpsc, key, value)
        except Exception:
            print("Field %s does not exist" % key)
    # If schedule is provided, create a new schedule object and assign it
    # to the shading control.
    if isinstance(schedule, dict):
        if check_schedule(idf, schedule["Name"]):
            raise ValueError(
                "Schedule with specified name is already present in the model"
            )
        else:
        # try:
        #     for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
        #         if sc.Name == schedule["Name"]:
        #             raise ValueError(
        #                 "Schedule with specified name is already present in the model"
        #             )
        # except ValueError as e:
        #     print(e)
        #     schedule["Name"] += str(uuid.uuid4()).replace("-", "")[0:6]

            sc = idf.newidfobject("SCHEDULE:COMPACT")
            set_fields(sc, schedule)
        # set_fields(
        #     wpsc,
        #     data={
        #         "Schedule Name": schedule["Name"],
        #         "Shading Control Type": "OnIfScheduleAllows",
        #         "Shading Control Is Scheduled": "Yes",
        #     },
        # )


def add_blind_v2(
    idf,
    blind_data,
    type="interior",
    filter_by="",
    filter_by_orientation=None,
    control_type="AlwaysOn",
    schedule=None,
    blind_type="blind"
):
    # TODO Allow shading control strategy customization.
    """Add interior or exterior blind to all windows.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param blind_data: WINDOWMATERIAL:BLIND" JSON data
    :type blind_data: dict
    :param type: Type of blind (interior or exterior), defaults to "interior"
    :type type: str, optional
    :param filter_by: Filter zone by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param filter_by_orientation: Specify the orientation of the windows to
        which the blind has to be added. Possible values are 0, 45, 90, 135,
        180, 225, 270, 315, 360 (the windows orientation in the building are
        converted to the nearest of these values for simplicity), defaults to
        None
    :type filter_by_orientation: int, optional
    :param control_type: Shading control type, defaults to "AlwaysOn"
    :type control_type: str, optional
    :param schedule: If specified blinds will follow the given schedule,
        otherwise they are supposed to be always on, defaults to None
    :type schedule: dict, optional
    """

    # Transform construction names into real construction objects.
    # construction_list = [
    #     c
    #     for c in idf.idfobjects["CONSTRUCTION"]
    #     if c.Name in construction_list
    # ]
    # Possible values for orientation (azimuth) of windows.
    possible_values = [0, 45, 90, 135, 180, 225, 270, 315, 360]
    shaded = "_shaded_" + str(uuid.uuid4()).replace("-", "")[0:4]
    control = "_control_" + str(uuid.uuid4()).replace("-", "")[0:4]
    # Add shaded construction object by copying window constructions.
    construction_list = []
    for f in idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]:
        o = possible_values[
            min(
                range(len(possible_values)),
                key=lambda i: abs(possible_values[i] - f.azimuth),
            )
        ]
        if o == 360:
            o = 0
        if filter_by_orientation == 360:
            filter_by_orientation = 0
        # Check if:
        #   - the window type is Window or GlazedDoor
        #   - the window does not overlook another room
        #   - the zone name is included in filter_by
        if (
            (f.Surface_Type == "Window" or f.Surface_Type == "GlazedDoor")
            and len(f.Outside_Boundary_Condition_Object) == 0
            and filter_by.lower() in f.Name.lower()
        ):
            if filter_by_orientation == None or o == filter_by_orientation:
                construction_list.append(f.Construction_Name)

                # Change Shading_Control_Name according to ShadingControl name.
                setattr(
                    f,
                    "Shading_Control_Name",
                    f.Construction_Name + shaded + control,
                )

    construction_list = list(set(construction_list))  # Remove duplicates

    # Transform construction names into real construction objects.
    construction_list = [
        c
        for c in idf.idfobjects["CONSTRUCTION"]
        if c.Name in construction_list
    ]

    if type.lower() in ["interior", "internal", "int"]:
        shad_type = "Interior" + blind_type
    elif type.lower() in ["exterior", "external", "ext"]:
        shad_type = "Exterior" + blind_type

    try:
        for wmb in idf.idfobjects[":".join(["WindowMaterial", blind_type]).upper()]:
            if wmb.Name == blind_data["Name"]:
                raise ValueError(
                    "Blind material with specified name is already present in the model"
                )
    except ValueError as e:
        print(e)
        print("Assigning a new name to the material")
        blind_data["Name"] += str(uuid.uuid4()).replace("-", "")[0:6]
    blind = idf.newidfobject(":".join(["WindowMaterial", blind_type]).upper())
    set_fields(blind, blind_data)

    for con in construction_list:
        # new_con = idf.copyidfobject(con)
        con_name = getattr(con, "Name")
        con_name += shaded
        # setattr(new_con, "Name", con_name)
        # con_name = getattr(con, "Name")
        # Create Windowproperty:ShadingControl object.
        wpsc = idf.newidfobject("WINDOWPROPERTY:SHADINGCONTROL")
        data = {
            "Name": con_name + control,
            "Shading Type": shad_type,  # InteriorBlind/ExteriorBlind
            "Construction with Shading Name": "",  # Automatically assigned
            "Shading Device Material Name": blind.Name,
            "Shading Control Type": control_type,
        }
        set_fields(wpsc, data=data)

    # If schedule is provided, create a new schedule object and assign it
    # to the shading control.
    if schedule is not None:
        try:
            for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
                if sc.Name == schedule["Name"]:
                    raise ValueError(
                        "Schedule with specified name is already present in the model"
                    )
        except ValueError as e:
            print(e)
            schedule["Name"] += str(uuid.uuid4()).replace("-", "")[0:6]

        sc = idf.newidfobject("SCHEDULE:COMPACT")
        set_fields(sc, schedule)
        set_fields(
            wpsc,
            data={
                "Schedule Name": schedule["Name"],
                "Shading Control Type": "OnIfScheduleAllows",
                "Shading Control Is Scheduled": "Yes",
            },
        )


def _set_shading_control(idf, name, data=None):
    """Add shading control to all external windows.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param name: WindowProperty:ShadingControl object name. It can be an
        object already existing or a new one; in the latter case, also data
        must be passed.
    :type name: str
    :param data: data of WindowProperty:ShadingControl object, defaults to None
    :type data: dict, optional
    """
    fsd = idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]
    for f in fsd:
        if f.Surface_Type == "Window" or f.Surface_Type == "GlazedDoor":
            # Check if the window does not overlook another room.
            if len(f.Outside_Boundary_Condition_Object) == 0:
                f.Shading_Control_Name = name
    if data is not None:
        sc = idf.newidfobject("WINDOWPROPERTY:SHADINGCONTROL")
        set_fields(sc, data)


def set_block_beam_solar(idf):
    """Set BlockBeamSolar to the shading control.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    """
    wps = idf.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]
    wps_data = {
        "Type_of_Slat_Angle_Control_for_Blinds": "BlockBeamSolar",
        "Slat_Angle_Schedule_Name": "",
    }
    for w in wps:
        set_fields(w, data=wps_data)


def add_slat_angle_control(idf, angle, setpoint=None):
    """Set shading control to fixed angle.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param angle: Angle of shading to be applied
    :type angle: float
    :param setpoint: The setpoint for activating window shading
    :type setpoint: float
    """
    for s in idf.idfobjects["SCHEDULE:COMPACT"]:
        if getattr(s, "Name") == "Slat angle control":
            del s

    # Create Schedule:Compact object
    sac = idf.newidfobject("SCHEDULE:COMPACT")
    data = {
        "Name": "Slat angle control",
        "Schedule Type Limits Name": "Any Number",
        "Field 1": "Through: 12/31",
        "Field 2": "For: AllDays",
        "Field 3": "Until: 24:00",
        "Field 4": str(angle),
    }
    set_fields(sac, data=data)

    # Set fields in WindowProperty:ShadingControl object
    wps = idf.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]
    wps_data = {
        # "Shading Control Type": "OnIfHighHorizontalSolar",
        # "Setpoint": float(setpoint),
        "Type of Slat Angle Control for Blinds": "ScheduledSlatAngle",
        "Slat Angle Schedule Name": "Slat angle control",
        # "Setpoint 2": 0.0,  # 0 because of OnIfHighHorizontalSolar type
    }
    for w in wps:
        set_fields(w, data=wps_data)


def change_opening_factor(idf, value):
    """Change opening factor.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New opening factor value
    :type value: float or str
    """
    amcd = idf.idfobjects["AIRFLOWNETWORK:MULTIZONE:COMPONENT:DETAILEDOPENING"]
    for a in amcd:
        num = getattr(a, "Number_of_Sets_of_Opening_Factor_Data")
        for n in range(1, num + 1):
            data = {
                "Height Factor for Opening Factor " + str(n): value,
                "Start Height Factor for Opening Factor "
                + str(n): round(1 - value, 2),
            }
            set_fields(a, data)


def change_supply_air_temperature(idf, which, value):
    if which == "heating":
        for z in idf.idfobjects["SIZING:ZONE"]:
            set_fields(
                z, {"Zone Heating Design Supply Air Temperature": value}
            )
        for i in idf.idfobjects["ZONEHVAC:IDEALLOADSAIRSYSTEM"]:
            set_fields(i, {"Maximum Heating Supply Air Temperature": value})
    elif which == "cooling":
        for z in idf.idfobjects["SIZING:ZONE"]:
            set_fields(
                z, {"Zone Cooling Design Supply Air Temperature": value}
            )
        for i in idf.idfobjects["ZONEHVAC:IDEALLOADSAIRSYSTEM"]:
            set_fields(i, {"Minimum Cooling Supply Air Temperature": value})


def change_heating_supply_air_humidity_ratio(idf, value):
    # TODO: Gestire insieme anche cooling (?)
    """Change heating supply air humidity ratio in the building.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New Heating Design Supply Air Humidity Ratio value
    :type value: float
    """
    for z in idf.idfobjects["SIZING:ZONE"]:
        set_fields(z, {"Zone Heating Design Supply Air Humidity Ratio": value})
    for i in idf.idfobjects["ZONEHVAC:IDEALLOADSAIRSYSTEM"]:
        set_fields(i, {"Maximum Heating Supply Air Humidity Ratio": value})


def change_hvac_limits(idf, heating=None, cooling=None):
    """Change HVAC heating and cooling limits.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param heating: New heating limit, defaults to None
    :type heating: str, optional
    :param cooling: New cooling limit, defaults to None
    :type cooling: str, optional
    :raises Exception: [description]
    :raises Exception: [description]
    """
    limit_list = [
        "NoLimit",
        "LimitFlowRate",
        "LimitCapacity",
        "LimitFlowRateAndCapacity",
    ]
    try:
        if heating not in limit_list:
            raise Exception
        if cooling not in limit_list:
            raise Exception
        for hvac in idf.idfobjects["ZoneHVAC:IdealLoadsAirSystem"]:
            if heating is not None:
                set_fields(hvac, {"Heating Limit": heating})
            if cooling is not None:
                set_fields(hvac, {"Cooling Limit": cooling})
    except Exception:
        print("Limit type is not valid")


def change_dhw_temperature(idf, supply=None, mains=None):
    """Change DHW Supply and DHW Mains temperatures.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param supply: Value of DHW supply temperature, defaults to None
    :type supply: int or str, optional
    :param mains: Value of DHW mains temperature, defaults to None
    :type mains: int or str, optional
    """
    schedules = idf.idfobjects["SCHEDULE:COMPACT"]
    dhw = idf.idfobjects["WATERUSE:EQUIPMENT"]

    # Read DHW schedule names in the WaterUse:Equipment object
    for d in dhw:
        dhw_supply = getattr(d, "Target_Temperature_Schedule_Name")
        dhw_mains = getattr(d, "Cold_Water_Supply_Temperature_Schedule_Name")

        # For each Schedule:Compact, search DHW Supply and DHW Mains
        for s in schedules:
            if dhw_supply in s.Name and supply is not None:
                found = 0
                for key in s.objls:
                    if found == 1:
                        setattr(s, key, str(supply))
                        found = 0
                    try:
                        if "Until" in getattr(s, key):
                            found = 1
                    except Exception:
                        pass

            if dhw_mains in s.Name and mains is not None:
                found = 0
                for key in s.objls:
                    if found == 1:
                        setattr(s, key, str(mains))
                        found = 0
                    try:
                        if "Until" in getattr(s, key):
                            found = 1
                    except Exception:
                        pass


def get_number_of_people(idf, zone):
    """Get nominal number of people of the given zone..

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param zone: Zone name or zone object (eppy)
    :type zone: str or class 'geomeppy.patches.EpBunch'
    :return: Nominal number of people (scalar).
    :rtype: float
    :raises KeyError: If given zone is not found in the IDF.
    """
    found = 0
    if isinstance(zone, str):
        for z in idf.idfobjects["ZONE"]:
            if z.Name.lower() == zone.lower():
                found = 1
                zone = z
                break

        if not found:
            raise KeyError(f"Zone '{zone}' not found.")

    found = 0
    for p in idf.idfobjects["PEOPLE"]:
        if zone.Name.lower() == p.Zone_or_ZoneList_Name.lower():
            found = 1
            break
    if not found:
        print(f"Warning: No PEOPLE object found for zone '{zone.Name}'.",
              "Assuming Number_of_people=0")
        return 0
    
    try:
        method = p.Number_of_People_Calculation_Method.lower()
    except Exception as e:
        print(e)

    if method == "people":
        return p.Number_of_People
    elif method == "people/area":
        return p.People_per_Zone_Floor_Area * zone.Floor_Area
    elif method == "area/person":
        return zone.Floor_Area / p.Zone_Floor_Area_per_Person


def change_occupancy(
    idf,
    value=None,
    schedule=None,
    filter_by="",
    relative=False,
    low=0.03,
    high=0.2,
    replace=False,
    **params

):
    """Change People per Zone Floor Area field with a new value.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New base occupancy value; it can be a float number or
        "random". If set to "random", minimum and maximum values for the random
        interval can be provided by `low` and `high` parameters, defaults to
        None
    :type value: float or str, optional
    :param schedule: New occupancy schedule,
        defaults to None
    :type schedule: dict, optional
    :param filter_by: Filter zone by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param relative: Specify if new value will be assigned as a percentage
        increment from the old value, defaults to `False`
    :type relative: bool, optional
    :param low: Minimum base occupancy value when "random" is chosen for `value`
        parameter, defaults to 0.03
    :type low: float, optional
    :param high: Maximum base occupancy value when "random" is chosen for
        `value` parameter, defaults to 0.2
    :type high: float, optional
    :param replace: If True, the current schedule is replaced by the new one,
        defaults to `False`
    :type replace: bool, optional

    .. warning:: When `replace` is set to `True`, schedule associations are
        preserved; therefore you might unintentionally modify several elements
        in the model. This happens if the modified schedule is also associated
        to other element than occupancy.
    """
    # Check if given schedule already exists in the model.
    if (schedule is not None) and (not replace):
        existing_schedules = [
            sc.Name for sc in idf.idfobjects["SCHEDULE:COMPACT"]
        ] + [sc.Name for sc in idf.idfobjects["SCHEDULE:FILE"]]
        if schedule["Name"] in existing_schedules:
            raise Exception(
                "A schedule named %s already exists in the model"
                % schedule["Name"]
            )

    for p in idf.idfobjects["PEOPLE"]:
        if isinstance(value, float):
            if filter_by.lower() in p.Name.lower():
                # Case 1: Relative value
                if relative:
                        try:
                            old_value = getattr(p, "People_per_Zone_Floor_Area")
                        except Exception:
                            raise Exception(
                                "The model does not have occupancy specified in People/Area unit"
                            )
                        new_value = old_value + old_value * value
                        setattr(p, "People_per_Zone_Floor_Area", new_value)
                        setattr(
                            p, "Number_of_People_Calculation_Method", "People/Area"
                        )
                    
                # Case 2: Absolute value
                else:
                        setattr(p, "People_per_Zone_Floor_Area", value)
                        setattr(
                            p, "Number_of_People_Calculation_Method", "People/Area"
                        )

                # # Case 3: Random value
                # elif value == "random":
                #     if min(low, high) < 0 or high <= low:
                #         raise Exception(
                #             "You must provide valid values for both low and high limits."
                #         )
                #     if filter_by.lower() in p.Name.lower():
                #         rand_value = round(random.uniform(low, high), 2)
                #         setattr(p, "People_per_Zone_Floor_Area", rand_value)
                #         setattr(
                #             p, "Number_of_People_Calculation_Method", "People/Area"
                #         )

                set_fields(p, data=None, **params)

                # Set new schedule.
                if (schedule is not None) and (not replace):
                    setattr(p, "Number_of_People_Schedule_Name", schedule["Name"])
                elif (schedule is not None) and (replace):
                    to_be_replaced = getattr(p, "Number_of_People_Schedule_Name")
                    for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
                        if sc.Name == to_be_replaced:
                            idf.removeidfobject(sc)
                            sc = idf.newidfobject("SCHEDULE:COMPACT")
                            set_fields(sc, data=schedule)
                            sc.Name = to_be_replaced
                            break

    # Add the new schedule to the IDF.
    if schedule is not None and (not replace):
        sc = idf.newidfobject("SCHEDULE:COMPACT")
        set_fields(sc, data=schedule)


def get_area(idf, name):
    """Compute and return area of the building by filtering zone names.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param name: Name of the zones
    :type name: str
    :return: Area
    :rtype: float
    """
    home_area = 0
    for zone in idf.idfobjects["ZONE"]:
        if name.lower() in zone.Name.lower():
            if zone.Floor_Area == "":
                pass
            else:
                home_area += zone.Floor_Area
    return home_area


def get_volume(idf, name):
    """Compute and return volume of the building by filtering zone names.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param name: Name of the zones
    :type name: str
    :return: Volume
    :rtype: float
    """
    home_volume = 0
    for zone in idf.idfobjects["ZONE"]:
        if name.lower() in zone.Name.lower():
            if zone.Volume == "":
                pass
            else:
                home_volume += zone.Volume
    return home_volume


def set_epw(idf, epw_file):
    """Set EPW file in the IDF

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param epw_file: EPW filename
    :type epw_file: path or str
    """
    idf.epw = epw_file


def get_hvac_elements(idf):
    """Get all HVAC elements that are present the building.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :return: List of HVAC objects
    :rtype: list
    """
    obj_list = [
        "Sizing:Zone",
        "DesignSpecification:ZoneAirDistribution",
        "ZoneControl:Thermostat",
        "ThermostatSetpoint:DualSetpoint",
        "ZoneHVAC:EquipmentConnections",
        "ZoneHVAC:EquipmentList",
        "ZoneHVAC:IdealLoadsAirSystem",
        "NodeList",
    ]
    hvac_ojects = [idf.idfobjects[el.upper()] for el in obj_list]
    hvac_ojects = [o for el in hvac_ojects for o in el]
    return hvac_ojects


def add_hvac(idf, heating_schedule=None, cooling_schedule=None, filter_by=""):
    """Add HVAC to a building without HVAC.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param heating_schedule: Heating schedule, defaults to None
    :type heating_schedule: dict, optional
    :param cooling_schedule: Cooling schedule, defaults to None
    :type cooling_schedule: dict, optional
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    """
    if len(get_hvac_elements(idf)) > 0:
        print("Warning: The building already has an HVAC system. This may cause problems.")

    zones = [z for z in idf.idfobjects["ZONE"] if filter_by.lower() in z.Name.lower()]
    for z in zones:
        add_zone_hvac(
            idf,
            z,
            heating_schedule=heating_schedule,
            cooling_schedule=cooling_schedule,
        )


def add_zone_hvac(idf, zone, heating_schedule=None, cooling_schedule=None):
    """Add HVAC to a zone without HVAC.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param zone: Zone object (eppy)
    :type zone: class 'geomeppy.patches.EpBunch'
    :param heating_schedule: Heating schedule, defaults to None
    :type heating_schedule: dict, optional
    :param cooling_schedule: Cooling schedule, defaults to None
    :type cooling_schedule: dict, optional
    """
    # Validation non needed maybe.
    # if heating_schedule == None and cooling_schedule == None:
    #     raise Exception("At least one schedule must be provided")

    # Heating.
    if heating_schedule is not None:
        heat_is_on = 1
        heating_schedule["Name"] = zone.Name + " Heating Setpoint Schedule"
    else:
        heat_is_on = 0
        heating_schedule = {
            "Name": zone.Name + " Heating Setpoint Schedule",
            "Schedule_Type_Limits_Name": "Any Number",
            "Field_1": "Through: 12/31",
            "Field_2": "For: AllDays",
            "Field_3": "Until: 24:00",
            "Field_4": -50,
        }
    hsc = idf.newidfobject("SCHEDULE:COMPACT")
    set_fields(hsc, data=heating_schedule)

    # Create availability heating schedule from setpoint schedule.
    havail = idf.newidfobject("SCHEDULE:COMPACT")
    heat_avail = {
        k: 0 if v == -50 else 1 if isinstance(v, int) else v
        for k, v in heating_schedule.items()
    }
    heat_avail["Name"] = zone.Name + " Heating Availability Sch"
    set_fields(havail, data=heat_avail)

    # Cooling.
    if cooling_schedule is not None:
        cool_is_on = 1
        cooling_schedule["Name"] = zone.Name + " Cooling SP Sch"
    else:
        cool_is_on = 0
        cooling_schedule = {
            "Name": zone.Name + " Cooling SP Sch",
            "Schedule_Type_Limits_Name": "Any Number",
            "Field_1": "Through: 12/31",
            "Field_2": "For: AllDays",
            "Field_3": "Until: 24:00",
            "Field_4": 100,
        }
    csc = idf.newidfobject("SCHEDULE:COMPACT")
    set_fields(csc, data=cooling_schedule)

    # Create availability cooling schedule from setpoint schedule.
    cavail = idf.newidfobject("SCHEDULE:COMPACT")
    cool_avail = {
        k: 0 if v == 100 else 1 if isinstance(v, int) else v
        for k, v in cooling_schedule.items()
    }
    cool_avail["Name"] = zone.Name + " Cooling Availability Sch"
    set_fields(cavail, data=cool_avail)

    # Sizing:Zone
    sz = idf.newidfobject("SIZING:ZONE")
    set_fields(
        sz,
        data={
            "Zone or ZoneList Name": zone.Name,
            "Zone Cooling Design Supply Air Temperature": 12,
            "Zone Cooling Design Supply Air Temperature Difference": 0,
            "Zone Heating Design Supply Air Temperature": 35,
            "Zone Heating Design Supply Air Temperature Difference": 0,
            "Zone Cooling Design Supply Air Humidity Ratio": 0.0077,  # Default
            "Zone Heating Design Supply Air Humidity Ratio": 0.0156,  # Default
            "Zone Heating Sizing Factor": 1.25,  # Recommended value
            "Zone Cooling Sizing Factor": 1.15,  # Recommended value
            "Cooling Design Air Flow Rate": 0,
            "Cooling Minimum Air Flow per Zone Floor Area": 0.000762,  # Default
            "Cooling Minimum Air Flow": 0,
            "Cooling Minimum Air Flow Fraction": 0.2,
            "Heating Maximum Air Flow per Zone Floor Area": 0.002032,  # Default
            "Heating Maximum Air Flow": 0.1415762,  # Default
            "Heating Maximum Air Flow Fraction": 0.3,  # Default
            "Design Specification Zone Air Distribution Object Name": zone.Name,
        },
    )

    # DesignSpecification:ZoneAirDistribution
    dszad = idf.newidfobject("DesignSpecification:ZoneAirDistribution".upper())
    set_fields(dszad, data={"Name": zone.Name})

    # ZoneControl:Thermostat
    zct = idf.newidfobject("ZoneControl:Thermostat".upper())
    set_fields(
        zct,
        data={
            "Name": zone.Name + " Thermostat",
            "Zone or ZoneList Name": zone.Name,
            "Control Type Schedule Name": "Zone Control Type Sched",  # Check
            "Control 1 Object Type": "ThermostatSetpoint:DualSetpoint",
            "Control 1 Name": "Dual Setpoint - Zone " + zone.Name,
        },
    )

    # ThermostatSetpoint:DualSetpoint
    tssd = idf.newidfobject("ThermostatSetpoint:DualSetpoint".upper())
    set_fields(
        tssd,
        data={
            "Name": "Dual Setpoint - Zone " + zone.Name,
            "Heating Setpoint Temperature Schedule Name": heating_schedule[
                "Name"
            ],
            "Cooling Setpoint Temperature Schedule Name": cooling_schedule[
                "Name"
            ],
        },
    )

    # ZoneHVAC:EquipmentConnections
    eq = idf.newidfobject("ZoneHVAC:EquipmentConnections".upper())
    set_fields(
        eq,
        data={
            "Zone Name": zone.Name,
            "Zone Conditioning Equipment List Name": zone.Name + " Equipment",
            "Zone Air Inlet Node or NodeList Name": zone.Name + " Inlets",
            "Zone Air Node Name": "Node " + zone.Name + " Zone",
            "Zone Return Air Node or NodeList Name": "Node "
            + zone.Name
            + " Out",
        },
    )

    # ZoneHVAC:EquipmentList
    el = idf.newidfobject("ZoneHVAC:EquipmentList".upper())
    set_fields(
        el,
        data={
            "Name": zone.Name + " Equipment",
            "Zone Equipment 1 Object Type": "ZoneHVAC:IdealLoadsAirSystem",
            "Zone Equipment 1 Name": zone.Name + " Ideal Loads Air",
            "Zone Equipment 1 Cooling Sequence": 1,
            "Zone Equipment 1 Heating or NoLoad Sequence": 1,
        },
    )

    # ZoneHVAC:IdealLoadsAirSystem
    il = idf.newidfobject("ZoneHVAC:IdealLoadsAirSystem".upper())
    set_fields(
        il,
        data={
            "Name": zone.Name + " Ideal Loads Air",
            "Zone Supply Air Node Name": "Node " + zone.Name + " In",
            "Maximum Heating Supply Air Temperature": 35,
            "Minimum Cooling Supply Air Temperature": 12,
            "Heating Availability Schedule Name": zone.Name
            + " Heating Availability Sch",
            "Cooling Availability Schedule Name": zone.Name
            + " Cooling Availability Sch",
            # "Design Specification Outdoor Air Object Name": zone.Name,
        },
    )
    if heat_is_on:  # Only if heating is active.
        set_fields(
            il,
            data={
                "Heating Limit": "LimitFlowRateAndCapacity",
                "Maximum Heating Air Flow Rate": "Autosize",
                "Maximum Sensible Heating Capacity": "Autosize",
            },
        )
    else:  # If heating is not active, set flow rate to 0.
        set_fields(
            il, data={"Heating Limit": "", "Maximum Heating Air Flow Rate": 0}
        )

    if cool_is_on:  # Only if cooling is active.
        set_fields(
            il,
            data={
                "Cooling Limit": "LimitFlowRateAndCapacity",
                "Maximum Cooling Air Flow Rate": "Autosize",
                "Maximum Total Cooling Capacity": "Autosize",
            },
        )
    else:  # If cooling is not active, set flow rate to 0.
        set_fields(
            il, data={"Cooling Limit": "", "Maximum Cooling Air Flow Rate": 0}
        )

    # NodeList
    nl = idf.newidfobject("NodeList".upper())
    set_fields(
        nl,
        data={
            "Name": zone.Name + " Inlets",
            "Node 1 Name": "Node " + zone.Name + " In",
        },
    )


def activate_cooling(
    idf,
    cooling_schedule,
    cool_sch_name="Cooling SP Sch",
    cool_avail_name="Cooling Availability Sch",
    filter_by="",
):
    """Activate cooling on the building. If the building does not have an HVAC
    system, the entire system is added.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param cooling_schedule: New schedule for the cooling system.
    :type cooling_schedule: dict
    :param cool_sch_name: Name of the cooling setpoint schedule compact which
        has to be replaced, defaults to "Cooling SP Sch"
    :type cool_sch_name: str, optional
    :param cool_avail_name: Name of the cooling availability schedule compact
        which has to be replaced, defaults to "Cooling Availability Sch"
    :type cool_avail_name: str, optional
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to "", ignored if ach is a `dict`
    :type filter_by: str, optional
    """
    # Check if the building already has an HVAC system, otherwise add it.
    if len(get_hvac_elements(idf)) == 0:
        add_hvac(idf, filter_by=filter_by)

    # Activate cooling in ZoneHVAC:IdealLoadsAirSystem.
    for ilas in idf.idfobjects["ZoneHVAC:IdealLoadsAirSystem".upper()]:
        if filter_by.lower() in ilas.Zone_Supply_Air_Node_Name.lower():
            set_fields(
                ilas,
                data={
                    "Cooling Limit": "LimitFlowRateAndCapacity",
                    "Maximum Cooling Air Flow Rate": "Autosize",
                    "Maximum Total Cooling Capacity": "Autosize",
                },
            )
    cool_avail = {
        k: 0 if v == 100 else 1 if isinstance(v, int) else v
        for k, v in cooling_schedule.items()
    }
    # Update cooling schedules.
    # FIXME: If the already existing cooling schedule has more fields than
    #        the new one, it causes problems. See possible fix below.
    del cooling_schedule["Name"]  # To prevent name overwriting.
    del cool_avail["Name"]  # To prevent name overwriting.
    for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
        if cool_sch_name.lower() in sc.Name.lower():
            set_fields(sc, data=cooling_schedule)
        if cool_avail_name.lower() in sc.Name.lower():
            set_fields(sc, data=cool_avail)
    # TODO: Test this fix:
    # for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
    #     if cool_sch_name.lower() in sc.Name.lower():
    #         cooling_schedule.update({"Name": sc.Name})
    #         idf.removeidfobject(sc)
    #         sc = idf.newidfobject("SCHEDULE:COMPACT")
    #         set_fields(sc, data=cooling_schedule)
    #     elif cool_avail_name.lower() in sc.Name.lower():
    #         cool_avail.update({"Name": sc.Name})
    #         idf.removeidfobject(sc)
    #         sc = idf.newidfobject("SCHEDULE:COMPACT")
    #         set_fields(sc, data=cool_avail)

def activate_humidistat(
    idf,
    hum_sched=None,
    dehum_sched=None,
    filter_by=""
):
    """Activate humidistat.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param hum_sched: Humidification schedule, defaults to None
    :type hum_sched: dict, optional
    :param dehum_sched: Dehumidification schedule, defaults to None
    :type dehum_sched: dict, optional
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name or a list of those, defaults to ""
    :type filter_by: str or list, optional
    """
    filter_by = to_list(filter_by)
    for z in idf.idfobjects["ZONE"]:
        if any([f.lower() in z.Name.lower() for f in filter_by]):
            _activate_zone_humidistat(idf, z, hum_sched, dehum_sched)


def _activate_zone_humidistat(
    idf,
    zone,
    hum_sched=None,
    dehum_sched=None
):
    """Activate humidistat in a specific zone.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param zone: Zone object (eppy)
    :type zone: class 'geomeppy.patches.EpBunch'
    :param hum_sched: Humidification schedule, defaults to None
    :type hum_sched: dict, optional
    :param dehum_sched: Dehumidification schedule, defaults to None
    :type dehum_sched: dict, optional
    """

    for zhec in idf.idfobjects["ZoneHVAC:EquipmentConnections".upper()]:
        if zhec.Zone_Name == zone.Name:
            break
    
    for zhel in idf.idfobjects["ZoneHVAC:EquipmentList".upper()]:
        if zhel.Name == zhec.Zone_Conditioning_Equipment_List_Name:
            break
    
    for ilas in idf.idfobjects["ZoneHVAC:IdealLoadsAirSystem".upper()]:
        if ilas.Name == zhel.Zone_Equipment_1_Name: # TODO: More checks needed.
            break

    # Create new Humidistat object.
    humidistat = idf.newidfobject("ZONECONTROL:HUMIDISTAT")
    set_fields(    
        humidistat,
        data={
            "Name": zone.Name + " Humidistat",
            "Zone Name": zone.Name
            },
    )

    # Update ZHILAS humidification controls.
    if hum_sched:
        ilas.Humidification_Control_Type = "Humidistat"
        if not check_schedule(idf, hum_sched["Name"]):
            hum_sc = idf.newidfobject("SCHEDULE:COMPACT")
            set_fields(
                hum_sc,
                data=hum_sched
            )

    if dehum_sched:
        ilas.Dehumidification_Control_Type = "Humidistat"
        if not check_schedule(idf, dehum_sched["Name"]):
            dehum_sc = idf.newidfobject("SCHEDULE:COMPACT")
            set_fields(
                dehum_sc,
                data=dehum_sched
            )

    if hum_sched and dehum_sched:
        humidistat.Humidifying_Relative_Humidity_Setpoint_Schedule_Name = hum_sched["Name"]
        humidistat.Dehumidifying_Relative_Humidity_Setpoint_Schedule_Name = dehum_sched["Name"]
    elif hum_sched and not dehum_sched:
        humidistat.Humidifying_Relative_Humidity_Setpoint_Schedule_Name = hum_sched["Name"]
    elif dehum_sched and not hum_sched:
        humidistat.Humidifying_Relative_Humidity_Setpoint_Schedule_Name = dehum_sched["Name"]


def deactivate_cooling(
    idf,
    cool_sch_name="Cooling SP Sch",
    cool_avail_name="Cooling Availability Sch",
    temp=None,
):
    # TODO: Aggiungere filter_by per agire su singole zone.
    """Deactivate cooling on already existing HVAC system.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param cool_sch_name: Name of the cooling setpoint schedule compact which
        has to be replaced, defaults to "Cooling SP Sch"
    :type cool_sch_name: str, optional
    :param cool_avail_name: Name of the cooling availability schedule compact
        which has to be replaced, defaults to "Cooling Availability Sch"
    :type cool_avail_name: str, optional
    :param temp: New value for maximum outdoor temperature. If None, no changes
        are applied to such parameter, defaults to None
    :type temp: int or float, optional
    """
    # Deactivate cooling in ZoneHVAC:IdealLoadsAirSystem.
    for ilas in idf.idfobjects["ZoneHVAC:IdealLoadsAirSystem".upper()]:
        set_fields(
            ilas,
            data={
                "Cooling Limit": "",
                "Maximum Cooling Air Flow Rate": 0,
                "Maximum Total Cooling Capacity": "",
            },
        )
        cooling_schedule = {
            "Schedule_Type_Limits_Name": "Any Number",
            "Field_1": "Through: 12/31",
            "Field_2": "For: AllDays",
            "Field_3": "Until: 24:00",
            "Field_4": 100,
        }
        # NOTE: Can be removed
        # empty_fields = {"Field_" + str(x): "" for x in range(5, 100)}
        # cooling_schedule.update(empty_fields)

        cool_avail = {
            k: 0 if v == 100 else 1 if isinstance(v, int) else v
            for k, v in cooling_schedule.items()
        }
        # Update cooling schedules.
        for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
            if cool_sch_name.lower() in sc.Name.lower():
                cooling_schedule.update({"Name": sc.Name})
                idf.removeidfobject(sc)
                sc = idf.newidfobject("SCHEDULE:COMPACT")
                set_fields(sc, data=cooling_schedule)
            elif cool_avail_name.lower() in sc.Name.lower():
                cool_avail.update({"Name": sc.Name})
                idf.removeidfobject(sc)
                sc = idf.newidfobject("SCHEDULE:COMPACT")
                set_fields(sc, data=cool_avail)

        if temp is not None:
            for dfr in idf.idfobjects["ZONEVENTILATION:DESIGNFLOWRATE"]:
                set_fields(dfr, {"Maximum Outdoor Temperature": temp})


def activate_heating(
    idf,
    heating_schedule,
    heat_sch_name="Heating Setpoint Schedule",
    heat_avail_name="Heating Availability Sch",
    filter_by="",
):
    """Activate heating on the building. If the building does not have an HVAC
    system, the entire system is added.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param heating_schedule: New schedule for the heating system.
    :type heating_schedule: dict
    :param heat_sch_name: Name of the heating setpoint schedule compact which
        has to be replaced, defaults to "Heating Setpoint Schedule"
    :type heat_sch_name: str, optional
    :param heat_avail_name: Name of the heating availability schedule compact
        which has to be replaced, defaults to "Heating Availability Sch"
    :type heat_avail_name: str, optional
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to "", ignored if ach is a `dict`
    :type filter_by: str, optional
    """
    # Check if the building already has an HVAC system, otherwise add it.
    if len(get_hvac_elements(idf)) == 0:
        add_hvac(idf, filter_by=filter_by)

    # Activate heating in ZoneHVAC:IdealLoadsAirSystem.
    for ilas in idf.idfobjects["ZoneHVAC:IdealLoadsAirSystem".upper()]:
        if filter_by.lower() in ilas.Zone_Supply_Air_Node_Name.lower():
            set_fields(
                ilas,
                data={
                    "Heating Limit": "LimitFlowRateAndCapacity",
                    "Maximum Heating Air Flow Rate": "Autosize",
                    "Maximum Sensible Heating Capacity": "Autosize",
                },
            )
    heat_avail = {
        k: 0 if v == -50 else 1 if isinstance(v, int) else v
        for k, v in heating_schedule.items()
    }

    del heating_schedule["Name"]  # To prevent name overwriting.
    del heat_avail["Name"]  # To prevent name overwriting.
    for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
        if heat_sch_name.lower() in sc.Name.lower():
            set_fields(sc, data=heating_schedule)
        if heat_avail_name.lower() in sc.Name.lower():
            set_fields(sc, data=heat_avail)


def deactivate_heating(
    idf,
    heat_sch_name="Heating Setpoint Schedule",
    heat_avail_name="Heating Availability Sch",
):
    # TODO: Aggiungere filter_by per agire su singole zone.
    """Deactivate heating on already existing HVAC system.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param cool_sch_name: Name of the heating setpoint schedule compact which
        has to be replaced, defaults to "Heating Setpoint Schedule"
    :type cool_sch_name: str, optional
    :param cool_avail_name: Name of the heating availability schedule compact
        which has to be replaced, defaults to "Heating Availability Sch"
    :type cool_avail_name: str, optional
    """
    # Deactivate heating in ZoneHVAC:IdealLoadsAirSystem.
    for ilas in idf.idfobjects["ZoneHVAC:IdealLoadsAirSystem".upper()]:
        set_fields(
            ilas,
            data={
                "Heating Limit": "",
                "Maximum Heating Air Flow Rate": 0,
                "Maximum Sensible Heating Capacity": "",
            },
        )
        heating_schedule = {
            "Schedule_Type_Limits_Name": "Any Number",
            "Field_1": "Through: 12/31",
            "Field_2": "For: AllDays",
            "Field_3": "Until: 24:00",
            "Field_4": -50,
        }

        heat_avail = {
            k: 0 if v == -50 else 1 if isinstance(v, int) else v
            for k, v in heating_schedule.items()
        }
        # Update heating schedules.
        for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
            if heat_sch_name.lower() in sc.Name.lower():
                heating_schedule.update({"Name": sc.Name})
                idf.removeidfobject(sc)
                sc = idf.newidfobject("SCHEDULE:COMPACT")
                set_fields(sc, data=heating_schedule)
            elif heat_avail_name.lower() in sc.Name.lower():
                heat_avail.update({"Name": sc.Name})
                idf.removeidfobject(sc)
                sc = idf.newidfobject("SCHEDULE:COMPACT")
                set_fields(sc, data=heat_avail)


def deactivate_humidistat(idf):
    """Disable humidification and dehumidification controls for the 
    ZoneHVAC:IdealLoadsAirSystem objects in the IDF.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    """
    for zhilas in idf.idfobjects["ZoneHVAC:IdealLoadsAirSystem".upper()]:
        zhilas.Dehumidification_Control_Type = "None"
        zhilas.Humidification_Control_Type = "None"


def change_window_opening(idf, value):
    """Change window opening factor in a building with controlled natural
    ventilation already active.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New percentage value, for example 0.3 or 30 will set the
        opening to 30 %
    :type value: int or float
    :raises ValueError: If specified value is not between 0 and 100.
    """
    if (value <= 0) or (value > 100):
        raise ValueError("Opening value must be between 0 and 100")
    if value > 1:
        value /= 100
    for do in idf.idfobjects[
        "AirflowNetwork:MultiZone:Component:DetailedOpening".upper()
    ]:
        data = {}
        num = do["Number of Sets of Opening Factor Data".replace(" ", "_")]
        for n in range(1, num + 1):
            entry = {
                "Height Factor for Opening Factor X".replace(
                    "X", str(n)
                ): value,
                "Start Height Factor for Opening Factor X".replace(
                    "X", str(n)
                ): round(1 - value, 2),
            }
            data.update(entry)
        set_fields(do, data=data)


def change_cp(idf, data):
    """Change Cp coefficients.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param data: Dictionary where keys are wind angles and values are Cp
        coefficients
    :type data: dict
    """
    # TODO: Implement a way to reset all angles and coefficients if necessary.
    angles = idf.idfobjects[
        "AirflowNetwork:MultiZone:WindPressureCoefficientArray".upper()
    ][0]
    for k, v in data.items():
        if float(k) not in angles.obj:
            angles.obj.append(float(k))

        for ang in angles.objls:
            if getattr(angles, ang) == float(k):
                number = ang[-1]
                for wpcv in idf.idfobjects[
                    "AirflowNetwork:MultiZone:WindPressureCoefficientValues".upper()
                ]:
                    set_fields(
                        wpcv,
                        data={"Wind Pressure Coefficient Value " + number: v},
                    )


def change_cd(idf, value):
    """Change window discharge coefficient in a building with controlled
    natural ventilation already active.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New percentage value, for example 0.3 or 30 will set the
        opening to 30 %
    :type value: int or float
    :raises ValueError: If specified value is not between 0 and 100.
    """
    if (value <= 0) or (value > 100):
        raise ValueError("Cd value must be between 0 and 100")
    if value > 1:
        value /= 100
    for do in idf.idfobjects[
        "AirflowNetwork:MultiZone:Component:DetailedOpening".upper()
    ]:
        data = {}
        num = do["Number of Sets of Opening Factor Data".replace(" ", "_")]
        for n in range(1, num + 1):
            entry = {
                "Discharge Coefficient for Opening Factor X".replace(
                    "X", str(n)
                ): value
            }
            data.update(entry)
        set_fields(do, data=data)


def change_runperiod(idf, start, end, fmt="%d-%m"):
    """Change simulation RunPeriod according to provided dates. If year is
    provided, the `RunPeriod:CustomRange` object will be used.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param start: Start date
    :type start: str
    :param end: End date
    :type end: str
    :param fmt: Format of the date according to datetime library, defaults to
        "%d-%m"
    :type fmt: str, optional

    .. warning:: If year is specified and it is the same for start and end date,
        a RunPeriod:CustomRange is still used but the eplusout.csv file will
        not have the year field in datetime column.
    """
    start = datetime.datetime.strptime(start, fmt)
    end = datetime.datetime.strptime(end, fmt)
    begin_m, begin_d = start.month, start.day
    end_m, end_d = end.month, end.day

    # Case 1: Simple runperiod existing, no year specified -> Update simple one.
    if "%y" not in fmt.lower():
        # print("IN len(idf.idfobjects['RUNPERIOD']) > 0")
        data = {
            "Begin Month": begin_m,
            "Begin Day of Month": begin_d,
            "End Month": end_m,
            "End Day of Month": end_d,
        }
        if len(idf.idfobjects["RUNPERIOD"]) == 0:
            rp = idf.newidfobject("RUNPERIOD")
        else:
            rp = idf.idfobjects["RUNPERIOD"][0]

        set_fields(rp, data=data)
        del idf.idfobjects["RUNPERIOD:CUSTOMRANGE"][:]
    
    # Case 2: Custom runperiod existing, year specified -> Update custom one.
    elif (len(idf.idfobjects["RUNPERIOD:CUSTOMRANGE"]) > 0) and ("%y" in fmt.lower()):
        begin_y = start.year
        end_y = end.year
        data = {
            "Begin Month": begin_m,
            "Begin Day of Month": begin_d,
            "Begin Year": begin_y,
            "End Month": end_m,
            "End Day of Month": end_d,
            "End Year": end_y,
        }
        set_fields(idf.idfobjects["RUNPERIOD:CUSTOMRANGE"][0], data=data)

    # Case 3: Simple runperiod existing (or basically no custom runperiod),
    #         year specified -> Add custom runperiod and delete simple one.
    elif ("%y" in fmt.lower()):
        begin_y = start.year
        end_y = end.year
        add_custom_runperiod(idf, datetime.datetime.strftime(start, fmt), datetime.datetime.strftime(end, fmt), fmt=fmt)

def add_custom_runperiod(idf, start, end, fmt="%d-%m-%Y"):
    """sobstitute Runperiod object with Runperiod:CostumRange object.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param start: Start date
    :type start: str
    :param end: End date
    :type end: str
    :param fmt: Format of the date, defaults to "%d-%m-%Y"
    :type fmt: str, optional
    """
    runperiod = idf.idfobjects["RUNPERIOD"][0]
    custom_runperiod = idf.newidfobject("RunPeriod:CustomRange".upper())
    start = datetime.datetime.strptime(start, fmt)
    end = datetime.datetime.strptime(end, fmt)
    begin_m, begin_d, begin_y = start.month, start.day, start.year
    end_m, end_d, end_y = end.month, end.day, end.year
    data = {
        "Begin Month": begin_m,
        "Begin Day of Month": begin_d,
        "Begin Year": begin_y,
        "End Month": end_m,
        "End Day of Month": end_d,
        "End Year": end_y,
        "Day of Week for Start Day": runperiod.Day_of_Week_for_Start_Day,
        "Use Weather File Holidays and Special Days": runperiod.Use_Weather_File_Holidays_and_Special_Days,
        "Use Weather File Daylight Saving Period": runperiod.Use_Weather_File_Daylight_Saving_Period,
        "Apply Weekend Holiday Rule": runperiod.Apply_Weekend_Holiday_Rule,
        "Use Weather File Rain Indicators": runperiod.Use_Weather_File_Rain_Indicators,
        "Use Weather File Snow Indicators": runperiod.Use_Weather_File_Snow_Indicators
    }
    set_fields(custom_runperiod, data=data)
    idf.removeidfobject(runperiod)


def set_first_day(idf, day):
    """Set the first day of year for the simulation.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param day: First day of the year. It can be a string like Tuesday, Friday
        or a number between 1 and 7.
    :type day: str or int
    :raises ValueError: If specified value is not between 1 and 7 or if it does
        not match any of the days of week.
    """
    if len(idf.idfobjects["RUNPERIOD"]) > 0:
        obj = idf.idfobjects["RUNPERIOD"][0]
    elif len(idf.idfobjects["RUNPERIOD:CUSTOMRANGE"]) > 0:
        obj = idf.idfobjects["RUNPERIOD:CUSTOMRANGE"][0]
    else:
        raise Exception("RunPeriod not found in the IDF.")

    table = {
        "1": "Monday",
        "2": "Tuesday",
        "3": "Wednesday",
        "4": "Thursday",
        "5": "Friday",
        "6": "Saturday",
        "7": "Sunday",
    }
    if isinstance(day, int):
        if (day >= 1) and (day <= 7):
            set_fields(obj, Day_of_Week_for_Start_Day=table[str(day)])
        else:
            raise ValueError("Error in the day format.")
    elif isinstance(day, str):
        if day in table.keys():
            set_fields(obj, Day_of_Week_for_Start_Day=table[day])
        elif day in table.values():
            set_fields(obj, Day_of_Week_for_Start_Day=day)
        else:
            raise ValueError("Error in the day format.")
    else:
        raise ValueError("Error in the day format.")


def set_daylight_saving(
    idf,
    status="Yes",
    start="Last Sunday in March",
    end="Last Sunday in October",
):
    """Activate daylight saving time in the IDF.
    Start date and end date can also be provided.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param status: 'Yes' or 'No', defaults to 'Yes'
    :type status: str, optional
    :param start: Start date, defaults to "Last Sunday in March"
    :type start: str, optional
    :param end: End date, defaults to "Last Sunday in October"
    :type end: str, optional
    """
    set_fields(
        idf.idfobjects["RunPeriodControl:DaylightSavingTime".upper()][0],
        data={"Start Date": start, "End Date": end},
    )
    set_fields(
        idf.idfobjects["RUNPERIOD"][0],
        data={"Use Weather File Daylight Saving Period": status},
    )


def remove_daylightsaving(
    idf
):
    """Remove/delete daylight saving for both internal schedules and EPW.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    """

    # Remove daylight saving for schedules.
    del idf.idfobjects["RunPeriodControl:DaylightSavingTime".upper()][:]

    # Remove daylight saving for weather file.
    for rp in ["RUNPERIOD", "RUNPERIOD:CUSTOMRANGE"]:
        try:
            set_fields(
                idf.idfobjects[rp][0],
                data={"Use Weather File Daylight Saving Period": "No"},
            )
        except Exception:
            pass


def set_outputs(
    idf,
    variable_list,
    key_value="*",
    frequency="Timestep",
    schedule_name="On",
    reset=False,
):
    """Edit EnergyPlus outputs. "Output:EnvironmentalImpactFactors" is always
    added if not present.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param variable_list: List of output variables to add
    :type variable_list: list
    :param key_value: Key value for added variables, defaults to "*"
    :type key_value: str or list, optional
    :param frequency: Reporting frequency for added variables, defaults to
        "Timestep"
    :type frequency: str, optional
    :param schedule_name: It can be used to limit the number of lines that
        appear in the output file (see EnergyPlus I/O documentation), defaults
        to "On".
    :type schedule_name: str, optional
    :param reset: If set to `True`, all existing output variables are deleted,
        defaults to False
    :type reset: bool, optional

    :Example:

    .. code-block:: python

        set_outputs(
            idf,
            variable_list=[
                "Zone Mean Air Temperature",
                "Site Outdoor Air Drybulb Temperature",
                "Zone Operative Temperature"
            ],
            key_value="Block1",
            reset=True
        )
        set_outputs(
            idf,
            variable_list=["Schedule Value"],
            key_value=["Block1:Zone1 Heating Setpoint Schedule"],
            frequency="timestep",
            reset=False
        )
    """
    key_value = to_list(key_value)
    variable_list = to_list(variable_list)

    # Reset all variable outputs.
    if reset:
        del idf.idfobjects["OUTPUT:VARIABLE"][:]
        del idf.idfobjects["OUTPUT:METER"][:]
        del idf.idfobjects["Output:EnvironmentalImpactFactors".upper()][:]

    # Add output variables:
    for var in variable_list:
        for kv in key_value:
            if var is not None:
                found = 0
                for check_ov in idf.idfobjects["OUTPUT:VARIABLE"]:
                    if check_ov.Key_Value == kv and check_ov.Variable_Name == var and check_ov.Reporting_Frequency == frequency:
                        print("[i] Output:Variable {}, {}, {} already exists. Skipping...".format(var, kv, frequency))
                        found = 1
                if not found:
                    ov = idf.newidfobject("OUTPUT:VARIABLE")
                    set_fields(
                        ov,
                        data={
                            "Key Value": kv,
                            "Variable Name": var,
                            "Reporting Frequency": frequency,
                        },
                    )
                    if schedule_name is not None:
                        set_fields(ov, Schedule_Name=schedule_name)
                    if schedule_name.lower() == "on" and not check_schedule(idf, schedule_name):
                        add_on_schedule(idf)

    # Add frequency for DistrictHeating and DistrictCooling.
    found = 0
    for eif in idf.idfobjects["Output:EnvironmentalImpactFactors".upper()]:
        if eif.Reporting_Frequency == frequency:
            found = 1
    if not found:
        new_eif = idf.newidfobject("Output:EnvironmentalImpactFactors".upper())
        set_fields(new_eif, Reporting_Frequency=frequency)


def set_ems_constants(idf, x, y, z):
    sr = idf.newidfobject("EnergyManagementSystem:Subroutine".upper())
    set_fields(
        sr,
        data={
            "Name": "Set_Globals",
            "Program Line 1": "SET X = %s" % x,
            "Program Line 2": "SET Y = %s" % y,
            "Program Line 3": "SET Z = %s" % z,
        },
    )


def _get_number_of_timesteps(n):
    n = 100 - n
    closest = min(TABLE.keys(), key=lambda x:abs(x-n))
    return TABLE[closest]


def edit_csv_schedule(
    idf,
    csv,
    hours,
    columns,
    value,
    sep=";",
    index_col=0,
    save=True,
    fix_paths=True,
    timestepped=False
):
    """Edit a CSV representing a Schedule:File object of EnergyPlus.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param csv: CSV file
    :type csv: str or class:`pandas.core.frame.DataFrame`
    :param hours: Hours of CSV file to be changed
    :type hours: int or list
    :param columns: Columns of the CSV file to be changed
    :type columns: list
    :param value: Value to be assigned
    :type value: float
    :param sep: Separator of the CSV file, defaults to ";"
    :type sep: str, optional
    :param index_col: Index column number, defaults to 0
    :type index_col: int, optional
    :param save: Save modified CSV to file, defaults to True
    :type save: bool, optional
    :param fix_paths: Convert CSV file paths in IDF to point at the new file,
        defaults to True
    :type fix_paths: bool, optional
    :raises Exception: [description]
    :raises NotImplementedError: [description]
    :raises Exception: [description]
    :return: DataFrame of the CSV file
    :rtype: class:`predyce.IDF_class.IDF`
    """
    if isinstance(csv, str):
        df = pd.read_csv(csv, sep=sep, index_col=index_col)
    elif isinstance(csv, pd.core.frame.DataFrame):
        df = csv

    if len(df) == 8760 and not timestepped:
        df.set_index(
            pd.date_range(
                start="01-JAN-2021 00:00", end="31-DEC-2021 23:50", freq="H"
            ),
            drop=True,
            inplace=True,
        )
    elif len(df) == 8784 and not timestepped:
        df.set_index(
            pd.date_range(
                start="01-JAN-2020 00:00", end="31-DEC-2020 23:50", freq="H"
            ),
            drop=True,
            inplace=True,
        )
    elif len(df) == 8760*6 and timestepped:
        pass

    else:
        print("file:", csv)
        raise Exception(
            "CSV file for Schedule:File must contain 8760 or 8784 rows"
        )

    if not timestepped:
        if isinstance(hours, np.float64):
            hours = int(hours)

        if isinstance(hours, (int, list)):
            df.loc[df.index[hours], columns] = value
        elif isinstance(hours, str):
            raise NotImplementedError("Parsing datetime is not implemented yet.")
        else:
            raise Exception("Error in editing CSV file: incompatible hour type.")

    elif timestepped:
        if isinstance(hours, np.float64):
            hours = int(hours)
        if isinstance(hours, int):
            hours = [hours]
        
        num_timesteps = _get_number_of_timesteps(int(value))
        minutes = HOUR_SET[num_timesteps]
        for h in hours:
            # Set blind to OFF for all timesteps.
            for m in [0, 10, 20, 30, 40, 50]:
                ix = df.iloc[h*6].name # FIXME
                ix = datetime.datetime.fromisoformat(ix)
                df.loc[str(ix + datetime.timedelta(minutes=m)), columns] = 0
            # Set blind to ON for some timesteps.
            for m in minutes:
                ix = df.iloc[h*6].name # FIXME
                ix = datetime.datetime.fromisoformat(ix)
                df.loc[str(ix + datetime.timedelta(minutes=m)), columns] = value

    if isinstance(csv, str) and save == True:
        df.to_csv(Path(csv).name, sep=";")

    if fix_paths == True:
        for sf in idf.idfobjects["SCHEDULE:FILE"]:
            if Path(csv).name in sf.File_Name:
                sf.File_Name = os.path.abspath(Path(csv).name)

    return df


def add_on_schedule(idf):
    """Add "ON" schedule which is usually used by e+ to produce output
    schedules in output.csv file.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    """
    sc = idf.newidfobject("SCHEDULE:COMPACT")
    on = {
        "Name": "On",
        "Schedule_Type_Limits_Name": "Any Number",
        "Field_1": "Through: 31 Dec",
        "Field_2": "For: AllDays",
        "Field_3": "Until: 24:00",
        "Field_4": 1
    }
    set_fields(sc, data=on)


def change_setpoint_thermostat(
    idf,
    value=None,
    schedule=None,
    availability_schedule=None
):
    """Change setpoint and/or availability schedule for radiators.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: Setpoint value, defaults to None
    :type value: int or float, optional
    :param schedule: Schedule for setpoint, defaults to None
    :type schedule: dict or str, optional
    :param availability_schedule: Schedule for availability, defaults to None
    :type availability_schedule: str or dict, optional
    :raises ValueError: If given schedule (dict) is already present in the IDF.
    """
    if isinstance(schedule, dict):
        schedule_name = schedule["Name"]
        if check_schedule(idf, schedule["Name"]):
            raise ValueError(f"Schedule named {schedule_name} already present in the IDF")
        sc = idf.newidfobject("SCHEDULE:COMPACT")
        set_fields(sc, schedule)
    elif isinstance(schedule, str):
        schedule_name = schedule
    else:
        schedule_name = None

    for zct in idf.idfobjects["ZONECONTROL:THERMOSTAT"]:
        cot = ""
        i = 0
        while cot != "" or i == 0:
            i += 1
            cot = zct[f"Control_{i}_Object_Type"]
            if cot == "":
                continue
            if "heating".lower() in cot.lower():
                which = "heating"
            elif "cooling".lower() in cot.lower():
                which = "cooling"

            cn = zct[f"Control_{i}_Name"]
            for ts in idf.idfobjects[cot.upper()]:
                if ts.Name.lower() == cn.lower():
                    if schedule_name:
                        ts.Setpoint_Temperature_Schedule_Name = schedule_name
                    if value:
                        sp_sch = ts.Setpoint_Temperature_Schedule_Name
                        for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
                            if sc.Name.lower() == sp_sch.lower():
                                _change_setpoint(sc, value, which)
    
    if availability_schedule is not None:
        change_availability_thermostat(idf, availability_schedule)


def change_availability_thermostat(idf, schedule):
    """Change availability schedule for radiators.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param schedule: Schedule to be assigned.
    :type schedule: dict or str
    :raises ValueError: If given schedule (dict) is already present in the IDF.
    """
    if isinstance(schedule, dict):
        schedule_name = schedule["Name"]
        if check_schedule(idf, schedule["Name"]):
            raise ValueError(f"Schedule named {schedule_name} already present in the IDF")
        sc = idf.newidfobject("SCHEDULE:COMPACT")
        set_fields(sc, schedule)
    elif isinstance(schedule, str):
        schedule_name = schedule
    else:
        schedule_name = None

    # Assign schedule name to radiators object.
    for zhbrcw in idf.idfobjects["ZoneHVAC:Baseboard:RadiantConvective:Water".upper()]:
        zhbrcw.Availability_Schedule_Name = schedule_name


def assign_radiators_schedules(idf, set):#)setpoint, setpoint_sch, availability_sch):
    change_setpoint_thermostat(
        idf,
        value=set["setpoint"],
        schedule=set["setpoint_sch"],
        availability_schedule=set["availability_sch"]
        )
