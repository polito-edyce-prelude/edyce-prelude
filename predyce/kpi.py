#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Functions for KPI computations."""
import pandas as pd
import datetime as dt
import numpy as np
from predyce import epw_reader
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.dates as mdates
import seaborn as sns
import statsmodels.api as sm
import copy
from pathlib import Path
import os
import warnings
import math
from psychrochart import PsychroChart, load_config
from predyce import mydatetime
from predyce.mydatetime import MyDateTime as mdt
from shapely.geometry import Point, Polygon
from sklearn.preprocessing import PolynomialFeatures


# TODO adjust nomenclature and udm (particularly for consumption)
class KPI:
    """Class which computes KPIs."""

    def __init__(self, plot_dir, graph=False):
        """
        :param plot_dir: Directory where to save plots
        :type plot_dir: str or Path
        :param graph: Whether to save plots, defaults to `False`
        :type graph: bool, optional
        """
        self.plot_dir = Path(plot_dir)
        self.graph = graph
        if self.graph:
            try:
                os.makedirs(self.plot_dir)
            except FileExistsError:
                pass

    def fict_cool(self, dataframe_off, dataframe_on,
                           eu_norm='16798-1:2019', alpha=0.8):
        """Return fictitious cooling computed as the amount of cooling
        consumption needed in a building without mechanical system in order
        to reach category I comfort of adaptive comfort model.

        :param dataframe_off: dataframe containing at least
            "Date/Time", "T_op_i[C]", "T_db_o[C]"
            columns in order to compute adaptive comfort model categories.
        :type dataframe_off: class:`pandas.core.frame.DataFrame`
        :param dataframe_on: dataframe containing at least "Date/Time" and
            "Q_c[kWh/m2]" columns, of the same building with
            HVAC installad and ACH ventilation set to 0.
        :type dataframe_on: class:`pandas.core.frame.DataFrame`
        :param eu_norm: It can be set to '15251:2007' if old UE norm
            computation is desired, defaults to '16798-1:2019'.
        :type eu_norm: str, optional
        :param alpha: With old UE norm '15251:2007 alpha is a free parameter in
            range [0,1), defaults to 0.8
        :type alpha: float, optional
        :return: fictitious cooling total [kWh/m2]
        :rtype: float
        """
        
        df_off = copy.deepcopy(dataframe_off)
        df_on = copy.deepcopy(dataframe_on)

        # Aggregate hourly
        df_on = df_on.set_index("Date/Time")
        df_on = df_on.resample("1H").agg({"Q_c[kWh/m2]": "sum"})

        df_off = df_off.set_index("Date/Time")
        df_off = df_off.resample("1H").mean()

        df_off["Q_c[kWh/m2]"] = df_on["Q_c[kWh/m2]"]
        df_off = df_off.reset_index()
        col_index = df_off.columns.get_loc("T_db_o[C]")  # FIXME: not used.

        # if needed to filter on occupancy --> add parameters
        df_off = self.compute_acm(df_off, eu_norm, alpha)

        # Compute distances from comfort lines
        df_middle = df_off.where((df_off["T_rm"] > 10) & (df_off["T_rm"] < 33)).dropna()
        df_up =  df_off.where(df_off["T_rm"] > 33).dropna()

        df_middle["dist"] = df_middle["T_op_i[C]"] -\
                            (0.33*df_middle["T_rm"]+18.8)
        df_up["dist"] = df_up["T_op_i[C]"] - (0.33*33+18.8)

        df =  df_middle.append(df_up)

        # Compute fictitious cooling
        conditions = [df['dist'] < 2,
                    (df["dist"] >= 2) & (df['dist']<= 3),
                    df["dist"] > 3]
        choices = [0,df["Q_c[kWh/m2]"]*(df["dist"]-2),df["Q_c[kWh/m2]"]]

        df['fictitious_cooling'] = np.select(conditions, choices, default=0)

        # Insert here desired graph
        if self.graph:
            # Fictitious cooling cdf
            plt.rcParams.update({"font.size": 45})
            fig, ax = plt.subplots(constrained_layout=True, figsize=(50, 25))
            x = np.sort(df["fictitious_cooling"])
            y = np.arange(len(df["fictitious_cooling"]))/float(len(df["fictitious_cooling"]))
            ax.plot(x, y, linewidth = 4)
            ax.set_xlabel("kWh/m2")
            plt.title("CDF fictitious cooling")
            plt.grid()
            plt.savefig(self.plot_dir / "cdf_fict_cool.png")
            plt.close(fig)

        return (df["fictitious_cooling"].sum())

    def fict_heat(self, dataframe_off, dataframe_on,
                           eu_norm='16798-1:2019', alpha=0.8):
        """Return fictitious heating computed as the amount of heating
        consumption needed in a building without mechanical system in order
        to reach category I comfort of adaptive comfort model.

        :param dataframe_off: dataframe containing at least
            "Date/Time", "T_op_i[C]", "T_db_o[C]"
            columns in order to compute adaptive comfort model categories.
        :type dataframe_off: class:`pandas.core.frame.DataFrame`
        :param dataframe_on: dataframe containing at least "Date/Time" and
            "Q_h[kWh/m2]" columns, of the same building with
            HVAC installad and ACH ventilation set to 0.
        :type dataframe_on: class:`pandas.core.frame.DataFrame`
        :param eu_norm: It can be set to '15251:2007' if old UE norm
            computation is desired, defaults to '16798-1:2019'.
        :type eu_norm: str, optional
        :param alpha: With old UE norm '15251:2007 alpha is a free parameter in
            range [0,1), defaults to 0.8
        :type alpha: float, optional
        :return: fictitious heating total [kWh/m2]
        :rtype: float
        """
        
        df_off = copy.deepcopy(dataframe_off)
        df_on = copy.deepcopy(dataframe_on)

        # Aggregate hourly
        df_on = df_on.set_index("Date/Time")
        df_on = df_on.resample("1H").agg({"Q_h[kWh/m2]": "sum"})

        df_off = df_off.set_index("Date/Time")
        df_off = df_off.resample("1H").mean()

        df_off["Q_h[kWh/m2]"] = df_on["Q_h[kWh/m2]"]
        df_off = df_off.reset_index()
        col_index = df_off.columns.get_loc("T_db_o[C]")  # FIXME: not used

        # if needed to filter on occupancy --> add parameters
        df_off = self.compute_acm(df_off, eu_norm, alpha)

        # Compute distances from comfort lines
        df_middle = df_off.where((df_off["T_rm"] > 10) & (df_off["T_rm"] < 33)).dropna()
        df_down =  df_off.where(df_off["T_rm"] < 10).dropna()

        df_middle["dist"] = df_middle["T_op_i[C]"] -\
                            (0.33*df_middle["T_rm"]+18.8)
        df_down["dist"] = df_down["T_op_i[C]"] - (0.33*10+18.8)

        df =  df_middle.append(df_down)
        #TODO funziona fare in questo modo per non cambiare le formule sotto?
        df["dist"] = -df["dist"]

        # Compute fictitious heating
        conditions = [df['dist'] < 2,
                    (df["dist"] >= 2) & (df['dist']<= 3),
                    df["dist"] > 3]
        choices = [0,df["Q_h[kWh/m2]"]*(df["dist"]-2),df["Q_h[kWh/m2]"]]

        df['fictitious_heating'] = np.select(conditions, choices, default=0)

        # Insert here desired graph
        if self.graph:
            # Fictitious cooling cdf
            plt.rcParams.update({"font.size": 45})
            fig, ax = plt.subplots(constrained_layout=True, figsize=(50, 25))
            x = np.sort(df["fictitious_heating"])
            y = np.arange(len(df["fictitious_heating"]))/float(len(df["fictitious_heating"]))
            ax.plot(x, y, linewidth = 4)
            ax.set_xlabel("kWh/m2")
            plt.title("CDF fictitious heating")
            plt.grid()
            plt.savefig(self.plot_dir / "cdf_fict_heat.png")
            plt.close(fig)

        return (df["fictitious_heating"].sum())
        
    def adaptive_residuals(self, dataframe_off, eu_norm='16798-1:2019', alpha=0.8,
                            filter_by_occupancy=0,when={}, hf=False):
        """Return average distance from Adaptive Comfort Model upper categories
        thresholds.

        :param dataframe_off: Dataframe containing at least 'Date/Time',
                'T_db_o[C]' and 'T_op_i[C]' columns.
        :type dataframe_off: class:`pandas.core.frame.DataFrame`
        :param eu_norm: EU normative to compute Adaptive Comfort Model
            thresholds. It can be '16798-1:2019' or '15251:2007', defaults to
            '16798-1:2019'
        :type eu_norm: str, optional
        :param alpha: EU '15251:2007' free parameter ranging [0,1), defaults to
            0.8
        :type alpha: float, optional
        :param filter_by_occupancy: It can be set 0 or 1, depending on wether
            activate occupancy filtering on thermal comfort KPIs computation or
            not, defaults to 0.
        :type filter_by_occupancy: int, optional
        :param when: dictionary with 'start' and 'end' keys and values in format
            'year/month/day hour:minutes:seconds'
        :type when: dict, optional
        :return: Dictionary where '>3': average distance from cat 1 upper
            bound; '>0': average distance from central line of cat 1
        :rtype: dict
        """      
        df_off = self.compute_acm(dataframe_off, eu_norm, alpha, filter_by_occupancy, when)

        df_middle = df_off.where((df_off["T_rm"] >= 10) & (df_off["T_rm"] <= 33)).dropna()
        df_up =  df_off.where(df_off["T_rm"] > 33).dropna()

        # Compute distances from comfort line and comfort+3 line
        df_middle["dist"] = df_middle["T_op_i[C]"]-\
                            (0.33*df_middle["T_rm"]+18.8)
        df_up["dist"] = df_up["T_op_i[C]"] - (0.33*33+18.8)
        df_middle["dist_2"] = df_middle["T_op_i[C]"] -\
                             (0.33*df_middle["T_rm"]+18.8+3)
        df_up["dist_2"] = df_up["T_op_i[C]"] - (0.33*33+18.8+3)

        df = pd.concat([df_middle, df_up])

        # to get all results
        # result_0 = list(df["dist"])
        # result_2 = list(df["dist_2"])

        # dfp = df.copy()
        # dfp.set_index("Date/Time", inplace=True, drop=True)
        # dfp = dfp[["T_op_i[C]", "dist"]]
        # dfp["day"] = dfp.index.day
        # dfp["hour"] = dfp.index.hour

        # # Plot 1
        # plt.rcParams.update({"font.size": 45})
        # _, ax = plt.subplots(constrained_layout=True, figsize=(50, 25))
        # ptab = dfp.pivot_table(index="hour", columns="day", values="dist")
        # sns.heatmap(ptab, cmap="gist_rainbow_r", cbar_kws={"label": "Distance"}, ax=ax)
        # ax.invert_yaxis()
        # plt.savefig(self.plot_dir / "test_dist.png")
        # plt.close()
        
        # return average hourly distance from lines
        # result_0 = round(df["dist"].where(df["dist"]>0).sum(), 2)
        # result_2 = round(df["dist_2"].where(df["dist_2"]>0).sum(), 2)
        res = {
            # ">0": round(df["dist"].where(df["dist"]>0).sum(), 2)/len(df),
            # ">3": round(df["dist_2"].where(df["dist_2"]>0).sum(), 2)/len(df),
            # "<0": round(df["dist"].where(df["dist"]<=0).sum(), 2)/len(df),
            "mean": round(abs(df["dist"]).sum(), 2)/len(df),
            # "cum": round(abs(df["dist"]).sum(), 2),
            # "mean_>0": round(df["dist"].where(df["dist"]>0).sum(), 2)/len(df),
            "cum_>0": round(df["dist"].where(df["dist"]>0).sum(), 2),
            "cum_<0": round(df["dist"].where(df["dist"]<=0).sum(), 2),
            # "mean_<0": round(df["dist"].where(df["dist"]<=0).sum(), 2)/len(df),
            }

        if hf:
            return res["mean"]  # For 24hf
        else:
            return res


    def pmv_ppd(self, df, vel=0.1, met=1.2, clo=0.5, wme=0, filter_by_occupancy=0, standard="ISO 7730-2006", season=None):
        """Return Predicted Mean Vote (PMV) and Predicted Percentage of
        Dissatisfied (PPD) calculated in accordance to ISO 7730-2006 standard.

        :param df: dataframe containing at least "Date/Time",
            "T_db_i[C]", "T_rad_i[C]" and "RH_i[%]" columns.
            Optional "Occupancy column" accepting only 0 and 1 values.
        :type df: class:`pandas.core.frame.DataFrame`
        :param vel: relative air speed, defaults 0.1
        :type vel: float, optional
        :param met: metabolic rate, [met] defaults 1.2
        :type met: float, optional
        :param clo: clothing insulation, [clo] defaults 0.5
        :type clo: float, optional
        :param wme: external work, [met] defaults 0
        :type wme: float, optional
        :param standard: Currently unused, defaults to "ISO 7730-2006"
        :type standard: str, optional
        :param filter_by_occupancy: It can be set 0 or 1, depending on wether
            activate occupancy filtering on thermal comfort KPIs computation or
            not, defaults to 0.
        :type filter_by_occupancy: int, optional
        :return: dictionary containing keys "POR" (Percentage Outside the Range) and
            ""No_h_discomfort", computed as percentage and number of hours in which
            PMV is above or below 0.7 and so PPD is above around 20%.
        :rtype: dict
        """
        df = self.compute_pmv_ppd(df, vel, met, clo, wme, season)
        
        # add season filter
        if filter_by_occupancy:
            df = df.where(df["Occupancy"] != 0).dropna()

        if season is not None:
            df = df.where(df[season+"_season"] != 0).dropna()
        
        if self.graph:
            pass
            # # Graph referred to chosen example month
            # #May
            # df["Date/Time"] = df.index
            # # assert False
            # df_month = df[pd.DatetimeIndex(df["Date/Time"]).month==6]

            # # Indoor operative temperature versus time
            # fig = plt.figure(constrained_layout=True)
            # plt.grid()
            # x_labels = [str(x.day) + "/" + str(x.month) for x in list(df_month["Date/Time"]) if x.hour == 1]
            # x_ticks = [x for x in list(df_month["Date/Time"]) if x.isocalendar()[2] == 1 and x.hour == 1]
            # x_labels = [str(x.day) + "/" + str(x.month) for x in list(df_month["Date/Time"]) if x.isocalendar()[2] == 1 and x.hour == 1]
            # plt.plot(df_month["Date/Time"], df_month["T_db_i[C]"],ls='-', lw=0.5,
            #          marker='.', color='indigo', alpha=0.8)
            # plt.xticks(ticks=x_ticks, labels = x_labels)
            # plt.xlabel(r'$time$')
            # plt.ylabel(r'$\theta_{\mathrm{op}}\,\left[°C\right]$')
            # plt.title("Indoor drybulb temperature over time")
            # plt.savefig(self.plot_dir / "KPI_db_temperature.png",
            #             transparent=False)
            # plt.close(fig)
       
        # POR
        por = (df["ppd"].where(abs(df["pmv"])>0.7).count())/len(df)
        # Number of hours of discomfort.
        n_h = (df["ppd"].where(abs(df["pmv"])>0.7).count())
        my_dict = {"POR": por, "No_h_discomfort": n_h}
        return my_dict

    def adaptive_comfort_model(self, df, eu_norm='16798-1:2019', alpha=0.8,
                                filter_by_occupancy=0, when={}, color="indigo"):
        """Compute adaptive comfort model in a standardized format.

        :param df: dataframe should contain "Date/Time" column in format
            'year/month/day hour:minutes:seconds', "T_db_o[C]" preferably with a
            subhourly timestep and "T_op_i[C]". Optional "Occupancy" column
            accepting only 0/1 values.
        :type df: class:`pandas.core.frame.DataFrame`
        :param eu_norm: It can be set to '15251:2007' if old UE norm
            computation is desired, defaults to '16798-1:2019'.
        :type eu_norm: str, optional
        :param alpha: With old UE norm '15251:2007 alpha is a free parameter in
            range [0,1), defaults to 0.8
        :type alpha: float, optional
        :param filter_by_occupancy: It can be set 0 or 1, depending on wether
            activate occupancy filtering on thermal comfort KPIs computation or
            not, defaults to 0.
        :type filter_by_occupancy: int, optional
        :param when: dictionary with 'start' and 'end' keys and values in format 'year/month/day hour:minutes:seconds'
        :type when: dict, optional
        :return: Number of hours in each of the 7 comfort
            categories and POR computed as % of hours outside cat 2 boundaries.
        :rtype: dict
        """

        df = self.compute_acm(df, eu_norm, alpha, filter_by_occupancy, when)

        df_down = df.where(df["T_rm"] < 10).dropna()
        df_middle = df.where((df["T_rm"] >= 10) & (df["T_rm"] <= 33)).dropna()
        df_up =  df.where(df["T_rm"] > 33).dropna()

        category_1 = ((df_middle["T_op_i[C]"] >=(0.33*df_middle["T_rm"]+18.8-3)) &
                    (df_middle["T_op_i[C]"] <= (0.33*df_middle["T_rm"]+18.8+2))).sum() +\
                    ((df_down["T_op_i[C]"] >=(0.33*10+18.8-3)) &
                    (df_down["T_op_i[C]"] <= (0.33*10+18.8+2))).sum() +\
                    ((df_up["T_op_i[C]"] >=(0.33*33+18.8-3)) &
                    (df_up["T_op_i[C]"] <= (0.33*33+18.8+2))).sum()

        category_2_up = ((df_middle["T_op_i[C]"] >(0.33*df_middle["T_rm"]+18.8+2)) &
                    (df_middle["T_op_i[C]"] <= (0.33*df_middle["T_rm"]+18.8+3))).sum() +\
                    ((df_down["T_op_i[C]"] >(0.33*10+18.8+2)) &
                    (df_down["T_op_i[C]"] <= (0.33*10+18.8+3))).sum() +\
                    ((df_up["T_op_i[C]"] >(0.33*33+18.8+2)) &
                    (df_up["T_op_i[C]"] <= (0.33*33+18.8+3))).sum()

        category_3_up = ((df_middle["T_op_i[C]"] >(0.33*df_middle["T_rm"]+18.8+3)) &
                    (df_middle["T_op_i[C]"] <= (0.33*df_middle["T_rm"]+18.8+4))).sum() +\
                    ((df_down["T_op_i[C]"] >(0.33*10+18.8+3)) &
                    (df_down["T_op_i[C]"] <= (0.33*10+18.8+4))).sum() +\
                    ((df_up["T_op_i[C]"] >(0.33*33+18.8+3)) &
                    (df_up["T_op_i[C]"] <= (0.33*33+18.8+4))).sum()

        category_over_3 = (df_middle["T_op_i[C]"] >(0.33*df_middle["T_rm"]+18.8+4)).sum() +\
                        (df_down["T_op_i[C]"] > (0.33*10+18.8+4)).sum() +\
                        (df_up["T_op_i[C]"] > (0.33*33+18.8+4)).sum()

        category_2_down = ((df_middle["T_op_i[C]"] >=(0.33*df_middle["T_rm"]+18.8-4)) &
                          (df_middle["T_op_i[C]"] < (0.33*df_middle["T_rm"]+18.8-3))).sum() +\
                          ((df_down["T_op_i[C]"] >=(0.33*10+18.8-4)) &
                          (df_down["T_op_i[C]"] < (0.33*10+18.8-3))).sum() +\
                          ((df_up["T_op_i[C]"] >=(0.33*33+18.8-4)) &
                          (df_up["T_op_i[C]"] < (0.33*33+18.8-3))).sum()

        category_3_down = ((df_middle["T_op_i[C]"] >=(0.33*df_middle["T_rm"]+18.8-5)) &
                    (df_middle["T_op_i[C]"] < (0.33*df_middle["T_rm"]+18.8-4))).sum() +\
                    ((df_down["T_op_i[C]"] >=(0.33*10+18.8-5)) &
                    (df_down["T_op_i[C]"] < (0.33*10+18.8-4))).sum() +\
                    ((df_up["T_op_i[C]"] >=(0.33*33+18.8-5)) &
                    (df_up["T_op_i[C]"] < (0.33*33+18.8-4))).sum()

        category_under_3 = (df_middle["T_op_i[C]"] <(0.33*df_middle["T_rm"]+18.8-5)).sum()+\
                    (df_down["T_op_i[C]"] <(0.33*10+18.8-5)).sum()+\
                    (df_up["T_op_i[C]"] < (0.33*33+18.8-5)).sum()

        if self.graph:
            plt.rcParams.update({"font.size": 45})
            # Graph referred to chosen example month
            #May
            # df_month = df[pd.DatetimeIndex(df["Date/Time"]).month==6]

            # Indoor operative temperature versus time
            # fig = plt.figure(constrained_layout=True)
            # plt.grid()
            # x_labels = [str(x.day) + "/" + str(x.month) for x in list(df_month["Date/Time"]) if x.hour == 1]
            # x_ticks = [x for x in list(df_month["Date/Time"]) if x.isocalendar()[2] == 1 and x.hour == 1]
            # x_labels = [str(x.day) + "/" + str(x.month) for x in list(df_month["Date/Time"]) if x.isocalendar()[2] == 1 and x.hour == 1]
            # plt.plot(df_month["Date/Time"], df_month["T_op_i[C]"],ls='-', lw=0.5,
            #          marker='.', color='indigo', alpha=0.8)
            # plt.xticks(ticks=x_ticks, labels = x_labels)
            # plt.xlabel(r'$time$')
            # plt.ylabel(r'$\theta_{\mathrm{op}}\,\left[°C\right]$')
            # plt.title("Indoor operative temperature over time")
            # plt.savefig(self.plot_dir / "KPI_fictitious_cooling_top.png",
            #             transparent=False)
            # plt.close(fig)

            fig = plt.figure(figsize=(18, 15), constrained_layout=True)
            plt.grid()
            X = np.linspace(-10,40)
            Y_comfort =  [x*0.33+18.8 if 10<=x<=33 else 10*0.33+18.8 if x<10 else 33*0.33+18.8 for x in X]
            Y_cat1_up = [x*0.33+18.8+2 if 10<=x<=33 else 10*0.33+18.8+2 if x<10 else 33*0.33+18.8+2 for x in X]
            Y_cat1_down = [x*0.33+18.8-3 if 10<=x<=33 else 10*0.33+18.8-3 if x<10 else 33*0.33+18.8-3 for x in X]
            Y_cat2_up = [x*0.33+18.8+3 if 10<=x<=33 else 10*0.33+18.8+3 if x<10 else 33*0.33+18.8+3 for x in X]
            Y_cat2_down = [x*0.33+18.8-4 if 10<=x<=33 else 10*0.33+18.8-4 if x<10 else 33*0.33+18.8-4 for x in X]
            Y_cat3_up = [x*0.33+18.8+4 if 10<=x<=33 else 10*0.33+18.8+4 if x<10 else 33*0.33+18.8+4 for x in X]
            Y_cat3_down = [x*0.33+18.8-5 if 10<=x<=33 else 10*0.33+18.8-5 if x<10 else 33*0.33+18.8-5 for x in X]
            plt.plot(df["T_rm"], df["T_op_i[C]"],ls='', marker='o', color=color, alpha=0.8)

            plt.plot(X,Y_comfort,color='r', lw=4)
            plt.plot(X,Y_cat1_up,color='r',linestyle='--', lw=4)
            plt.plot(X,Y_cat1_down,color='r',linestyle='--', lw=4)
            plt.plot(X,Y_cat2_up,color='r',linestyle='-.', lw=4)
            plt.plot(X,Y_cat2_down,color='r',linestyle='-.', lw=4)
            plt.plot(X,Y_cat3_up,color='r',linestyle=':', lw=4)
            plt.plot(X,Y_cat3_down,color='r',linestyle=':', lw=4)
            plt.xlabel(r'$\theta_{\mathrm{rm}}$ [°C]')
            plt.ylabel(r'$\theta_{\mathrm{op}}$ [°C]')
            plt.ylim(15, 33)
            plt.savefig(self.plot_dir / "KPI_adaptive_comfort_model.png",
                        transparent=False)
            plt.close(fig)
 
            fig, ax = plt.subplots(constrained_layout=True, figsize=(50, 25))
            x = np.sort(df["T_op_i[C]"])
            y = np.arange(len(df["T_op_i[C]"]))/float(len(df["T_op_i[C]"]))
            ax.plot(x, y, linewidth = 4)
            ax.set_xlabel('°C')
            plt.title("CDF indoor operative temperature")
            plt.grid()
            plt.savefig(self.plot_dir / "cdf_t_op_i.png")
            plt.close(fig)

        # return No of hours in categories, add /len(df) to get fraction
        return {"cat I": category_1,
                "cat II up": category_2_up,
                "cat III up": category_3_up,
                "cat over III": category_over_3,
                "cat II down": category_2_down,
                "cat III down": category_3_down,
                "cat under III": category_under_3,
                "POR": (category_3_up+category_over_3+category_3_down+category_under_3)/len(df)}

    def energy_signature(self, df, show_points=True, variable_heat=None, variable_cool=None):
        """Compute energy signature in a standardized format: dataframe used
        should contain "Date/Time", "T_db_o[C]", "T_db_i[C]",
        "GHI_o[W/m2]" and HVAC consumptions columns, "Q_c[Wh/m3]" and
        "Q_h[Wh/m3]".

        :param dataframe: dataframe containing at least "Date/Time",
            "T_db_o[C]", "T_db_i[C]", "GHI_o[W/m2]","Q_h[Wh/m3]",
            "Q_c[Wh/m3]" columns
        :type dataframe: class:`pandas.core.frame.DataFrame`
        :param show_points: Show original scatter points that are used to
            compute energy signatures, defaults to False
        :type show_points: bool, optional
        :return: Minimum points allowing to build 1D and 2D graphs of energy
            signature.
        :rtype: dict

        :Example:

        Returned dictionary structure:

        .. code-block:: python

            {
                "1D": {
                    "cooling": {
                        "deltaT": list of 2 points,
                        "cooling": list of 2 points,
                    },
                    "heating": {
                        "deltaT": list of 2 points,
                        "heating": list of 2 points,
                    },
                },
                "2D": {
                    "cooling": {
                        "deltaT": list of 4 points,
                        "solarRadiation": list of 4 points,
                        "cooling": list of 4 points,
                    },
                    "heating": {
                        "deltaT": list of 4 points,
                        "solarRadiation": list of 4 points,
                        "heating": list of 4 points,
                    },
                },
            }
        """
        # df["deltaT"] = df["T_db_i[C]"]-df["T_db_o[C]"]  # NOTE: To revert.
        df["deltaT"] = df["T_db_o[C]"]

        df["GHI_o[W/m2]"][df["GHI_o[W/m2]"] == 0] = np.nan
        df = df.resample("1W").mean()
        # Remove DHW contribution
        # TODO ancora da gestire nel caso non ci sia/null boh --> varie comformazioni acqua calda
        # dhw_points = df.where((df["Q_c[Wh/m3]"]!=0.0) &
        #                     (df["Q_h[Wh/m3]"]!=0.0)).dropna()
        # dhw_mean_value = dhw_points.mean(axis=0)["Q_h[Wh/m3]"]
        # # FIXME: Fix formula.
        # df["Q_h[Wh/m3]"] = df["Q_h[Wh/m3]"]-dhw_mean_value
        data_cool = df[df["Q_c[Wh/m3]"]!=0.0]
        data_heat = df[df["Q_h[Wh/m3]"]>= 1]#TODO change
        flag_c = 1
        flag_h = 1

        line = lambda x, m, q: m*x + q
        x_interseption = lambda m, q: -q/m

        if len(data_cool) != 0:
            # 1D energy signature [External temperature].
            model_cool_1D = sm.OLS(data_cool["Q_c[Wh/m3]"],
                                sm.add_constant(data_cool.deltaT))
            # Models fitting.
            res_cool_1D = model_cool_1D.fit()
            # Cooling line equations.
            m_cool = res_cool_1D.params[1]
            q_cool = res_cool_1D.params[0]

            # NOTE: Use for Delta Tin - Tout
            # line_cool = pd.DataFrame(
            #     {"x": [data_cool.deltaT.min(), x_interseption(m_cool, q_cool)]}
            # )

            # NOTE: Use for Delta Tout
            line_cool = pd.DataFrame(
                {"x": [x_interseption(m_cool, q_cool), data_cool.deltaT.max()]}
            )

            line_cool["y"] = line_cool["x"].apply(line, args=(m_cool, q_cool))
            # 1D energy signature [Solar].
            model_cool_1D_solar = sm.OLS(data_cool["Q_c[Wh/m3]"],
                            sm.add_constant(data_cool["GHI_o[W/m2]"]))
            # Models fitting [Solar].
            res_cool_1D_solar = model_cool_1D_solar.fit()
            # Cooling line equations [Solar].
            m_cool_solar = res_cool_1D_solar.params[1]
            q_cool_solar = res_cool_1D_solar.params[0]
            
            line_cool_solar = pd.DataFrame(
                {
                    "x": [
                        x_interseption(m_cool_solar, q_cool_solar),
                        data_cool["GHI_o[W/m2]"].max(),
                    ]
                }
            )
            line_cool_solar["y"] = line_cool_solar["x"].apply(
                line, args=(m_cool_solar, q_cool_solar)
            )
       
            # 2D energy signature
            model_cool_2D = sm.OLS(data_cool["Q_c[Wh/m3]"],
                                sm.add_constant(data_cool.loc[:,["deltaT",\
                                                                "GHI_o[W/m2]"]]))
            res_cool_2D = model_cool_2D.fit()
            xx1c, xx2c = np.meshgrid(np.linspace(data_cool.deltaT.min(),\
                                                data_cool.deltaT.max(),2), 
                                    np.linspace(data_cool["GHI_o[W/m2]"].min(),\
                                                data_cool["GHI_o[W/m2]"].max(), 2))
            Z_cool = res_cool_2D.params[0] + res_cool_2D.params[1] * xx1c +\
                    res_cool_2D.params[2] * xx2c
        
        else:
            flag_c = 0

        if len(data_heat) != 0:
            # 1D energy signature [External temperature].
            model_heat_1D = sm.OLS(data_heat["Q_h[Wh/m3]"],
                                sm.add_constant(data_heat.deltaT))
            # Models fitting.
            res_heat_1D = model_heat_1D.fit()
            # Heating line equations.
            m_heat = res_heat_1D.params[1]
            q_heat = res_heat_1D.params[0]

            # NOTE: Use for Delta Tin - Tout
            # line_heat = pd.DataFrame(
            #     {"x": [x_interseption(m_heat, q_heat), data_heat.deltaT.max()]}
            # )

            # NOTE: Use for only Tout
            line_heat = pd.DataFrame(
                {"x": [data_heat.deltaT.min(), x_interseption(m_heat, q_heat)]}
            )

            line_heat["y"] = line_heat["x"].apply(line, args=(m_heat, q_heat))
            # 1D energy signature [Solar].
            model_heat_1D_solar = sm.OLS(data_heat["Q_h[Wh/m3]"],
                                sm.add_constant(data_heat["GHI_o[W/m2]"]))
            # Models fitting [Solar].
            res_heat_1D_solar = model_heat_1D_solar.fit()
            # Heating line equations [Solar].
            m_heat_solar = res_heat_1D_solar.params[1]
            q_heat_solar = res_heat_1D_solar.params[0]
            line_heat_solar = pd.DataFrame(
                {
                    "x": [
                        data_heat["GHI_o[W/m2]"].min(),
                        x_interseption(m_heat_solar, q_heat_solar),
                    ]
                }
            )
            line_heat_solar["y"] = line_heat_solar["x"].apply(
                line, args=(m_heat_solar, q_heat_solar)
            )

            # 2D energy signature
            model_heat_2D = sm.OLS(data_heat["Q_h[Wh/m3]"],
                                sm.add_constant(data_heat.loc[:,["deltaT",\
                                                                "GHI_o[W/m2]"]]))

            res_heat_2D = model_heat_2D.fit()

            xx1h, xx2h = np.meshgrid(np.linspace(data_heat.deltaT.min(),\
                                                data_heat.deltaT.max(), 2),
                                    np.linspace(data_heat["GHI_o[W/m2]"].min(),\
                                                data_heat["GHI_o[W/m2]"].max(), 2))
            Z_heat = res_heat_2D.params[0] + res_heat_2D.params[1] * xx1h +\
                    res_heat_2D.params[2] * xx2h
        
        else:
            flag_h = 0

        if flag_h and flag_c:
            data = {
                "1D": {
                    "cooling": {
                        "deltaT": line_cool["x"].values,
                        "cooling": line_cool["y"].values,
                    },
                    "heating": {
                        "deltaT": line_heat["x"].values,
                        "heating": line_heat["y"].values,
                    },
                },
                "2D": {
                    "cooling": {
                        "deltaT": list(xx1c[0]) + list(xx1c[1]),
                        "solarRadiation": list(xx2c[0]) + list(xx2c[1]),
                        "cooling": list(Z_cool[0]) + list(Z_cool[1]),
                    },
                    "heating": {
                        "deltaT": list(xx1h[0]) +  list(xx1h[1]),
                        "solarRadiation": list(xx2h[0]) + list(xx2h[1]),
                        "heating": list(Z_heat[0]) + list(Z_heat[1]),
                    },
                },
            }
        
        elif flag_c == 0:
            data = {
                "1D": {
                    "heating": {
                        "deltaT": line_heat["x"].values,
                        "heating": line_heat["y"].values,
                    },
                },
                "2D": {
                    "heating": {
                        "deltaT": list(xx1h[0]) +  list(xx1h[1]),
                        "solarRadiation": list(xx2h[0]) + list(xx2h[1]),
                        "heating": list(Z_heat[0]) + list(Z_heat[1]),
                    },
                },
            }
        
        elif flag_h == 0:
            data = {
                "1D": {
                    "cooling": {
                        "deltaT": line_cool["x"].values,
                        "cooling": line_cool["y"].values,
                    }
                },
                "2D": {
                    "cooling": {
                        "deltaT": list(xx1c[0]) + list(xx1c[1]),
                        "solarRadiation": list(xx2c[0]) + list(xx2c[1]),
                        "cooling": list(Z_cool[0]) + list(Z_cool[1]),
                    }
                },
            }


        if self.graph:
            plt.rcParams.update({"font.size": 45})

            # Energy signature 1D (External temperature).
            fig, ax = plt.subplots(figsize=(18, 15), constrained_layout=True)
            if flag_c:
                line_cool.plot(x="x", y="y", color='tab:cyan', lw=5, label='Cooling', ax=ax)
            if flag_h:
                line_heat.plot(x="x", y="y", color='tab:orange', lw=5, label='Heating', ax=ax)
            if show_points:
                if flag_c:
                    ax.scatter(list(data_cool["deltaT"]), list(data_cool["Q_c[Wh/m3]"]), color="tab:cyan")
                if flag_h:
                    ax.scatter(list(data_heat["deltaT"]), list(data_heat["Q_h[Wh/m3]"]), color="tab:orange")
                
            plt.title('Energy Signature 1D\nWeekly Data Aggregation')
            # TODO dare opzione di scelta 
            # plt.xlabel(r'$T_{\mathrm{in}} - T_{\mathrm{ex}}$ [°C]')
            plt.xlabel(r'$T_{\mathrm{ex}}$ [°C]')
            plt.grid()
            plt.ylabel(r'Energy Need $\left[\mathrm{W/m^3}\right]$')
            plt.legend()
            # plt.tight_layout()
            plt.savefig(self.plot_dir / "en_sig_1d_temp.png")
            plt.close(fig)

            # Eergy signature 1D (Solar).
            fig, ax = plt.subplots(figsize=(18, 15), constrained_layout=True)
            if flag_c:
                line_cool_solar.plot(x="x", y="y", color='tab:cyan', lw=5, label='Cooling', ax=ax)
            if flag_h:
                line_heat_solar.plot(x="x", y="y", color='tab:orange', lw=5, label='Heating', ax=ax)
            if show_points:
                if flag_c:
                    ax.scatter(list(data_cool["GHI_o[W/m2]"]), list(data_cool["Q_c[Wh/m3]"]), color="tab:cyan")
                if flag_h:
                    ax.scatter(list(data_heat["GHI_o[W/m2]"]), list(data_heat["Q_h[Wh/m3]"]), color="tab:orange")
            plt.title('Energy Signature 1D\nWeekly Data Aggregation')
            plt.xlabel(r'Global solar radiation ' + r'$\left[\mathrm{Wh/m^2}\right]$')
            plt.grid()
            plt.ylabel(r'Energy Need $\left[\mathrm{W/m^3}\right]$')
            plt.legend()
            # plt.tight_layout()
            plt.savefig(self.plot_dir / "en_sig_1d_solar.png")
            plt.close(fig)

            # Eergy signature 2D.
            fig = plt.figure(figsize=(18, 15))
            ax = fig.add_subplot(111, projection='3d')
            
            if flag_c:
                ax.plot_surface(np.array(list(xx1c[0]) + list(xx1c[1])).reshape(2,2),
                                np.array(list(xx2c[0]) + list(xx2c[1])).reshape(2,2),
                                np.array(list(Z_cool[0]) + list(Z_cool[1])).reshape(2,2),
                                color='b', alpha=0.3, linewidth=0)
            if flag_h:
                ax.plot_surface(np.array(list(xx1h[0]) +  list(xx1h[1])).reshape(2,2),
                                np.array(list(xx2h[0]) +  list(xx2h[1])).reshape(2,2),
                                np.array(list(Z_heat[0]) + list(Z_heat[1])).reshape(2,2),
                                color='orange', alpha=0.3, linewidth=0)
            # TODO dare opzione di scelta
            # ax.set_xlabel(r'$T_{\mathrm{in}} - T_{\mathrm{ex}}$ [°C]', labelpad=40)
            ax.set_xlabel(r'T_{\mathrm{ex}}$ [°C]', labelpad=40)
            ax.set_ylabel('Global solar\nradiation   \n' + r'$\left[\mathrm{Wh/m^2}\right]$', labelpad=80)
            ax.set_zlabel('Energy Need\n' + r'$\left[\mathrm{W/m^3}\right]$', labelpad=40)
            ax.set_title('Energy Signature 2D\nWeekly Data Aggregation')
            ax.view_init(elev=34, azim=-35)
            # # rotate the axes and update
            ax.view_init(45, 55)
            plt.draw()

            handles = []
            if flag_c:
                handles.append(mpatches.Patch(color='blue',label='Surface Cooling'))
            if flag_h:
                handles.append(mpatches.Patch(color='orange',label='Surface Heating'))
            ax.legend(handles=handles, bbox_to_anchor=(0.5, 0.4, 0.5, 0.5))
            plt.savefig(self.plot_dir / "en_sig_2d.png")
            plt.close(fig)

        return data

    def cidh(self, df, thresholds=[18, 20, 22, 24, 26, 28]):
        """Compute standardized CIDH (Cooling Internal Degree
        Hours) computation and finally return sum of residuals from defined
        thresholds.

        :param df: dataframe containing at least "Date/Time" and "T_db_i[C]"
            columns
        :type df: class:`pandas.core.frame.DataFrame`
        :param thresholds: temperature thresholds from which compute residuals,
            defaults to [18, 20, 22, 24, 26, 28]
        :type thresholds: list, optional
        :return: dictionary containing sum of residuals from thresholds
        :rtype: dict
        """

        df.set_index("Date/Time", inplace=True, drop=True)
        df = df.resample("1H").mean()
        df["Date/Time"] = df.index

        my_dict = {}
        for th in thresholds:
            df["dist_"+str(th)] = df["T_db_i[C]"]-th
            sum_dist = df["dist_"+str(th)].where(df["dist_"+str(th)]>0).sum()
            my_dict["dist_"+str(th)] = sum_dist

        return my_dict

    def hidh(self, df, thresholds=[18, 20, 22]):
        """Compute standardized HIDH (Heating Internal Degree
        Hours) computation and finally return sum of residuals from defined
        thresholds.

        :param df: dataframe containing at least "Date/Time" and "T_db_i[C]"
            columns
        :type df: class:`pandas.core.frame.DataFrame`
        :param thresholds: temperature thresholds from which compute residuals,
            defaults to [18, 20, 22]
        :type thresholds: list, optional
        :return: dictionary containing sum of residuals from thresholds
        :rtype: dict
        """

        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()

        my_dict = {}
        for th in thresholds:
            df["dist_"+str(th)] = th - df["T_db_i[C]"]
            sum_dist = df["dist_"+str(th)].where(df["dist_"+str(th)]>0).sum()
            my_dict["dist_"+str(th)] = sum_dist

        return my_dict

    def cdh(self, df, thresholds=[18, 21, 24, 26, 28]):
        """Compute Cooling Degree Hours

        :param df: DataFrame containing at least "Date/Time" and "T_db_o[C]"
            columns
        :type df: class:`pandas.core.frame.DataFrame`
        :param thresholds: temperature thresholds from which compute residuals,
            defaults to [18, 21, 24, 26, 28]
        :type thresholds: list, optional
        :return: Cooling degree hour
        :rtype: dict
        """
        df = df.resample("1H").mean()
        
        my_dict = {}
        for th in thresholds:
            df["dist_"+str(th)] = df["T_db_o[C]"]-th
            sum_dist = df["dist_"+str(th)].where(df["dist_"+str(th)]>0).sum()
            my_dict["dist_"+str(th)] = sum_dist

        return my_dict

    def cdh_res_eahx(self, df, var1, var2, var3, diffus = 6.177*(10**(-7)), depth = 2.5, eff= 0.52, t_b = 24, with_control=True):
        """Compute CDH_res considering EAHX technology.

        :param df: DataFrame containing at least "Date/Time" and "T_db_o[C]"
            columns
        :type df: class:`pandas.core.frame.DataFrame`
        :param var1: average soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var1: float
        :param var2: amplitude of soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var2: float
        :param var3: phase of soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var3: float
        :param depth: soil depth of EAHX tubes in meters
        :type depth: float
        :param diffus: soil thermal diffusity in m2/s
        :type diffus: float
        :param eff: EAHX efficiency, defaults to 0.8
        :type eff: float, optional
        :param t_b: base temperature, defaults to 24
        :type t_b: float, optional
        :param with_control: wether to sum positive values only when control
            condition is satisfied, default True
        :type with_control: bool, optional
        :return: CDH_res_EAHX
        :rtype: float
        """
        df = df.resample("1H").mean()
        df["Date/Time"] = df.index
        df["tmp"] = df[["T_db_o[C]", "Date/Time"]].apply(self.compute_ttreat_eahx, args=(
                                                       var1, var2, var3, depth, diffus, eff), axis=1)
        df["soilT"], df["t_treat"] = zip(*df.tmp)
        del df["tmp"]

        if self.graph:
            plt.rcParams.update({"font.size": 50})
            fig, ax = plt.subplots(figsize=(30, 15))
            ax.plot(df["soilT"], color="sienna", linewidth=7.5, label="Soil")
            ax.plot(df["t_treat"], color="firebrick", linewidth=1, label="Treat")
            ax.plot(df["T_db_o[C]"], color="burlywood", linewidth=1, label="Dry-bulb")
            plt.ylim([-5, 40])
            plt.ylabel("°C")
            plt.legend(title="Temperature")
            ax.xaxis.set_major_formatter(mdates.DateFormatter('%b')) 
            fig.autofmt_xdate()
            plt.grid()
            plt.tight_layout()
            plt.savefig(self.plot_dir / ("soil_t_vs_time.png"))
            plt.close(fig)

            fig, ax = plt.subplots(figsize=(30, 15))
            ax.plot(df["soilT"], color="sienna", linewidth=7.5, label="Soil")
            ax.plot(df["t_treat"].resample("1D").mean(), color="firebrick", linewidth=4, label="Treat (Daily Mean)", alpha=0.8)
            ax.plot(df["T_db_o[C]"].resample("1D").mean(), color="burlywood", linewidth=4, label="Dry-bulb (Daily Mean)", alpha=0.8)
            plt.ylim([-5, 40])
            plt.ylabel("°C")
            plt.legend(title="Temperature")
            ax.xaxis.set_major_formatter(mdates.DateFormatter('%b')) 
            fig.autofmt_xdate()
            plt.grid()
            plt.tight_layout()
            plt.savefig(self.plot_dir / ("soil_t_vs_time_mean.png"))
            plt.close(fig)

        df["dist ttreat-tbase"] = df["t_treat"] - t_b
        df["dist tamb-tbase"] = df["T_db_o[C]"] - t_b
        df["dist tamb-ttreat"] = df["t_treat"] - df["T_db_o[C]"]

        # TODO fare check della formula prima di usarla
        if with_control:
            sum_dist = 0
            sum_dist = df["dist ttreat-tbase"].loc[(df["dist ttreat-tbase"]>0) & (df["dist tamb-ttreat"] >0)].sum()
            sum_dist = sum_dist + df["dist tamb-tbase"].loc[(df["dist tamb-tbase"]>0) & (df["dist tamb-ttreat"]<=0)].sum()
        else: 
            sum_dist =  df["dist ttreat-tbase"].where(df["dist ttreat-tbase"]>0).sum()

        return sum_dist

    def hdh_res_eahx(self, df, var1, var2, var3,  diffus = 6.177*(10**(-7)), depth = 2.5, eff= 0.52, t_b = 20, with_control=True):
        """Compute HDH_res considering EAHX technology.

        :param df: DataFrame containing at least "Date/Time" and "T_db_o[C]"
            columns
        :type df: class:`pandas.core.frame.DataFrame`
        :param var1: average soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var1: float
        :param var2: amplitude of soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var2: float
        :param var3: phase of soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var3: float
        :param depth: soil depth of EAHX tubes in meters
        :type depth: float
        :param diffus: soil thermal diffusity
        :type diffus: float
        :param eff: EAHX efficiency, defaults to 0.8
        :type eff: float, optional
        :param t_b: base temperature, defaults to 24
        :type t_b: float, optional
        :param with_control: wether to sum positive values only when control 
            condition is satisfied, default True
        :type with_control: bool, optional
        :return: HDH_res_EAHX
        :rtype: float
        """
        df = df.resample("1H").mean()
        df["t_treat"] = df[["T_db_o[C]"]].apply(self.compute_ttreat_eahx, args=(
                                                       var1, var2, var3, depth, diffus, eff), axis=1)

        df["dist ttreat-tbase"] = df["t_treat"] - t_b
        df["dist tamb-tbase"] = df["T_db_o[C]"] - t_b
        df["dist tamb-ttreat"] = df["t_treat"] - df["T_db_o[C]"]

        # TODO fare check della formula prima di usarla
        if with_control:
            sum_dist = 0
            sum_dist = df["dist ttreat-tbase"].loc[(df["dist ttreat-tbase"]<0) & (df["dist tamb-ttreat"] <0)].sum()
            sum_dist = sum_dist + df["dist tamb-tbase"].loc[(df["dist tamb-tbase"]<0) & (df["dist tamb-ttreat"]>=0)].sum()
        else: 
            sum_dist =  df["dist ttreat-tbase"].where(df["dist ttreat-tbase"]<0).sum()
        
        sum_dist = -sum_dist
        return sum_dist

    def cdh_res_pdec(self, df, t_b = 24, eff = 0.8, with_control=True):
        """Compute CDH_res considering PDEC technology.

        :param df: DataFrame containing at least "Date/Time", "T_db_o[C]"
            and "RH_o[%]" columns
        :type df: class:`pandas.core.frame.DataFrame`
        :param t_b: default 24
        :type t_b: float, optional
        :param eff: default 0.8
        :type eff: float, optional
        :param with_control: default True
        :type with_control: bool, optional
        :return: CDH_res_PDEC
        :rtype: float
        """
        
        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()

        df["T_wb_o[C]"] = df[["T_db_o[C]", "RH_o[%]"]].apply(self.compute_wbt, axis=1)
        df["t_treat"] = df[["T_db_o[C]", "T_wb_o[C]"]].apply(self.compute_ttreat_pdec, args=(eff, ), axis=1)

        df["dist ttreat-tbase"] = df["t_treat"] - t_b
        df["dist tamb-tbase"] = df["T_db_o[C]"] - t_b
        df["dist tamb-ttreat"] = df["t_treat"] - df["T_db_o[C]"]

        # TODO fare check della formula prima di usarla
        if with_control:
            sum_dist = 0
            sum_dist = df["dist ttreat-tbase"].loc[(df["dist ttreat-tbase"]>0) & (df["dist tamb-ttreat"] >0)].sum()
            sum_dist = sum_dist + df["dist tamb-tbase"].loc[(df["dist tamb-tbase"]>0) & (df["dist tamb-ttreat"]<=0)].sum()
        else: 
            sum_dist =  df["dist ttreat-tbase"].where(df["dist ttreat-tbase"]>0).sum()

        return sum_dist

    def hdh(self, df, thresholds=[18, 20, 22]):
        """Compute Heating Degree Hours

        :param df: DataFrame containing at least "Date/Time" and "T_db_o[C]"
            columns
        :type df: class:`pandas.core.frame.DataFrame`
        :param thresholds: temperature thresholds from which compute residuals,
            defaults to [18, 20, 22]
        :type thresholds: list, optional
        :return: Heating degree hour
        :rtype: dict
        """
        
        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()
        
        my_dict = {}
        for th in thresholds:
            df["dist_"+str(th)] = th - df["T_db_o[C]"]
            sum_dist = df["dist_"+str(th)].where(df["dist_"+str(th)]>0).sum()
            my_dict["dist_"+str(th)] = sum_dist

        return my_dict

    def cdd(self, df):
        """Compute Cooling Degree Days.

        :param df: DataFrame containing at least "Date/Time" and "T_db_o[C]"
            columns
        :type df: class:`pandas.core.frame.DataFrame`
        :return: Cooling degree day
        :rtype: float
        """
        
        df = df.set_index("Date/Time")
        df = df.resample("1D").mean()
        # to return dict instead of float
        # my_dict = {}
        df["cdd"] = df["T_db_o[C]"]-21
        sum_dist = df["cdd"].where((df["T_db_o[C]"]-24)>=0).sum()
        # my_dict["cdd"] = sum_dist

        return sum_dist

    def hdd(self, df):
        """Compute Heating Degree Days.

        :param df: DataFrame containing at least "Date/Time" and "T_db_o[C]"
            columns
        :type df: class:`pandas.core.frame.DataFrame`
        :return: Heating degree day
        :rtype: float
        """
        
        df = df.set_index("Date/Time")
        df = df.resample("1D").mean()
        # to return dict instead of float
        # my_dict = {}
        df["hdd"] = 18 - df["T_db_o[C]"]
        sum_dist = df["hdd"].where((15 - df["T_db_o[C]"])>=0).sum()
        # my_dict["cdd"] = sum_dist

        return sum_dist

    def cdd_res(self, df, tset=26, ef=0.5):
        """Compute Cooling Degree Days residuals.

        :param df: DataFrame containing at least "Date/Time" and "T_db_o[C]"
            columns
        :param tset: Setpoint temperature, defaults 26
        :type tset: int, optional
        :param ef: free parameter, defaults to 0.5
        :type ef: float, optional
        :return: Cooling degree days residuals
        :rtype: int
        """
        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()
        df = df.reset_index()
        cnt = 0
        sum_dist = 0
        
        while cnt + 24 < len(df):
            dist = 0
            for i in range(cnt, cnt + 24):
                delta_t = df["T_db_o[C]"][i] - tset
                if delta_t >= 0:
                    dist += delta_t
                else: 
                    dist += delta_t*ef
            if dist > 0:
                sum_dist += dist
            cnt += 24
        
        return sum_dist

    def ccp(self, df, delta_T=2, hours_ccp = None):
        """Compute Climate Cooling Potential

        :param df: DataFrame containing at least "Date/Time" and "T_db_o[C]"
            columns
        :type df: class:`pandas.core.frame.DataFrame`
        :param delta_T: Delta temperature, defaults to 2
        :type delta_T: int, optional
        :param hours_ccp: list of hours to be included in ccp computation, defaults to None
        :type hours_ccp: list of int, optional
        :return: Climate Cooling Potential and daily Climate Cooling Potential
            in a dictionary
        :rtype: dict
        """
        hours = np.arange(0,24,1)
        t_base = 24.5+2.5*np.cos(2*np.pi*(hours-19)/24)
        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()
        dist = [t_base[df.index[i].hour] - df["T_db_o[C]"][i] for i in range(0, len(df))]
        df["dist"] = dist
        
        # personalized hours range
        if hours_ccp is not None:
            ok_list = []
            for el in df.index.hour:
                if el in hours_ccp:
                    ok_list.append(1)
                else:
                    ok_list.append(0)
        else:
            ok_list = np.ones(len(df))
        df["check_hours"] = ok_list
        
        my_dict = {}
        ccp = df["dist"].where(list(df["dist"]>=delta_T) or list(df["check_hours"] == 1)).sum()
        ccpd = ccp/int(len(df)/24) #TODO 24 o len hours_ccp ?
        my_dict["ccp"] = ccp
        my_dict["ccpd"] = ccpd
        return my_dict 

    def qach(self, df, vol, ach=2.5, pair=1.2, cair=0.278, tset=26, delta_T=2):
        """Compute climate heat gain dissipation potential of
        ventilative cooling, Qach. 

        :param df: DataFrame containing at least "Date/Time" and "T_db_o[C]"
            columns
        :type df: class:`pandas.core.frame.DataFrame`
        :param vol: Volume of the building
        :type vol: float
        :param ach: Air changes per hour (ventilation), defaults to 2.5
        :type ach: float, optional
        :param pair: Air density in kg/m, defaults to 1.2
        :type pair: float, optional
        :param cair: Air specific heat capacity in W/kg°C, defaults
            to 0.278
        :type cair: float, optional
        :param tset: Setpoint temperature in °C, defaults to 26
        :type tset: int, optional
        :param delta_T: Delta temperature, defaults to 2 °C
        :type delta_T: int, optional
        :return: Qach
        :rtype: float
        """
        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()
        # my_dict={}
        df["dist"] = tset - df["T_db_o[C]"]
        sum_dist = df["dist"].where(df["dist"]>=delta_T).sum()
        qach = sum_dist*ach*pair*cair*vol/1000
        # my_dict["qach"] = qach
        
        return qach

    def qc_pdec(self, df, A, vair, pair=1.2, cair=0.278, eff= 0.8, delta_T=2):
        """Compute climate heat gain dissipation potential of
        ventilative cooling, Qc_PDEC. 

        :param df: DataFrame containing at least "Date/Time", "T_db_o[C]"
            and "RH_o[%]" columns
        :type df: class:`pandas.core.frame.DataFrame`
        :param A: pad area
        :type A: float
        :param vair: flow rate
        :type vair: float
        :param pair: Air density in kg/m, defaults to 1.2
        :type pair: float, optional
        :param cair: Air specific heat capacity in W/kg°C, defaults
            to 0.278
        :type cair: float, optional
        :param eff: PDEC efficiency, defaults to 0.8
        :type eff: float, optional
        :param delta_T: Delta temperature, defaults to 2 °C
        :type delta_T: int, optional
        :return: Qc_PDEC
        :rtype: float
        """
        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()

        df["T_wb_o[C]"] = df[["T_db_o[C]", "RH_o[%]"]].apply(self.compute_wbt, axis=1)
        df["t_treat"] = df[["T_db_o[C]", "T_wb_o[C]"]].apply(self.compute_ttreat_pdec, args=(eff, ), axis=1)
        # my_dict={}
        df["dist"] = df["T_db_o[C]"] - df["t_treat"]
        # TODO check formula
        sum_dist = df["dist"].where(df["dist"]>=delta_T).sum()
        qach = 3600*sum_dist*A*vair*pair*cair
        # my_dict["qach"] = qach
        
        return qach

    def qc_eahx(self, df, A, vair, var1, var2, var3, pair=1.2,\
         cair=0.278, diffus=6.177*(10**(-7)), depth=2.5, eff=0.52, delta_T=2):
        """Compute climate heat gain dissipation potential of
        ventilative cooling, Qc_EAHX.

        :param df: DataFrame containing at least "Date/Time" and "T_db_o[C]"
            columns
        :type df: class:`pandas.core.frame.DataFrame`
        :param A: tube area
        :type A: float
        :param vair: flow rate
        :type vair: float
        :param var1: average soil surface temperature
        :type var1: float
        :param var2: amplitude of soil surface temperature
        :type var2: float
        :param var3: phase of soil surface temperature
        :type var3: float
        :param pair: Air density in kg/m, defaults to 1.2
        :type pair: float, optional
        :param cair: Air specific heat capacity in W/kg°C, defaults
            to 0.278
        :type cair: float, optional
        :param depth: soil depth of EAHX tubes in meters, defaults to 2.5
        :type depth: float, optional 
        :param diffus: soil thermal diffusity, defaults to 6.177*(10**(-7))
        :type diffus: float, optional
        :param eff: EAHX efficiency, defaults to 0.52
        :type eff: float, optional
        :param delta_T: Delta temperature, defaults to 2 °C
        :type delta_T: int, optional
        :return: Qc_EAHX
        :rtype: float
        """
        df = df.resample("1H").mean()
        df["T_wb_o[C]"] = df[["T_db_o[C]", "RH_o[%]"]].apply(self.compute_wbt, axis=1)
        df["t_treat"] = df[["T_db_o[C]"]].apply(self.compute_ttreat_eahx, args=(
                                                       var1, var2, var3, depth, diffus, eff), axis=1)
        # my_dict={}
        df["dist"] = df["T_db_o[C]"] - df["t_treat"]
        # TODO check formula
        sum_dist = df["dist"].where(df["dist"]>=delta_T).sum()
        qach = 3600*sum_dist*A*vair*pair*cair
        # my_dict["qach"] = qach
        
        return qach

    def qh_eahx(self, df, A, vair, var1, var2, var3, pair=1.2,\
         cair=0.278, diffus=6.177*(10**(-7)), depth=2.5, eff=0.52, t_b=24, delta_T=2):
        """Compute climate heat gain dissipation potential of
        ventilative cooling, Qh_EAHX.

        :param df: DataFrame containing at least "Date/Time" and "T_db_o[C]"
            columns
        :type df: class:`pandas.core.frame.DataFrame`
        :param A: tube area
        :type A: float
        :param vair: flow rate
        :type vair: float
        :param var1: average soil surface temperature
        :type var1: float
        :param var2: amplitude of soil surface temperature
        :type var2: float
        :param var3: phase of soil surface temperature
        :type var3: float
        :param pair: Air density in kg/m, defaults to 1.2
        :type pair: float, optional
        :param cair: Air specific heat capacity in W/kg°C, defaults
            to 0.278
        :type cair: float, optional
        :param depth: soil depth of EAHX tubes in meters, defaults to 2.5
        :type depth: float, optional
        :param diffus: soil thermal diffusity, defaults to 6.177*(10**(-7))
        :type diffus: float, optional
        :param eff: EAHX efficiency, defaults to 0.52
        :type eff: float, optional
        :param delta_T: Delta temperature, defaults to 2 °C
        :type delta_T: int, optional
        :return: Qh_EAHX
        :rtype: float
        """
        df = df.resample("1H").mean()
        df["T_wb_o[C]"] = df[["T_db_o[C]", "RH_o[%]"]].apply(self.compute_wbt, axis=1)
        df["t_treat"] = df[["T_db_o[C]"]].apply(self.compute_ttreat_eahx, args=(
                                                       var1, var2, var3, depth, diffus, eff), axis=1)
        # my_dict={}
        df["dist"] = df["t_treat"] - df["T_db_o[C]"]
        # TODO check formula
        sum_dist = df["dist"].where(df["dist"]>=delta_T).sum()
        qach = 3600*sum_dist*A*vair*pair*cair
        # my_dict["qach"] = qach
        
        return qach

    def n_h_kwh(self, df, th_list = [0,0.6,1], cop_cool = 3, cop_heat = 0.3):
        """Number of hours HVAC consumption is higher then given thresholds.

        :param df: Dataframe which must contain at least cooling and heating 
                consumption columns "Q_c[Wh/m2]" and "Q_h[Wh/m2]" plus
                "Date/Time" column.
        :type df: class:`pandas.core.frame.DataFrame`
        :param th_list: List of thresholds to be compared with consumption,
            defaults to [0,0.6,1]
        :type th_list: list, optional
        :param cop_cool: Cop value for cooling system, defaults to 3
        :type cop_cool: int, optional
        :param cop_heat: Cop value for heating system, defaults to 0.3
        :type cop_heat: float, optional
        :return: Dictionary containing No. hours above defined thresholds
                for heating and cooling.
        :rtype: dict
        """
        df = df.set_index("Date/Time")
        df = df.resample("1H").sum()
        
        my_dict = {}
        for t in th_list:
            my_dict[str(t) + "_cool"] = df["Q_c[Wh/m2]"].where((df["Q_c[Wh/m2]"])>t*cop_cool).count()
            my_dict[str(t) + "_heat"] = df["Q_h[Wh/m2]"].where((df["Q_h[Wh/m2]"])>t*cop_heat).count()
        
        return my_dict

    def n_fr(self, df, season, filter_by_occupancy=0,variable=None):
        """_summary_

        :param df: _description_
        :type df: _type_
        :param season: _description_
        :type season: _type_
        :param filter_by_occupancy: _description_, defaults to 0
        :type filter_by_occupancy: int, optional
        :return: _description_
        :rtype: _type_
        """
        df = df.set_index("Date/Time")
        
        if season.upper() == "heating".upper():
            df = df.where(df["Q_h[kWh/m2]"] <= 0.01).dropna()
        elif season.upper() == "cooling".upper():
            df = df.where(df["Q_c[kWh/m2]"] <= 0.01).dropna()
        
        if filter_by_occupancy:
            df = df.where(df["Occupancy"] != 0).dropna()
        
        n_fr_h = len(df)
        
        return n_fr_h

    def timeseries_n_fr(self, df, season, filter_by_occupancy=0,variable=None):
        """_summary_

        :param df: _description_
        :type df: _type_
        :param season: _description_
        :type season: _type_
        :param filter_by_occupancy: _description_, defaults to 0
        :type filter_by_occupancy: int, optional
        :return: _description_
        :rtype: _type_
        """
        try:
            df.set_index("Date/Time", inplace=True, drop=False)
        except:
            pass
        
        if season.upper() == "heating".upper():
            df["n_fr"] = [1 if x <= 0.01 else 0 for x in df["Q_h[kWh/m2]"]]
        elif season.upper() == "cooling".upper():
            df["n_fr"] = [1 if x <= 0.01 else 0 for x in df["Q_c[kWh/m2]"]]
        # print(df)
        if filter_by_occupancy:
            df = df.where(df["Occupancy"] != 0).dropna()
        
        return df["n_fr"]

    def co2(self,df,th_list=[600,900,1200,1500,1800,2100,2500]):
        """Compute number of hours with CO2 concentration above given thresholds
        and CO2 simple statistics (max, min, mean).

        :param df: Dataframe which must contain at least CO2 concentration 
                outdoor and indoor columns "CO2_o[ppm]" and "CO2_i[ppm]" plus
                "Date/Time" column.
        :type df: class:`pandas.core.frame.DataFrame`
        :param th_list: thresholds from which compute number of hours with
            average CO2 above given threshold
        :type th_list: list, optional
        :return: dictionary containing CO2 levels statistics
        :rtype: dict
        """
        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()
        # my_dict = {}
        # for th in th_list:
        #     my_dict[str(th)] = df["CO2_i[ppm]"].where((df["CO2_i[ppm]"])>th).count()
        
        # my_dict["max"] = df["CO2_i[ppm]"].max()
        # my_dict["min"] = df["CO2_i[ppm]"].min()
        # my_dict["mean"] = df["CO2_i[ppm]"].mean()
        # # TODO check if it works
        # my_dict["800+out"] = df["CO2_i[ppm]"].where((df["CO2_i[ppm]"])>800+df["CO2_o[ppm]"]).count()
        
        # if self.graph:
        #     pass

        # return my_dict
        return df["CO2_i[ppm]"].mean()

    def n_co2_bI(self,df, filter_by_occupancy=1,season=None):
        """Compute number of hours with CO2 concentration below 600 ppm
        highlighting possible over-ventilated conditions.

        :param df: Dataframe which must contain at least CO2 concentration 
                indoor "CO2_i[ppm]" and "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :return: number of hours below threshold I
        :rtype: int
        """
        th = 600
        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()
        if filter_by_occupancy:
            df = df.where(df["Occupancy"] != 0).dropna()
        if season is not None:
            df = df.where(df[season+"_season"] != 0).dropna()
        value = df["CO2_i[ppm]"].where((df["CO2_i[ppm]"])<th).count()  

        return value

    def timeseries_n_co2_bI(self,df, filter_by_occupancy=1,season=None,timestep="1H"):
        """Compute number of hours with CO2 concentration below 600 ppm
        highlighting possible over-ventilated conditions.

        :param df: Dataframe which must contain at least CO2 concentration
                indoor "CO2_i[ppm]" and "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :return: number of hours below threshold I
        :rtype: int
        """
        th = 600
        df = df.set_index("Date/Time")
        df = df.resample(timestep).mean()

        if filter_by_occupancy:
            df = df.where(df["Occupancy"] != 0).dropna()
        if season is not None:
            df = df.where(df[season+"_season"] != 0).dropna()
       
        
        # print(f'df:\n{df}')
        df["boolean"] = [1 if x < th else 0 for x in df["CO2_i[ppm]"]]
        # df["boolean"] = bool([df["CO2_i[ppm]"] < th])

        return df["boolean"]

    def n_co2_aIII(self,df, filter_by_occupancy=1,season=None):
        """Compute number of hours with CO2 concentration above 1000 ppm
        highlighting possible under-ventilated conditions.

        :param df: Dataframe which must contain at least CO2 concentration
                indoor "CO2_i[ppm]" and "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :return: number of hours above threshold III
        :rtype: int
        """
        th = 1000
        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()
        if filter_by_occupancy:
            df = df.where(df["Occupancy"] != 0).dropna()
        if season is not None:
            df = df.where(df[season+"_season"] != 0).dropna()
        value = df["CO2_i[ppm]"].where((df["CO2_i[ppm]"])>th).count()

        return value

    def timeseries_n_co2_aIII(self,df, filter_by_occupancy=1,season=None,timestep="1H"):
        """Compute number of hours with CO2 concentration above 1000 ppm
        highlighting possible under-ventilated conditions.

        :param df: Dataframe which must contain at least CO2 concentration
                indoor "CO2_i[ppm]" and "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :return: number of hours above threshold III
        :rtype: int
        """
        th = 1000
        df = df.set_index("Date/Time")
        df = df.resample(timestep).mean()
        if filter_by_occupancy:
            df = df.where(df["Occupancy"] != 0).dropna()
        if season is not None:
            df = df.where(df[season+"_season"] != 0).dropna()

        df["boolean"] = [1 if x > th else 0 for x in df["CO2_i[ppm]"]]

        return df["boolean"]

    def f_Q_c(self,df,gen_factor=1):
        """Compute final energy need for cooling in kWh/m2, applying correction
        for generation losses but considering correction
        factors for emission, regulation and distribution as already applied.
        
        :param df: Dataframe which must contain at "Q_c[kWh/m2]" and "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :param gen_factor: correction factor for generation losses,
            in range [0,1].
        :type gen_factor: float, defaults to 1
        :return: Primary energy need for cooling
        :rtype: float
        """
        df.set_index("Date/Time", inplace=True, drop=True)
        df = df.resample("1H").sum()
        df["Date/Time"] = df.index
        df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(self.generator_losses,\
                                    args=(gen_factor, ))
        return df["Q_c[kWh/m2]"].sum()

    def f_Q_h(self,df,gen_factor=1,variable=None):
        """Compute final energy need for heating in kWh/m2, applying correction
        for generation losses but considering correction
        factors for emission, regulation and distribution as already applied.
        
        :param df: Dataframe which must contain at "Q_h[kWh/m2]" and "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :param gen_factor: correction factor for generation losses,
            in range [0,1].
        :type gen_factor: float, defaults to 1
        :return: Primary energy need for heating
        :rtype: float
        """
        df.set_index("Date/Time", inplace=True, drop=True)
        df = df.resample("1H").sum()
        df["Date/Time"] = df.index
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(self.generator_losses,
                                    args=(gen_factor, ))
        return df["Q_h[kWh/m2]"].sum()

    def timeseries_f_Q_h(self,df,gen_factor=1, timestep="1H",variable=None):
        """Compute final energy need for heating in kWh/m2, applying correction
        for generation losses but considering correction
        factors for emission, regulation and distribution as already applied.
        
        :param df: Dataframe which must contain at "Q_h[kWh/m2]" and "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :param gen_factor: correction factor for generation losses,
            in range [0,1].
        :type gen_factor: float, defaults to 1
        :return: Primary energy need for heating
        :rtype: float
        """
        df.set_index("Date/Time", inplace=True, drop=True)
        df = df.resample(timestep).sum()
        df["Date/Time"] = df.index
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(self.generator_losses,
                                    args=(gen_factor, ))
        return df["Q_h[kWh/m2]"]

    def Q_c(self,df,gen_factor=1,ep_factor=1):
        """Compute primary energy need for cooling in kWh/m2, applying correction
        factors for generation losses, then multiplying by primary energy factor.
        Corrections for emission, regulation and distribution losses are considered
        previously applied.

        :param df: Dataframe which must contain at "Q_c[kWh/m2]" and "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :param gen_factor: correction factor for generation losses,
            in range [0,1].
        :type gen_factor: float, defaults to 1
        :param ep_factor: primary energy conversion factor,
            in range [0,1].
        :type ep_factor: float, defaults to 1
        :return: Primary energy need for cooling
        :rtype: float
        """
        df.set_index("Date/Time", inplace=True, drop=True)
        df = df.resample("1H").sum()
        df["Date/Time"] = df.index
        df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(self.generator_losses,
                                    args=(gen_factor, ))
        df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(self.convert_to_ep,
                                    args=(ep_factor, ))
        return df["Q_c[kWh/m2]"].sum()

    def Q_I(self,df,gen_factor=1,ep_factor=1):
        """Compute primary energy need for lighting in kWh/m2, applying
        correction factors for generation losses, then multiplying by primary
        energy factor. Corrections for emission, regulation and distribution
        losses are considered previously applied.

        :param df: Dataframe which must contain at "Q_c[kWh/m2]" and
            "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :param gen_factor: correction factor for generation losses,
            in range [0,1].
        :type gen_factor: float, defaults to 1
        :param ep_factor: primary energy conversion factor,
            in range [0,1].
        :type ep_factor: float, defaults to 1
        :return: Primary energy need for lighting
        :rtype: float
        """
        df.set_index("Date/Time", inplace=True, drop=True)
        df = df.resample("1H").sum()
        df["Date/Time"] = df.index
        df["Q_I[kWh/m2]"] = df["Q_I[kWh/m2]"].apply(self.generator_losses,
                                    args=(gen_factor, ))
        df["Q_I[kWh/m2]"] = df["Q_I[kWh/m2]"].apply(self.convert_to_ep,
                                    args=(ep_factor, ))
        return df["Q_I[kWh/m2]"].sum()

    def Q_h(self,df,gen_factor=1,ep_factor=1, timestep=None,variable=None):
        """Compute primary energy need for heating in kWh/m2, applying
        correction factors for generation losses, then multiplying by primary
        energy factor. Corrections for emission, regulation and distribution
        losses are considered previously applied.

        :param df: Dataframe which must contain at "Q_h[kWh/m2]" and
            "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :param gen_factor: correction factor for generation losses,
            in range [0,1].
        :type gen_factor: float, defaults to 1
        :param ep_factor: primary energy conversion factor,
            in range [0,1].
        :type ep_factor: float, defaults to 1
        :return: Primary energy need for heating
        :rtype: float
        """
        df.set_index("Date/Time", inplace=True, drop=True)

        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(self.generator_losses,
                                    args=(gen_factor, ))
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(self.convert_to_ep,
                                    args=(ep_factor, ))

        if timestep is not None:
            df = df.resample(timestep).sum()
            df["Date/Time"] = df.index
            q_h = {}
            for i in range(0,len(df)):
                q_h[str(df["Date/Time"][i])] = df["Q_h[kWh/m2]"][i]
        else:
            q_h = df["Q_h[kWh/m2]"].sum()
        return q_h


    def carpet_plot(self, df, variable, vmin=None, vmax=None, cmap="gist_rainbow_r", title=None, fontsize=60):
        """Generate a carpet plot

        :param df: DataFrame which must have datetime index and variable column.
        :type df: class:`pandas.core.frame.DataFrame`
        :param variable: variable name according to standardized nomenclature
        :type variable: str
        :param title: Title of the figure, defaults to None
        :type title: str, optional
        """
        # FIXME: Sometimes the carpet plot shows a white column.
        df = df[[variable]]
        df["hour"] = df.index.hour
        df["doy"] = [i.timetuple().tm_yday for i in df.index]
        plt.rcParams.update({
            "font.size": fontsize,
            "xtick.major.size": 40,
            "xtick.major.width": 6
        })
        fig, ax = plt.subplots(constrained_layout=True, figsize=(45, 30))
        ptab = df.pivot_table(index="hour", columns="doy", values=variable)
        sns.heatmap(ptab, cmap=cmap, cbar_kws={"label": variable},
                    ax=ax, vmin=vmin, vmax=vmax)
        plt.title(title)
        ax.invert_yaxis()
        ax.set_xticks(ax.get_xticks()[::3])
        ax.set_xlabel(None)
        ax.set_ylabel("Hour of Day")
        xticks = [str(int(t)+df["doy"][0]) for t in ax.get_xticks()]
        xlabels = []
        year = 1970

        for t in xticks:
            t = int(t)
            while t > 365:
                t -= 365
                year += 1
            t = str(t).rjust(3, '0')
            label = dt.datetime.strptime("-".join((str(year), t)), "%Y-%j").strftime("%b %d")
            xlabels.append(label)

        ax.set_xticklabels(xlabels)
        ax.tick_params(axis="x", rotation=50)
        ax.tick_params(axis="y", rotation=0)
        plt.savefig(self.plot_dir / ("carpet_plot_" + variable + ".png"))
        plt.close(fig)
        plt.rcParams.update(plt.rcParamsDefault)  # Reset rcParams

    def carrier_plot(
        self,
        range_temp_c=[0, 35],
        range_humidity_g_kg=[0, 25],
        altitude_m=0,
        data=None,
        suffix=""
    ):
        """Generate a Carrier psychrometric chart.

        :param range_temp_c: x-axis limits, defaults to [0, 35]
        :type range_temp_c: list, optional
        :param range_humidity_g_kg: y-axis limits, defaults to [0, 25]
        :type range_humidity_g_kg: list, optional
        :param altitude_m: Altitute in metres, used to perform calculations of
            absolute humidity, defaults to 0
        :type altitude_m: int, optional
        :param data: Points to plot on the chart, passed as a DataFrame
            containing dry_bulb_temperature and relative_humidity columns,
            defaults to None
        :type data:  class:`pandas.core.frame.DataFrame`, optional
        :param suffix: Suffix for title and filename, defaults to ""
        :type suffix: str, optional
        """
        custom_style = load_config("ashrae")
        custom_style.update(
            {
                "figure": {
                    "x_label": "Dry-Bulb Temp. $\mathregular{[°C]}$",
                    "y_label": "Humidity Ratio $\mathregular{[w, g_w / kg_{da}]}$",
                    "title": f"Psychrometric Chart ({suffix})"
                },
                "limits": {
                    "range_temp_c": range_temp_c,
                    "range_humidity_g_kg": range_humidity_g_kg,
                    "altitude_m": altitude_m,
                    "step_temp": 0.5,
                },
                "chart_params": {
                    "with_constant_rh": True,
                    "constant_rh_curves": [10, 25, 50, 75],
                    "constant_rh_labels": [10, 25, 50, 75],
                    "with_constant_v": False,
                    "with_constant_h": False,
                    "with_constant_wet_temp": False,
                    "with_zones": True,
                    "constant_h_step": 10,
                    "constant_humid_step": 5,
                    "constant_humid_label_step": 5,
                    "constant_temp_step": 35,
                    "constant_temp_label_step": 5,
                },
            }
        )
        givoni = {
            "zones": [
                {
                    "zone_type": "xy-points",
                    "style": {
                        "edgecolor": [0, 0.749, 0.0, 0.5],
                        "facecolor": [0, 0.749, 0.0, 0.3],
                        "linewidth": 0,#2,
                        "linestyle": "-",
                    },
                    "points_x": [20, 25, 27, 27, 20, 20],  # Temperature
                    "points_y": [12, 15, 12, 4, 4, 12],  # Absolute humidity
                }
            ]
        }
        polygon = Polygon(  # Temp. and absolute humidity
            [(20, 4), (20, 12), (25, 15), (27, 12), (27, 4), (20, 4)]
            )
        chart = PsychroChart(styles=custom_style, zones_file=givoni)
        plt.rcParams.update(plt.rcParamsDefault)  # Reset figsize
        plt.rcParams.update({"font.size": 12})
        fig, ax = plt.subplots()
        chart.plot(ax=ax)

        if data is not None:
            counter = 0
            for row, _ in data.iterrows():
                t = data.at[row, "dry_bulb_temperature"]
                rh = data.at[row, "relative_humidity"]
                mr = self.compute_mix_ratio(t, rh)
                if polygon.contains(Point(t, mr*1000)):
                    counter += 1
                    c = 'forestgreen'
                else:
                    c = 'dimgrey'

                points = {
                    "points_series_name": {
                        "style": {
                            "color": c,
                            "markersize": 1,
                            'alpha': 1
                        },  'marker':'.',
                        "xy": (t, rh),
                    }
                }
                chart.plot_points_dbt_rh(points)

        plt.savefig(self.plot_dir / f"carrier_{suffix}.png", dpi=600, facecolor="w")
        plt.close(fig)
        return {"Givoni": counter}

    def costo(self, sim_params):
        cost_list = []
        cost_matrix = {
            'Thickness': [0.01,0.02,0.03,0.04,0.05,0.06,0.07,0.08,0.09,0.10,0.12,0.14,0.16,0.18,0.20,0.22,0.24,0.26,0.28,0.30],
            'Cappotto 01.P09.A01': [47.64,48.72,49.69,50.74,51.81,52.86,53.88,54.91,55.89,56.98,59.03,61.13,83.17,85.13,87.31,89.36,91.41,93.51,95.61,97.71],
            'Cappotto 01.P09.A04': [47.97, 49.32, 50.66, 52,53.36,54.70,56.04,57.39,58.74,60.10,62.76,65.47,88.13,90.83,93.55,96.21,98.87,101.58,104.29,106.99],
            'Cappotto 01.P09.B02': [47.92,48.99,50.02,50.88,51.75,52.50,53.17,53.69,54.21,54.70,78.35,79.68,80.73,81.77,82.75,106.58,107.77,108.78,109.82,110.80],
            'Cappotto 01.P09.B12': [48.37,49.92,51.39,52.62,53.87,54.72,56.07,57.42,58.77,60.12,82.79,85.49,88.19,90.89,93.59,116.26,118.96,121.66,124.36,127.06],
            'Sottotetto 01.P09.A25': [9.42,11.57,13.72,15.86,18.00,20.13,22.28,24.43,26.57,28.73,40.26,44.55,48.86,53.15,57.46,68.99,73.28,77.59,81.88,86.19],
            'Sottotetto 01.P09.A52': [10.00,12.71,15.36,18.10,20.77,23.52,26.20,28.86,31.47,34.06,47.04,52.39,57.72,62.94,68.12,75.77,81.12,86.45,91.67,96.85],
            'Sottotetto 01.P09.B08': [9.69,12.12,15.57,15.68,19.04,21.40,23.73,26.10,28.45,30.81,42.80,47.46,52.20,56.90,61.62,71.53,76.19,80.93,85.63,90.35],
            'Sottotetto 01.P09.C10': [9.65,11.89,13.97,15.90,17.80,19.91,20.71,23.62,25.57,27.41,39.82,41.43,47.24,51.13,54.83,68.55,70.16,75.97,79.86,83.56],
            'Seminterrato 01.P09.A25': [60.82,62.97,65.12,67.26,69.40,71.53,73.68,75.83,77.97,80.13,101.37,105.66,109.97,114.26,118.57,139.81,144.10,148.41,152.70,157.01],
            'Seminterrato 01.P09.A52': [61.40,64.11,66.76,69.50,72.17,74.92,77.60,80.26,82.87,85.46,108.15,113.50,118.83,124.05,129.23,146.59,151.94,157.27,162.49,167.67],
            'Seminterrato 01.P09.B85': [61.09,63.52,64.97,67.08,70.44,72.80,75.13,77.50,79.85,82.21,103.91,108.57,113.31,118.01,122.73,142.35,147.01,151.75,156.45,161.17],
            'Seminterrato 01.P09.B05': [61.05,63.29,65.37,67.30,69.20,71.31,72.11,75.02,76.97,78.81,100.93,102.54,108.35,112.24,115.94,139.37,140.98,146.79,150.68,154.38],
            'Seminterrato 01.P09.A53': [85.70,89.84,92.20,95.93,99.68,103.40,101.88,110.66,114.19,117.58,125.82,122.78,140.34,147.40,154.18,162.42,169.38,176.94,184.00,190.78],
            'Seminterrato 03.P09.H06': [91.01,98.18,104.96,111.58,118.24,124.92,131.66,138.38,145.16,151.95,158.74,165.45,172.19,178.89,185.58,192.29,199.02,211.83,221.39,231.78],
            'plaster lime and gypsum 10 mm': [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        }
        cost_df = pd.DataFrame(cost_matrix)
        cost_df.set_index("Thickness", inplace=True)
        shad_cost = {
            "exterior": 126724,
        }
        dmv_cost = 60444.66
        win_dict = {
            "TPM 1": 744.30,
            "TPM 2": 833.23,
        }
        # cop_dict = {
        #     "Q_h_base": 0,
        #     "Q_h_split": None,      # TODO: Update
        #     "Q_h_elettrico": None,  # TODO: Update
        #     "Q_c_split": None       # TODO: Update
        # }

        for c in sim_params.index:

            if c == "add_blind.type":
                try:
                    cost_list.append(shad_cost[sim_params[c]])
                except KeyError:
                    cost_list.append(0)

            elif c == "add_mechanical_ventilation.deactivate_natural_ventilation":
                if sim_params[c] == None or np.isnan(sim_params[c]):
                    pass
                else:
                    cost_list.append(dmv_cost)

            elif c == "set_object_params_windows.Outside_Layer":
                try:
                    cost_list.append(win_dict[sim_params[c]] * 426.6)
                except KeyError:
                    cost_list.append(0)
            
            elif c == "add_external_insulation_floor_asilo.ins":
                thickness = sim_params["add_external_insulation_floor_asilo.Thickness"]
                material = sim_params[c]
                # print(f"Debug 1:\n {cost_df.loc[thickness, material]}___________\n\n")
                try:
                    cost_list.append(cost_df.loc[thickness, material] * 471.38)
                except KeyError:
                    cost_list.append(0)

            elif c == "add_external_insulation_floor.ins":
                thickness = sim_params["add_external_insulation_floor.Thickness"]
                material = sim_params[c]
                # print(f"Debug 2:\n {cost_df.loc[thickness, material]}___________\n\n")
                try:
                    cost_list.append(cost_df.loc[thickness, material] * 475.95)
                except KeyError:
                    cost_list.append(0)

            elif c == "add_external_insulation_walls.ins_data":
                thickness = sim_params["add_external_insulation_walls.Thickness"]
                if isinstance(sim_params[c], list):
                    material = sim_params[c][0]
                else:
                    material = sim_params[c]
                # print(f"Debug 3:\n {cost_df.loc[thickness, material]}___________\n\n")
                try:
                    cost_list.append(cost_df.loc[thickness, material] * 1029)
                except KeyError:
                    cost_list.append(0)

            # elif "Q_h" in c:
            #     cost_list.append(cop_dict[c])

            # elif "Q_c" in c and "Q_h" not in c:
            #     cost_list.append(cop_dict[c])

        return sum(cost_list)

    def initial_cost_torino(self, sim_params):
        cost_list = []
        cost_matrix = {
            'Thickness': [0.01,0.02,0.03,0.04,0.05,0.06,0.07,0.08,0.09,0.10,0.12,0.14,0.16,0.18,0.20,0.22,0.24,0.26,0.28,0.30],
            'Cappotto 01.P09.A01': [47.64,48.72,49.69,50.74,51.81,52.86,53.88,54.91,55.89,56.98,59.03,61.13,83.17,85.13,87.31,89.36,91.41,93.51,95.61,97.71],
            'Cappotto 01.P09.A04': [47.97, 49.32, 50.66, 52,53.36,54.70,56.04,57.39,58.74,60.10,62.76,65.47,88.13,90.83,93.55,96.21,98.87,101.58,104.29,106.99],
            'Cappotto 01.P09.B02': [47.92,48.99,50.02,50.88,51.75,52.50,53.17,53.69,54.21,54.70,78.35,79.68,80.73,81.77,82.75,106.58,107.77,108.78,109.82,110.80],
            'Cappotto 01.P09.B12': [48.37,49.92,51.39,52.62,53.87,54.72,56.07,57.42,58.77,60.12,82.79,85.49,88.19,90.89,93.59,116.26,118.96,121.66,124.36,127.06],
            'Sottotetto 01.P09.A25': [9.42,11.57,13.72,15.86,18.00,20.13,22.28,24.43,26.57,28.73,40.26,44.55,48.86,53.15,57.46,68.99,73.28,77.59,81.88,86.19],
            'Sottotetto 01.P09.A52': [10.00,12.71,15.36,18.10,20.77,23.52,26.20,28.86,31.47,34.06,47.04,52.39,57.72,62.94,68.12,75.77,81.12,86.45,91.67,96.85],
            'Sottotetto 01.P09.B08': [9.69,12.12,15.57,15.68,19.04,21.40,23.73,26.10,28.45,30.81,42.80,47.46,52.20,56.90,61.62,71.53,76.19,80.93,85.63,90.35],
            'Sottotetto 01.P09.C10': [9.65,11.89,13.97,15.90,17.80,19.91,20.71,23.62,25.57,27.41,39.82,41.43,47.24,51.13,54.83,68.55,70.16,75.97,79.86,83.56],
            'Seminterrato 01.P09.A52': [61.40,64.11,66.76,69.50,72.17,74.92,77.60,80.26,82.87,85.46,108.15,113.50,118.83,124.05,129.23,146.59,151.94,157.27,162.49,167.67],
            'Seminterrato 01.P09.B85': [61.09,63.52,64.97,67.08,70.44,72.80,75.13,77.50,79.85,82.21,103.91,108.57,113.31,118.01,122.73,142.35,147.01,151.75,156.45,161.17],
            'plaster lime and gypsum 10 mm': [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        }
        cost_df = pd.DataFrame(cost_matrix)
        cost_df.set_index("Thickness", inplace=True)
        shad_cost = {
            "exterior": 1181.83*238.9,
        }
        dmv_cost = 234230
        luci_scale = 1920
        luci_abitazioni = 29980
        win_dict = {
            "Torino 1": 744.30,
            "Torino 2": 776.57,
        }

        for c in sim_params.index:

            if c == "add_blind.type":
                try:
                    cost_list.append(shad_cost[sim_params[c]])
                except KeyError:
                    cost_list.append(0)

            elif c == "add_mechanical_ventilation.deactivate_natural_ventilation":
                if sim_params[c] == None or np.isnan(sim_params[c]):
                    pass
                else:
                    cost_list.append(dmv_cost)

            #TODO aggiustare and check
            elif c == "set_category_params_scale.filter_by":
                if sim_params[c] != "Stairs0":
                    pass
                else:
                    cost_list.append(luci_scale)
            #TODO aggiustare and check
            elif c == "set_category_params_abitazioni.filter_by":
                if sim_params[c] != "ff0":
                    pass
                else:
                    cost_list.append(luci_abitazioni)

            elif c == "set_object_params_windows.Outside_Layer":
                try:
                    cost_list.append(win_dict[sim_params[c]] * 470.5)
                except KeyError:
                    cost_list.append(0)

            elif c == "add_external_insulation_floor.ins":
                thickness = sim_params["add_external_insulation_floor.Thickness"]
                material = sim_params[c]
                # print(f"Debug 2:\n {cost_df.loc[thickness, material]}___________\n\n")
                try:
                    cost_list.append(cost_df.loc[thickness, material] * 383.2)
                except KeyError:
                    cost_list.append(0)

            elif c == "add_external_insulation_walls.ins_data":
                thickness = sim_params["add_external_insulation_walls.Thickness"]
                if isinstance(sim_params[c], list):
                    material = sim_params[c][0]
                else:
                    material = sim_params[c]
                # print(f"Debug 3:\n {cost_df.loc[thickness, material]}___________\n\n")
                try:
                    cost_list.append(cost_df.loc[thickness, material] * 1502.5)
                except KeyError:
                    cost_list.append(0)

            # elif "Q_h" in c:
            #     cost_list.append(cop_dict[c])

            # elif "Q_c" in c and "Q_h" not in c:
            #     cost_list.append(cop_dict[c])

        return sum(cost_list)
    
    # Timeseries functions 
    def timeseries_Q_c(self, df, timestep="1H", gen_factor=1, ep_factor=1):
        """Compute timeseries values (sum over the time period) of primary energy need for cooling in kWh/m2,
        applying correction factors for generation losses, then multiplying by primary energy factor.
        Corrections for emission, regulation and distribution losses are considered
        previously applied.

        :param df: Dataframe which must contain at "Q_c[kWh/m2]" and "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :param gen_factor: correction factor for generation losses,
            in range [0,1].
        :type gen_factor: float, defaults to 1
        :param ep_factor: primary energy conversion factor,
            in range [0,1].
        :type ep_factor: float, defaults to 1
        :return: DataFrame of cooling primary energy values over time
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        df.set_index("Date/Time", inplace=True, drop=True)
        df = df.resample(timestep).sum()
        df["Date/Time"] = df.index
        df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(self.generator_losses,\
                                    args=(gen_factor, ))
        df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(self.convert_to_ep,\
                                    args=(ep_factor, ))        
        return df["Q_c[kWh/m2]"]

    def timeseries_Q_h(self, df, timestep="1H", gen_factor=1, ep_factor=1,variable=None):
        """Compute timeseries values (sum over the time period) of primary energy need for heating in kWh/m2,
        applying correction factors for generation losses, then multiplying by primary energy factor.
        Corrections for emission, regulation and distribution losses are considered
        previously applied.

        :param df: Dataframe which must contain at "Q_h[kWh/m2]" and "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :param gen_factor: correction factor for generation losses,
            in range [0,1].
        :type gen_factor: float, defaults to 1
        :param ep_factor: primary energy conversion factor,
            in range [0,1].
        :type ep_factor: float, defaults to 1
        :return: DataFrame of heating primary energy values over time
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        df.set_index("Date/Time", inplace=True, drop=True)

        df = df.resample(timestep).sum()
        df["Date/Time"] = df.index
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(self.generator_losses,\
                                    args=(gen_factor, ))
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(self.convert_to_ep,\
                                    args=(ep_factor, ))

        if False:
            plt.rcParams.update({"font.size": 45})
            fig, ax = plt.subplots(figsize=(18, 15))
            ax.plot(df["Q_h[kWh/m2]"], color="red", linewidth=6)
            plt.grid()
            plt.title("Heating")
            plt.ylim((0, 30))
            plt.ylabel(r'Energy Need $\left[\mathrm{W/m^3}\right]$')
            ax.xaxis.set_major_formatter(mdates.DateFormatter('%b %d')) 
            fig.autofmt_xdate(rotation=45)
            plt.tight_layout()
            plt.savefig(self.plot_dir / (f"timeseries_Q_h.png"))

        return df["Q_h[kWh/m2]"]

    def timeseries_t_db_i(self, df, timestep="1H", graph=False):
        """Compute timeseries of indoor drybulb temperature according to
        requested time aggregation (average on the time period).
       
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :param df: Dataframe which must contain at least CO2 concentration 
                indoor "T_db_i[C]" and "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame of indoor co2 values over time
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        df = df.set_index("Date/Time")
        data = df["T_db_i[C]"].resample(timestep).mean()
        if graph:
            plt.rcParams.update({"font.size": 45})
            fig, ax = plt.subplots(figsize=(18, 15))
            ax.plot(df["T_db_i[C]"], color="red", linewidth=6)
            plt.grid()
            plt.title("Drybulb Temp.")
            plt.ylim((15, 45))
            plt.ylabel('[°C]')
            ax.xaxis.set_major_formatter(mdates.DateFormatter('%b %d')) 
            fig.autofmt_xdate(rotation=45)
            plt.tight_layout()
            plt.savefig(self.plot_dir / (f"timeseries_t_db_i.png"))       
        return data

    def timeseries_t_db_o(self, df, timestep="1H"):
        """Compute timeseries of indoor drybulb temperature according to
        requested time aggregation (average on the time period).
       
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :param df: Dataframe which must contain at least CO2 concentration 
                indoor "T_db_i[C]" and "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame of indoor co2 values over time
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        df = df.set_index("Date/Time")
        data = df["T_db_o[C]"].resample(timestep).mean()
        return data
    
    def timeseries_co2(self, df, timestep="1H"):
        """Compute timeseries of indoor CO2 concentration (ppm) according to
        requested time aggregation (average on the time period).
       
        :param timestep: time aggregation required
        :type timestep: str, defaults to = "1H"
        :param df: Dataframe which must contain at least CO2 concentration 
                indoor "CO2_i[ppm]" and "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame of indoor co2 values over time
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        df = df.set_index("Date/Time")
        data = df["CO2_i[ppm]"].resample(timestep).mean()
        return data

    @staticmethod
    def compute_wbt(s):
        """Compute wet-bulb temperature from dry-bulb temperature and relative
        humidity.

        :param s: Series which must contain outdoor drybulb temperature values
            "T_db_o[C]" and relative humidity "RH_o[%]".
        :type s: class:`pandas.core.series.Series` 
        :return: Wet-bulb temperature
        :rtype: float
        """
        t_db = s["T_db_o[C]"]
        rh = s["RH_o[%]"]
        t_wb_o = (
            t_db * np.arctan(0.151977 * np.sqrt(rh + 8.313659))
            + np.arctan(t_db + rh)
            - np.arctan(rh - 1.676331)
            + 0.00391838 * np.sqrt(rh ** 3) * np.arctan(0.023101 * rh)
            - 4.686035
        ) - 4.686035
        return t_wb_o

    @staticmethod
    def compute_ttreat_pdec(s, eff):
        """Compute wet-bulb Temperature from dry-bulb temperature and relative
        humidity.

        :param s: Series which must contain at least outdoor drybulb
            temperature values "T_db_o[C]", relative humidity "RH_o[%]" and
            "Date/Time" columns.
        :type s: class:`pandas.core.series.Series` 
        :param timestep: Time resample, defaults to "1H"
        :type timestep: str, optional
        :return: []
        :rtype: []
        """
        t_db = s["T_db_o[C]"]
        t_wb = s["T_wb_o[C]"]
        ttreat = t_db - eff*(t_db - t_wb)
        return ttreat

    @staticmethod
    # TODO: Fix output, keep tuple?
    def compute_ttreat_eahx(s, var1, var2, var3, depth, diffus, eff):
        """Compute wet-bulb Temperature from dry-bulb temperature and relative
        humidity.

        :param s: Series which must contain at least outdoor drybulb
            temperature values "T_db_o[C]", relative humidity "RH_o[%]" and
            "Date/Time" columns.
        :type s: class:`pandas.core.series.Series` 
        :param timestep: Time resample, defaults to "1H"
        :type timestep: str, optional
        :return: []
        :rtype: []
        """
        t_db = s["T_db_o[C]"]
        diffusH = diffus*3600
        var3H=(var3-1)*24+3
        t0 = 365*24
        con = mydatetime.hours_of_year(s.name)
        soilT=var1-var2*math.exp(-depth*(((math.pi)/(t0*diffusH))**(0.5)))*math.cos(((2*math.pi)/t0)*(
                                                    con-var3H-(depth/2)*(t0/(math.pi*diffusH))**(0.5)))
        ttreat = t_db - eff*(t_db - soilT)
        return soilT, ttreat

    def timeseries_wbt(self, df, timestep="1H"):
        """Compute timeseries of outdoor wetbulb temperature according to
        requested time aggregation (average on the time period).

        :param df: Dataframe which must contain at least outdoor drybulb
            temperature values "T_db_o[C]", relative humidity "RH_o[%]" and
            "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`    
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"

        :return: DataFrame of outdoor wetbulb temperature values over time
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        df = df.set_index("Date/Time")
        df = df.resample(timestep).mean()
        df["T_wb_o[C]"] = df[["T_db_o[C]", "RH_o[%]"]].apply(self.compute_wbt, axis=1)

        # TODO: Move check into compute_wbt() function and set invalide values
        #       to NaNs.
        df["check"] = df[["T_db_o[C]", "RH_o[%]"]].apply(self.check_wbt, axis=1)
        df.to_csv(self.plot_dir / "test_check.csv", sep=";")
        plt.rcParams.update({"font.size": 45})
        _, ax = plt.subplots(figsize=(18, 15), constrained_layout=True)
        df[df["check"]==True].plot(x="T_db_o[C]", y="RH_o[%]", marker="x",
                                    color="grey", ls="None", ax=ax)
        valid = df[df["check"]==False]

        # generate wbt plot with stull validity ranges
        plt.scatter(valid["T_db_o[C]"], valid["RH_o[%]"], c=valid["T_wb_o[C]"], label="WBT")
        plt.grid()
        plt.xlabel("Dry-Bulb Temp. [°C]")
        plt.ylabel("Relative Humidity [%]")
        plt.legend()
        ax.get_legend().remove()
        plt.xticks([-20, -10, 0, 10, 20, 30, 40, 50])
        plt.yticks([0, 20, 40, 60, 80, 100])
        xlims  = ax.get_xlim()
        plt.plot([xlims[0], 9, xlims[1]], [-70/29*(xlims[0]) + 775/29, 5, 5], color="black")
        plt.fill(
            [xlims[0], 9, xlims[1], xlims[1], xlims[0]],
            [-70/29*(xlims[0]) + 775/29, 5, 5, 0, 0],
            color="lightgray", alpha=0.4
            )
        cbar = plt.colorbar(shrink=0.6)
        cbar.set_label("Wet-Bulb Temp. [°C]")
        plt.title("Wet-Bulb Temperature")
        plt.xlim(xlims)
        plt.ylim([0, 100])
        plt.savefig(self.plot_dir / "timeseries_wbt.png")
        plt.close()

        return df["T_wb_o[C]"]

    @staticmethod
    def check_wbt(p):
        # Line passing through (-20, 75) and (9, 5).
        line = lambda x: -70/29*x + 775/29

        if p["T_db_o[C]"] >= 9:
            if p["RH_o[%]"] >= 5:
                return False
            else:
                return True
        else:
            return p["RH_o[%]"] - line(p["T_db_o[C]"]) < 0

    def timeseries_wbd(self, df, timestep="1H"):
        """Compute timeseries of wetbulb depression according to
        requested time aggregation (average on the time period).
       
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :param df: Dataframe which must contain at least outdoor drybulb temperature
                values "T_db_o[C]", relative humidity "RH_o[%]"
                and "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame of wetbulb depression values over time
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        df = df.set_index("Date/Time")
        df = df.resample(timestep).mean()
        df["T_wb_o[C]"] = df[["T_db_o[C]", "RH_o[%]"]].apply(self.compute_wbt, axis=1)
        df["WBD[C]"] = df["T_db_o[C]"] - df["T_wb_o[C]"]
        return df["WBD[C]"]

    def wbd_averages(self, df, timestep="1M", start=None, end=None):
        #TODO change description
        """Compute timeseries of wetbulb depression according to
        requested time aggregation (average on the time period).
    
        :param df: Dataframe which must contain at least outdoor drybulb temperature
                values "T_db_o[C]", relative humidity "RH_o[%]"
                and "Date/Time" columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1M"
        :param start: optional parameter, format "%d-%m"
        :type start: str, defaults to None
        :param end: optional parameter, format "%d-%m"
        :type end: str, defaults to None
        :return: DataFrame of wetbulb depression values over time
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        df = df.set_index("Date/Time")
        df["T_wb_o[C]"] = df[["T_db_o[C]", "RH_o[%]"]].apply(
            self.compute_wbt, axis=1
        )
        df["WBD[C]"] = df["T_db_o[C]"] - df["T_wb_o[C]"]
        wbd_dict = {}
        
        if start == None and end == None:
            df = df.resample(timestep).mean()
            for i in range(0,len(df)):
                wbd_dict[df.index[i]] = df["WBD[C]"].iloc[i]

            wbd_dict = {"wbd_values": df["WBD[C]"].values}
        
        elif start != None and end != None:
            df = df.resample("1H").mean()
            start_day = int(start.split("-")[0])
            start_month = int(start.split("-")[1])
            end_day = int(end.split("-")[0])
            end_month = int(end.split("-")[1])
            year = df.index[0].year
            start_date = dt.datetime(year, int(start_month), int(start_day))
            end_date = dt.datetime(year, int(end_month), int(end_day))
            df = df[df.index > start_date]
            df = df[df.index < end_date]
            wbd_dict[start + " - " + end] = df["WBD[C]"].dropna().mean()
        
        return wbd_dict

    def timeseries_rh_from_mix_ratio(self, df, timestep="1H"):
        """Compute timeseries of relative humidity [%] starting from 
        mixed ratio vapour quality [kg/kg] according
        to requested time aggregation (average on the time period).
       
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :param df: Dataframe which must contain at least  drybulb temperature
            values "T_db[C]", vapour quality "mix_ratio[kg/kg]" and "Date/Time"
            columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame of relative humidity [%]
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        df = df.set_index("Date/Time")
        df = df.resample(timestep).mean()
        p_atm = 101325
        #df["ro"] = [353.118/t for t in (df["T_db"] + 273.15)]
        pdin = 0  # TODO: in futuro sarà 0.5*ro*v2
        p = p_atm + pdin
        pvs_list = []
        for t in df["T_db[C]"]:
            try:
                if -40 < t <= 0:
                    A = 22.376
                    B = 271.68
                    C = 6.4146
                elif 0 < t < 40:
                    A = 17.438
                    B = 239.78
                    C = 6.4147
                else:
                    raise ValueError("Invalid temperature range for mix ratio computation")
                pvs_list.append(np.exp((A*t)/(B+t)+C))
            except ValueError as e:
                if not np.isnan(t):
                    print(e + ": " + str(t))
                pvs_list.append(float("nan"))

        df["pvs"] = pvs_list
        # TODO test formula, forse il * tra due colonne non funzionerà
        df["rh_from_mix_raio"] = df["mix_ratio[kg/kg]"]*p/(0.622*df["pvs"] + df["mix_ratio[kg/kg]"]*df["pvs"])

        return df["rh_from_mix_raio"]       

    @staticmethod
    def compute_mix_ratio(t, rh):
        p_atm = 101325
        #df["ro"] = [353.118/t for t in (df["T_db"] + 273.15)]
        pdin = 0  # TODO: in futuro sarà 0.5*ro*v2
        p = p_atm + pdin
        try:
            if -40 < t <= 0:
                A = 22.376
                B = 271.68
                C = 6.4146
            elif 0 < t < 40:
                A = 17.438
                B = 239.78
                C = 6.4147
            else:
                raise ValueError("Invalid temperature range for mix ratio computation")
            pvs = np.exp((A*t)/(B+t)+C)
        except ValueError as e:
            if not np.isnan(t):
                pass
                # print(e + ": " + str(t))
            pvs = float("nan")

        pv = rh/100 * pvs
        return 0.622*(pv/(p-pv))


    def timeseries_mix_ratio(self, df, where, timestep="1H"):
        """Compute timeseries of mixed ratio vapour quality [kg/kg] according
        to requested time aggregation (average on the time period).
       
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :param df: Dataframe which must contain at least  drybulb temperature
            values "T_db[C]", relative humidity "RH[%]" and "Date/Time"
            columns.
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame of mixed ratio vapour quality values over time
            [kg/kg]
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        df = df.set_index("Date/Time")
        df = df.resample(timestep).mean()
        p_atm = 101325
        #df["ro"] = [353.118/t for t in (df["T_db"] + 273.15)]
        pdin = 0  # TODO: in futuro sarà 0.5*ro*v2
        p = p_atm + pdin
        pvs_list = []
        for t in df["T_db[C]"]:
            try:
                if -40 < t <= 0:
                    A = 22.376
                    B = 271.68
                    C = 6.4146
                elif 0 < t < 40:
                    A = 17.438
                    B = 239.78
                    C = 6.4147
                else:
                    raise ValueError("Invalid temperature range for mix ratio computation")
                pvs_list.append(np.exp((A*t)/(B+t)+C))
            except ValueError as e:
                if not np.isnan(t):
                    print(e + ": " + str(t))
                pvs_list.append(float("nan"))

        df["pvs"] = pvs_list
        df["pv"] = df["RH[%]"]/100 * df["pvs"]
        df["mix_ratio_" + where  + "[kg/kg]"] = 0.622*(df["pv"]/(p-df["pv"]))

        if self.graph:
            plt.rcParams.update({"font.size": 45})
            fig, ax = plt.subplots(figsize=(18, 15))
            df["mix_ratio_" + where  + "[kg/kg]"].plot(color="grey", ax=ax)
            plt.grid()
            plt.title(
                {"i": "Interior", "o": "Exterior"}[where]
                + " Mixed Ratio Vapour Quality"
            )
            plt.ylabel("Mixed Ratio [kg/kg]")
            fig.autofmt_xdate(rotation=45)
            plt.tight_layout()
            plt.savefig(self.plot_dir / ("timeseries_mix_ratio_" + where + ".png"))

        return df["mix_ratio_" + where + "[kg/kg]"]

    def timeseries_t_op(self, df, season = None, timestep="1H",filter_by=""):
        """_summary_

        :param df: _description_
        :type df: _type_
        :param season: _description_, defaults to None
        :type season: _type_, optional
        :param filter_by: _description_, defaults to ""
        :type filter_by: str, optional
        :return: _description_
        :rtype: _type_
        """
        df = df.set_index("Date/Time")
        df = df.resample(timestep).mean()
        
        if season is not None:
            df = df.where(df[season+"_season"] != 0).dropna()
        # print(df)
        return df["T_op_i[C]"]        

    def t_op(self, df, season = None, timestep="1H", filter_by=""):
        """_summary_

        :param df: _description_
        :type df: _type_
        :param season: _description_, defaults to None
        :type season: _type_, optional
        :param filter_by: _description_, defaults to ""
        :type filter_by: str, optional
        :return: _description_
        :rtype: _type_
        """
        df = df.set_index("Date/Time")
        df = df.resample(timestep).mean()
        
        if season is not None:
            df = df.where(df[season+"_season"] != 0).dropna()

        return df["T_op_i[C]"].mean()

    def ach_tot(self, df, timestep="1H", filter_by=""):
        """_summary_

        :param df: _description_
        :type df: _type_
        :param season: _description_, defaults to None
        :type season: _type_, optional
        :param filter_by: _description_, defaults to ""
        :type filter_by: str, optional
        :return: _description_
        :rtype: _type_
        """
        df = df.set_index("Date/Time")
        # df = df.resample(timestep).sum()
        # assert False
        return df["Airflow_i[m3]"].sum()

    def timeseries_acm_hourly_cat(self, df, eu_norm='16798-1:2019', alpha=0.8,
                                  filter_by_occupancy=0, when={}, title=None,
                                  suffix="", axfmt='%b %d'):
        """Compute hourly ACM category according to different European
        normatives. Filters according to occupancy or dates can be applied. 
        ACM category is expressed in terms of distance from comfort central
        line. 

        :param df: dataframe which should contain "Date/Time" column in format
            'year/month/day hour:minutes:seconds', "T_db_o[C]" preferably with
            a subhourly timestep and "T_op_i[C]". Optional "Occupancy" column
            accepting only 0/1 values.
        :type df: class:`pandas.core.frame.DataFrame`
        :param eu_norm: It can be set to '15251:2007' if old UE norm 
            computation is desired, defaults to '16798-1:2019'.
        :type eu_norm: str, optional
        :param alpha: With old UE norm '15251:2007 alpha is a free parameter in
            range [0,1), defaults to 0.8
        :type alpha: float, optional
        :param filter_by_occupancy: It can be set 0 or 1, depending on wether
            activate occupancy filtering on thermal comfort KPIs computation or
            not, defaults to 0.
        :type filter_by_occupancy: int, optional
        :param when: dictionary with 'start' and 'end' keys and values in
            format 'year/month/day hour:minutes:seconds'
        :type when: dict, optional
        :return: Dataframe containing "Date/Time" and "dist" columns.
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        df = self.compute_acm(df, eu_norm, alpha, filter_by_occupancy, when)
        

        df_middle = df.where((df["T_rm"] >= 10) & (df["T_rm"] <= 33)).dropna()
        df_up =  df.where(df["T_rm"] > 33).dropna()
        df_down =  df.where(df["T_rm"] < 10).dropna()

        # Compute distances from comfort line and comfort+3 line
        df_middle["dist"] = df_middle["T_op_i[C]"]-\
                            (0.33*df_middle["T_rm"]+18.8)
        df_up["dist"] = df_up["T_op_i[C]"] - (0.33*33+18.8)
        df_down["dist"] = df_down["T_op_i[C]"] - (0.33*10+18.8)
        
        df =  df_middle.append(df_up).append(df_down)
        # df = pd.concat((df_middle, df_up, df_down), axis=0)
        try:
            df = df.set_index("Date/Time")
        except KeyError:
            pass
        df = df.sort_index()
        data = df["dist"]
        if self.graph:
            plt.rcParams.update({"font.size": 45})
            fig, ax = plt.subplots(figsize=(18, 15))
            ax.plot(data, color="midnightblue", linewidth=6)
            plt.grid()
            if title is not None:
                plt.title(title)
            else:
                plt.title("Hourly Adaptive Comfort Model")
            plt.ylabel("Distance from central line [°C]")

            plt.axhline(y=0, color='red', ls='-', lw=2)

            plt.axhline(y=2, color='red', ls='-', lw=2)
            plt.axhline(y=-3, color='red', ls='-', lw=2)
            

            plt.axhline(y=3, color='red', ls='-', lw=2)
            plt.axhline(y=-4, color='red', ls='-', lw=2)

            plt.axhline(y=4, color='red', ls='-', lw=2)
            plt.axhline(y=-5, color='red', ls='-', lw=2)

            # Dark sage: #598556
            # Light sage: #bcecac
            xlims = plt.gca().get_xlim()
            ylims = [-8, 8]
            plt.fill_between(x=xlims, y1=-3, y2=2, color="#598556", alpha=0.25)  # old: "#e5c89c"
            plt.fill_between(xlims, y1=2, y2=3, color="#598556", alpha=0.15)
            plt.fill_between(xlims, y1=-3, y2=-4, color="#598556", alpha=0.15)
            plt.fill_between(xlims, y1=3, y2=4, color="peachpuff", alpha=0.6)
            plt.fill_between(xlims, y1=-4, y2=-5, color="peachpuff", alpha=0.6)
            plt.fill_between(xlims, y1=4, y2=20, color="peachpuff", alpha=0.9)
            plt.fill_between(xlims, y1=-5, y2=-20, color="peachpuff", alpha=0.9)
            plt.xlim(xlims)
            plt.ylim(ylims)

            ax.set_yticks(
                ticks=[-5, -4, -3, 0, 2, 3, 4]
                )
            ax2 = ax.twinx()
            ax2.set_ylabel("Category boundaries")
            ax2.set_yticks(
                ticks=[-5, -4, -3, 2, 3, 4],
                labels=["III", "II", "I", "I", "II", "III"],
                fontname="serif"
                )
            ax2.set_ylim(ylims)
            ax.xaxis.set_major_formatter(mdates.DateFormatter(axfmt)) 
            fig.autofmt_xdate(rotation=45)
            plt.tight_layout()
            plt.savefig(self.plot_dir / (f"hourly_acm_dist_{suffix}.png"))
        # print("acm data:\n", data)
        # print("self.df:\n", self.df)
        return data 

    def timeseries_pmv_ppd(self, df, vel=0.1, met=1.2, clo=0.5, wme=0, 
                    filter_by_occupancy=0, standard="ISO 7730-2006", season=None):
        """Return Predicted Mean Vote (PMV) and Predicted Percentage of
        Dissatisfied (PPD) calculated in accordance to ISO 7730-2006 standard.

        :param df: dataframe containing at least "Date/Time",
            "T_db_i[C]", "T_rad_i[C]" and "RH_i[%]" columns.
            Optional "Occupancy column" accepting only 0 and 1 values.
        :type df: class:`pandas.core.frame.DataFrame`
        :param vel: relative air speed, defaults 0.1
        :type vel: float, optional
        :param met: metabolic rate, [met] defaults 1.2
        :type met: float, optional
        :param clo: clothing insulation, [clo] defaults 0.5
        :type clo: float, optional
        :param wme: external work, [met] defaults 0
        :type wme: float, optional
        :param standard: Currently unused, defaults to "ISO 7730-2006"
        :type standard: str, optional
        :param filter_by_occupancy: It can be set 0 or 1, depending on wether
            activate occupancy filtering on thermal comfort KPIs computation or
            not, defaults to 0.
        :type filter_by_occupancy: int, optional
        :return: dictionary containing keys "POR" (Percentage Outside the Range) and
            ""No_h_discomfort", computed as percentage and number of hours in which
            PMV is above or below 0.7 and so PPD is above around 20%.
        :rtype: dict
        """
        df = self.compute_pmv_ppd(df, vel, met, clo, wme)
        # add season filter
        if filter_by_occupancy:
            df = df.where(df["Occupancy"] != 0).dropna()

        if season is not None:
            df = df.where(df[season+"_season"] != 0).dropna()
        # print(df)
        # return df.loc[:, ["pmv", "ppd"]]
        return df["ppd"]

    def correlations(self, df, x, y, degree=None, err_meas="r2", split_mod=None):

        #frame = df[[x, y]]
        xname = x
        yname = y
        df = df.dropna().copy()
        x = df[x].tolist()
        y = df[y].tolist()
        final_res = {}
        
        # Split DF
        if split_mod is not None:
            if split_mod == "default":
                # TODO 70-30 mia maniera (1 week consecutiva ma scelta casualmente per mese)
                mondays = [i for i in df.index.weekday]
        else:
            pass
        
        def grade_1(x, y):
            grade_1_model = sm.OLS(y, sm.add_constant(x))
            grade_1_results = grade_1_model.fit()

            m_1 = round(grade_1_results.params[1],2)
            q_1 = round(grade_1_results.params[0],2)

            equation = "y={}x+{}".format(m_1, q_1)

            summary = {
                "err": round(grade_1_results.rsquared,2),
                "eq": equation
                }

            return summary

        def grade_2(x, y):

            polynomial_features= PolynomialFeatures(degree=2)
            xp = polynomial_features.fit_transform(sm.add_constant(x))

            grade_2_model = sm.OLS(y, xp)

            grade_2_results = grade_2_model.fit()

            m_2 = round(grade_2_results.params[2],2)
            m_1 = round(grade_2_results.params[1],2)
            q_1 = round(grade_2_results.params[0],2)

            equation = "y={}x2+{}x+{}".format(m_2, m_1, q_1)

            summary = {
                "err": round(grade_2_results.rsquared,2),
                "eq": equation
                }
                    
            return summary

        def grade_3(x, y):
            polynomial_features= PolynomialFeatures(degree=3)
            xp = polynomial_features.fit_transform(sm.add_constant(x))

            grade_3_model = sm.OLS(y, xp)

            grade_3_results = grade_3_model.fit()

            m_3 = round(grade_3_results.params[3],2)
            m_2 = round(grade_3_results.params[2],2)
            m_1 = round(grade_3_results.params[1],2)
            q_1 = round(grade_3_results.params[0],2)

            equation = "y={}x3+{}x2+{}x+{}".format(m_3,m_2, m_1, q_1)

            summary = {
                "err": round(grade_3_results.rsquared,2),
                "eq": equation
                }
           
            return summary

        def grade_4(x, y):
            polynomial_features= PolynomialFeatures(degree=4)
            xp = polynomial_features.fit_transform(sm.add_constant(x))

            grade_4_model = sm.OLS(y, xp)

            grade_4_results = grade_4_model.fit()

            m_4 = round(grade_4_results.params[4],2)
            m_3 = round(grade_4_results.params[3],2)
            m_2 = round(grade_4_results.params[2],2)
            m_1 = round(grade_4_results.params[1],2)
            q_1 = round(grade_4_results.params[0],2)

            equation = "y={}x4+{}x3+{}x2+{}x+{}".format(m_4,m_3,m_2, m_1, q_1)

            summary = {
                "err": round(grade_4_results.rsquared,2),
                "eq": equation
                }
                
            return summary

        def grade_5(x, y):
            polynomial_features= PolynomialFeatures(degree=5)
            xp = polynomial_features.fit_transform(sm.add_constant(x))

            grade_5_model = sm.OLS(y, xp)

            grade_5_results = grade_5_model.fit()

            m_5 = round(grade_5_results.params[5],2)
            m_4 = round(grade_5_results.params[4],2)
            m_3 = round(grade_5_results.params[3],2)
            m_2 = round(grade_5_results.params[2],2)
            m_1 = round(grade_5_results.params[1],2)
            q_1 = round(grade_5_results.params[0],2)

            equation = "y={}x5+{}x4+{}x3+{}x2+{}x+{}".format(m_5,m_4,m_3,m_2, m_1, q_1)

            summary = {
                "err": round(grade_5_results.rsquared,2),
                "eq": equation
                }
                
            return summary

        if not isinstance(degree, list):
            if degree is None:
                degree = [1,2,3,4,5]
            else:
                degree = [degree]

        for d in degree:
            if d == 1:
                res = grade_1(x, y)
            elif d == 2:
                res = grade_2(x, y)
            elif d == 3:
                res = grade_3(x, y)
            elif d == 4:
                res = grade_4(x, y)
            elif d == 5:
                res = grade_5(x, y)
            else:
                res = None

            final_res["degree_"+str(d)] = res

            data = pd.DataFrame({"x": x, "y": y})
            plt.rcParams.update({"font.size": 45})
            _, ax = plt.subplots(figsize=(15, 15), constrained_layout=True)
            sns.regplot(x="x", y="y", data=data, ax=ax,
                line_kws={"color": "black", "linewidth": 7, "alpha": 0.7},
                scatter_kws={"alpha": 0.5},
                order=d
            )
            plt.title(f"Order {d}")
            ax.set_xlabel(xname)
            ax.set_ylabel(yname)
            plt.grid()
            plt.savefig(self.plot_dir / f"grade{d}.png")
        
        return final_res

    @staticmethod
    def compute_pmv_ppd(df, vel=0.1, met=1.2, clo=0.7, wme=0,
                        standard="ISO 7730-2006") -> pd.DataFrame():
        """Return Predicted Mean Vote (PMV) and Predicted Percentage of
        Dissatisfied (PPD) calculated in accordance to ISO 7730-2006 standard.

        :param df: dataframe containing at least "Date/Time",
            "T_db_i[C]", "T_rad_i[C]" and "RH_i[%]" columns.
            Optional "Occupancy column" accepting only 0 and 1 values.
        :type df: class:`pandas.core.frame.DataFrame`
        :param vel: relative air speed, defaults 0.1
        :type vel: float, optional
        :param met: metabolic rate, [met] defaults 1.2
        :type met: float, optional
        :param clo: clothing insulation, [clo] defaults 0.5
        :type clo: float, optional
        :param wme: external work, [met] defaults 0
        :type wme: float, optional
        :param standard: Currentl unused, defaults to "ISO 7730-2006"
        :type standard: str, optional
        :param filter_by_occupancy: It can be set 0 or 1, depending on wether
            activate occupancy filtering on thermal comfort KPIs computation or
            not, defaults to 0.
        :type filter_by_occupancy: int, optional
        :return: DataFrame containing PMV and PPD hourly values.
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        df = df.resample("1H").mean()
        rh = np.array(list(df["RH_i[%]"]))
        ta = np.array(list(df["T_db_i[C]"]))
        tr = np.array(list(df["T_rad_i[C]"]))
        pmv = []
        ppd = []

        fnps = np.exp(16.6536 - 4030.183 / (ta + 235)) # water vapor pressure in ambient air kPa
        pa = rh * 10 * fnps # partial water vapor pressure in ambient air 	
        icl = 0.155 * clo  # thermal insulation of the clothing in M2K/W
        m = met * 58.15  # metabolic rate in W/M2
        w = wme * 58.15  # external work in W/M2
        mw = m - w  # internal heat production in the human body

        # ratio of clothed body surface over total body surface
        if icl <= 0.078:
            fcl = 1 + (1.29 * icl)  
        else:
            fcl = 1.05 + (0.645 * icl)

        # heat transfer coefficient by forced convection
        hcf = 12.1 * np.sqrt(vel)

        # temperatures in Kelvin
        taa = ta + 273 
        tra = tr + 273 

        # iterative computation of clothing surface temperature
        tcla = taa + (35.5 - ta) / (3.5*icl + 0.1) # first tempt

        p1 = icl * fcl
        p2 = p1 * 3.96
        p3 = p1 * 100
        p4 = p1 * taa
        p5 = (308.7 - 0.028*mw) + (p2*(tra/100.0)**4)
        xn = tcla / 100
        xf = tcla / 50
        # end criterion
        eps = 0.00015
        for i in range (0,len(df)):
            n = 0 # number of iterations
            while abs(xn[i] - xf[i]) > eps:
                xf[i] = (xf[i] + xn[i]) / 2
                # heat transfer coefficient for natural convection
                hcn = 2.38 * abs(100.0*xf[i] - taa[i])**0.25
                if hcf > hcn:
                    hc = hcf                
                else:
                    hc = hcn
                xn[i] = (p5[i] + p4[i]*hc - p2*xf[i]**4) / (100 + p3*hc)
                n += 1
                if n > 150:
                    pmv.append(np.inf)
                    ppd.append(100)

            # clothing surface temperature      
            tcl = 100*xn[i] - 273

            # heat loss diff. through skin
            hl1 = 3.05*0.001*(5733 - (6.99*mw) - pa[i])
            # heat loss by sweating
            if mw > 58.15:
                hl2 = 0.42 * (mw - 58.15)
            else:
                hl2 = 0
            # latent respiration heat loss
            hl3 = 1.7 * 0.00001 * m * (5867 - pa[i])
            # dry respiration heat loss
            hl4 = 0.0014 * m * (34 - ta[i])
            # heat loss by radiation
            hl5 = 3.96 * fcl * (xn[i] ** 4 - (tra[i] / 100.0) ** 4)
            # heat loss by convection
            hl6 = fcl * hc * (tcl - ta[i])
            # conversion coefficient of thermal sensation
            ts = 0.303 * np.exp(-0.036 * m) + 0.028

            # final formulas
            pmv.append(round(ts * (mw-hl1-hl2-hl3-hl4-hl5-hl6), 1))
            ppd.append(int(100.0 - 95.0 * np.exp(-0.03353 * pow(pmv[-1],\
                                                 4.0) - 0.2179 * pow(pmv[-1],\
                                                                     2.0))))

        # to return list of all values
        df["ppd"] = ppd
        df["pmv"] = pmv
        # my_dict = {"pmv": pmv, "ppd": ppd}
        # new_df = pd.DataFrame(my_dict, index=df.index)
        # return my_dict

        return df

    @staticmethod
    def compute_acm(df, eu_norm='16798-1:2019', alpha=0.8,
                    filter_by_occupancy=0,when={}) -> pd.DataFrame():
        """Compute running mean according to different European normatives.
        Filters according to occupancy or dates can be applied.

        :param df: dataframe should contain "Date/Time" column in format
            'year/month/day hour:minutes:seconds', "T_db_o[C]" preferably with a
            subhourly timestep and "T_op_i[C]". Optional "Occupancy" column
            accepting only 0/1 values.
        :type df: class:`pandas.core.frame.DataFrame`
        :param eu_norm: It can be set to '15251:2007' if old UE norm 
            computation is desired, defaults to '16798-1:2019'.
        :type eu_norm: str, optional
        :param alpha: With old UE norm '15251:2007 alpha is a free parameter in
            range [0,1), defaults to 0.8
        :type alpha: float, optional
        :param filter_by_occupancy: It can be set 0 or 1, depending on wether
            activate occupancy filtering on thermal comfort KPIs computation
            or not, defaults to 0.
        :type filter_by_occupancy: int, optional
        :param when: dictionary with 'start' and 'end' keys and values in format 'year/month/day hour:minutes:seconds'
        :type when: dict, optional
        :return: Dataframe containing new 'T_rm' variable for the considered time period.
        :rtype: class:`pandas.core.frame.DataFrame`
        """

        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()
        df = df.reset_index()   
        col_index = df.columns.get_loc("T_db_o[C]")  # Deprecated
        # col_index = df.columns.get_indexer(["T_db_o[C]"])[0]

        # just in case passed with t air --> handle better
        df = df.rename(columns={"T_db_i[C]":"T_op_i[C]"})

        if eu_norm == '16798-1:2019':
            for i in range(7*24,df.shape[0]):
                df.loc[i,"T_rm"] = (df.iloc[i-1*24:i,col_index].mean() +\
                                            0.8*df.iloc[i-2*24:i-1*24,\
                                                col_index].mean() +\
                                            0.6*df.iloc[i-3*24:i-2*24,\
                                                col_index].mean() +\
                                            0.5*df.iloc[i-4*24:i-3*24,\
                                                col_index].mean() +\
                                            0.4*df.iloc[i-5*24:i-4*24,\
                                                col_index].mean() +\
                                            0.3*df.iloc[i-6*24:i-5*24,\
                                                col_index].mean() +\
                                            0.2*df.iloc[i-7*24:i-6*24,\
                                                col_index].mean())/3.8

        if eu_norm == '15251:2007':
            # TODO still to be tested (verificare che funzioni e che abbia anche senso)
            # set first day running means
            for i in range(1*24,2*24):
                df.loc[i,"T_rm"] = (1-alpha)*df.iloc[i-1*24:i,col_index].mean()

            for i in range(2*24+1,df.shape[0]):
                df.loc[i,"T_rm"] = (1-alpha)*(df.iloc[i-1*24:i,\
                                              col_index].mean()) +\
                                              alpha*df.iloc[i-1*24,-1]

        if filter_by_occupancy:
            df = df.where(df["Occupancy"] != 0).dropna()

        if "start" in when.keys() and "end" in when.keys():
            df = df.set_index("Date/Time")
            start = when["start"]
            end=when["end"]
            start = dt.datetime.strptime(start, "%Y/%m/%d %H:%M:%S")
            end = dt.datetime.strptime(end, "%Y/%m/%d %H:%M:%S")
            start_idx = df.index.get_indexer([start], method='nearest')[0]
            end_idx = df.index.get_indexer([end], method='nearest')[0]
            df = df.iloc[start_idx:end_idx+1, :]

        return df

    @staticmethod
    def emission_losses(value,em_factor = 1):
        """Apply emission losses correction factor.
        
        :param em_factor: emission factor, in range [0,1]
        :type em_factor: float, defaults to 1
        :return: consumption value with correction applied
        :rtype: float
        """
        return value/em_factor

    @staticmethod
    def regulation_losses(value,reg_factor = 1):
        """Apply regulation losses correction factor.
        
        :param reg_factor: regulation factor, in range [0,1]
        :type reg_factor: float, defaults to 1
        :return: consumption value with correction applied
        :rtype: float
        """
        return value/reg_factor

    @staticmethod
    def distribution_losses(value,distr_factor = 1):
        """Apply distribution losses correction factor.
        
        :param distr_factor: distribution factor, in range [0,1]
        :type distr_factor: float, defaults to 1
        :return: consumption value with correction applied
        :rtype: float
        """
        return value/distr_factor

    @staticmethod
    def generator_losses(value,gen_factor = 1):
        """Apply generation losses correction factor.
        
        :param gen_factor: generation factor, in range [0,1]
        :type gen_factor: float, defaults to 1
        :return: consumption value with correction applied
        :rtype: float
        """
        return value/gen_factor

    @staticmethod
    def convert_to_ep(value,ep_factor = 1):
        """Apply primary energy conversion factor.
        
        :param ep_factor: primary energy factor, in range [0,1]
        :type ep_factor: float, defaults to 1
        :return: consumption converted to primary energy
        :rtype: float
        """
        return value*ep_factor


class EnergyPlusKPI(KPI):
    """Class which computes KPIs for EnergyPlus.
    It extends :class:`predyce.kpi.KPI` by preprocessing the eplusout.csv
    generated by EnergyPlus."""

    def __init__(self, df, idf, plot_dir, start_date=None, end_date=None, graph=False, area=None):
        """
        :param df: DataFrame of EnergyPlus outputs
        :type df: class:`pandas.core.frame.DataFrame`
        :param idf: IDF object
        :type idf: class:`predyce.IDF_class.IDF`
        :param plot_dir: Directory where to save plots
        :type plot_dir: str or Path
        :param graph: `True` to save plots, defaults to `False`
        :type graph: bool, optional
        """
        super().__init__(plot_dir, graph)
        self.df = df.copy()
        self.df.columns = [c.replace(':ON', '') for c in self.df.columns]
        self.building_name = idf.main_block
        if area:
            self.area = area
        else:
            self.area = idf.area
        self.volume = idf.volume
        self.idf = idf

        if len(idf.idfobjects["RUNPERIOD"]) > 0:
            if start_date:
                start_year = mdt.strptime(start_date, "%d-%m-%Y").year
            else:
                start_year = 1970
            end_year = start_year
            epw_df = epw_reader.retrieve_column(self.idf.epw, year=start_year)
            # print("[i] retrieving EPW data (single-year)")
        else:
            rpcr = idf.idfobjects["RUNPERIOD:CUSTOMRANGE"][0]
            start_year = int(rpcr.Begin_Year)
            end_year = int(rpcr.End_Year)
            # print(start_year, end_year)
            epw_df = epw_reader.retrieve_column(self.idf.epw)
            # print("[i] retrieving EPW data (multi-year)")

        self.df["Date/Time"] = self.df["Date/Time"].apply(self.convert_to_datetime, args=(start_year, ))
        if start_year != end_year:
            # self.df.to_csv(plot_dir / "out-pre.csv", sep=";")
            current_year = self.df["Date/Time"][0].year
            # print("start year:", current_year)
            for i, r in enumerate(self.df["Date/Time"]):
                if r.month == 1 and r.day == 1 and r.hour == 0 and r.minute == 0: # and end_day_check:
                    current_year +=1
                    # print("change year:", current_year)
                # print(self.df["Date/Time"].iloc[i],r)
                self.df["Date/Time"].iloc[i] = r.replace(year=current_year)

            if (end_year != current_year):
                raise(Exception, "Wrong year assignment, check KPI function")
            

        self.df.set_index("Date/Time", inplace=True, drop=True)
        var_df = self.df[[c for c in self.df.columns if "site" in c.lower()]]
        self.df.drop(columns=[c for c in self.df.columns if "site" in c.lower()], inplace=True)

        dates = self.df.index

        if start_date is not None:
            if len(start_date.split("-"))==3:
                start_date = mdt.strptime(start_date, "%d-%m-%Y")
            else:
                start_date = mdt.strptime(start_date + "-1970", "%d-%m-%Y")
            
            dates = dates[dates >= start_date]

        if end_date is not None:
            if len(end_date.split("-"))==3:
                end_date = mdt.strptime(end_date, "%d-%m-%Y")
            else:
                end_date = mdt.strptime(end_date + "-1970", "%d-%m-%Y")
            dates = dates[dates <= end_date]

        if start_date is not None or end_date is not None:
            self.df = self.df.loc[dates]

        self.df = self.df.join(var_df, how="outer")
        self.df = self.df.join(epw_df, how="left")
        self.df["Date/Time"] = self.df.index

        # NOTE: Fix for AAU.
        self.df = self.df.rename(columns={"DistrictHeating:Facility [J](TimeStep) ": "DistrictHeating:Facility [J](TimeStep)"})

        self.df.to_csv(plot_dir / "out.csv", sep=";")

    def correlations(self, x, y, timestep = "1H", degree = None, err_meas = "Combined",\
                     split_mod = None, filter_by = "", orientation = None, nv=None):
        """_summary_
        """
        
        df= copy.deepcopy(self.df)
        # y axis possibilities
        if y == "T_op_i[C]":
            df = self.mean_indoor_op_air_temp(df, self.building_name, filter_by) 
        elif y == "Airflow_i[L/s]":
            pass
        elif y == "Ill_i[lux]":
            pass

        # x axis possibilities
        if x == "T_db_o[C]":
            df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        elif x == "v_air[m/s]":
            df = df.rename(columns={"wind_speed":"v_air[m/s]"})
        elif x == "GHI_o[W/m2]":
            df = df.rename(columns={"global_horizontal_radiation":"GHI_o[W/m2]"})
        elif x == "deltaT_inv":
            df = self.indoor_mean_air_temp(df, self.building_name, filter_by)
            df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
            df["deltaT_inv"] = 1/(df["T_db_i[C]"]-df["T_db_o[C]"])

        if nv is not None:
            df = self.mean_vent_ach(df,self.building_name, filter_by)
            # print(set(df["Airflow_i[L/s]"]))
            # TODO conversione in litri al secondo in mean_vent_ach
            if nv == 0:               
                df = df[df["Airflow_i[L/s]"] == 0].copy()
            elif nv == 1:
                df = df[df["Airflow_i[L/s]"] != 0].copy()              
        else:
            pass
        
        df = df.loc[:,["Date/Time", x, y]]
        # TODO sarà semmpre da fare la media o in alcuni casi ci sarà la sum?
        df= df.resample(timestep).mean()
        return super().correlations(df,x,y, degree, err_meas, split_mod)

    def cdh(self, thresholds=[18, 21, 24, 26, 28]):
        """Prepare eplosout.csv to standardized cooling degree hours
        computation.

        :param thresholds: thresholds from which compute residuals, defaults to
            [18, 21, 24, 26, 28]
        :type thresholds: list, optional
        :return: Cooling degree hours with respect to each specified threshold
        :rtype: dict
        """
        df= copy.deepcopy(self.df)
        df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        df = df.loc[:,["Date/Time", "T_db_o[C]"]]
        return super().cdh(df,thresholds=thresholds)

    def cdh_res_pdec(self, t_b=24, eff=0.8, with_control=True):
        """Prepare eplosout.csv to standardized PDEC CDH_res
        computation.

        :param t_b: base temperature, default 24
        :type t_b: float, optional
        :param with_control: wether to sum positive values only when control 
            condition is satisfied, default True
        :type with_control: bool, optional
        :return: PDEC CDH_res with respect to specified base temperature
        :rtype: float
        """
        df= copy.deepcopy(self.df)
        df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        df = df.rename(columns={"relative_humidity":"RH_o[%]"})
        df = df.loc[:,["Date/Time", "T_db_o[C]", "RH_o[%]"]]
        return super().cdh_res_pdec(df, t_b = t_b, eff = eff, with_control = with_control)

    def cdh_res_eahx(self, var1, var2, var3, soil_condition= 2, soil_surf_cond = 2, diffus=6.177*(10**(-7)), depth=2.5, eff=0.52, t_b=24, with_control=True):
        """Prepare eplosout.csv to standardized EAHX CDH_res computation.
        
        :param soil_condition: it can be 1. HeavyAndSaturated, 2. HeavyAndDamp, 3. HeavyAndDry or 4. LightAndDry, defaults to None
        :type soil_condition: int, optional
        :param soil_surf_cond: it can be 1. BARE AND WET, 2. BARE AND MOIST, 3. BARE AND ARID, 4. BARE AND DRY, 
            5. COVERED AND WET, 6. COVERED AND MOIST, 7. COVERED AND ARID and 8. COVERED AND DRY, defaults to None
        :type soil_surf_cond: int, optional
        :param var1: average soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var1: float
        :param var2: amplitude of soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var2: float
        :param var3: phase of soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var3: float
        :param depth: soil depth of EAHX tubes in meters
        :type depth: float
        :param diffus: soil thermal diffusity
        :type diffus: float
        :param eff: EAHX efficiency, defaults to 0.8
        :type eff: float, optional
        :param t_b: base temperature, defaults to 24
        :type t_b: float, optional
        :param with_control: wether to sum positive values only when control 
            condition is satisfied, default True
        :type with_control: bool, optional
        :return: EAHX CDH_res with respect to specified base temperature
        :rtype: float
        """
        df= copy.deepcopy(self.df)
        df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        df = df.loc[:,["Date/Time", "T_db_o[C]"]]
        return super().cdh_res_eahx(df, var1=var1, var2=var2, var3=var3, diffus = diffus,\
                                depth=depth, eff= eff, t_b = t_b, with_control = with_control)

    def hdh_res_eahx(self, soil_condition, soil_surf_cond, var1, var2, var3, depth, diffus, eff=0.8, t_b=24, with_control=True):
        """Prepare eplosout.csv to standardized EAHX HDH_res computation.
        
        :param soil_condition: it can be 1. HeavyAndSaturated, 2. HeavyAndDamp, 3. HeavyAndDry or 4. LightAndDry, defaults to None
        :type soil_condition: int, optional
        :param soil_surf_cond: it can be 1. BARE AND WET, 2. BARE AND MOIST, 3. BARE AND ARID, 4. BARE AND DRY, 
            5. COVERED AND WET, 6. COVERED AND MOIST, 7. COVERED AND ARID and 8. COVERED AND DRY, defaults to None
        :type soil_surf_cond: int, optional
        :param var1: average soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var1: float
        :param var2: amplitude of soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var2: float
        :param var3: phase of soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var3: float
        :param depth: soil depth of EAHX tubes in meters
        :type depth: float
        :param diffus: soil thermal diffusity
        :type diffus: float
        :param eff: EAHX efficiency, defaults to 0.8
        :type eff: float, optional
        :param t_b: base temperature, defaults to 24
        :type t_b: float, optional
        :param with_control: wether to sum positive values only when control 
            condition is satisfied, default True
        :type with_control: bool, optional
        :return: EAHX HDH_res with respect to specified base temperature
        :rtype: float
        """
        df= copy.deepcopy(self.df)
        df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        df = df.loc[:,["Date/Time", "T_db_o[C]"]]
        return super().hdh_res_eahx(df, var1=var1, var2=var2, var3=var3, diffus = diffus,\
                                depth=depth, eff= eff, t_b = t_b, with_control = with_control)                   

    def hdh(self, thresholds=[18, 20, 22]):
        """Prepare eplosout.csv to standardized heating degree hours
        computation.

        :param thresholds: thresholds from which compute residuals, defaults to
            [18, 20, 22]
        :type thresholds: list, optional
        :return: Heating degree hours with respect to each specified threshold
        :rtype: dict
        """
        df= copy.deepcopy(self.df)
        df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        df = df.loc[:,["Date/Time", "T_db_o[C]"]]
        return super().hdh(df,thresholds=thresholds)

    def timeseries_n_fr(self, season = None, variable= None, filter_by_occupancy = 0):
        """Prepare eplosout.csv to standardized number of hours above
        consumption thresholds computation.

        :param season: it can be Heating or Cooling
        :type season: str, optional
        :param variable: name of cooling/heating consumption variable, by default district is considered
        :type variable: str, optional
        :param filter_by_occupancy: 
        :type filter_by_occupancy: int, optional
        :return: N. of free running hours in defined season
        :rtype: int
        """
        
        df = copy.deepcopy(self.df)
        df.set_index("Date/Time", inplace=True, drop=True)
        # df = df.resample("1H").sum()   

        if season.upper() == "heating".upper():
            
            if variable is None:
                df = df.rename(columns={"DistrictHeating:Facility [J](TimeStep)": "Q_h[kWh/m2]"})
            
            else:
                df_new = df.copy()
                for col in df_new.columns:
                    if variable not in col or self.building_name.upper() not in col:
                        df_new = df_new.drop(col, axis=1)
                df_new = df_new.dropna()
                df["Q_h[kWh/m2]"] = df_new.sum(axis=1)

            df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(self.convert_to_kWh)  
            if filter_by_occupancy:
                df = self.get_occupancy(df, self.building_name)
            
            
            try:
                df = df.loc[:,["Q_h[kWh/m2]","Occupancy"]]
                df = df.resample("1H").agg({"Q_h[kWh/m2]": "sum",
                                            "Occupancy": "mean"})
                df["Date/Time"] = df.index             
                return super().timeseries_n_fr(df, season, filter_by_occupancy)
            except:
                df = df.loc[:,["Q_h[kWh/m2]"]]
                df = df.where(df["Q_h[kWh/m2]"] != np.nan).dropna()
                df = df.resample("1H").sum()
                df["Date/Time"] = df.index
                return super().timeseries_n_fr(df, season, filter_by_occupancy)

        elif season.upper() == "cooling".upper():
            
            if variable is None:
                df = df.rename(columns={"DistrictCooling:Facility [J](TimeStep)": "Q_c[kWh/m2]"})
            
            else:
                df_new = df.copy()
                for col in df_new.columns:
                    if variable not in col or self.building_name.upper() not in col:
                        df_new = df_new.drop(col, axis=1)
                df_new = df_new.dropna()                
                df["Q_c[kWh/m2]"] = df_new.sum(axis=1)
           
            df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(self.convert_to_kWh)  
            df["Date/Time"] = df.index
            
            if filter_by_occupancy:
                df = self.get_occupancy(df, self.building_name)
            df = df.dropna()
            try:
                df = df.loc[:,["Q_c[kWh/m2]","Occupancy"]]
                df = df.resample("1H").agg({"Q_c[kWh/m2]": "sum",
                                            "Occupancy": "mean"})
                df["Date/Time"] = df.index
                return super().timeseries_n_fr(df, season, filter_by_occupancy)
            except:
                df = df.loc[:,["Q_c[kWh/m2]"]]
                df = df.resample("1H").sum()
                df["Date/Time"] = df.index
                return super().timeseries_n_fr(df, season, filter_by_occupancy)

        else:
            raise("Please provide a correct season name between Heating or Cooling")

    def n_fr(self, season = None, variable= None, filter_by_occupancy = 0):
        """Prepare eplosout.csv to standardized number of hours above
        consumption thresholds computation.

        :param season: it can be Heating or Cooling
        :type season: str, optional
        :param variable: name of cooling/heating consumption variable, by default district is considered
        :type variable: str, optional
        :param filter_by_occupancy: 
        :type filter_by_occupancy: int, optional
        :return: N. of free running hours in defined season
        :rtype: int
        """
        
        df = copy.deepcopy(self.df)
        df.set_index("Date/Time", inplace=True, drop=True)      

        if season.upper() == "heating".upper():
            
            if variable is None:
                df = df.rename(columns={"DistrictHeating:Facility [J](TimeStep)": "Q_h[kWh/m2]"})
            
            else:
                df_new = df.copy()
                for col in df_new.columns:
                    if variable not in col or self.building_name.upper() not in col:
                        df_new = df_new.drop(col, axis=1)
                df_new = df_new.dropna()
                df["Q_h[kWh/m2]"] = df_new.sum(axis=1)

            df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(self.convert_to_kWh)  
            if filter_by_occupancy:
                df = self.get_occupancy(df, self.building_name)
            # df = df.dropna()
            try:
                df = df.loc[:,["Q_h[kWh/m2]","Occupancy"]]
                df = df.resample("1H").agg({"Q_h[kWh/m2]": "sum",
                                            "Occupancy": "mean"})
                df["Date/Time"] = df.index
                return super().n_fr(df, season, filter_by_occupancy)
            except:
                df = df.loc[:,["Q_h[kWh/m2]"]]
                df = df.where(df["Q_h[kWh/m2]"] != np.nan).dropna()
                df = df.resample("1H").sum()
                df["Date/Time"] = df.index
                return super().n_fr(df, season, filter_by_occupancy)

        elif season.upper() == "cooling".upper():
            
            if variable is None:
                df = df.rename(columns={"DistrictCooling:Facility [J](TimeStep)": "Q_c[kWh/m2]"})
            
            else:
                df_new = df.copy()
                for col in df_new.columns:
                    if variable not in col or self.building_name.upper() not in col:
                        df_new = df_new.drop(col, axis=1)
                df_new = df_new.dropna()
                df["Q_c[kWh/m2]"] = df_new.sum(axis=1)
           
            df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(self.convert_to_kWh)  
            df["Date/Time"] = df.index
            
            if filter_by_occupancy:
                df = self.get_occupancy(df, self.building_name)
            df = df.dropna()
            try:
                df = df.loc[:,["Q_c[kWh/m2]","Occupancy"]]
                df = df.resample("1H").agg({"Q_c[kWh/m2]": "sum",
                                            "Occupancy": "mean"})
                df["Date/Time"] = df.index
                return super().n_fr(df, season, filter_by_occupancy)
            except:
                df = df.loc[:,["Q_c[kWh/m2]"]]
                df = df.resample("1H").sum()
                df["Date/Time"] = df.index
                return super().n_fr(df.loc[:,["Date/Time", "Q_c[kWh/m2]"]], season, filter_by_occupancy)

        else:
            raise("Please provide a correct season name between Heating or Cooling")

    def n_h_kwh(self, th_list=[0, 0.6, 1], cop_cool=3, cop_heat=0.3):
        """Prepare eplosout.csv to standardized number of hours above
        consumption thresholds computation.

        :param th_list: List of thresholds to be compared with consumption,
            defaults to [0,0.6,1]
        :type th_list: list, optional
        :param cop_cool: Cop value for cooling system, defaults to 3
        :type cop_cool: int, optional
        :param cop_heat: Cop value for heating system, defaults to 0.3
        :type cop_heat: float, optional
        :return: Dictionary containing no. of hours above defined thresholds
                for heating and cooling.
        :rtype: dict
        """
        df= copy.deepcopy(self.df)
        df = df.rename(columns={"DistrictCooling:Facility [J](TimeStep)": "Q_c[Wh/m2]"})
        df = df.rename(columns={"DistrictHeating:Facility [J](TimeStep)": "Q_h[Wh/m2]"})
        df["Q_c[Wh/m2]"] = df["Q_c[Wh/m2]"].apply(self.convert_to_kWh) 
        df["Q_h[Wh/m2]"] = df["Q_h[Wh/m2]"].apply(self.convert_to_kWh)                                    
        df = df.loc[:,["Date/Time", "Q_c[Wh/m2]", "Q_h[Wh/m2]"]]
        return super().n_h_kwh(df,th_list = th_list, cop_cool = cop_cool, cop_heat = cop_heat)

    def cdd(self):
        """Prepare eplosout.csv to standardized Cooling Degree Days computation.

        :return: Cooling degree day
        :rtype: float
        """
        df= copy.deepcopy(self.df)
        df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        df = df.loc[:,["Date/Time", "T_db_o[C]"]]
        return super().cdd(df)

    def hdd(self):
        """Prepare eplosout.csv to standardized Heating Degree Days computation.

        :return: Heating degree day
        :rtype: float
        """
        df= copy.deepcopy(self.df)
        df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        df = df.loc[:,["Date/Time", "T_db_o[C]"]]
        return super().hdd(df)
 
    def cdd_res(self, tset=26, ef=0.5):
        """Prepare eplosout.csv to standardized Cooling Degree Days residuals computation.

        :param tset: Cooling system setpoint, defaults to 26
        :type tset: int, optional
        :param ef: Free parameter, defaults to 0.5
        :type ef: float, optional
        :return: Cooling Degree Days residuals
        :rtype: int
        """
        #TODO far si che il cooling setpoint non venga dato come parametro ma preso in automatico dalla schedule
        df= copy.deepcopy(self.df)
        df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        df = df.loc[:,["Date/Time", "T_db_o[C]"]]
        return super().cdd_res(df,tset=tset,ef=ef)  
        
    def ccp(self):
        """Prepare eplosout.csv to standardized climate cooling potential
        computation.

        :return: Dictionary containing CCP and CCPd
        :rtype: dict
        """
        df= copy.deepcopy(self.df)
        df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        df = df.loc[:,["Date/Time", "T_db_o[C]"]]
        delta_T = self.idf.mean_dt
        return super().ccp(df,delta_T=delta_T) #

    def qach(self, pair=1.2, cair=0.278, tset=26):
        """Prepare eplosout.csv to standardized climate heat gain dissipation
        potential of ventilative cooling, Qach. 

        :param pair: Air density in kg/m, defaults to 1.2
        :type pair: float, optional
        :param cair: Air specific heat capacity in W/kg°C, defaults
            to 0.278
        :type cair: float, optional
        :param tset: Setpoint temperature in °C, defaults to 26
        :type tset: int, optional
        :return: Qach
        :rtype: float
        """
        # TODO far si che anche il tset possa essere preso dalla cooling schedule
        df= copy.deepcopy(self.df)
        df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        df = df.loc[:,["Date/Time", "T_db_o[C]"]]
        delta_T = self.idf.mean_dt
        ach = self.idf.mean_ach
        return super().qach(df, vol=self.volume, pair=pair, cair=cair,
                            tset=tset, delta_T=delta_T, ach=ach)

    def qc_pdec(self, A, vair, pair=1.2, cair=0.278, delta_T=2):
        """Prepare eplosout.csv to standardized climate heat gain dissipation
        potential of ventilative cooling, Qc_PDEC. 

        :param A: pad area
        :type A: float
        :param vair: flow rate
        :type vair: float
        :param pair: Air density in kg/m, defaults to 1.2
        :type pair: float, optional
        :param cair: Air specific heat capacity in W/kg°C, defaults
            to 0.278
        :type cair: float, optional
        :return: Qc_PDEC
        :rtype: float
        """
        df= copy.deepcopy(self.df)
        df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        df = df.rename(columns={"relative_humidity":"RH_o[%]"})
        df = df.loc[:,["Date/Time", "T_db_o[C]","RH_o[%]"]]
        return super().qc_pdec(df, A = A, vair=vair, pair=pair, cair=cair, delta_T=delta_T)

    def qc_eahx(self, A, vair, var1, var2, var3, soil_condition= 2, soil_surf_cond = 2, pair=1.2,\
         cair=0.278, diffus=6.177*(10**(-7)), depth=2.5, eff=0.52):
        """Prepare eplosout.csv to standardized climate heat gain dissipation
        potential of ventilative cooling, Qc_EAHX. 

        :param A: tube area
        :type A: float
        :param vair: flow rate
        :type vair: float
        :param soil_condition: it can be 1. HeavyAndSaturated, 2. HeavyAndDamp, 3. HeavyAndDry or 4. LightAndDry, defaults to None
        :type soil_condition: int, optional
        :param soil_surf_cond: it can be 1. BARE AND WET, 2. BARE AND MOIST, 3. BARE AND ARID, 4. BARE AND DRY, 
            5. COVERED AND WET, 6. COVERED AND MOIST, 7. COVERED AND ARID and 8. COVERED AND DRY, defaults to None
        :type soil_surf_cond: int, optional
        :param var1: average soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var1: float
        :param var2: amplitude of soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var2: float
        :param var3: phase of soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var3: float
        :param pair: Air density in kg/m, defaults to 1.2
        :type pair: float, optional
        :param cair: Air specific heat capacity in W/kg°C, defaults
            to 0.278
        :type cair: float, optional
        :param depth: soil depth of EAHX tubes in meters
        :type depth: float
        :param diffus: soil thermal diffusity
        :type diffus: float
        :param eff: EAHX efficiency, defaults to 0.52
        :type eff: float, optional
        :return: Qc_EAHX
        :rtype: float
        """
        df= copy.deepcopy(self.df)
        df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        df = df.loc[:,["Date/Time", "T_db_o[C]"]]
        return super().qc_eahx(df, A=A, vair=vair, var1=var1, var2=var2, var3=var3, pair=pair,\
         cair=cair, diffus=diffus, depth=depth, eff=eff)

    def qh_eahx(self, A, vair, var1, var2, var3, soil_condition= 2, soil_surf_cond = 2, pair=1.2,\
         cair=0.278, diffus=6.177*(10**(-7)), depth=2.5, eff=0.52, delta_T=2):
        """Prepare eplosout.csv to standardized climate potential 
        of ventilative heating, Qh_EAHX. 

        :param A: tube area
        :type A: float
        :param vair: flow rate
        :type vair: float
        :param soil_condition: it can be 1. HeavyAndSaturated, 2. HeavyAndDamp, 3. HeavyAndDry or 4. LightAndDry, defaults to None
        :type soil_condition: int, optional
        :param soil_surf_cond: it can be 1. BARE AND WET, 2. BARE AND MOIST, 3. BARE AND ARID, 4. BARE AND DRY, 
            5. COVERED AND WET, 6. COVERED AND MOIST, 7. COVERED AND ARID and 8. COVERED AND DRY, defaults to None
        :type soil_surf_cond: int, optional
        :param var1: average soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var1: float
        :param var2: amplitude of soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var2: float
        :param var3: phase of soil surface temperature (computed inside ColcSoilSurfTemp.exe)
        :type var3: float
        :param pair: Air density in kg/m, defaults to 1.2
        :type pair: float, optional
        :param cair: Air specific heat capacity in W/kg°C, defaults
            to 0.278
        :type cair: float, optional
        :param depth: soil depth of EAHX tubes in meters
        :type depth: float
        :param diffus: soil thermal diffusity
        :type diffus: float
        :param eff: EAHX efficiency, defaults to 0.52
        :type eff: float, optional
        :return: Qh_EAHX
        :rtype: float
        """
        df= copy.deepcopy(self.df)
        df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        df = df.loc[:,["Date/Time", "T_db_o[C]"]]
        return super().qh_eahx(df, A=A, vair=vair, var1=var1, var2=var2, var3=var3, pair=pair,\
         cair=cair, diffus=diffus, depth=depth, eff=eff)

    def cidh(self, thresholds=[18, 20, 22, 24, 26, 28]):
        """Prepare eplusout.csv to standardized CIDH (Cooling Internal Degree
        Hours) computation and finally return sum of residuals from defined
        thresholds.

        :param thresholds: temperature thresholds from which compute residuals,
         defaults to [18, 20, 22, 24, 26, 28]
        :type thresholds: list, optional
        :return: dictionary containing sum of residuals from thresholds
        :rtype: dict
        """

        df= copy.deepcopy(self.df)
        df = self.indoor_mean_air_temp(df, self.building_name)
        df = df.loc[:,["Date/Time", "T_db_i[C]"]]
        return super().cidh(df, thresholds=thresholds) # Call original function

    def hidh(self, thresholds=[18, 20, 22]):
        """Prepare eplusout.csv to standardized HIDH (Heating Internal Degree
        Hours) computation and finally return sum of residuals from defined
        thresholds.

        :param thresholds: temperature thresholds from which compute residuals,
         defaults to [18, 20, 22]
        :type thresholds: list, optional
        :return: dictionary containing sum of residuals from thresholds
        :rtype: dict
        """

        df= copy.deepcopy(self.df)
        df = self.indoor_mean_air_temp(df, self.building_name)
        df = df.loc[:,["Date/Time", "T_db_i[C]"]]
        return super().hidh(df, thresholds=thresholds) # Call original function

    def wbd_averages(self, timestep="1M", start = None, end = None):
        #TODO change description
        """Prepare eplosout.csv to compute timeseries wetbulb depression according to
        requested time aggregation (average on the time period).
        
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :return: DataFrame of wetbulb depression values over time
        :rtype: pd.DataFrame()
        """
        df = copy.deepcopy(self.df)
        df = df.set_index("Date/Time", drop=False)
        dates = df["Date/Time"]
        df_weather = df.copy()
        df_weather = df_weather.loc[dates.index]
        df_weather = df_weather.rename(columns={"dry_bulb_temperature": "T_db_o[C]"})
        df_weather = df_weather.rename(columns={"relative_humidity": "RH_o[%]"})
        df_weather = df_weather.loc[:, ["Date/Time", "T_db_o[C]", "RH_o[%]" ]]
        return super().wbd_averages(df_weather, timestep=timestep, start= start, end=end)

    def fict_cool(self, dataframe_on, eu_norm='16798-1:2019',
                           alpha=0.8):
        """Return fictitious cooling computed as the amount of cooling
        consumption needed in a building without mechanic system in order to
        reach category I comfort of adaptive comfort model. It also prepares
        eplusout.csv to standard KPI computation.

        :param dataframe_on: dataframe containing at least "Date/Time" and 
            "Q_c[kWh/m2]" columns, of the same building with 
            HVAC installad and ACH ventilation set to 0.
        :type df: class:`pandas.core.frame.DataFrame`
        :param eu_norm: It can be set to '15251:2007' if old UE norm 
            computation is desired, defaults to '16798-1:2019'.
        :type eu_norm: str, optional
        :param alpha: With old UE norm '15251:2007 alpha is a free parameter in
            range [0,1), defaults to 0.8
        :type alpha: float, optional
        :return: fictitious cooling total
        :rtype: float
        """
        df_off = copy.deepcopy(self.df)
        df_on = copy.deepcopy(dataframe_on)
        df_on.columns = [c.replace(':ON', '') for c in df_on.columns]
        df_on = df_on.rename(columns={"DistrictCooling:Facility [J](TimeStep)": "Q_c[kWh/m2]"})
        df_on["Q_c[kWh/m2]"] = df_on["Q_c[kWh/m2]"].apply(
            self.convert_to_kWh_m2, args=(self.area, )
        )
        df_on = df_on.loc[:, ["Date/Time", "Q_c[kWh/m2]"]]      
        df_off = self.mean_indoor_op_air_temp(df_off, self.building_name)
        df_off = df_off.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        df_off = df_off.loc[:,["Date/Time", "T_op_i[C]","T_db_o[C]"]]
        return super().fict_cool(df_off, df_on, eu_norm=eu_norm, alpha=alpha)

    def fict_heat(self, dataframe_on, eu_norm='16798-1:2019',
                           alpha=0.8):
        """Return fictitious heating computed as the amount of heating
        consumption needed in a building without mechanic system in order to
        reach category I comfort of adaptive comfort model. It also prepares
        eplusout.csv to standard KPI computation.

        :param dataframe_on: dataframe containing at least "Date/Time" and 
            "Q_h[kWh/m2]" columns, of the same building with 
            HVAC installad and ACH ventilation set to 0.
        :type df: class:`pandas.core.frame.DataFrame`
        :param eu_norm: It can be set to '15251:2007' if old UE norm 
            computation is desired, defaults to '16798-1:2019'.
        :type eu_norm: str, optional
        :param alpha: With old UE norm '15251:2007 alpha is a free parameter in
            range [0,1), defaults to 0.8
        :type alpha: float, optional
        :return: fictitious heating total
        :rtype: float
        """
        df_off = copy.deepcopy(self.df)
        df_on = copy.deepcopy(dataframe_on)
        df_on.columns = [c.replace(':ON', '') for c in df_on.columns]
        df_on = df_on.rename(columns={"DistrictHeating:Facility [J](TimeStep)": "Q_h[kWh/m2]"})
        df_on["Q_h[kWh/m2]"] = df_on["Q_h[kWh/m2]"].apply(self.convert_to_kWh_m2,\
                                                        args=(self.area, ))   
        df_on = df_on.loc[:,["Date/Time","Q_h[kWh/m2]"]]      
        df_off = self.mean_indoor_op_air_temp(df_off, self.building_name)
        df_off = df_off.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        df_off = df_off.loc[:,["Date/Time", "T_op_i[C]","T_db_o[C]"]]
        return super().fict_heat(df_off, df_on, eu_norm=eu_norm,\
                                         alpha=alpha)

    def adaptive_residuals(self, eu_norm='16798-1:2019', alpha=0.8,
                           filter_by_occupancy=0, when={}, hf=False):
        """Prepare eplusout.csv to standardized adaptive comfort model categories
        computation.

        :param eu_norm: EU normative to compute Adaptive Comfort Model
            thresholds. It can be '16798-1:2019' or '15251:2007', defaults to
            '16798-1:2019'.
        :type eu_norm: str, optional
        :param alpha: EU '15251:2007' free parameter ranging [0,1), defaults to
            0.8
        :type alpha: float, optional
        :param filter_by_occupancy: It can be set 0 or 1, depending on wether
            activate occupancy filtering on thermal comfort KPIs computation or
            not, defaults to 0.
        :type filter_by_occupancy: int, optional
        :param when: dictionary with 'start' and 'end' keys and values in format
            'year/month/day hour:minutes:seconds'
        :type when: dict, optional
        :return: Dictionary where '>3': average distance from cat 1 upper
            bound; '>0': average distance from central line of cat 1
        :rtype: dict
        """

        df_off = copy.deepcopy(self.df)     
        df_off = self.mean_indoor_op_air_temp(df_off, self.building_name)
        df_off = df_off.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        df_off = df_off.loc[:,["Date/Time", "T_op_i[C]","T_db_o[C]"]]
        return super().adaptive_residuals(df_off, eu_norm=eu_norm, alpha=alpha,\
            filter_by_occupancy=filter_by_occupancy,when=when, hf=hf)


    def pmv_ppd(self, vel=0.1, met=1.2, clo=0.7, wme=0,
                standard="ISO 7730-2006", filter_by_occupancy=0, season = None):
        """Prepare eplosout.csv to standardized predicted mean vote and
        percentage of dissatisfied (PMV, PPD) computation. Then return hourly
        pmv and ppd.

        :param vel: relative air speed, defaults to 0.1
        :type vel: float, optional
        :param met: metabolic rate, [met] defaults to 1.2
        :type met: float, optional
        :param clo: clothing insulation, [clo] defaults to 0.5
        :type clo: float, optional
        :param wme: external work, [met] defaults to 0
        :type wme: float, optional
        :param standard: Currently unused, defaults to "ISO 7730-2006"
        :type standard: str, optional
        :param filter_by_occupancy: It can be set 0 or 1, depending on wether
            activate occupancy filtering on thermal comfort KPIs computation
            or not, defaults to 0.
        :type filter_by_occupancy: int, optional
        :return: dictionary containing keys "pmv" (Predicted Mean Vote) and
            "ppd" (Predicted Percentage of Dissatisfied occupants)
        :rtype: dict
        """
        df = copy.deepcopy(self.df)
        df = self.indoor_mean_air_temp(df, self.building_name)
        df = self.indoor_mean_air_RH(df, self.building_name)
        df = self.indoor_mean_rad_temp(df, self.building_name)
        
        vars = ["T_rad_i[C]", "RH_i[%]", "T_db_i[C]"]
        
        if season is not None:
            df[season+"_season"] = self.check_season(df,season)
            vars.append(season+"_season")

        if filter_by_occupancy:
            df = self.get_occupancy(df, self.building_name)
            vars.append("Occupancy")

        df = df.loc[:, vars]

        df.dropna(how="all", inplace=True)

        return super().pmv_ppd(df, vel=vel, met=met, clo=clo, wme=wme,
                               standard=standard,
                               filter_by_occupancy=filter_by_occupancy,season=season)

    def adaptive_comfort_model(self, eu_norm='16798-1:2019',
                               alpha=0.8, filter_by_occupancy=0, when={}, color="indigo"):
        """Prepare eplosout.csv to standardized adaptive comfort 
        model computation. Then return number of hours in each comfort 
        category and POR considering cat II boundaries.

        :param eu_norm: It can be set to '15251:2007' if old UE norm
            computation is desired, defaults to '16798-1:2019'.
        :type eu_norm: str, optional
        :param filter_by_occupancy: It can be set 0 or 1, depending on wether
            activate occupancy filtering on thermal comfort KPIs computation or
            not, defaults to 0.
        :type filter_by_occupancy: int, optional
        :param alhpa: With old UE norm '15251:2007 alpha is a free parameter in
            range [0,1), defaults to 0.8
        :type alhpa: float, optional
        :param when: dictionary with 'start' and 'end' keys and values in format
            'year/month/day hour:minutes:seconds'
        :type when: dict, optional
        :return: Number of hours in each comfort category.
        :rtype: dictionary
        """
        df = copy.deepcopy(self.df)
        # TODO implement a choice btw T_db and T_op ? 
        # df = self.mean_indoor_op_air_temp(df, self.building_name)
        df = self.indoor_mean_air_temp(df,self.building_name)
        df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        if filter_by_occupancy:
            df = self.get_occupancy(df, self.building_name)
            # df = df.loc[:,["Date/Time", "T_op_i[C]","T_db_o[C]","Occupancy"]]
            df = df.loc[:,["Date/Time", "T_db_i[C]","T_db_o[C]","Occupancy"]]
        else:
            # df = df.loc[:,["Date/Time", "T_op_i[C]","T_db_o[C]"]]
            df = df.loc[:,["Date/Time", "T_db_i[C]","T_db_o[C]"]]
        return super().adaptive_comfort_model(df, eu_norm=eu_norm, alpha=alpha,
            filter_by_occupancy=filter_by_occupancy, when=when, color=color)

    def energy_signature(self,variable_heat=None, variable_cool=None):
        """Prepare eplosout.csv to standardized 1D and 2D energy signature
        computation. Then return points to build HVAC graphs.

        :return: Points allowing to build 1D and 2D graphs of energy signature.
        :rtype: dict
        """
        df = copy.deepcopy(self.df)
        df = df.set_index("Date/Time", drop=True) 
        
        # handle cooling bot if variable is none or if it is specified
        if variable_cool is None:
            
            # if variable is none and district cooling is not present --> not active cooling, set always 0
            if "DistrictCooling:Facility [J](TimeStep)" not in self.df.columns:
                data_cool = pd.Series(data=0, index=df.index)
            else:
                data_cool = pd.Series(data=df["DistrictCooling:Facility [J](TimeStep)"], index=df.index)
       
        else:

            df_new = df.copy()
            for col in df_new.columns:
                if variable_cool not in col or self.building_name.upper() not in col:
                    df_new = df_new.drop(col, axis=1)
            df_new = df_new.dropna()  
            data_cool = pd.Series(df_new.sum(axis=1), index=df_new.index)
        
        data_cool = data_cool.apply(self.convert_to_W_m3, args=(self.volume, ))

        # handle heating bot if variable is none or if it is specified
        if variable_heat is None:

            # if variable is none and district heating is not present --> not active heating, set always 0
            if "DistrictHeating:Facility [J](TimeStep)" not in self.df.columns:
                data_heat = pd.Series(data=0, index=df.index)
            else:
                data_heat = pd.Series(data=df["DistrictHeating:Facility [J](TimeStep)"], index=df.index)
        else:

            df_new = df.copy()
            for col in df_new.columns:
                if variable_heat not in col or self.building_name.upper() not in col:
                    df_new = df_new.drop(col, axis=1)
            df_new = df_new.dropna() 
            data_heat = pd.Series(df_new.sum(axis=1), index=df_new.index)
        
        data_heat = data_heat.apply(self.convert_to_W_m3, args=(self.volume, ))
        
        df["Q_h[Wh/m3]"] = data_heat
        df["Q_c[Wh/m3]"] = data_cool

        df = self.indoor_mean_air_temp(df, self.building_name)
        df = df.rename(columns={
            "dry_bulb_temperature":"T_db_o[C]",
            "direct_normal_radiation": "DHI_o[W/m2]",
            "diffuse_horizontal_radiation": "DIF_o[W/m2]",
            "global_horizontal_radiation": "GHI_o[W/m2]"
            })
        df = df.loc[:, ["T_db_i[C]", "T_db_o[C]",
                        "GHI_o[W/m2]","Q_c[Wh/m3]","Q_h[Wh/m3]"]]

        return super().energy_signature(df)

    def co2(self, th_list = [600,900,1200,1500,1800,2100,2500]):
        """Prepare eplosout.csv to standardized CO2 concentration
        computation. 
        
        :param th_list: thresholds from which compute number of hours with average CO2 above
        :type th_list: list, optional
        :return: dictionary containing CO2 information
        :rtype: dict
        """
        df = copy.deepcopy(self.df)
        df = self.indoor_mean_co2(df, self.building_name)
        schedule = self.idf.idfobjects["ZONEAIRCONTAMINANTBALANCE"][0].Outdoor_Carbon_Dioxide_Schedule_Name
        df = df.rename(columns={schedule.upper() + ":Schedule Value [](TimeStep)":"CO2_o[ppm]"})
        df = df.loc[:, ["Date/Time","CO2_i[ppm]", "CO2_o[ppm]"]]
        return super().co2(df,th_list= th_list)

    def n_co2_bI(self, filter_by_occupancy=1,season=None):
        """Prepare eplosout.csv to standardized CO2 concentration
        computation, then compute number of hours with CO2 concentration below 600 ppm
        highlighting possible over-ventilated conditions.

        :return: number of hours below threshold I
        :rtype: int
        """
        df = copy.deepcopy(self.df)
        df = self.indoor_mean_co2(df, self.building_name)
        vars = ["Date/Time", "CO2_i[ppm]"]
        
        if season is not None:
            df[season+"_season"] = self.check_season(df,season)
            vars.append(season+"_season")

        if filter_by_occupancy:
            df = self.get_occupancy(df, self.building_name)
            vars.append("Occupancy")

        df = df.loc[:, vars]

        return super().n_co2_bI(df, filter_by_occupancy=filter_by_occupancy,season=season)

    def timeseries_n_co2_bI(self, filter_by_occupancy=1,season=None, timestep="1H"):
        """Prepare eplosout.csv to standardized CO2 concentration
        computation, then compute number of hours with CO2 concentration below 600 ppm
        highlighting possible over-ventilated conditions.

        :return: number of hours below threshold I
        :rtype: int
        """
        df = copy.deepcopy(self.df)
        df = self.indoor_mean_co2(df, self.building_name)
        vars = ["Date/Time", "CO2_i[ppm]"]
        
        if season is not None:
            df[season+"_season"] = self.check_season(df,season)
            vars.append(season+"_season")

        if filter_by_occupancy:
            df = self.get_occupancy(df, self.building_name)
            vars.append("Occupancy")

        df = df.loc[:, vars]

        return super().timeseries_n_co2_bI(df, filter_by_occupancy=filter_by_occupancy,season=season, timestep=timestep)

    def n_co2_aIII(self,filter_by_occupancy=1,season=None):
        """Prepare eplosout.csv to standardized CO2 concentration
        computation, then compute number of hours with CO2 concentration above 1000 ppm
        highlighting possible under-ventilated conditions.

        :return: number of hours above threshold III
        :rtype: int
        """
        df = copy.deepcopy(self.df)
        df = self.indoor_mean_co2(df, self.building_name)
        vars = ["Date/Time", "CO2_i[ppm]"]
        
        if season is not None:
            df[season+"_season"] = self.check_season(df,season)
            vars.append(season+"_season")

        if filter_by_occupancy:
            df = self.get_occupancy(df, self.building_name)
            vars.append("Occupancy")

        df = df.loc[:, vars]

        return super().n_co2_aIII(df,filter_by_occupancy=filter_by_occupancy,season=season)

    def timeseries_n_co2_aIII(self,filter_by_occupancy=1,season=None,timestep="1H"):
        """Prepare eplosout.csv to standardized CO2 concentration
        computation, then compute number of hours with CO2 concentration above 1000 ppm
        highlighting possible under-ventilated conditions.

        :return: number of hours above threshold III
        :rtype: int
        """
        df = copy.deepcopy(self.df)
        df = self.indoor_mean_co2(df, self.building_name)
        vars = ["Date/Time", "CO2_i[ppm]"]
        
        if season is not None:
            df[season+"_season"] = self.check_season(df,season)
            vars.append(season+"_season")

        if filter_by_occupancy:
            df = self.get_occupancy(df, self.building_name)
            vars.append("Occupancy")

        df = df.loc[:, vars]

        return super().timeseries_n_co2_aIII(df,filter_by_occupancy=filter_by_occupancy,season=season,timestep=timestep)

    def Q_c(self, em_factor=1, reg_factor=1, distr_factor=1, gen_factor=1,
            ep_factor=1, variable = None):
        """Prepare eplosout.csv to compute primary energy need for cooling in kWh/m2, applying correction
        factors for emission, regulation, distribution and generation losses, then
        multiplying by primary energy factor.

        :param em_factor: correction factor for emission losses,
            in range [0,1].
        :type em_factor: float, defaults to 1
        :param reg_factor: correction factor for regulation losses,
            in range [0,1].
        :type reg_factor: float, defaults to 1
        :param distr_factor: correction factor for distribution losses,
            in range [0,1].
        :type distr_factor: float, defaults to 1
        :param gen_factor: correction factor for generation losses,
            in range [0,1].
        :type gen_factor: float, defaults to 1
        :param ep_factor: primary energy conversion factor,
            in range [0,1].
        :type ep_factor: float, defaults to 1
        :return: Primary energy need for cooling
        :rtype: float
        """
        df = copy.deepcopy(self.df)
        df.set_index("Date/Time", inplace=True, drop=True)      

        if variable is None:
            if "DistrictCooling:Facility [J](TimeStep)" not in self.df.columns:
                return 0  # No cooling system -> zero net needs.
            else:
                df = df.rename(columns={"DistrictCooling:Facility [J](TimeStep)": "Q_c[kWh/m2]"})
                df = df.resample("1H").sum(numeric_only=True)
        
        else:
            df_new = df.copy()
            for col in df_new.columns:
                if variable not in col or self.building_name.upper() not in col:
                    df_new = df_new.drop(col, axis=1)
            df_new = df_new.dropna()  
            df["Q_c[kWh/m2]"] = df_new.sum(axis=1)
            df = df.resample("1H").sum()
        
        df["Date/Time"] = df.index
        # df = df.rename(columns={"DistrictCooling:Facility [J](TimeStep)": "Q_c[kWh/m2]"})

        # df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(
        #     self.convert_to_kWh_m2, args=(self.area, ))
        df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(
            self.convert_to_kWh)
       
        df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(
            self.emission_losses, args=(em_factor, ))
        df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(
            self.regulation_losses, args=(reg_factor, ))
        df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(
            self.distribution_losses, args=(distr_factor, ))

        df = df.loc[:, ["Date/Time", "Q_c[kWh/m2]"]]
        df = df.dropna()
        return super().Q_c(df, gen_factor=gen_factor, ep_factor=ep_factor)

    def Q_h(self, em_factor=1, reg_factor=1, distr_factor=1, gen_factor=1,
            ep_factor=1, variable = None, timestep=None):
        """Prepare eplosout.csv to compute primary energy need for heating in kWh/m2, applying correction
        factors for emission, regulation, distribution and generation losses, then
        multiplying by primary energy factor.

        :param em_factor: correction factor for emission losses,
            in range [0,1].
        :type em_factor: float, defaults to 1
        :param reg_factor: correction factor for regulation losses,
            in range [0,1].
        :type reg_factor: float, defaults to 1
        :param distr_factor: correction factor for distribution losses,
            in range [0,1].
        :type distr_factor: float, defaults to 1
        :param gen_factor: correction factor for generation losses,
            in range [0,1].
        :type gen_factor: float, defaults to 1
        :param ep_factor: primary energy conversion factor,
            in range [0,1].
        :type ep_factor: float, defaults to 1
        :return: Primary energy need for heating
        :rtype: float
        """
        df = copy.deepcopy(self.df)
        df.set_index("Date/Time", inplace=True, drop=True)      

        if variable is None:
            if "DistrictHeating:Facility [J](TimeStep)" not in self.df.columns:
                return 0  # No heating system -> zero net needs.
            else:
                df = df.rename(columns={"DistrictHeating:Facility [J](TimeStep)": "Q_h[kWh/m2]"})
                df = df.resample("1H").sum(numeric_only=True)
        
        else:
            df_new = df.copy()
            for col in df_new.columns:
                if variable not in col or self.building_name.upper() not in col:
                    df_new = df_new.drop(col, axis=1)
            df_new = df_new.dropna() 
            df["Q_h[kWh/m2]"] = df_new.sum(axis=1)
            df = df.resample("1H").sum()
        
        df["Date/Time"] = df.index
        #TODO rimettere a posto - solo per AAU test
        # df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(
        #     self.convert_to_kWh_m2, args=(self.area, ))
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(self.convert_to_kWh)
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(
            self.emission_losses, args=(em_factor, ))
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(
            self.regulation_losses, args=(reg_factor, ))
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(
            self.distribution_losses, args=(distr_factor, ))

        df = df.loc[:, ["Date/Time", "Q_h[kWh/m2]"]]
        df = df.dropna()
        return super().Q_h(df, gen_factor=gen_factor, ep_factor=ep_factor,timestep=timestep)

    def f_Q_h(self, em_factor=1, reg_factor=1, distr_factor=1, gen_factor=1, variable=None):
        """Prepare eplosout.csv to compute final energy need for heating in kWh/m2, applying correction
        factors for emission, regulation, distribution and generation losses.

        :param em_factor: correction factor for emission losses,
            in range [0,1].
        :type em_factor: float, defaults to 1
        :param reg_factor: correction factor for regulation losses,
            in range [0,1].
        :type reg_factor: float, defaults to 1
        :param distr_factor: correction factor for distribution losses,
            in range [0,1].
        :type distr_factor: float, defaults to 1
        :param gen_factor: correction factor for generation losses,
            in range [0,1].
        :type gen_factor: float, defaults to 1
        :return: Primary energy need for heating
        :rtype: float
        """
        df = copy.deepcopy(self.df)
        df.set_index("Date/Time", inplace=True, drop=True)      

        if variable is None:
            if "DistrictHeating:Facility [J](TimeStep)" not in self.df.columns:
                return 0  # No heating system -> zero net needs.
            else:
                df = df.rename(columns={"DistrictHeating:Facility [J](TimeStep)": "Q_h[kWh/m2]"})
                df = df.resample("1H").sum()
        
        else:
            df_new = df.copy()
            for col in df_new.columns:
                if variable not in col or self.building_name.upper() not in col:
                    df_new = df_new.drop(col, axis=1)
            df_new = df_new.dropna()      
            df["Q_h[kWh/m2]"] = df_new.sum(axis=1)
            df = df.resample("1H").sum()
        
        df["Date/Time"] = df.index

        #TODO rimettere a posto - solo per AAU test
        # df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(
        #     self.convert_to_kWh_m2, args=(self.area, ))
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(self.convert_to_kWh)
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(
            self.emission_losses, args=(em_factor, ))
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(
            self.regulation_losses, args=(reg_factor, ))
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(
            self.distribution_losses, args=(distr_factor, ))

        df = df.loc[:, ["Date/Time", "Q_h[kWh/m2]"]]
        df = df.dropna()
        return super().f_Q_h(df, gen_factor=gen_factor)

    def timeseries_f_Q_h(self, em_factor=1, reg_factor=1, distr_factor=1, gen_factor=1, variable=None,timestep="1H"):
        """Prepare eplosout.csv to compute final energy need for heating in kWh/m2, applying correction
        factors for emission, regulation, distribution and generation losses.

        :param em_factor: correction factor for emission losses,
            in range [0,1].
        :type em_factor: float, defaults to 1
        :param reg_factor: correction factor for regulation losses,
            in range [0,1].
        :type reg_factor: float, defaults to 1
        :param distr_factor: correction factor for distribution losses,
            in range [0,1].
        :type distr_factor: float, defaults to 1
        :param gen_factor: correction factor for generation losses,
            in range [0,1].
        :type gen_factor: float, defaults to 1
        :return: Primary energy need for heating
        :rtype: float
        """
        df = copy.deepcopy(self.df)
        df.set_index("Date/Time", inplace=True, drop=True)      

        if variable is None:
            if "DistrictHeating:Facility [J](TimeStep)" not in self.df.columns:
                return 0  # No heating system -> zero net needs.
            else:
                df = df.rename(columns={"DistrictHeating:Facility [J](TimeStep)": "Q_h[kWh/m2]"})
        
        else:
            df_new = df.copy()
            for col in df_new.columns:
                if variable not in col or self.building_name.upper() not in col:
                    df_new = df_new.drop(col, axis=1)
            df_new = df_new.dropna()                
            df["Q_h[kWh/m2]"] = df_new.sum(axis=1)
        
        df["Date/Time"] = df.index

        #TODO rimettere a posto - solo per AAU test
        # df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(
        #     self.convert_to_kWh_m2, args=(self.area, ))
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(self.convert_to_kWh)
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(
            self.emission_losses, args=(em_factor, ))
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(
            self.regulation_losses, args=(reg_factor, ))
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(
            self.distribution_losses, args=(distr_factor, ))

        df = df.loc[:, ["Date/Time", "Q_h[kWh/m2]"]]
        df = df.dropna()
        return super().timeseries_f_Q_h(df, gen_factor=gen_factor, timestep="1H")

    def f_Q_c(self, em_factor=1, reg_factor=1, distr_factor=1, gen_factor=1, variable=None):
        """Prepare eplosout.csv to compute final energy need for cooling in kWh/m2, applying correction
        factors for emission, regulation, distribution and generation losses.

        :param em_factor: correction factor for emission losses,
            in range [0,1].
        :type em_factor: float, defaults to 1
        :param reg_factor: correction factor for regulation losses,
            in range [0,1].
        :type reg_factor: float, defaults to 1
        :param distr_factor: correction factor for distribution losses,
            in range [0,1].
        :type distr_factor: float, defaults to 1
        :param gen_factor: correction factor for generation losses,
            in range [0,1].
        :type gen_factor: float, defaults to 1
        :return: Primary energy need for cooling
        :rtype: float
        """
        df = copy.deepcopy(self.df)
        df.set_index("Date/Time", inplace=True, drop=True)      

        if variable is None:
            if "DistrictCooling:Facility [J](TimeStep)" not in self.df.columns:
                return 0  # No cooling system -> zero net needs.
            else:
                df = df.rename(columns={"DistrictCooling:Facility [J](TimeStep)": "Q_c[kWh/m2]"})
        
        else:
            df_new = df.copy()
            for col in df_new.columns:
                if variable not in col or self.building_name.upper() not in col:
                    df_new = df_new.drop(col, axis=1)
            df_new = df_new.dropna()        
            df["Q_c[kWh/m2]"] = df_new.sum(axis=1)
        
        df["Date/Time"] = df.index

        # df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(
        #     self.convert_to_kWh_m2, args=(self.area, ))
        df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(
            self.convert_to_kWh)
        df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(
            self.emission_losses, args=(em_factor, ))
        df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(
            self.regulation_losses, args=(reg_factor, ))
        df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(
            self.distribution_losses, args=(distr_factor, ))

        df = df.loc[:, ["Date/Time", "Q_c[kWh/m2]"]]
        df = df.dropna()
        return super().f_Q_c(df, gen_factor=gen_factor)

    def Q_I(self, em_factor=1, reg_factor=1, distr_factor=1, gen_factor=1,
            ep_factor=1):
        """Prepare eplosout.csv to compute primary energy need for lighting in kWh/m2, applying correction
        factors for emission, regulation, distribution and generation losses, then
        multiplying by primary energy factor.

        :param em_factor: correction factor for emission losses,
            in range [0,1].
        :type em_factor: float, defaults to 1
        :param reg_factor: correction factor for regulation losses,
            in range [0,1].
        :type reg_factor: float, defaults to 1
        :param distr_factor: correction factor for distribution losses,
            in range [0,1].
        :type distr_factor: float, defaults to 1
        :param gen_factor: correction factor for generation losses,
            in range [0,1].
        :type gen_factor: float, defaults to 1
        :param ep_factor: primary energy conversion factor,
            in range [0,1].
        :type ep_factor: float, defaults to 1
        :return: Primary energy need for lighting
        :rtype: float
        """
        df = copy.deepcopy(self.df)
        df.set_index("Date/Time", inplace=True, drop=True)
        df = df.resample("1H").sum()
        df["Date/Time"] = df.index
        df = df.rename(columns={"InteriorLights:Electricity [J](TimeStep)": "Q_I[kWh/m2]"})

        df["Q_I[kWh/m2]"] = df["Q_I[kWh/m2]"].apply(
            self.convert_to_kWh_m2, args=(self.area, ))

        df["Q_I[kWh/m2]"] = df["Q_I[kWh/m2]"].apply(
            self.emission_losses, args=(em_factor, ))
        df["Q_I[kWh/m2]"] = df["Q_I[kWh/m2]"].apply(
            self.regulation_losses, args=(reg_factor, ))
        df["Q_I[kWh/m2]"] = df["Q_I[kWh/m2]"].apply(
            self.distribution_losses, args=(distr_factor, ))
            
        return super().Q_I(df, gen_factor=gen_factor, ep_factor=ep_factor)

    def carpet_plot(self, variable, vmin=None, vmax=None, cmap="gist_rainbow_r", fontsize=60, **params):
        """Prepare eplosout.csv to generate carpet plot of specified variable.
        
        :param variable: variable name according to standardized nomenclature
        :type variable: str
        """
        title = None
        df = copy.deepcopy(self.df)
        try:
            if variable == "T_db_i[C]":
                title = r"Indoor Dry Bulb Temperature"
                df = self.indoor_mean_air_temp(df, self.building_name)

            elif variable == "T_db_o[C]":
                df = df.rename(columns={"dry_bulb_temperature": "T_db_o[C]"})
                title = r"Outdoor Dry Bulb Temperature"

            elif variable == "CO2_i[ppm]":
                df = self.indoor_mean_co2(df, self.building_name)
                title = r"Indoor $\mathrm{CO_2}$ Concentration"

            elif variable == "CO2_o[ppm]":
                schedule = self.idf.idfobjects["ZONEAIRCONTAMINANTBALANCE"][0].Outdoor_Carbon_Dioxide_Schedule_Name
                df = df.rename(columns={schedule.upper() + ":Schedule Value [](TimeStep)": "CO2_o[ppm]"})
                title = r"Outdoor $\mathrm{CO_2}$ Concentration"

            elif variable == "T_op_i[C]":
                title = r"Indoor Operative Temperature"
                df = self.mean_indoor_op_air_temp(df, self.building_name)

            elif variable == "ppd":
                title = "Predicted Percentage of Dissatisfied"
                df = self.indoor_mean_air_temp(df, self.building_name)
                df = self.indoor_mean_air_RH(df, self.building_name)
                df = self.indoor_mean_rad_temp(df, self.building_name)
                df = df.loc[:, ["T_rad_i[C]", "RH_i[%]", "T_db_i[C]"]].dropna(how="all")
                df = self.compute_pmv_ppd(df, **params)
                # cmap = "RdYlGn_r"
                vmin, vmax = 0, 100

            elif variable == "pmv":
                title = "Predicted Mean Vote"
                df = self.indoor_mean_air_temp(df, self.building_name)
                df = self.indoor_mean_air_RH(df, self.building_name)
                df = self.indoor_mean_rad_temp(df, self.building_name)
                df = df.loc[:, ["T_rad_i[C]", "RH_i[%]", "T_db_i[C]"]].dropna(how="all")
                df = self.compute_pmv_ppd(df, **params)
                # cmap = "RdYlGn_r"
                vmin, vmax = -3, 3

            elif variable == "acm":
                title = "Adaptive Comfort Model (dist. from central line)"
                df = self.indoor_mean_air_temp(df, self.building_name)
                df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})

                # Always get occupancy, even if not used.
                df = self.get_occupancy(df, self.building_name)
                df = df.loc[:,["Date/Time", "T_db_i[C]","T_db_o[C]","Occupancy"]]

                df = self.compute_acm(df, **params)
                df_middle = df.where((df["T_rm"] >= 10) & (df["T_rm"] <= 33)).dropna()
                df_up =  df.where(df["T_rm"] > 33).dropna()
                df_down =  df.where(df["T_rm"] < 10).dropna()

                # Compute distances from comfort line and comfort+3 line
                df_middle["dist"] = df_middle["T_op_i[C]"]-\
                                    (0.33*df_middle["T_rm"]+18.8)
                df_up["dist"] = df_up["T_op_i[C]"] - (0.33*33+18.8)
                df_down["dist"] = df_down["T_op_i[C]"] - (0.33*10+18.8)
                
                df =  df_middle.append(df_up).append(df_down)
                df["acm"] = df["dist"]
                df.index = df["Date/Time"]
                cmap = "rainbow"
                vmin, vmax = -10, +6
                
            elif variable == "WBD[C]":
                df = self.timeseries_wbd()
                title = r"Wet-Bulb Depression"
            elif variable == "DD_o[DD]":
                raise NotImplementedError("Variable %s is not yet supported for carpet plot" % variable)
            else:
                raise ValueError("Invalid variable %s for carpet plot" % variable)
        
        except (NotImplementedError, ValueError) as e:
            print(e)

        else:
            if isinstance(df, pd.Series):
                df = df.to_frame()
            df["Date/Time"] = df.index
            df = df[["Date/Time", variable]]
            return super().carpet_plot(df, variable=variable, vmin=vmin, vmax=vmax, cmap=cmap, title=title, fontsize=fontsize)


    def carrier_plot(
        self,
        range_temp_c=[0, 35],
        range_humidity_g_kg=[0, 25],
        altitude_m=0,
        where="outdoor"
    ):
        """Prepare eplusout.csv to generate a psychrometric Carrier chart by
        retrieving dry-bulb temperature and realtive humidity from the
        dataframe.

        :param range_temp_c: x-axis limits, defaults to [0, 35]
        :type range_temp_c: list, optional
        :param range_humidity_g_kg: y-axis limits, defaults to [0, 25]
        :type range_humidity_g_kg: list, optional
        :param altitude_m: Altitute in metres, used to perform calculations of
            absolute humidity, defaults to 0
        :type altitude_m: int, optional
        """

        df = self.df.copy()
        df.set_index("Date/Time", inplace=True, drop=False)

        if where == "outdoor":
            data = pd.DataFrame({
                "dry_bulb_temperature": df["dry_bulb_temperature"].resample("1H").mean(),
                "relative_humidity": df["relative_humidity"].resample("1H").mean()
                })
        elif where == "indoor":
            dbt = self.indoor_mean_air_temp(df, self.building_name)
            rh = self.indoor_mean_air_RH(df, self.building_name)
            data = pd.DataFrame({
                "dry_bulb_temperature": dbt["T_db_i[C]"].resample("1H").mean(),
                "relative_humidity": rh["RH_i[%]"].resample("1H").mean()
                })
        
        return super().carrier_plot(
            range_temp_c, range_humidity_g_kg, altitude_m, data,
            suffix=f"{where}"
        )

    def initial_cost_torino(self, sim_params):
        return super().initial_cost_torino(sim_params)

    # Timeseries functions
    def timeseries_Q_c(self, timestep="1H", em_factor=1, reg_factor=1,
                       distr_factor=1, gen_factor=1, ep_factor=1, variable=None):
        """Prepare eplosout.csv to to compute timeseries values (sum over the time period)
        of primary energy need for cooling in kWh/m2,
        applying correction factors for generation losses, then multiplying by primary energy factor.
        Corrections for emission, regulation and distribution losses are considered
        previously applied.

        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :param em_factor: correction factor for emission losses,
            in range [0,1].
        :type em_factor: float, defaults to 1
        :param reg_factor: correction factor for regulation losses,
            in range [0,1].
        :type reg_factor: float, defaults to 1
        :param distr_factor: correction factor for distribution losses,
            in range [0,1].
        :type distr_factor: float, defaults to 1
        :param gen_factor: correction factor for generation losses,
            in range [0,1].
        :type gen_factor: float, defaults to 1
        :param ep_factor: primary energy conversion factor,
            in range [0,1].
        :type ep_factor: float, defaults to 1
        :return: DataFrame of cooling primary energy values over time
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        df = copy.deepcopy(self.df)
        df.set_index("Date/Time", inplace=True, drop=True)      

        if variable is None:
            df = df.rename(columns={"DistrictCooling:Facility [J](TimeStep)": "Q_c[kWh/m2]"})

        else:
            df_new = df.copy()
            for col in df_new.columns:
                if variable not in col or self.building_name.upper() not in col:
                    df_new = df_new.drop(col, axis=1)

            df_new = df_new.dropna()
            df["Q_c[kWh/m2]"] = df_new.sum(axis=1)

        df["Date/Time"] = df.index

        # df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(
        #     self.convert_to_kWh_m2, args=(self.area, ))
        df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(
            self.convert_to_kWh)   
        df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(
            self.emission_losses, args=(em_factor, ))
        df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(
            self.regulation_losses, args=(reg_factor, ))
        df["Q_c[kWh/m2]"] = df["Q_c[kWh/m2]"].apply(
            self.distribution_losses, args=(distr_factor, ))

        df = df.loc[:, ["Date/Time", "Q_c[kWh/m2]"]]
        df = df.dropna()

        return super().timeseries_Q_c(df, timestep=timestep, gen_factor=gen_factor, ep_factor=ep_factor)

    def timeseries_Q_h(self, timestep="1H", em_factor=1, reg_factor=1,
                       distr_factor=1, gen_factor=1, ep_factor=1, variable=None):
        """Prepare eplosout.csv to to compute timeseries values (sum over the time period)
        of primary energy need for heating in kWh/m2,
        applying correction factors for generation losses, then multiplying by primary energy factor.
        Corrections for emission, regulation and distribution losses are considered
        previously applied.

        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :param em_factor: correction factor for emission losses,
            in range [0,1].
        :type em_factor: float, defaults to 1
        :param reg_factor: correction factor for regulation losses,
            in range [0,1].
        :type reg_factor: float, defaults to 1
        :param distr_factor: correction factor for distribution losses,
            in range [0,1].
        :type distr_factor: float, defaults to 1
        :param gen_factor: correction factor for generation losses,
            in range [0,1].
        :type gen_factor: float, defaults to 1
        :param ep_factor: primary energy conversion factor,
            in range [0,1].
        :type ep_factor: float, defaults to 1
        :return: DataFrame of heating primary energy values over time
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        df = copy.deepcopy(self.df)
        df.set_index("Date/Time", inplace=True, drop=True)


        if variable is None:
            df = df.rename(columns={"DistrictHeating:Facility [J](TimeStep)": "Q_h[kWh/m2]"})

        else:
            df_new = df.copy()
            for col in df_new.columns:
                if variable not in col or self.building_name.upper() not in col:
                    df_new = df_new.drop(col, axis=1)

            df_new = df_new.dropna()
            df["Q_h[kWh/m2]"] = df_new.sum(axis=1)

        df["Date/Time"] = df.index


        #TODO rimettere a posto - solo per AAU test
        # df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(
        #     self.convert_to_kWh_m2, args=(self.area, ))
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(self.convert_to_kWh)
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(
            self.emission_losses, args=(em_factor, ))
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(
            self.regulation_losses, args=(reg_factor, ))
        df["Q_h[kWh/m2]"] = df["Q_h[kWh/m2]"].apply(
            self.distribution_losses, args=(distr_factor, ))

        df = df.loc[:, ["Date/Time", "Q_h[kWh/m2]"]]
        df = df.dropna()

        return super().timeseries_Q_h(df, timestep=timestep, gen_factor=gen_factor, ep_factor=ep_factor)

    def timeseries_t_db_i(self, timestep="1H", graph=False):
        """Prepare eplosout.csv to compute timeseries indoor drybulb temperature according to
        requested time aggregation (average on the time period).
        
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :return: DataFrame of indoor drybulb temperature values over time
        :rtype: pd.DataFrame()
        """
        df = copy.deepcopy(self.df)

        df = self.indoor_mean_air_temp(df, self.building_name)
        df = df.loc[:, ["Date/Time","T_db_i[C]"]]
        return super().timeseries_t_db_i(df, timestep=timestep, graph=graph)

    def timeseries_t_db_o(self, timestep="1H"):
        """Prepare eplosout.csv to compute timeseries indoor drybulb temperature according to
        requested time aggregation (average on the time period).
        
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :return: DataFrame of indoor drybulb temperature values over time
        :rtype: pd.DataFrame()
        """
        df = copy.deepcopy(self.df)
        df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        df = df.loc[:, ["Date/Time","T_db_o[C]"]]
        return super().timeseries_t_db_o(df, timestep=timestep)

    def timeseries_wbt(self, timestep="1H"):
        """Prepare eplosout.csv to compute timeseries outdoor wet-bulb temperature according to
        requested time aggregation (average on the time period).
        
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :return: DataFrame of outdoor wetbulb temperature values over time
        :rtype: pd.DataFrame()
        """
        df = copy.deepcopy(self.df)
        df = df.rename(columns={
            "dry_bulb_temperature":"T_db_o[C]",
            "relative_humidity": "RH_o[%]"
            })
        df = df.loc[:, ["Date/Time","T_db_o[C]","RH_o[%]" ]]
        return super().timeseries_wbt(df, timestep=timestep)

    def timeseries_wbd(self, timestep="1H"):
        """Prepare eplosout.csv to compute timeseries wetbulb depression according to
        requested time aggregation (average on the time period).
        
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :return: DataFrame of wetbulb depression values over time
        :rtype: pd.DataFrame()
        """
        df = copy.deepcopy(self.df)
        df = df.rename(columns={
            "dry_bulb_temperature":"T_db_o[C]",
            "relative_humidity": "RH_o[%]"
            })
        df = df.loc[:, ["Date/Time","T_db_o[C]","RH_o[%]" ]]
        return super().timeseries_wbd(df, timestep=timestep)

    def timeseries_mix_ratio_o(self, timestep="1H"):
        """Prepare eplosout.csv to compute timeseries outdoor mixed ratio vapour quality according to
        requested time aggregation (average on the time period).
        
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :return: DataFrame of outdoor mixed ratio vapour quality [kg/kg] values over time
        :rtype: pd.DataFrame()
        """
        df = copy.deepcopy(self.df)
        df = df.rename(columns={
            "dry_bulb_temperature":"T_db_o[C]",
            "relative_humidity": "RH_o[%]"
            })
        df = df.loc[:, ["Date/Time","T_db[C]","RH[%]" ]]
        return super().timeseries_mix_ratio(df, where= "o", timestep=timestep)

    def timeseries_mix_ratio_i(self, timestep="1H"):
        """Prepare eplosout.csv to compute timeseries indoor mixed ratio vapour quality according to
        requested time aggregation (average on the time period).
        
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :return: DataFrame of indoor mixed ratio vapour quality [kg/kg] values over time
        :rtype: pd.DataFrame()
        """
        df = copy.deepcopy(self.df)
        df = self.indoor_mean_air_temp(df, self.building_name)
        df = self.indoor_mean_air_RH(df, self.building_name)
        df = df.rename(columns={"T_db_i[C]":"T_db[C]"})
        df = df.rename(columns={"RH_i[%]":"RH[%]"})
        df = df.loc[:, ["Date/Time","T_db[C]","RH[%]" ]]
        return super().timeseries_mix_ratio(df, where= "i", timestep=timestep)

    def timeseries_mix_ratio_eahx(self, timestep="1H"):
        """Prepare eplosout.csv to compute timeseries mixed ratio vapour quality
        inside EAHX pipes according to
        requested time aggregation (average on the time period).
        
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :return: DataFrame of mixed ratio vapour quality [kg/kg] values over time
        :rtype: pd.DataFrame()
        """
        df = copy.deepcopy(self.df)
        # TODO change function collecting from idf correct values to pass
        df = self.indoor_mean_air_temp(df, self.building_name)
        df = self.indoor_mean_air_RH(df, self.building_name)
        df = df.rename(columns={"T_db_i[C]":"T_db[C]"})
        df = df.rename(columns={"RH_i[%]":"RH[%]"})
        df = df.loc[:, ["Date/Time","T_db[C]","RH[%]" ]]
        return super().timeseries_mix_ratio(df, where= "eahx", timestep=timestep)
    
    def timeseries_co2(self, timestep="1H"):
        """Prepare eplosout.csv to compute timeseries of indoor CO2 concentration (ppm) according to
        requested time aggregation (average on the time period).
        
        :param timestep: time aggregation required
        :type timestep: str, defaults to "1H"
        :return: DataFrame of indoor co2 values over time
        :rtype: pd.DataFrame()
        """
        df = copy.deepcopy(self.df)
        df = self.indoor_mean_co2(df, self.building_name)
        df = df.loc[:, ["Date/Time","CO2_i[ppm]"]]
        return super().timeseries_co2(df, timestep=timestep)
     
    def timeseries_acm_hourly_cat(self, eu_norm='16798-1:2019', alpha=0.8,
                                  filter_by_occupancy=0, when={}, title=None,
                                  suffix="", axfmt='%b %d'):
        """Prepare eplosout.csv to standardized ACM computation and
        compute hourly ACM category according to different European normatives.
        Filters according to occupancy or dates can be applied. 
        ACM category is expressed in terms of distance from comfort central line. 

        :param eu_norm: It can be set to '15251:2007' if old UE norm 
            computation is desired, defaults to '16798-1:2019'.
        :type eu_norm: str, optional
        :param alpha: With old UE norm '15251:2007 alpha is a free parameter in
            range [0,1), defaults to 0.8
        :type alpha: float, optional
        :param filter_by_occupancy: It can be set 0 or 1, depending on wether
            activate occupancy filtering on thermal comfort KPIs computation or
            not, defaults to 0.
        :type filter_by_occupancy: int, optional
        :param when: dictionary with 'start' and 'end' keys and values in format 'year/month/day hour:minutes:seconds'
        :type when: dict, optional
        :return: Dataframe containing "Date/Time" and "dist" columns.
        :rtype: pd.DataFrame()
        """

        df = copy.deepcopy(self.df)
        # TODO implement a choice btw T_db and T_op ? 
        # df = self.mean_indoor_op_air_temp(df, self.building_name)
        df = self.indoor_mean_air_temp(df, self.building_name)
        df = df.rename(columns={"dry_bulb_temperature":"T_db_o[C]"})
        if filter_by_occupancy:
            df = self.get_occupancy(df, self.building_name)
            # df = df.loc[:,["Date/Time", "T_op_i[C]","T_db_o[C]","Occupancy"]]
            df = df.loc[:,["Date/Time", "T_db_i[C]","T_db_o[C]","Occupancy"]]
        else:
            # df = df.loc[:,["Date/Time", "T_op_i[C]","T_db_o[C]"]]
            df = df.loc[:,["Date/Time", "T_db_i[C]","T_db_o[C]"]]
        return super().timeseries_acm_hourly_cat(
            df,
            eu_norm=eu_norm,
            alpha=alpha,
            filter_by_occupancy=filter_by_occupancy,
            when=when,
            title=title,
            suffix=suffix,
            axfmt=axfmt
        )

    def timeseries_pmv_ppd(self, vel=0.1, met=1.2, clo=0.7, wme=0,
                standard="ISO 7730-2006", filter_by_occupancy=0, season = None):
        """Prepare eplosout.csv to standardized predicted mean vote and
        percentage of dissatisfied (PMV, PPD) computation. Then return hourly
        pmv and ppd.

        :param vel: relative air speed, defaults to 0.1
        :type vel: float, optional
        :param met: metabolic rate, [met] defaults to 1.2
        :type met: float, optional
        :param clo: clothing insulation, [clo] defaults to 0.5
        :type clo: float, optional
        :param wme: external work, [met] defaults to 0
        :type wme: float, optional
        :param standard: Currently unused, defaults to "ISO 7730-2006"
        :type standard: str, optional
        :param filter_by_occupancy: It can be set 0 or 1, depending on wether
            activate occupancy filtering on thermal comfort KPIs computation
            or not, defaults to 0.
        :type filter_by_occupancy: int, optional
        :return: dictionary containing keys "pmv" (Predicted Mean Vote) and
            "ppd" (Predicted Percentage of Dissatisfied occupants)
        :rtype: dict
        """
        df = copy.deepcopy(self.df)
        df = self.indoor_mean_air_temp(df, self.building_name)
        df = self.indoor_mean_air_RH(df, self.building_name)
        df = self.indoor_mean_rad_temp(df, self.building_name)

        vars = ["T_rad_i[C]", "RH_i[%]", "T_db_i[C]"]
        
        if season is not None:
            df[season+"_season"] = self.check_season(df,season)
            vars.append(season+"_season")

        if filter_by_occupancy:
            df = self.get_occupancy(df, self.building_name)
            vars.append("Occupancy")

        df = df.loc[:, vars]
        return super().timeseries_pmv_ppd(df, vel=vel, met=met, clo=clo, wme=wme,
                               standard=standard,
                               filter_by_occupancy=filter_by_occupancy,season=season)   

    def t_op(self, season = None, filter_by="", timestep="1H"):
        """_summary_

        :param season: _description_, defaults to None
        :type season: _type_, optional
        :return: _description_
        :rtype: _type_
        """
        df = copy.deepcopy(self.df)
        df = self.mean_indoor_op_air_temp(df, self.building_name, filter_by)


        vars = ["Date/Time", "T_op_i[C]"]
        
        if season is not None:
            df[season+"_season"] = self.check_season(df,season)
            vars.append(season+"_season")

        df = df.loc[:, vars]

        return super().t_op(df, season=season, timestep=timestep)

    def ach_tot(self, filter_by="", timestep="1H"):
        """_summary_

        :return: _description_
        :rtype: _type_
        """
        df = copy.deepcopy(self.df)
        volume = self.volume
        df = self.sum_vent_volume(df, self.building_name, volume, filter_by)


        vars = ["Date/Time", "Airflow_i[m3]"]
        

        df = df.loc[:, vars]
        # print(df["Airflow_i[m3/s]"])
        
        return super().ach_tot(df, timestep=timestep)

    def timeseries_t_op(self, season = None, filter_by="", timestep="1H"):
        """_summary_

        :param season: _description_, defaults to None
        :type season: _type_, optional
        :return: _description_
        :rtype: _type_
        """
        df = copy.deepcopy(self.df)
        df = self.mean_indoor_op_air_temp(df, self.building_name, filter_by)


        vars = ["Date/Time", "T_op_i[C]"]
        
        if season is not None:
            df[season+"_season"] = self.check_season(df,season)
            vars.append(season+"_season")

        df = df.loc[:, vars]

        return super().timeseries_t_op(df, season=season, timestep=timestep)

    def check_season(self,df,season):
        df_new = df.copy()
        
        if season.upper() == "heating".upper():
            for col in df_new.columns:
                if self.idf.heat_season not in col:
                    df_new = df_new.drop(col, axis=1)
                else:
                    complete_name = col
            df["heating_season"] = [0 if x <= 0 or np.isnan(x) else 1 for x in df_new[complete_name]]
        
        elif season.upper() == "cooling".upper():
            for col in df_new.columns:
                if self.idf.cool_season not in col:
                    df_new = df_new.drop(col, axis=1)
                else:
                    complete_name = col
            if set(df_new[complete_name]) != {0.0, 1.0}:
                df["cooling_season"] = [0 if x >= 50 or np.isnan(x) else 1 for x in df_new[complete_name]]
            else:
                df["cooling_season"] = [0 if x <= 0 or np.isnan(x) else 1 for x in df_new[complete_name]]
        
        elif season.upper() == "free_running".upper():
            if self.idf.heat_season is not None:
                for col in df_new.columns:
                    if self.idf.heat_season not in col:
                        df_new = df_new.drop(col, axis=1)
                    else:
                        complete_name = col
                heat_seas = [0 if x <= 0 or np.isnan(x) else 1 for x in df_new[complete_name]]
            else:
                heat_seas = np.zeros(len(df_new))
            if self.idf.cool_season is not None:
                for col in df_new.columns:
                    if self.idf.cool_season not in col:
                        df_new = df_new.drop(col, axis=1)
                    else:
                        complete_name = col
                cool_seas = [0 if x >= 50 or np.isnan(x) else 1 for x in df_new[complete_name]]
            else:
                cool_seas = np.zeros(len(df_new))
            df["free_running_season"] = self.fr_season(heat_season=heat_seas, cool_season=cool_seas)
        else:
            raise("Invalid Season name")
        return df[season+"_season"]
    
    @staticmethod
    def convert_to_datetime(date_str, year=None):
        """It converts EnergyPlus "Date/Time" column to a timestamp referred to
        1970 as a sample year.

        :param date_str: EnergyPlus "Date/Time"
        :type date_str: str
        :return: timestamp
        :rtype: class:`pandas._libs.tslibs.timestamps.Timestamp`
        """
        if not isinstance(date_str, str):
            return date_str

        if date_str[0] == " ":
            date_str = date_str[1:]
        
        date_str ="1970/" + date_str
        # if year == None:
        #     date_str ="1970/" + date_str
        # else:
        #     date_str = str(year) + "/" + date_str
        
        date = date_str.split(" ")[0]
        time = date_str.split(" ")[2]
        hour = time.split(":")[0]
        
        if hour != '24':
            dt_obj = pd.to_datetime(date_str, format='%Y/%m/%d  %H:%M:%S').replace(year=year)
        else:
            time = time.replace('24','00')
            dt_obj = (pd.to_datetime(date+'  '+time, format='%Y/%m/%d  %H:%M:%S') + dt.timedelta(days=1)).replace(year=year)
        
        return dt_obj

    @staticmethod
    def get_occupancy(df, building_name, filter_by=""):
        """Get occupancy values from eplusout.csv in boolean (0/1) form for each
        simulated timestep.

        :param df: eplusout.csv
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame with new `Occupancy` column
        :rtype: class:`pandas.core.frame.DataFrame`

        .. warning:: **Output:Variable, \*, Zone People Occupant Count,
            Timestep** is required in IDF.

        """
        building_name = building_name.upper()
        df_new = df.copy()
        for col in df_new.columns:
            if "Zone People Occupant Count [](TimeStep" not in col or building_name not in col or filter_by.lower() not in col.lower():
                df_new.drop(col, axis=1, inplace=True)

        df["Occupancy"] = df_new.mean(axis=1, skipna=True)
        df["Occupancy"] = [0 if np.isnan(x) else x for x in df["Occupancy"]]
        df.loc[df["Occupancy"] != 0, "Occupancy"] = 1
        return df

    @staticmethod
    def mean_indoor_op_air_temp(df, building_name,filter_by=""):
        """Compute indoor mean operative temperature considering all thermal
        zones of the main building block.

        :param df: eplusout.csv
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame with new "T_op_i[C]" column
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        building_name = building_name.upper()
        df_new = df.copy()
        for col in df_new.columns:
            if "Zone Operative Temperature [C](TimeStep)" not in col or building_name not in col or filter_by.lower() not in col.lower():
                df_new = df_new.drop(col, axis=1)
    
        df["T_op_i[C]"] = df_new.mean(axis=1)
        return df

    @staticmethod
    def mean_vent_ach(df, building_name,filter_by=""):
        """Compute indoor mean operative temperature considering all thermal
        zones of the main building block.

        :param df: eplusout.csv
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame with new "T_op_i[C]" column
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        building_name = building_name.upper()
        df_new = df.copy()
        for col in df_new.columns:
            if "Zone Ventilation Air Change Rate [ach](TimeStep)" not in col or building_name not in col or filter_by.lower() not in col.lower():
                df_new = df_new.drop(col, axis=1)


    @staticmethod
    def sum_vent_volume(df, building_name,volume,filter_by=""):
        """Compute indoor mean operative temperature considering all thermal
        zones of the main building block.

        :param df: eplusout.csv
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame with new "T_op_i[C]" column
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        building_name = building_name.upper()
        df_new = df.copy()
        for col in df_new.columns:
            if "Zone Ventilation Current Density Volume [m3](TimeStep)" not in col or building_name not in col or filter_by.lower() not in col.lower():
                df_new = df_new.drop(col, axis=1)

        # print(df_new)
        df_new = df_new.resample("1H").sum()
        df["Airflow_i[m3]"] = df_new.sum(axis=1)
        
        # print(df["Airflow_i[m3/s]"])
        return df

    @staticmethod
    def indoor_mean_air_temp(df, building_name, filter_by="", keep_single=False):
        """Compute indoor mean air temperature considering all thermal zones of
        the main building block.

        :param df: eplusout.csv
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame with new "T_db_i[C]" column
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        building_name = building_name.upper()
        df_new = df.copy()
        var = "Zone Mean Air Temperature [C](TimeStep)"

        if not any([var in c for c in df_new.columns]):
            warnings.warn(
                var
                + " is not in the EnergyPlus outputs. NaN values may be used."
            )

        for col in df_new.columns:
            if var not in col or building_name not in col or filter_by.lower() not in col.lower():
                df_new = df_new.drop(col, axis=1)

        if keep_single:
            df_new["T_db_i[C]"] = df_new.mean(axis=1)
            return df_new
        else:
            df["T_db_i[C]"] = df_new.mean(axis=1)
            return df

    @staticmethod
    def indoor_mean_air_RH(df, building_name,filter_by=""):
        """Compute indoor mean air humidity considering all thermal zones of
        the main building block.

        :param df: eplusout.csv
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame with new "RH_i[%]" column
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        building_name = building_name.upper()
        df_new = df.copy()
        for col in df_new.columns:
            if "Zone Air Relative Humidity [%](TimeStep)" not in col or building_name not in col or filter_by.lower() not in col.lower():
                df_new = df_new.drop(col, axis=1)
                
        df["RH_i[%]"] = df_new.mean(axis=1)
        return df

    @staticmethod
    def indoor_mean_rad_temp(df, building_name,filter_by=""):
        """Compute indoor mean radiant temperature considering all thermal
        zones of the main building block.

        :param df: eplusout.csv
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame with new "T_rad_i[C]" column
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        building_name = building_name.upper()
        df_new = df.copy()
        for col in df_new.columns:
            if "Zone Mean Radiant Temperature [C](TimeStep)" not in col or building_name not in col or filter_by.lower() not in col.lower():
                df_new = df_new.drop(col, axis=1)
                
        df["T_rad_i[C]"] = df_new.mean(axis=1)
        return df

    @staticmethod
    def indoor_mean_co2(df,building_name, filter_by=""):
        """Compute indoor mean radiant temperature considering all thermal
        zones of the main building block.

        :param df: eplusout.csv
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame with new "CO2[ppm]" column
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        building_name = building_name.upper()
        df_new = df.copy()
        for col in df_new.columns:
            if "Zone Air CO2 Concentration [ppm](TimeStep)" not in col or building_name not in col or filter_by.lower() not in col.lower():
                df_new = df_new.drop(col, axis=1)

        df["CO2_i[ppm]"] = df_new.mean(axis=1)
        return df

    @staticmethod
    def convert_to_W_m3(value, volume):
        """Convert energy consumption expressed in J to W/m3.

        :param value: consumption value in J
        :type value: float
        :param volume: Volume
        :type volume: int or float
        :return: consumption value in W/m3
        :rtype: float
        """
        # TODO: change 600 in 60*timestep...
        return value/(600*volume)

    @staticmethod
    def convert_to_W_m2(value, area):
        """Convert energy consumption expressed in J to W/m3.

        :param value: consumption value in J
        :type value: float
        :param volume: Volume
        :type volume: int or float
        :return: consumption value in W/m3
        :rtype: float
        """
        # TODO: change 600 in 60*timestep...
        return value/(600*area)

    @staticmethod
    def convert_to_kWh_m2(value, area):
        """Convert energy consumption expressed in J to kWh/m2.

        :param value: consumption value in J
        :type value: float
        :param area: Area
        :type area: int or float
        :return: consumption value in kWh/m2
        :rtype: float
        """ 
        return value/(3.6*(10**6)*area)

    @staticmethod
    def convert_to_kWh(value):
        """Convert energy consumption expressed in J to kWh

        :param value: consumption value in J
        :type value: float
        :return: consumption value in kWh
        :rtype: float
        """ 
        return value/(3.6*(10**6))

    @staticmethod
    def fr_season(heat_season=None, cool_season=None):
        """Compute free running season in form of availability schedule (1 if free running), 
        so if both heating and cooling are not available

        :param heat_season: schedule for heating season (1 if on, 0 if off)
        :type heat_season: list
        :param cool_season: schedule for cooling season (1 if on, 0 if off)
        :type cool_season: list
        :return: free running season shourly chedule (1 if free running, 0 if not)
        :rtype: list
        """ 
        fr = [x or y for x,y in zip(heat_season,cool_season)]
        return [1-x for x in fr] 

