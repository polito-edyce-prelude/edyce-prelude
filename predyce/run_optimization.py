#%%
from email.policy import default
from pymoo.optimize import minimize
from pymoo.core.problem import Problem
import numpy as np
from pymoo.algorithms.moo.nsga2 import NSGA2
from pymoo.algorithms.moo.nsga3 import NSGA3
from pymoo.algorithms.moo.unsga3 import UNSGA3
from pymoo.factory import get_problem, get_reference_directions
from pymoo.visualization.scatter import Scatter

from predyce.IDF_class import IDF
from predyce.runner import Runner
from pathlib import Path
import json
import yaml
import argparse
import tempfile
import pandas as pd

import matplotlib.pyplot as plt
import plotly.express as px 

import csv

from pymoo.algorithms.moo.rnsga2 import RNSGA2
from pymoo.problems import get_problem
from pymoo.optimize import minimize

# Absolute path of the directory of current script.
P = Path(__file__).parent.absolute()

# Configuration file.
with open(P / "config.yml") as config_file:
    CONFIG = yaml.load(config_file, Loader=yaml.FullLoader)

# Argument parameters which override the ones in configuration file.
parser = argparse.ArgumentParser()
parser.add_argument("--checkpoint-data", help="Checkpoint data absolute path (default: None)", default=None)
parser.add_argument("-c", "--checkpoint-interval", help="Checkpoint interval (default: 128)", default=128, type=int)
parser.add_argument("-d", "--output-directory", help="Output directory absolute path (default: /predyce-out)", default=CONFIG["out_dir"])
parser.add_argument("-i", "--idd", help='IDD version', default=CONFIG["idd_version"])
parser.add_argument("-f", "--input-file", help="Input data file absolute path (default: in.json in executable directory" , default=P / "in.json")
parser.add_argument("-j", "--jobs", help= "Multi-process with N processes (0=auto)", default=CONFIG["num_of_cpus"], type=int)
parser.add_argument("-o", "--original", help="Include original model in dataframe permutations (default: True)", action="store_false", default=True)
parser.add_argument("-m", "--measured-data", help="Measured data file absolute path (default: None)", default="")
parser.add_argument("-p", "--plot-directory", help="Plot directory path (default: /predyce-plots)", default=CONFIG["plot_dir"])
parser.add_argument("-t", "--temp-directory", help="Temp directory path (default: System temp folder)", default=Path(tempfile.gettempdir()) / "predyce")
parser.add_argument("-w", "--weather", help= "Weather file absolute path (default: "" and must be in case specified in input JSON file)", default=CONFIG["epw"])
parser.add_argument("model", help= "Building model path (IDF format only accepted)")

argms = parser.parse_args()

# IDF file.
fname1 = argms.model

# Find and set IDD file.
idd_folder = Path(__file__).parent.absolute() / "resources" / "iddfiles"
idd_version = str(argms.idd).replace(".", "-")
for entry in idd_folder.iterdir():
    if idd_version in entry.name:
        idd = entry
IDF.setiddname(idd)

# Input JSON file.
INPUT = argms.input_file

# Find and set EPW file.
weather = Path(argms.weather).resolve()  # Convert to absolute path.
if weather.is_file():
    EPW = weather
    EPW_DIR = weather.parent
elif weather.is_dir():
    EPW = None
    EPW_DIR = weather
else:
    raise IOError("Error in the EPW weather file")

idf1 = IDF(fname1, EPW)

# Checkpoint data.
CHECKPOINT_DATA = argms.checkpoint_data



def updateJSONnew(input,designs):

    with open(input) as f:
        data=json.load(f)
    
    print(data)
    i=0
    for action2 in data["optimization_parameters"]:
        for action in data["actions"]:
             if action2 ==  action:
                 print("actions:  ",action2,action)
                 for param2 in data["optimization_parameters"][action2]:
                     for param in data["actions"][action]:
                         if param2 == param:
                             print(param2,param)
                             data["actions"][action][param]=[]
                             data["actions"][action][param]+= list(designs[:,i])
                             i=i+1

    with open(input, 'w') as f:
        json.dump(data, f)
                 
# to define my problem
class ProblemWrapper(Problem):
    #designs: all the combination of input parameters
    #out: evaluation of my kpi/objectives (only 2 for NSGA-II)
    def _evaluate(self, designs, out,*args, **kwargs):
        #print("designs:   ",designs)
        #print("*****")
        #print("out:   ",out)
        res = []
        
        #acts=[("add_external_insulation_walls","Thickness"),("add_ceilin_insulation_tilted_roof","Thickness")]
        #modificare il json in input
        #updateJSON(designs)
        updateJSONnew(INPUT,designs)
        
        #print("        ***********\n       new iteration:  ")

        with open(INPUT, "r") as f:  
            input = json.loads(f.read())
        
        
        config = {
        "plot_dir": argms.plot_directory,
        "temp_dir": argms.temp_directory,
        "out_dir": argms.output_directory,
        "num_of_cpus": argms.jobs,
        "epw_dir": EPW_DIR,
        "data_true_dir": argms.measured_data,
    }
        #config = {"plot_dir": "plot_dir", "temp_dir": argms.temp_directory,"out_dir": "out_dir","num_of_cpus": argms.jobs,"epw_dir": EPW_DIR,"data_true_dir": argms.measured_data,}
        #config = {'plot_dir': r'C:\Users\user\Documents\predyce_runner\plot_dir', 'temp_dir':r'C:\Users\user\AppData\Local\Temp\predyce', 'out_dir': r"C:\Users\user\Documents\predyce_runner\out_dir", 'num_of_cpus': 10, 'epw_dir': r'C:\Users\user\Documents\predyce_runner\test\JSON', 'data_true_dir': ''}
        # Runner instance per creare il dataset
        runner = Runner(idf1,input,**config,checkpoint_interval=128,checkpoint_data=CHECKPOINT_DATA,graph=True,opt=True)  
        
        #runner.create_dataframe(input, argms.original)
        runner.create_dataframe(input, False)
        print("\n*********\n")
        print(runner.df)
        #aa=runner.df
        #aa.save_data(self.out_dir / "data_res.csv")
        #print("Heloooooooooooooo")
        runner.run()
        mydata=runner.results
        runner.print_summary()
        #print("!!!!!!!!!!!!!\n")
        
        objs=input["optimization_objectives"]
        
        res=[]
        #print("iteration:  ")
        
        if len(input["optimization_objectives"]) == 1 :
            for i in range(len(designs)):              #secondo for per valutare gli objectives con i parametri aggiornati
            #print("evaluationnnnnnnnnn"   ####??????????????
                res.append((mydata.loc[i,objs[0]])) 


        if len(input["optimization_objectives"]) == 2 :
            for i in range(len(designs)):              #secondo for per valutare gli objectives con i parametri aggiornati
            #print("evaluationnnnnnnnnn"   ####??????????????
                res.append((mydata.loc[i,objs[0]], mydata.loc[i,objs[1]])) 
        
        if len(input["optimization_objectives"]) == 3 :
            for i in range(len(designs)):              #secondo for per valutare gli objectives con i parametri aggiornati
            #print("evaluationnnnnnnnnn"   ####??????????????
                res.append((mydata.loc[i,objs[0]], mydata.loc[i,objs[1]], mydata.loc[i,objs[2]]))                 
            #print("res:  ",res[i])
            
        #print(len(res))  
        #print("res data", type(res))
        out['F'] = np.array(res)

        m=open('alldata.csv',"a")
        np.savetxt(m, res, delimiter =",",fmt ='% s')
        m.close()
    # Load input file.
def main():   

    with open(INPUT) as p:
        jsonData=json.load(p)
    nvar=0
    n_obj=2
    xlow=[]
    xup=[]
    for element in jsonData["optimization_parameters"]:
        for par in jsonData["optimization_parameters"][element]:
            nvar=nvar+1
            xlow.append(float(jsonData["optimization_parameters"][element][par]["lower"]))
            xup.append(float(jsonData["optimization_parameters"][element][par]["upper"]))
    #print("num var: ", numvar)
    #problem = ProblemWrapper(n_var=4, n_obj=2, xl=[0.025, 0.025 ,0.025,0], xu=[0.3, 0.3, 0.3,5])
    #prrendere upper and lower dal json
    print("Problem wrapper attributes:",nvar,n_obj,xlow,xup)
    problem = ProblemWrapper(n_var=nvar, n_obj=len(jsonData["optimization_objectives"]), xl=xlow, xu=xup)
    pf = problem.pareto_front()
    #algorithm = NSGA2(pop_size=10)
    #ref_points = np.array([[9, 75]])
    

    if jsonData["optimization_algorithm"] == "RNSGA2":
        ref_points = jsonData["ref_points_coordinates"]
        weights = jsonData["ref_points_weights"]
        algorithm = RNSGA2(ref_points=np.array(ref_points), pop_size=jsonData["population_size"], epsilon=0.01, normalization='front', extreme_points_as_reference_points=False, weights=weights)
        
    
    if jsonData["optimization_algorithm"] == "NSGA2":
        algorithm = NSGA2(pop_size=jsonData["population_size"])
        

    if jsonData["optimization_algorithm"] == "NSGA3":
        ref_dirs = get_reference_directions("das-dennis", len(jsonData["optimization_objectives"]), jsonData["D-D_partitions_number"] )
        algorithm = NSGA3(ref_dirs, pop_size= jsonData["population_size"])

    if jsonData["optimization_algorithm"] == "RNSGA3":
        print("RNSGA3 not implemented yet")
    
    #ref_dirs = get_reference_directions("das-dennis", 3, n_partitions=12)
    #algorithm = NSGA3(ref_dirs,pop_size=100)
    
    stop_criteria = ("n_gen", jsonData["num_generations"])
    algorithm.setup(problem, termination=('n_gen', jsonData["num_generations"]), seed=1, verbose=False)

    while algorithm.has_next():

        # do the next iteration
        algorithm.next()

        # do same more things, printing, logging, storing or even modifying the algorithm object
        print("\nIteration:  ",algorithm.n_gen,"\nnum_evaluations:  ", algorithm.evaluator.n_eval)
        print("\nNext iteration?:",algorithm.has_next())

    results = minimize(problem=problem,algorithm=algorithm,save_history=True,termination=stop_criteria,seed=1,pf=pf,disp=False)
    #plot RNSGA2
    #Scatter().add(pf, label="pf").add(results.F, label="F").add(np.array(ref_points), label="ref_points").save("pareto")
    #plot NSGA2
    Scatter().add(results.F,label="F").save("NSGA2")
    plt.xlabel("Q_c")
    plt.ylabel("Discomfort")
    plt.show()

    print("optimum solutions\n",results.F)
    print("optimum parameters\n",results.X)

    with open('alldata.csv', 'r') as f:
        alldata = list(csv.reader(f, delimiter=","))

    alldata = np.array(alldata)
    
    #Scatter().add(results.F).show()
    res_data = results.F.T
    #Scatter().add(alldata).show()
    alldata= alldata.astype(np.float)
    print(type(res_data))
    print(alldata)
    print(type(alldata))

    plt.figure()
    
    plt.plot(alldata[:,0],alldata[:,1],"x")
    plt.plot(res_data[0], res_data[1],"ro")
    plt.title("Pareto Front")
    plt.legend(["Sub-optimal solutions","Optimal solutions"])
    plt.xlabel("Q_c")
    plt.ylabel("Discomfort")
    plt.savefig("pareto_plot")

    plt.show()
    '''
    ##### color plot for optimal solution 
    dfcolorX = pd.DataFrame(results.X, columns = ["set_object_params_temp.Setpoint","set_object_params_GHI.Setpoint_2","set_object_params_windandstack_delta.Delta_Temperature","4","5","set_object_params_windandstack_max_out_t.Maximum_Outdoor_Temperature","set_object_params_windandstack_max_in_t.Maximum_Indoor_Temperature"])
    dfcolorX.reset_index(level=0, inplace=True)
    print(dfcolorX.columns)
    
    dfcolorF = pd.DataFrame(results.F, columns = ['Q_c','PPD'])
    dfcolorF.reset_index(level=0, inplace=True)
    print(dfcolorF.columns)
    
    dfcolor = pd.merge(left=dfcolorX, right=dfcolorF)
    print(dfcolor)
    fig = px.parallel_coordinates(dfcolor, color="Q_c",dimensions=["set_object_params_temp.Setpoint","set_object_params_GHI.Setpoint_2","set_object_params_windandstack_delta.Delta_Temperature","4","5","set_object_params_windandstack_max_out_t.Maximum_Outdoor_Temperature","set_object_params_windandstack_max_in_t.Maximum_Indoor_Temperature"],
                             color_continuous_scale=px.colors.diverging.Tealrose)
    fig.show()
    
    fig = px.parallel_coordinates(dfcolor, color="PPD",dimensions=["set_object_params_temp.Setpoint","set_object_params_GHI.Setpoint_2","set_object_params_windandstack_delta.Delta_Temperature","4","5","set_object_params_windandstack_max_out_t.Maximum_Outdoor_Temperature","set_object_params_windandstack_max_in_t.Maximum_Indoor_Temperature"],
                             color_continuous_scale=px.colors.diverging.Tealrose)
    fig.show()
    '''
    results_F = pd.DataFrame(results.F,columns = ['Q_c','PPD'])
    results_X = pd.DataFrame(results.X,columns = ["Tout[°C]","GHI[W/m2]","Delta[°C]","Min.Tin[°C]","Min.Tout[°C]","Max.Tout[°C]"])
    results_df = pd.concat([results_F,results_X],axis=1)
    results_df.to_csv("Ry_results.csv")
    print("\n******************\n",results_df.head())

if __name__ == "__main__":
    main()

#new command line
#c:/Users/user/Documents/predyce_runner/.venv/Scripts/python.exe c:/Users/user/Documents/predyce_runner/predyce/run_optimization.py C:\Users\user\Documents\predyce_runner\test\IDF\second_try.idf -w C:\Users\user\Documents\predyce_runner\test\predyce_esempio\Alborg_Airp_hour.epw -f C:\Users\user\Documents\predyce_runner\test\predyce_esempio\example_new.json -o -j 9

#c:/Users/user/Documents/predyce_runner/.venv/Scripts/python.exe c:/Users/user/Documents/predyce_runner/predyce/run_optimization.py C:\Users\user\Documents\predyce_runner\T44\WP3o4-Turin-Scheduled0-5.idf -w C:\Users\user\Documents\predyce_runner\T44\TPM-hour_new.epw -f C:\Users\user\Documents\predyce_runner\T44\E3a_opt.json -o -j 9
#T44 pc vecchio
#c:/Users/user/Documents/predyce_runner/.venv/Scripts/python.exe c:/Users/user/Documents/predyce_runner/predyce/run_optimization.py C:\Users\user\Documents\predyce_runner\T44\WP3o4-Turin-Scheduled0-5.idf -w C:\Users\user\Documents\predyce_runner\T44\Italy_Torino-hour.epw -f C:\Users\user\Documents\predyce_runner\T44\E1_opt.json -o -j 9
#pc nuovo
#c:/Users/user/Documents/predyce_runner/.venv/Scripts/python.exe c:/Users/user/Documents/predyce_runner/predyce/run_optimization.py C:\Users\user\Documents\predyce_runner\T44\WP3o4-Turin-Scheduled0-5.idf -w C:\Users\user\Documents\predyce_runner\T44\Italy_Torino-hour.epw -f C:\Users\user\Documents\predyce_runner\T44\E1_opt.json -o -j 9 
#C:\Users\pcarr_c7uejdj\Documents\predyce\.venv\Scripts\python.exe C:\Users\pcarr_c7uejdj\Documents\predyce\predyce\run_optimization.py C:\Users\pcarr_c7uejdj\Documents\predyce\T44\WP3o4-Turin-Scheduled0-5.idf -w C:\Users\pcarr_c7uejdj\Documents\predyce\T44\Italy_Torino-hour.epw -f C:\Users\pcarr_c7uejdj\Documents\predyce\T44\E1_opt.json -o -j 9

#c:/Users/user/Documents/predyce_runner/.venv/Scripts/python.exe c:/Users/user/Documents/predyce_runner/predyce/run_optimization.py C:\Users\user\Documents\predyce_runner\test\T44\WP3o4-Turin-Scheduled0-5.idf -w C:\Users\user\Documents\predyce_runner\test\T44\TPM-hour_new.epw -f C:\Users\user\Documents\predyce_runner\test\T44\E1_opt.json -o -j 9