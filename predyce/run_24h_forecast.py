# coding: utf-8
from time import sleep
from predyce.IDF_class import IDF
from predyce.runner import Runner, idf_editor
from pathlib import Path
import pandas as pd
import json
import yaml
import argparse
import tempfile
import datetime
from kpi import EnergyPlusKPI
import os
import copy
import matplotlib.pyplot as plt
from predyce import epw_reader
import numpy as np
import re
from predyce.forecast import set_ach, set_lle_shading, set_mechanical_ventilation, set_stack_ventilation, set_blind, set_blind_std, set_stack_ventilation_std

DOW = {
    6: "Sunday",
    0: "Monday",
    1: "Tuesday",
    2: "Wednesday",
    3: "Thursday",
    4: "Friday",
    5: "Saturday"
    }

FIRST_DAY = None
REFERENCE = 1

# Absolute path of the directory of current script.
P = Path(__file__).parent.absolute()


class MyDateTime(datetime.datetime):
    """Add some methods to datetime."""
    def __new__(cls, *args, **kwargs):
        return datetime.datetime.__new__(cls, *args, **kwargs)

    def minutes_of_year(self):
        return self.seconds_of_year() // 60

    def hours_of_year(self):
        return self.minutes_of_year() // 60

    def seconds_of_year(self):
        dt0 = datetime.datetime(self.year, 1, 1, tzinfo=self.tzinfo)
        delta = self-dt0
        return int(delta.total_seconds())

# Configuration file.
with open(P / "config.yml") as config_file:
    CONFIG = yaml.load(config_file, Loader=yaml.FullLoader)

# Argument parameters which override the ones in configuration file.
parser = argparse.ArgumentParser()
parser.add_argument("--checkpoint-data", help="Checkpoint data absolute path (default: None)", default=None)
parser.add_argument("-c", "--checkpoint-interval", help="Checkpoint interval (default: 128)", default=128, type=int)
parser.add_argument("-d", "--output-directory", help="Output directory absolute path (default: /predyce-out)", default=CONFIG["out_dir"])
parser.add_argument("-i", "--idd", help='IDD version', default=CONFIG["idd_version"])
parser.add_argument("-f", "--input-file", help="Input data file absolute path (default: in.json in executable directory" , default=P / "in.json")
parser.add_argument("-j", "--jobs", help= "Multi-process with N processes (0=auto)", default=CONFIG["num_of_cpus"], type=int)
parser.add_argument("-o", "--original", help="Include original model in dataframe permutations (default: True)", action="store_false", default=True)
parser.add_argument("-m", "--measured-data", help="Measured data file absolute path (default: None)", default="")
parser.add_argument("-p", "--plot-directory", help="Plot directory path (default: /predyce-plots)", default=CONFIG["plot_dir"])
parser.add_argument("-t", "--temp-directory", help="Temp directory path (default: System temp folder)", default=CONFIG["temp_dir"])
parser.add_argument("-w", "--weather", help= "Weather file absolute path (default: "" and must be in case specified in input JSON file)", default=CONFIG["epw"])
parser.add_argument("model", help= "Building model path (IDF format only accepted)")

args = parser.parse_args()


# if True:
# def setup():
    # IDF file.
fname1 = args.model

# Find and set IDD file.
idd_folder = Path(__file__).parent.absolute() / "resources" / "iddfiles"
idd_version = str(args.idd).replace(".", "-")
for entry in idd_folder.iterdir():
    if idd_version in entry.name:
        idd = entry
IDF.setiddname(idd)

# Input JSON file.
INPUT = args.input_file

# Find and set EPW file.
weather = Path(args.weather).resolve()  # Convert to absolute path.
if weather.is_file():
    EPW = weather
    EPW_DIR = weather.parent
elif weather.is_dir():
    EPW = None
    EPW_DIR = weather
else:
    raise IOError("Error in the EPW weather file")

epw = EPW
# Load input file.
with open(INPUT, "r") as f:
    input_file = json.loads(re.sub("//.*", "", f.read(), flags=re.MULTILINE))

idf1 = IDF(fname1, EPW)
idf_editor.remove_daylightsaving(idf1)
idf1.set_block(input_file["building_name"])

config = {
    "plot_dir": args.plot_directory,
    "temp_dir": args.temp_directory,
    "out_dir": args.output_directory,
    "num_of_cpus": args.jobs,
    "epw_dir": EPW_DIR,
    "data_true_dir": args.measured_data,
}

CONFIG = config
INPUT_FILE = input_file
# return (idf1, config, EPW, input_file)

    # simulation_day = "27/07"

CNT = 0
SET = 0
def main(idf1, config, epw, input_file, simulation_day=None):
    global CNT, SET
    # Get current day and month
    # day = str(datetime.datetime.today().day)
    # month = str(datetime.datetime.today().month)
    year = datetime.datetime.today().year
    dow = datetime.datetime(year, 1, 1).weekday()
    dow = DOW[dow]

    global FIRST_DAY
    if simulation_day is not None:
        FIRST_DAY = MyDateTime.strptime(simulation_day, "%d/%m")  # To test
    else:
        FIRST_DAY = MyDateTime.strptime(input_file["day"], "%d/%m")
        cur_day = FIRST_DAY + datetime.timedelta(days=CNT)
        CNT = CNT + 1

    # print("24h forecast. Now is: ", day)
    print("24h forecast for day: ", "/".join((str(cur_day.day), str(cur_day.month))))

    try:
        os.makedirs(Path(args.temp_directory) / "idf_steps")
    except FileExistsError:
        pass

    columns_to_remove = []
    # step0 = copy.deepcopy(idf1)
    # set_step_outputs(step0)
    # step0.save(Path(args.temp_directory) / "idf_steps" / "step_0.idf")

    if not SET:
        idf2 = copy.deepcopy(idf1)
        input_file2 = copy.deepcopy(input_file)
        # Change runperiod to make 24h forecast analysis supporting ACM.
        start = cur_day - datetime.timedelta(days=16)
        end = cur_day + + datetime.timedelta(days=2)
        idf_editor.change_runperiod(
            idf2,
            "-".join((str(start.day).zfill(2), str(start.month).zfill(2))),
            "-".join((str(end.day).zfill(2), str(end.month).zfill(2)))
        )
        idf2.idfobjects["TIMESTEP"][0].Number_of_Timesteps_per_Hour = 6
        idf_editor.set_outputs(idf2, [None], None, frequency="RunPeriod", reset=True)
        idf_editor.set_outputs(idf2, [None], None, frequency="Timestep", reset=False)
        idf_editor.set_outputs(idf2, ["Site Outdoor Air Drybulb Temperature"], "*", frequency="Timestep", reset=False)
        idf_editor.set_outputs(idf2, ["Zone Operative Temperature"], "*", frequency="Timestep", reset=False)

        # NOTE: Enable to perform comparison.

        if REFERENCE:
            # Original IDF.
            # comparison(idf2, input_file2, folder="reference", title="Hourly ACM\nNo blind; No ventilation", suffix="o")
            # assert False

            # Standard shading IDF with temperature SP.
            idf3 = copy.deepcopy(idf2)
            set_blind_std(idf3)
            # comparison(idf3, input_file2, folder="reference", title="Hourly ACM\nBlind SP=24 °C", suffix="blind_std")
            # assert False

            # B1 Standard shading IDF with schedule.
            idf3 = copy.deepcopy(idf2)
            schedule = {
                "Name": "blind_std",
                "Schedule_Type_Limits_Name": "Any Number",
                "Field_1": "Through: 12/31",
                "Field_2": "For: AllDays",
                "Field_3": "Until: 10:00",
                "Field_4": 0,
                "Field_5": "Until: 17:00",
                "Field_6": 1,
                "Field_7": "Until: 24:00",
            }
            set_blind_std(idf3, schedule)
            # comparison(idf3, input_file2, folder="reference", title="Hourly ACM\nBlind 10:00-17:00", suffix="blind_std")
            # assert False

            # Standard shading + wind and stack IDF.
            idf4 = copy.deepcopy(idf2)
            set_blind_std(idf4)
            schedule = {
                "Name": "stack_std",
                "Schedule_Type_Limits_Name": "Any Number",
                "Field_1": "Through: 12/31",
                "Field_2": "For: AllDays",
                "Field_3": "Until: 6:00",
                "Field_4": 0.3,
                "Field_5": "Until: 23:00",
                "Field_6": 0,
                "Field_7": "Until: 24:00",
                "Field_8": 0.3,
            }
            set_stack_ventilation_std(idf4, schedule)
            # comparison(idf4, input_file2, folder="reference", title="Hourly ACM\nBlind SP=24 °C; Night vent.", suffix="blind_std_vnt_nght")
            # assert False

            # Standard shading + wind and stack IDF.
            idf4 = copy.deepcopy(idf2)
            set_blind_std(idf4)
            schedule = {
                "Name": "stack_std",
                "Schedule_Type_Limits_Name": "Any Number",
                "Field_1": "Through: 12/31",
                "Field_2": "For: AllDays",
                "Field_3": "Until: 7:00",
                "Field_4": 0,
                "Field_5": "Until: 10:00",
                "Field_6": 0.3,
                "Field_7": "Until: 19:00",
                "Field_8": 0,
                "Field_9": "Until: 23:00",
                "Field_10": 0.3,
                "Field_11": "Until: 24:00",
                "Field_12": 0,
            }
            set_stack_ventilation_std(idf4, schedule)
            comparison(idf4, input_file2, folder="reference", title="Hourly ACM\nBlind SP=24 °C; Morn./even. vent.", suffix="blind_std_vnt_dd")
            assert False

            # Standard shading + wind and stack IDF.
            idf4 = copy.deepcopy(idf2)
            set_blind_std(idf4)
            schedule = {
                "Name": "stack_std",
                "Schedule_Type_Limits_Name": "Any Number",
                "Field_1": "Through: 12/31",
                "Field_2": "For: AllDays",
                "Field_3": "Until: 24:00",
                "Field_4": 0.3,
            }
            set_stack_ventilation_std(idf4, schedule)
            # comparison(idf4, input_file2, folder="reference", title="Hourly ACM\nBlind SP=24 °C; Vent. 24/7)", suffix="blind_std_vnt_always")

            # Standard shading + wind occ and stack IDF.
            idf5 = copy.deepcopy(idf2)
            set_blind_std(idf5)
            schedule = {
                "Name": "stack_std",
                "Schedule_Type_Limits_Name": "Any Number",
                "Field_1": "Through: 31 Dec",
                "Field_2": "For: Weekdays SummerDesignDay",
                "Field_3": "Until: 07:00",
                "Field_4": 0,
                "Field_5": "Until: 08:00",
                "Field_6": 0.3,#0.5,
                "Field_7": "Until: 09:00",
                "Field_8": 0.3,#1,
                "Field_9": "Until: 10:00",
                "Field_10": 0.5,
                "Field_11": "Until: 17:00",
                "Field_12": 0,
                "Field_13": "Until: 18:00",
                "Field_14": 0.3,#0.25,
                "Field_15": "Until: 19:00",
                "Field_16": 0.5,
                "Field_17": "Until: 20:00",
                "Field_18": 0.5,#0.75,
                "Field_19": "Until: 22:00",
                "Field_20": 0.5,#1,
                "Field_21": "Until: 23:00",
                "Field_22": 0.3,#0.75,
                "Field_23": "Until: 24:00",
                "Field_24": 0.3,#0.25,
                "Field_25": "For: Weekends",
                "Field_26": "Until: 09:00",
                "Field_27": 0,
                "Field_28": "Until: 21:00",
                "Field_29": 0.5,#1,
                "Field_30": "Until: 24:00",
                "Field_31": 0,
                "Field_32": "For: Holidays",
                "Field_33": "Until: 09:00",
                "Field_34": 0,
                "Field_35": "Until: 21:00",
                "Field_36": 0.5,#1,
                "Field_37": "Until: 24:00",
                "Field_38": 0,
                "Field_39": "For: WinterDesignDay AllOtherDays",
                "Field_40": "Until: 24:00",
                "Field_41": 0,
            }
            set_stack_ventilation_std(idf5, schedule)
            comparison(idf5, input_file2, folder="reference", title="Hourly ACM\nBlind SP=24 °C; occ. vent.", suffix="blind_std_vnt_occ")
            assert False

        


        # idf_editor.change_ach(idf1, 0, "ventilation")

    # day

    # ACH.
    if "natural_ventilation" in input_file["forecast_actions"]:
        if not SET:
            set_ach(idf1, temp_dir=args.temp_directory)
        ach_filename = Path(args.temp_directory) / "schedule_runs/ach_schedules.csv"
        ach_file = pd.read_csv(Path(args.temp_directory) / "schedule_runs/ach_schedules.csv", sep=";", index_col=0)
        input_file["actions"]["edit_csv_schedule_ach"] = {
            "csv": [str(ach_filename)],
            "columns": [list(ach_file.columns)],
            "value": input_file["forecast_actions"]["natural_ventilation"]
        }
    
    # Blind and slats.
    if "blind" in input_file["forecast_actions"]:
        if not SET:
            if input_file["demo_case"] == "lle":
                set_lle_shading(idf1, temp_dir=args.temp_directory)
            else:
                set_blind(idf1, temp_dir=args.temp_directory)
        blind_filename = Path(args.temp_directory) / "_blind_runs/_blind.csv"
        blind_file = pd.read_csv(Path(args.temp_directory) / "_blind_runs/_blind.csv", sep=";", index_col=0)
        slat_filename = Path(args.temp_directory) / "_blind_runs/slat.csv"
        slat_file = pd.read_csv(Path(args.temp_directory) / "_blind_runs/slat.csv", sep=";", index_col=0)
        if input_file["demo_case"] == "lle":
            timestepped = True
        else:
            set_blind(idf1, temp_dir=args.temp_directory)
            timestepped = False  
        input_file["actions"]["edit_csv_schedule_blind"] = {
            "csv": [str(blind_filename)],
            "columns": [list(blind_file.columns)],
            "timestepped": [timestepped],
            "value": input_file["forecast_actions"]["blind"]
        }
        input_file["actions"]["edit_csv_schedule_slat"] = {
            "csv": [str(slat_filename)],
            "columns": [list(slat_file.columns)],
            "value": input_file["forecast_actions"]["slat"]
        }

    # Mechanical ventilation.
    if "mechanical_ventilation" in input_file["forecast_actions"]:
        if not SET:
            set_mechanical_ventilation(idf1, temp_dir=args.temp_directory)
        mech_filename = Path(args.temp_directory) / "mech.csv"
        mech_file = pd.read_csv(Path(args.temp_directory) / "mech.csv", sep=";", index_col=0)
        # for c in mech_file.columns:
        #     name = c[c.find("_ZONING_") + len("_ZONING_"):c.find(":")]
        #     input_file["actions"]["edit_csv_schedule_mech_" + name] = {
        #         "csv": [str(mech_filename)],
        #         "columns": [c],
        #         "value": input_file["forecast_actions"]["mechanical_ventilation"]
        #     }
        input_file["actions"]["edit_csv_schedule_mech"] = {
            "csv": [str(mech_filename)],
            "columns": [list(mech_file.columns)],
            "value": input_file["forecast_actions"]["mechanical_ventilation"]
        }

    # Stack and wind ventilation.
    if "stack_and_wind_ventilation" in input_file["forecast_actions"]:
        if not SET:
            set_stack_ventilation(idf1, temp_dir=args.temp_directory, add=False, set_std=True)
        stack_filename = Path(args.temp_directory) / "_stack.csv"
        stack_file = pd.read_csv(Path(args.temp_directory) / "_stack.csv", sep=";", index_col=0)
        for c in list(stack_file.columns):
            if input_file["demo_case"] == "lle":
                if "open" not in c:
                    continue
            columns_to_remove.append("edit_csv_schedule_stack" + "_" + c[0:3] + ".hours")
            columns_to_remove.append("edit_csv_schedule_stack" + "_" + c[0:3] + ".columns")

        for c in list(stack_file.columns):
            if input_file["demo_case"] == "lle":
                if "open" not in c:
                    continue
            input_file["actions"]["edit_csv_schedule_stack" + "_" + c[0:3]] = {
                "csv": [str(stack_filename)],
                "columns": [c],
                "value": input_file["forecast_actions"]["stack_and_wind_ventilation"]
            }

    # Find occupancy values.
    idf_editor.change_runperiod(idf1, "-".join((str(cur_day.day), str(cur_day.month), "22")), "-".join((str(cur_day.day), str(cur_day.month), "22")), fmt="%d-%m-%y")
    idf_editor.set_outputs(idf1, ["Zone People Occupant Count"], "*", frequency="Timestep", reset=True)

    if "hour_aggregations" not in input_file:
        try:
            os.makedirs(Path(args.temp_directory) / "occupancy_runs")
        except FileExistsError:
            pass
        idf1.save(Path(args.temp_directory) / "occupancy_runs" / "test.idf")
        print("Evaluating occupancy schedules...")
        idf1.run(output_directory=Path(args.temp_directory) / "occupancy_runs", readvars=True, verbose="q")
        res = pd.read_csv(Path(args.temp_directory) / "occupancy_runs" / "eplusout.csv")
        res = EnergyPlusKPI.get_occupancy(res, "")  # FIXME: Set building name
        res.index = res["Date/Time"].apply(EnergyPlusKPI.convert_to_datetime)
        res = res.resample("1H").mean()

        # Elaborate occupancy values and select hours.
        a = res["Occupancy"][:-1]

        g = []
        x = []
        last = 0
        for h in a.index:
            if a[h] == 0:
                if last == 0:
                    x.append(h.hour)
                else:
                    g.append(x)
                    x = []
                    x.append(h.hour)
                    last = 0
            else:
                g.append(x)
                x = []
                x.append(h.hour)
                last = 1
        g.append(x)
        occupancy = g

    # Change runperiod to make 24h forecast analysis supporting ACM.
    start = cur_day - datetime.timedelta(days=16)
    end = cur_day + + datetime.timedelta(days=2)
    idf_editor.change_runperiod(
        idf1,
        "-".join((str(start.day).zfill(2), str(start.month).zfill(2), "22")),
        "-".join((str(end.day).zfill(2), str(end.month).zfill(2), "22")),
        fmt="%d-%m-%y"
    )
    idf1.idfobjects["TIMESTEP"][0].Number_of_Timesteps_per_Hour = 6
    idf_editor.set_outputs(idf1, [None], None, frequency="RunPeriod", reset=True)
    idf_editor.set_outputs(idf1, [None], None, frequency="Timestep", reset=False)
    idf_editor.set_outputs(idf1, ["Site Outdoor Air Drybulb Temperature"], "*", frequency="Timestep", reset=False)
    idf_editor.set_outputs(idf1, ["Zone Operative Temperature"], "*", frequency="Timestep", reset=False)

    current_hour = cur_day.hours_of_year()
    current_mid_hour = MyDateTime.combine(cur_day, MyDateTime.min.time()).hours_of_year() + 24  # NOTE: 24, not 23
    # current_mid_hour = DAY.hours_of_year()
    print("Initial hour: ", current_mid_hour)

    if "hour_aggregations" not in input_file:
        print("Using auto aggregations...\n", occupancy)
        input_file["hour_aggregations"] = occupancy
    elif input_file["hour_aggregations"] == []:
        print("Using auto aggregations...\n", occupancy)

        input_file["hour_aggregations"] = occupancy


    SET = 1
    for h in input_file["hour_aggregations"]:
        if isinstance(h, list):
            h = [int(x) + current_mid_hour for x in h]
        else:
            h = int(h + current_mid_hour)

        # Assign hours to be modified.
        for _, v in input_file["actions"].items():
            v["hours"] = [h]
    
        input_file["kpi"]["adaptive_comfort_model"]["when"] = {
            "start": "2022/"
            + datetime.datetime.strftime(cur_day + datetime.timedelta(days=1), "%m/%d ")
            + "00:00:00",
            "end": "2022/"
            + datetime.datetime.strftime(cur_day + datetime.timedelta(days=1), "%m/%d ")
            + "23:00:00",
        }
        input_file["kpi"]["adaptive_residuals"]["when"] = {
            "start": "2022/"
            + datetime.datetime.strftime(cur_day + datetime.timedelta(days=1), "%m/%d ")
            + "00:00:00",
            "end": "2022/"
            + datetime.datetime.strftime(cur_day + datetime.timedelta(days=1), "%m/%d ")
            + "23:00:00",
        }
        # Edit schedule in order to use the current hour.
        idf_editor.set_outputs(
            idf1,
            [
                "Zone Ventilation Air Change Rate",
                "Zone Infiltration Air Change Rate",
            ],
            "*",
            "hourly",
            reset=False,
        )
        idf_editor.set_outputs(
            idf1,
            [
                "Zone Mean Air Temperature",
            ],
            "*",
            "timestep",
            reset=False,
        )

        # Runner instance.
        runner = Runner(
            idf1,
            input_file,
            **config,
            checkpoint_interval=args.checkpoint_interval,
            graph=True
        )
        # Simulating hour h.
        runner.create_dataframe(input_file, args.original)
        print("runner.df, day", cur_day, runner.df)
        df = copy.deepcopy(runner.df)

        if "blind" in input_file["forecast_actions"]:
            to_remove = df[df["edit_csv_schedule_blind.value"] == 0]
            to_remove.sort_values(by="edit_csv_schedule_slat.value", inplace=True)
        else:
            to_remove = df.copy()
        columns_to_remove.extend([
                                    "edit_csv_schedule_slat.value",
                                    "edit_csv_schedule_slat.hours",
                                    "edit_csv_schedule_slat.columns",
                                    "edit_csv_schedule_ach.hours",
                                    "edit_csv_schedule_ach.columns",
                                    "edit_csv_schedule_blind.hours",
                                    "edit_csv_schedule_blind.columns",
                                    "edit_csv_schedule_mech.hours",
                                    "edit_csv_schedule_mech.columns",                 
                                    ])
        

        for c in columns_to_remove:
            if c in to_remove.columns:
                # print("Dropping", c)
                to_remove.drop(columns=[c], inplace=True)
            else:
                pass
                # print("Cannot drop", c)

        # print("to_remove.columns", to_remove.columns)  # NOTE: Do not delete me.
        # print("to_remove", to_remove)  # NOTE: Do not delete me.
        to_remove = to_remove.duplicated()
        to_remove = to_remove[to_remove].index
        runner.df.drop(to_remove, inplace=True)
        runner.df.reset_index(drop=True, inplace=True)
        print(runner.df)
        runner.df.to_csv("dataset.csv", sep=";")
        print("Running simulations for hour", h)
        if len(h) > 1:
            string_hour = str(h[0]) + "to" + str(h[-1])
        else:
            string_hour = str(h[0])
        hour_plot_dir = Path(args.plot_directory + "/hour_" + string_hour)
        try:
            os.makedirs(hour_plot_dir)
        except FileExistsError:
            pass
        print("Plotting to: ", hour_plot_dir)
        runner.plot_dir = hour_plot_dir
        print(idf1.idfobjects["RUNPERIOD"])
        runner.run()

        # Read eplusout.csv
        filename = Path(args.output_directory) / "data_res.csv"
        res = pd.read_csv(filename, sep=';', index_col=0)

        res = res.join(res["adaptive_comfort_model"].apply(json.loads).apply(pd.Series))

        # Start scoring system.
        def category_score(x):
            if isinstance(x, str):
                x = json.loads(x)

            scoring = {
                "cat I": 1,
                "cat II up": 0.95,
                "cat II down": 0.95,
                "cat III up": 0.25,
                "cat III down": 0.25,
                "cat over III": 0,
                "cat under III": 0
            }
            try:
                del x["POR"]
            except KeyError:
                pass

            for k, v in x.items():
                # print(k, v, v * scoring[k])
                x[k] = v * scoring[k]

            return sum(x.values())

        def shading_score(x):
            blind = str(int(x["edit_csv_schedule_blind.value"]))
            slat = str(int(x["edit_csv_schedule_slat.value"]))
            # FIXME: Fix scoring for blind OFF
            scoring = {
                "blind": {
                    "0": 1,
                    "1": 0.2
                },
                "slat": {
                    "90": 0.7,  # Fully open
                    "0": 0.3    # Fully closed
                }
            }

            if blind == 0:
                score = scoring["blind"][blind]
            else:
                # Old method.
                # score = scoring["blind"][blind] + scoring["slat"][slat]

                # Timesteps method.
                score = (100-float(blind))/100 + (90-float(slat))/90
            
            return score

        def ventilation_score(x):
            if x < 0.5:
                return 1
            elif 0.5 <= x < 1:
                return 0.9
            elif 1 <= x < 2:
                return 0.7
            elif 2 <= x < 3:
                return 0.6
            elif x > 3:
                return 0.4

        def mech_score(v, idf):
            score = 0
            cnt = 0
            for dsoa in idf.idfobjects["DESIGNSPECIFICATION:OUTDOORAIR"]:
                # if dsoa.Outdoor_Air_Schedule_Name.lower() in s.name.lower():
                x = idf_editor.get_mech_ach(idf, dsoa.Outdoor_Air_Schedule_Name) * v

                # return idf_editor.get_mech_ach(idf, dsoa.Outdoor_Air_Schedule_Name)
                return x
                    # break
                if x < 0.02:
                    score += 1
                elif 0.02 <= x < 0.03:
                    score += 0.9
                elif 0.03 <= x < 0.04:
                    score += 0.7
                elif 0.04 <= x < 0.05:
                    score += 0.6
                elif x > 0.05:
                    score += 0.4
                cnt += 1

            return score/cnt

        # def mech_score(x):
        #     if x < 0.2:
        #         return 1
        #     elif 0.2 <= x < 0.3:
        #         return 0.9
        #     elif 0.3 <= x < 0.4:
        #         return 0.7
        #     elif 0.4 <= x < 0.5:
        #         return 0.6
        #     elif x > 0.5:
        #         return 0.4

        def pasd():
            pass

        res["category_score"] = res["adaptive_comfort_model"].apply(category_score)
        res["category_test"] = res["adaptive_residuals"]

        if "blind" in input_file["forecast_actions"]:
            res["blind_score"] = res[["edit_csv_schedule_blind.value", "edit_csv_schedule_slat.value"]].apply(shading_score, axis=1)#, args=(idf1, ))
        else:
            res["blind_score"] = 1

        if "stack_and_wind_ventilation" in input_file["forecast_actions"]:
            stack_cols = [c for c in res.columns if "edit_csv_schedule_stack" in c and "value" in c]
            stack_sum = res[stack_cols].sum(axis=1)
            res["stack_score"] = stack_sum.apply(ventilation_score)
        else:
            res["stack_score"] = 1

        if "mechanical_ventilation" in input_file["forecast_actions"]:
            res["mech_score"] = res["edit_csv_schedule_mech.value"].apply(mech_score, args=(idf1, ))
        else:
            res["mech_score"] = 1
        # print("RES MECH SCORE:\n", res["mech_score"])

        # print(stats.zscore(res["category_score"]))
        # print(stats.zscore(res["blind_score"]))
        # print(stats.zscore(res["stack_score"]))

        res["score"] = np.nansum(
            (
                (
                    res["category_score"],
                    res["blind_score"],
                    res["stack_score"],
                    res["mech_score"]
                    # stats.zscore(res["category_score"]),
                    # stats.zscore(res["blind_score"]),
                    # stats.zscore(res["stack_score"]),
                    # stats.zscore(res["mech_score"])
                )
            ),
            axis=0,
        )
        res.to_csv(Path("/home/pgrasso/work/predyce-temp/res.csv"), sep=";")
        # res.sort_values(
        #     by=[
        #         "cat over III",
        #         "cat under III",
        #         "cat III up",
        #         "cat III down",
        #         # "cat II up",
        #         # "cat II down",
        #         "edit_csv_schedule_blind.value",
        #         "edit_csv_schedule_ach.value",
        #         "cat I"
        #     ],
        #     ascending=[True, True, True, True,
        #     # True, True,
        #     True, True, False],
        #     inplace=True
        # )

        # Adaptive comfort: distance from line score.
        res["category_test"] = res["adaptive_residuals"].apply(lambda x: round(json.loads(x)[">0"], 2))

        # by = ["score"]
        # ascending = False
        by = ["category_test", "edit_csv_schedule_blind.value", "edit_csv_schedule_slat.value"]#, "edit_csv_schedule_mech.value"]
        ascending = [True, True, True]


        if "blind" in input_file["forecast_actions"]:
            for c in res.columns:
                if "stack" in c and "value" in c:
                    by.append(c)
                    if not isinstance(ascending, list):
                        ascending = [ascending]
                    ascending.append(True)

        # if "blind" in input_file["forecast_actions"]:
        #     by.append("edit_csv_schedule_blind.value")
        #     ascending = [ascending]
        #     ascending.append(True)
        #     ascending.append(False)
        #     by.append("edit_csv_schedule_slat.value")

        print("res:\n", res.columns)
        res.sort_values(
            by=by,
            ascending=ascending,
            inplace=True)

        res.to_csv(Path(args.output_directory) / "data_res_score.csv", sep=";")

        if "natural_ventilation" in input_file["forecast_actions"]:
            ach = res["edit_csv_schedule_ach.value"].iloc[0]
            ach_file.loc[ach_file.index[h], ach_file.columns] = ach
            ach_file.to_csv(ach_filename, sep=";")
            ach_file.iloc[current_mid_hour : current_mid_hour + 24].to_csv(
                Path(args.output_directory) / "temp_ach.csv", sep=";"
            )
            print("ACH: ", ach)

        if "blind" in input_file["forecast_actions"]:
            slat = res["edit_csv_schedule_slat.value"].iloc[0]
            blind = res["edit_csv_schedule_blind.value"].iloc[0]

            slat_file.loc[slat_file.index[h], slat_file.columns] = slat

            if "lle" in input_file["demo_case"]:
                idf_editor.edit_csv_schedule(
                    idf1,
                    csv=blind_file,
                    hours=h,  # It is already a list.
                    columns=blind_file.columns,
                    value=blind,
                    fix_paths=False,
                    timestepped=True
                    )
                blind_file.iloc[[x*6 for x in h]] = blind
            else:
                blind_file.loc[blind_file.index[h], blind_file.columns] = blind

            slat_file.to_csv(slat_filename, sep=";")
            blind_file.to_csv(blind_filename, sep=";")
            slat_file.iloc[current_mid_hour : current_mid_hour + 24].to_csv(
                Path(args.output_directory) / "temp_slat.csv", sep=";"
            )

            if "lle" in input_file["demo_case"]:  # (Sub-hourly shading).
                blind_file.iloc[current_mid_hour*6 : (current_mid_hour + 24) * 6 : 6].to_csv(
                    Path(args.output_directory) / "temp_blind.csv", sep=";"
                )

            else:
                blind_file.iloc[current_mid_hour : current_mid_hour + 24].to_csv(
                    Path(args.output_directory) / "temp_blind.csv", sep=";"
                )

            print("Slat angle: ", slat)
            print("Blind activation: ", blind)

        if "stack_and_wind_ventilation" in input_file["forecast_actions"]:
            stack_cols = [c for c in res.columns if "edit_csv_schedule_stack" in c and "value" in c]
            for c in stack_cols:
                stack = res[c].iloc[0]
                # print("STACK: ", stack)
                # print("\n\n\n")
                # assert False
                num = re.search(r'\d+', c).group()  # Extract number
                # column = [k for k in stack_file.columns if "stack_opening_factor" in k and num in k]
                column = [k for k in stack_file.columns if "STACK" in k and num in k]
                if input_file["demo_case"] == "lle":
                    column = [k for k in stack_file.columns if "open" in k]
                stack_file.loc[stack_file.index[h], column] = stack
                print("Opening factors (one orientation): ", stack)
                # print("h", [int(x) + current_mid_hour for x in h])
                # Stack new
                # print("value: ", stack)
                # print([column])
                # idf_editor.edit_csv_schedule(
                #     idf=idf1,
                #     csv="/home/pgrasso/work/predyce-24/_stack.csv",
                #     hours=[int(x) + current_mid_hour for x in h],
                #     columns=[c],
                #     value=stack,
                #     fix_paths=False,
                #     verbose=True
                #     )

            stack_file.to_csv(stack_filename, sep=";")
            stack_file.iloc[current_mid_hour : current_mid_hour + 24].to_csv(
                Path(args.output_directory) / "temp_stack.csv", sep=";"
            )
        # assert False

        if "mechanical_ventilation" in input_file["forecast_actions"]:
            mech = res["edit_csv_schedule_mech.value"].iloc[0]
            mech_file.loc[mech_file.index[h], mech_file.columns] = mech
            mech_file.to_csv(mech_filename, sep=";")
            mech_file.iloc[current_mid_hour : current_mid_hour + 24].to_csv(
                Path(args.output_directory) / "temp_mech.csv", sep=";"
            )
            print("Mech value: ", mech)


    if "lle" in input_file["demo_case"]:
        stack = pd.read_csv(Path(args.output_directory) / "temp_stack.csv", sep=";", index_col=0)
        stack = stack["P1_sim:Sim_room_2_Wall_7_0_0_0_0_5_Win_open"]
        slat = pd.read_csv(Path(args.output_directory) / "temp_slat.csv", sep=";", index_col=0)
        slat = slat["P1_sim:Sim_room_2_Wall_7_0_0_0_0_5_Win_slat"].apply(lambda x: int(x))
        blind = pd.read_csv(Path(args.output_directory) / "temp_blind.csv", sep=";", index_col=0)
        blind = blind["P1_sim:Sim_room_2_Wall_7_0_0_0_0_5_Win_blind"].apply(lambda x: int(x))
        # print("slat:\n", slat)
        forecasts = pd.concat((blind.reset_index(), slat.reset_index(drop=True), stack.reset_index(drop=True)), axis=1)
        forecasts.to_csv(Path(args.output_directory) / "forecasts.csv", sep=";")
        forecasts.set_index("index", inplace=True)
        forecasts.index.rename("Datetime", inplace=True)
        forecasts.index = pd.to_datetime(forecasts.index)
        forecasts.index = forecasts.index.tz_convert("Europe/Vienna") - pd.DateOffset(hours=2)
        forecasts.columns = ["Blind (%)", "Slat (°)", "Win"]
        forecasts["Win"] = forecasts["Win"].apply(lambda x: "{:.2%}".format(x))
        if "lle" in input_file["demo_case"]:
            table = {
                "90": 0,
                "60": 30,
                "120": -30,
                "0": 90,
                "180": -90
            }
            forecasts["Slat (°)"] = forecasts["Slat (°)"].apply(lambda x: table[str(x)])

        string_df = forecasts.to_csv(sep="\t")

        print(string_df)
        with open(Path(args.output_directory) / ("24hf-2022-%s.txt" % datetime.datetime.strftime(cur_day, "%m-%d ")), "w") as f:
            f.write(string_df)

        


##################### PLOTS ###########################
    # Blind activation
    # df = pd.read_csv(Path(args.output_directory) / "temp_blind.csv", index_col=0, sep=";")
    # df.columns = df.columns.str.replace(":Schedule Value", " ")
    # df.columns = df.columns.str.replace("Hourly:ON", " ")
    # df.columns = df.columns.str.replace(" ", "")
    # (df.iloc[:,0]*1).plot(kind="bar", figsize=(20, 10), color="tab:orange")
    # plt.title("Best blind activations in the future 24 hours")
    # # plt.legend(loc="center right")
    # plt.gcf().autofmt_xdate()
    # plt.savefig(Path(args.output_directory) / "24h-blind.png", transparent=False, facecolor="white")

    bulb = epw_reader.retrieve_column(epw, "dry_bulb_temperature", year=1970)
    radiat = epw_reader.retrieve_column(epw, "global_horizontal_radiation", year=1970)
    plt.rcParams.update({"font.size": 30})

    # Blind activation (New version)
    if "blind" in input_file["forecast_actions"]:
        df = pd.read_csv(
            Path(args.output_directory) / "temp_blind.csv",
            index_col=0,
            sep=";",
            parse_dates=True,
        )
        df2 = pd.read_csv(
            Path(args.output_directory) / "temp_slat.csv",
            index_col=0,
            sep=";",
            parse_dates=True,
        )
        if "lle" in input_file["demo_case"]:
            df.set_index(df.index.hour, inplace=True)
            # df2.set_index(df2.index.hour, inplace=True)
            fig, ax = plt.subplots(figsize=(20, 5))
            bulb_ax = ax.twinx()
            # print("df:\n", df)
            # blind = df.iloc[:, 0].where(where.iloc[:, 0])
            df.iloc[:, 0].plot(kind="bar", color="tab:orange", ax=ax)
            plt.title("Best blind values in the future 24 hours")
            ax.set_ylim([0, 100])
            ax.set_ylabel("%")
            bulb_ax.set_ylim([0, 1000])
            ax.set_xlabel("Hour of the day")
            fig.autofmt_xdate(rotation=0, ha="center")
            bulb_ax.plot(radiat.iloc[current_mid_hour:current_mid_hour + 24].values, color="tab:olive", lw=2, marker="s", ms="10")
            bulb_ax.set_ylabel(r"Solar Radiation $\left[\frac{W}{m^2}\right]$", color="tab:olive")
            bulb_ax.tick_params(axis='y', labelcolor="tab:olive")
            plt.savefig(
                Path(args.output_directory) / ("_".join(("24h-blind-lle", str(cur_day.month), str(cur_day.day))) + ".png"),
                transparent=True,
                # facecolor="white",
            )
            plt.close()



            # df_blind = df.copy()


        else:
            pass
            # df2 = df2.replace(0, 1).replace(90, 0.5)
            # df.set_index(df.index.hour, inplace=True)
            # df2.set_index(df2.index.hour, inplace=True)
            # # print("df1:\n", df)
            # # print("df2:\n", df2)
            # where = df | df2
            # fig, ax = plt.subplots(figsize=(20, 5))
            # bulb_ax = ax.twinx()
            # blind = df2.iloc[:, 0].where(where.iloc[:, 0])
            # blind.plot(kind="bar", color="tab:orange", ax=ax)
            # plt.title("Best blind values in the future 24 hours")
            # ax.set_ylim([0, 1])
            # bulb_ax.set_ylim([0, 1000])
            # ax.set_xlabel("Hour of the day")
            # fig.autofmt_xdate(rotation=0, ha="center")

            # bulb_ax.plot(radiat.iloc[current_mid_hour:current_mid_hour + 24].values, color="tab:olive", lw=2, marker="s", ms="10")
            # bulb_ax.set_ylabel(r"Solar Radiation $\left[\frac{W}{m^2}\right]$", color="tab:olive")
            # bulb_ax.tick_params(axis='y', labelcolor="tab:olive")
            # plt.savefig(
            #     Path(args.output_directory) / ("_".join(("24h-blind-v2", str(cur_day.month), str(cur_day.day))) + ".png"),
            #     transparent=True,
            #     # facecolor="white",
            # )
            # plt.close()
            # df_blind = df.copy()

    # ACH
    if "natural_ventilation" in input_file["forecast_actions"]:
        df = pd.read_csv(
            Path(args.output_directory) / "temp_ach.csv",
            index_col=0,
            sep=";",
            parse_dates=True,
        )

        df.columns = df.columns.str.replace(":Schedule Value", " ")
        df.columns = df.columns.str.replace("Hourly:ON", " ")
        df.columns = df.columns.str.replace(" ", "")
        df.set_index(df.index.hour, inplace=True)
        fig, ax = plt.subplots(figsize=(20, 5))
        bulb_ax = ax.twinx()
        (df.iloc[:, 0]*100).plot(kind="bar", ax=ax)
        plt.title("Best ACH values in the future 24 hours")
        ax.set_ylim([0, 5])
        bulb_ax.set_ylim([10, 40])
        ax.set_xlabel("Hour of the day")
        fig.autofmt_xdate(rotation=0, ha="center")

        bulb_ax.plot(bulb.iloc[current_mid_hour:current_mid_hour + 24].values, color="tab:red", lw=2, marker="s", ms="10")
        bulb_ax.set_ylabel("Dry Bulb Temp. [°C]", color="tab:red")
        bulb_ax.tick_params(axis='y', labelcolor="tab:red")
        plt.savefig(
            Path(args.output_directory) / ("24h-ach" + "_".join((str(cur_day.month), str(cur_day.day))) + ".png"),
            transparent=True,
            # facecolor="white",
        )
        plt.close()
        df_ach = df.copy()

    # Wind and stack activation
    if "stack_and_wind_ventilation" in input_file["forecast_actions"]:
        temp_orientations = ["North", "East", "South", "West"]
        df = pd.read_csv(
            Path(args.output_directory) / "temp_stack.csv",
            index_col=0,
            sep=";",
            parse_dates=True,
        )

        df.columns = df.columns.str.replace(":Schedule Value", " ")
        df.columns = df.columns.str.replace("Hourly:ON", " ")
        df.columns = df.columns.str.replace(" ", "")
        df.set_index(df.index.hour, inplace=True)
        # stack_cols = [c for c in res.columns if "edit_csv_schedule_stack" in c and "value" in c]
        # print(stack_cols)
        fig, axs = plt.subplots(len(stack_cols), sharex=True, sharey=True, figsize=(20, 20))

        for i, col in enumerate(df.columns):
            df[col].plot(kind="bar", ax=axs[i])
            bulb_ax = axs[i].twinx()
            bulb_ax.set_ylim([10, 40])
            bulb_ax.plot(bulb.iloc[current_mid_hour:current_mid_hour + 24].values, color="tab:red", lw=2, marker="s", ms="10")
            # bulb_ax.set_ylabel("Dry Bulb Temp. [°C]", color="tab:red")
            bulb_ax.tick_params(axis='y', labelcolor="tab:red")
            # axs[i].set_ylim([0, 5])
            plt.title(col.split("_")[0] + "°")

        fig.add_subplot(111, frameon=False)
        plt.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
        plt.xlabel("Hour of the day")
        bulb_ax.yaxis.set_label_position("right")
        plt.ylabel("Dry Bulb Temp. [°C]", color="tab:red")
        # axs[i].set_xlabel("Hour of the day")
        

        fig.autofmt_xdate(rotation=0, ha="center")
        fig.suptitle("Best Opening Factor values in the future 24 hours")
        plt.savefig(
            Path(args.output_directory) / ("24h-stack" + "_".join((str(cur_day.month), str(cur_day.day))) + ".png"),
            transparent=True,
            facecolor="white",
        )
        plt.close()
        # df_stack = df.copy()

    if "mechanical_ventilation" in input_file["forecast_actions"]:
        df = pd.read_csv(
            Path(args.output_directory) / "temp_mech.csv",
            index_col=0,
            sep=";",
            parse_dates=True,
        )
        df.set_index(df.index.hour, inplace=True)
        fig, ax = plt.subplots(figsize=(30, 20))
        df[[c for c in df.columns if "v_air_in" in c]].plot(ax=ax)
        plt.title("Best mechanical ventilation values in the future 24 hours")
        ax.set_xlabel("Hour of the day")
        fig.autofmt_xdate(rotation=0, ha="center")
        plt.savefig(
            Path(args.output_directory) / ("24h-mech" + "_".join((str(cur_day.month), str(cur_day.day))) + ".png"),
            transparent=False,
            facecolor="white"
        )
        plt.close()
    # Save final schedules:
    # df = df_blind.join(df_stack)
    # df.index = df.index + current_mid_hour
    # df.to_csv(Path(args.output_directory) / "schedules.csv", sep=";")
 
    print("End of simulation")
    # return


def comparison(building, in_file, folder, title="Hourly Adaptive Comfort Model", suffix=""):
    idf = copy.deepcopy(building)
    idf_editor.set_outputs(
        idf,
        [
            "Zone Ventilation Air Change Rate",
            "Zone Infiltration Air Change Rate",
        ],
        "*",
        "hourly",
        reset=False,
    )
    idf_editor.set_outputs(
        idf,
        [
            "Zone Mean Air Temperature",
        ],
        "*",
        "timestep",
        reset=False,
    )
    input_file = copy.deepcopy(in_file)
    input_file["actions"] = {}
    start = FIRST_DAY
    end = FIRST_DAY + datetime.timedelta(days=7)
    start = "/".join((str(start.month).zfill(2), str(start.day).zfill(2)))
    end = "/".join((str(end.month).zfill(2), str(end.day).zfill(2)))
    input_file["kpi"].update(
        {"timeseries_acm_hourly_cat":
            {"when": {
                "start": "1970/" + start + " 00:00:00",
                "end": "1970/" + end + " 00:00:00"
                },
            "title": title,
            "suffix": suffix
            }
        }
    )

    input_file["kpi"].update(
        {"adaptive_comfort_model":
            {"when": {
                "start": "1970/" + start + " 00:00:00",
                "end": "1970/" + end + " 00:00:00"
                }
            }
        }
    )

    print(input_file["kpi"]["timeseries_acm_hourly_cat"])
    start = FIRST_DAY - datetime.timedelta(days=20)
    end = FIRST_DAY + datetime.timedelta(days=7)
    idf_editor.change_runperiod(
        idf,
        "-".join((str(start.day).zfill(2), str(start.month).zfill(2))),
        "-".join((str(end.day).zfill(2), str(end.month).zfill(2))),
    )

    config = {
        "plot_dir": "/home/pgrasso/work/predyce-24/" + folder,
        "temp_dir": args.temp_directory,
        "out_dir": "/home/pgrasso/work/predyce-24/" + folder,
        "num_of_cpus": 1,
        "epw_dir": EPW_DIR
    }
    runner = Runner(
        idf,
        input_file,
        **config,
        graph=True
    )
    runner.create_dataframe(input_file, args.original)
    print(runner.df)
    runner.run()
    runner.print_summary()


if __name__ == "__main__":
    # idf, config, epw, input_file = setup()
    for _ in range(7):
        main(idf1, config, epw, input_file)
        comparison(idf1, input_file, folder="final", title="Hourly ACM\n24hf suggestions", suffix="24hf")
    # for d in DAYS:        main(idf, config, epw, input_file, d)
