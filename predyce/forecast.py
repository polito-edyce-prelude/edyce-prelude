#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from codecs import ascii_encode
from predyce.idf_editor import set_fields
from predyce.runner import idf_editor
import uuid
from pathlib import Path
import pandas as pd
from time import sleep
import os
from kpi import EnergyPlusKPI
import datetime


start = datetime.datetime.strptime("01-01-1970 01:00", "%d-%m-%Y %H:%M")
end = datetime.datetime.strptime("01-01-1971 00:00", "%d-%m-%Y %H:%M")
IX = pd.date_range(start, end, freq="H")  # Indeces


def set_mechanical_ventilation(idf, add="auto", zoning=True, temp_dir=""):
    if add == "auto":
        # No outdoor air -> Add it.
        if len(idf.idfobjects["DesignSpecification:OutdoorAir".upper()]) == 0:
            print("[+] Adding mechanical ventilation to the building!")
            add = 1

        # Outdoor air already present -> Do not add it.
        else:
            print("[i] Mechanical ventilation system detected in the building.")
            add = 0

    # Create a blank schedule with outdoor air always off.
    if add:
        schedule = {
			"Name": "mech_schedule",
			"Schedule_Type_Limits_Name": "Fraction",
			"Field_1": "Through: 31 Dec",
			"Field_2": "For: AllDays",
			"Field_3": "Until: 24:00",
			"Field_4": 0
        }
        idf_editor.add_mechanical_ventilation(idf, schedule=schedule)

        # Create empty mechanical ventilation CSV file.
        mech_df = pd.DataFrame()
        for dsoa in idf.idfobjects["DESIGNSPECIFICATION:OUTDOORAIR"]:
            set_fields(dsoa, data={
                "Outdoor Air Schedule Name": "mech_schedule_" + str(uuid.uuid4()).replace("-", "")[0:8],
            })
            mech_df[dsoa.Outdoor_Air_Schedule_Name] = [0 for _ in range(8760)]

        mech_df.set_index(IX, inplace=True)
        mech_df.to_csv(Path(temp_dir) / "mech.csv", sep=";")
        mech_df.to_csv("/home/pgrasso/work/predyce-24/original/mech.csv", sep=";")
        # mech_df.to_csv("/home/pgrasso/work/predyce-24/final/mech.csv", sep=";")

        # Create Schedule:Files for mechanical ventilation
        mech_file = pd.read_csv(Path(temp_dir) / "mech.csv", sep=";", index_col=0)
        columns = [c.replace("ON) ", "ON)") for c in list(mech_file.columns)]
        for dsoa in idf.idfobjects["DESIGNSPECIFICATION:OUTDOORAIR"]:
            sf = idf.newidfobject("SCHEDULE:FILE")
            set_fields(sf, data={
                "Name": dsoa.Outdoor_Air_Schedule_Name,
                "Schedule Type Limits Name": "Any Number",  # TODO: Change it
                "Rows to Skip at Top": 1,
                "Column Separator": "Semicolon",
                "File Name": str(Path(temp_dir) / "mech.csv"),
                "Column Number": columns.index(dsoa.Outdoor_Air_Schedule_Name) + 2,
            })    
    elif not add:
        # Get original mechanical ventilation CSV file.
        print("Exporting mechanical ventilation schedules to CSV file...")

        schedule_names = []
        for dsoa in idf.idfobjects["DESIGNSPECIFICATION:OUTDOORAIR"]:
            found_sf = 0
            for sf in idf.idfobjects["SCHEDULE:FILE"]:
                if sf.Name == dsoa.Outdoor_Air_Schedule_Name:
                    found_sf = 1
                    break
            if not found_sf:  # If the associated schedule was not already a schedule file.
                if zoning:
                    new_schedule = dsoa.Outdoor_Air_Schedule_Name + "_zoning_" + str(uuid.uuid4()).replace("-", "")[0:8]
                    for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
                        if sc.Name == dsoa.Outdoor_Air_Schedule_Name:
                            new_sc_obj = idf.copyidfobject(sc)
                            new_sc_obj.Name = new_schedule
                            dsoa.Outdoor_Air_Schedule_Name = new_schedule
                            break
                        # print(len(idf.idfobjects["SCHEDULE:COMPACT"]))
                schedule_names.append(dsoa.Outdoor_Air_Schedule_Name)
    
        # print("schedule_names:\n", schedule_names)
        if len(schedule_names) > 0:  # Only if necessary.
            idf_editor.set_outputs(idf, [None], None, reset=True)

            # Add new schedules in the output variables to create a file that will
            # be used in new Schedule:File schedules later.
            for schedule in list(set(schedule_names)):
                idf_editor.set_outputs(idf, ["Schedule Value"], schedule, frequency="Hourly", reset=False)  # O forse "Hourly"

            # Delete useless outputs that slow down the simulation.
            del idf.idfobjects["Output:EnvironmentalImpactFactors".upper()][:]
            del idf.idfobjects["Output:Meter".upper()][:]
            idf.run(output_directory=Path(temp_dir) / "schedule_runs", readvars=True, verbose="q")

            # Read eplusout.csv and use it as a CSV for Schedule:File
            mech_df = pd.read_csv(Path(temp_dir) / "schedule_runs" / "eplusout.csv")
            mech_df.index = mech_df["Date/Time"].apply(EnergyPlusKPI.convert_to_datetime)
            del mech_df["Date/Time"]
            mech_df.to_csv(Path(temp_dir) / "mech.csv", sep=";")
            mech_file = pd.read_csv(
                    Path(temp_dir) / "mech.csv",
                    sep=";",
                    index_col=0,
                )
            columns = [c.replace("ON) ", "ON)") for c in list(mech_file.columns)]

            # Assign ACH Schedule:Files
            if not zoning:
                schedule_names = list(set(schedule_names))

            # print("schedule names:", schedule_names)
            for schedule in schedule_names:
                for dsoa in idf.idfobjects["DESIGNSPECIFICATION:OUTDOORAIR"]:
                    # if idf.main_block.lower() in zvdfr.Zone_or_ZoneList_Name.lower() and zvdfr.Schedule_Name == schedule:
                        # zvdfr.Schedule_Name += ("_" + zvdfr.Zone_or_ZoneList_Name)
                        # Create Schedule:File
                    if dsoa.Outdoor_Air_Schedule_Name == schedule:
                        if not zoning:
                            dsoa_schedule = dsoa.Outdoor_Air_Schedule_Name + str(uuid.uuid4()).replace("-", "")[0:8]
                        else:
                            dsoa_schedule = dsoa.Outdoor_Air_Schedule_Name
                        for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
                            if sc.Name == dsoa.Outdoor_Air_Schedule_Name:
                                pass
                                # idf.removeidfobject(sc)
                        sf = idf.newidfobject("SCHEDULE:FILE")
                        set_fields(sf, data={
                            "Name": dsoa_schedule,
                            "Schedule Type Limits Name": "Fraction",
                            "Rows to Skip at Top": 1,
                            "Column Separator": "Semicolon",
                            "File Name": str(Path(temp_dir) / "mech.csv"),
                            # "File Name": "ach_schedules.csv",
                            "Column Number": columns.index(dsoa.Outdoor_Air_Schedule_Name.upper() + ":Schedule Value [](Hourly:ON)") + 2,
                        })
                        dsoa.Outdoor_Air_Schedule_Name = dsoa_schedule

                        # Remove original Schedule:File object to avoid duplicates.
                        for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
                            if sc.Name == dsoa_schedule:
                                idf.removeidfobject(sc)
                                break
    
        # Deal with Schedule:File.
        # TODO: Handle case with different schedule filenames.
        else:
            print("Schedule:File for OutdoorAir already set, copying the data to %s" % str(Path(temp_dir) / "mech.csv"))
            print("Warning: multiple csv files are not supported yet.")
            sleep(1)
            for dsoa in idf.idfobjects["DESIGNSPECIFICATION:OUTDOORAIR"]:
                for sf in idf.idfobjects["SCHEDULE:FILE"]:
                    if sf.Name.lower() == dsoa.Outdoor_Air_Schedule_Name.lower():
                        sf.File_Name = str(Path(temp_dir) / "mech.csv")
                        mech_df = pd.read_csv(sf.File_Name, index_col=0, sep=";")
                        mech_df.to_csv(Path(temp_dir) / "mech.csv", sep=";")
        
        # Save original schedule file to csv.
        mech_df.to_csv("/home/pgrasso/work/predyce-24/original/mech.csv", sep=";")


def set_lle_shading(idf, temp_dir):
    """Set shading and slat angle for Living Lab Environment Energetikum."""
    # for wpsc in idf.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]:
    #     if "simxroomx2" in wpsc.Schedule_Name.lower():
    #         schedule_name = wpsc.Schedule_Name
    #         break

    # Set IDF schedule files to CSVs.
    for sf in idf.idfobjects["SCHEDULE:FILE"]:
        if "blind" in sf.Name:
            blind_file_name = sf.File_Name
            sf.File_Name = str(Path(temp_dir) / "_blind_runs" / "_blind.csv")
        elif "slat" in sf.Name:
            slat_file_name = sf.File_Name
            sf.File_Name = str(Path(temp_dir) / "_blind_runs" / "slat.csv")

    blind_df = pd.read_csv(blind_file_name, index_col=0, sep=";")
    blind_df.to_csv(Path(temp_dir) / "_blind_runs" / "_blind.csv", sep=";")
    blind_df.to_csv("/home/pgrasso/work/predyce-24/original/_blind.csv", sep=";")

    slat_df = pd.read_csv(slat_file_name, index_col=0, sep=";")
    slat_df.to_csv(Path(temp_dir) / "_blind_runs" / "slat.csv", sep=";")
    slat_df.to_csv("/home/pgrasso/work/predyce-24/original/slat.csv", sep=";")


def set_blind_std(idf, schedule=None):
    for o in idf.windows_orientations:
        idf_editor.add_blind(
            idf, blind_data = {
                "Name": "20001",
                "Slat Width": 0.025,
                "Slat Separation": 0.01875,
                "Slat Thickness": 0.001,
                "Slat Angle": 30,
                "Slat Conductivity": 0.9,
                "Front Side Slat Beam Solar Reflectance": 0.8,
                "Back Side Slat Beam Solar Reflectance": 0.8,
                "Front Side Slat Diffuse Solar Reflectance": 0.8,
                "Back Side Slat Diffuse Solar Reflectance": 0.8,
                "Slat Beam Visible Transmittance": 0,
                "Front Side Slat Beam Visible Reflectance": 0.8,
                "Back Side Slat Beam Visible Reflectance": 0.8,
                "Front Side Slat Diffuse Visible Reflectance": 0.8,
                "Back Side Slat Diffuse Visible Reflectance": 0.8,
                "Blind to Glass Distance": 0.015,
                "Blind Bottom Opening Multiplier": 0.5,
            },
            type="exterior",
            filter_by=idf.main_block,
            filter_by_orientation=o
        )
    
    # Scheduled blind.
    if schedule is not None:
        for wpsc in idf.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]:
            set_fields(wpsc, data={
                "Schedule Name": schedule["Name"],
                "Shading Control Type": "OnIfScheduleAllows",
                "Shading Control Is Scheduled": "Yes",
                "Type of Slat Angle Control for Blinds": "FixedSlatAngle ",
            })
        sc = idf.newidfobject("SCHEDULE:COMPACT")
        set_fields(sc, data=schedule)

    # Blind with temperature setpoint.
    else:
        for wpsc in idf.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]:
            set_fields(wpsc, data={
                "Shading Control Type": "OnIfHighOutdoorAirTemperature",
                "Shading Control Is Scheduled": "No",
                "Type of Slat Angle Control for Blinds": "FixedSlatAngle",
                "Setpoint": 24,
            })
       

def set_blind(idf, add=True, temp_dir=""):
    # add = False
    if add:
        # Add Blinds and assign them new schedules based on orientation.
        for o in idf.windows_orientations:
            idf_editor.add_blind(
                idf, blind_data = {
                    "Name": "20001",
                    "Slat Width": 0.025,
                    "Slat Separation": 0.01875,
                    "Slat Thickness": 0.001,
                    "Slat Conductivity": 0.9,
                    "Front Side Slat Beam Solar Reflectance": 0.8,
                    "Back Side Slat Beam Solar Reflectance": 0.8,
                    "Front Side Slat Diffuse Solar Reflectance": 0.8,
                    "Back Side Slat Diffuse Solar Reflectance": 0.8,
                    "Slat Beam Visible Transmittance": 0,
                    "Front Side Slat Beam Visible Reflectance": 0.8,
                    "Back Side Slat Beam Visible Reflectance": 0.8,
                    "Front Side Slat Diffuse Visible Reflectance": 0.8,
                    "Back Side Slat Diffuse Visible Reflectance": 0.8,
                    "Blind to Glass Distance": 0.015,
                    "Blind Bottom Opening Multiplier": 0.5,
                },
                type="exterior",
                filter_by=idf.main_block,
                filter_by_orientation=o
            )

    # NOTE. A step of indentation has been removed.
    try:
        os.makedirs(Path(temp_dir) / "_blind_runs")
    except FileExistsError:
        pass
    idf.save(Path(temp_dir) / "_blind_runs" / "with_blind.idf")

    # Create empty slat CSV file.
    blind_df = pd.DataFrame()
    slat_df = pd.DataFrame()
    for wpsc in idf.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]:
        set_fields(wpsc, data={
            "Schedule Name": "shading_schedule_"+ str(uuid.uuid4()).replace("-", "")[0:8],
            "Shading Control Type": "OnIfScheduleAllows",
            "Shading Control Is Scheduled": "Yes",
            "Type of Slat Angle Control for Blinds": "ScheduledSlatAngle ",
            "Slat Angle Schedule Name": "slat_schedule_"+ str(uuid.uuid4()).replace("-", "")[0:8],
        })
        blind_df[wpsc.Schedule_Name] = [0 for _ in range(8760)]
        slat_df[wpsc.Slat_Angle_Schedule_Name] = [0 for _ in range(8760)]
    
    blind_df.set_index(IX, inplace=True)
    blind_df.to_csv(Path(temp_dir) / "_blind_runs" / "_blind.csv", sep=";")
    print("Blind_df:\n", (Path(temp_dir) / "_blind_runs" / "_blind.csv"))
    blind_df.to_csv("/home/pgrasso/work/predyce-24/original/_blind.csv", sep=";")

    slat_df.set_index(IX, inplace=True)
    slat_df.to_csv(Path(temp_dir) / "_blind_runs" / "slat.csv", sep=";")
    slat_df.to_csv("/home/pgrasso/work/predyce-24/original/slat.csv", sep=";")

    # Create Schedule:Files for Blind
    blind_file = pd.read_csv(Path(temp_dir) / "_blind_runs" / "_blind.csv", sep=";", index_col=0)
    columns = [c.replace("ON) ", "ON)") for c in list(blind_file.columns)]
    for wpsc in idf.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]:
        sf = idf.newidfobject("SCHEDULE:FILE")
        set_fields(sf, data={
            "Name": wpsc.Schedule_Name,
            "Schedule Type Limits Name": "On/Off",
            "Rows to Skip at Top": 1,
            "Column Separator": "Semicolon",
            "File Name": str(Path(temp_dir) / "_blind_runs" / "_blind.csv"),
            "Column Number": columns.index(wpsc.Schedule_Name) + 2,
        })

    # Create Schedule:Files for slat
    slat_file = pd.read_csv(Path(temp_dir) / "_blind_runs" / "slat.csv", sep=";", index_col=0)
    columns = [c.replace("ON) ", "ON)") for c in list(slat_file.columns)]
    for wpsc in idf.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]:
        sf = idf.newidfobject("SCHEDULE:FILE")
        set_fields(sf, data={
            "Name": wpsc.Slat_Angle_Schedule_Name,
            "Schedule Type Limits Name": "Any Number",  # TODO: Change it
            "Rows to Skip at Top": 1,
            "Column Separator": "Semicolon",
            "File Name": str(Path(temp_dir) / "_blind_runs" / "slat.csv"),
            "Column Number": columns.index(wpsc.Slat_Angle_Schedule_Name) + 2,
        })
    print("Blind and slat have been SET.")
    # assert False

def set_ach(idf, temp_dir):
    # Find ACH schedule values.
    idf_editor.change_runperiod(idf, "01-01", "31-12", fmt="%d-%m")
    idf.idfobjects["TIMESTEP"][0].Number_of_Timesteps_per_Hour = 1
    schedule_names = []
    for zvdfr in idf.idfobjects["ZoneVentilation:DesignFlowRate".upper()]:
        if idf.main_block.lower() in zvdfr.Zone_or_ZoneList_Name.lower():
            for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
                if sc.Name == zvdfr.Schedule_Name:
                    # Make a copy of the schedule and save its name in a list.
                    new_sc = idf.copyidfobject(sc)
                    new_sc.Name += ("_" + zvdfr.Zone_or_ZoneList_Name)
                    new_sc.Name = new_sc.Name.replace(" ", "_").replace("(", "_").replace(")", "_")
                    zvdfr.Schedule_Name = new_sc.Name
                    zvdfr.Schedule_Name = zvdfr.Schedule_Name.replace(" ", "_").replace("(", "_").replace(")", "_")
                    schedule_names.append(zvdfr.Schedule_Name)
                    break  # NOTE: Important!

    # Delete useless outputs that slow down the simulation.
    idf_editor.set_outputs(idf, [None], None, reset=True)

    # Add new schedules in the output variables to create a file that will
    # be used in new Schedule:File schedules later.
    for schedule in schedule_names:
        idf_editor.set_outputs(idf, ["Schedule Value"], schedule, frequency="Hourly", reset=False)  # O forse "Hourly"

    # Delete useless outputs that slow down the simulation.
    del idf.idfobjects["Output:EnvironmentalImpactFactors".upper()][:]
    del idf.idfobjects["Output:Meter".upper()][:]

    # Match first weekday of the year.
    # idf_editor.set_fields(idf.idfobjects["RUNPERIOD"][0], Day_of_Week_for_Start_Day=dow)  # NOTE: De-comment me

    try:
        os.makedirs(Path(temp_dir) / "schedule_runs")
    except FileExistsError:
        pass

    print("Exporting ventilation ACH schedules to CSV file")
    idf.run(output_directory=Path(temp_dir) / "schedule_runs", readvars=True, verbose="q")
    print("Converting ACH Schedule:Compact to Schedule:File objects")

    # Read eplusout.csv and use it as a CSV for Schedule:File
    res = pd.read_csv(Path(temp_dir) / "schedule_runs" / "eplusout.csv")
    res.index = res["Date/Time"].apply(EnergyPlusKPI.convert_to_datetime)
    del res["Date/Time"]
    res.to_csv(Path(temp_dir) / "schedule_runs" / "ach_schedules.csv", sep=";")
    res.to_csv(Path(temp_dir) / "schedule_runs" / "ach_schedules_original.csv", sep=";")
    res.to_csv("/home/pgrasso/work/predyce-24/original/ach_schedules.csv", sep=";")
    # res.to_csv("/home/pgrasso/work/predyce-24/final/ach_schedules.csv", sep=";")
    res = res.resample("1H").mean()
 
    ach_file = pd.read_csv(
        Path(temp_dir) / "schedule_runs" / "ach_schedules.csv",
        sep=";",
        index_col=0,
    )
    columns = [c.replace("ON) ", "ON)") for c in list(ach_file.columns)]

    # Assign ACH Schedule:Files
    for schedule in schedule_names:
        for zvdfr in idf.idfobjects["ZoneVentilation:DesignFlowRate".upper()]:
            if idf.main_block.lower() in zvdfr.Zone_or_ZoneList_Name.lower() and zvdfr.Schedule_Name == schedule:
                # zvdfr.Schedule_Name += ("_" + zvdfr.Zone_or_ZoneList_Name)
                # Create Schedule:File
                for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
                    if sc.Name == schedule:
                        idf.removeidfobject(sc)
                sf = idf.newidfobject("SCHEDULE:FILE")
                set_fields(sf, data={
                    "Name": zvdfr.Schedule_Name,
                    "Schedule Type Limits Name": "Fraction",
                    "Rows to Skip at Top": 1,
                    "Column Separator": "Semicolon",
                    "File Name": str(Path(temp_dir) / "schedule_runs" / "ach_schedules.csv"),
                    # "File Name": "ach_schedules.csv",
                    "Column Number": columns.index(zvdfr.Schedule_Name.upper() + ":Schedule Value [](Hourly:ON)") + 2,
                })

    del idf.idfobjects["OUTPUT:VARIABLE"][:]  # Delete schedule reports.

    # Elaborate values in order to make it compatible with old and tested values
    # (maybe ACH=100 and multiply/divide all other values).
    for zvdfr in idf.idfobjects["ZoneVentilation:DesignFlowRate".upper()]:
        if idf.main_block.lower() in zvdfr.Zone_or_ZoneList_Name.lower():
            for z in idf.idfobjects["ZONE"]:
                if z.Name.lower() == zvdfr.Zone_or_ZoneList_Name.lower():
                    break
            ach = idf_editor.get_ach(z, zvdfr.Design_Flow_Rate)
            idf_editor.change_ach(idf, 100, ach_type="ventilation", filter_by=z.Name)
            factor = 100 / ach
            column_to_edit_index = columns.index(zvdfr.Schedule_Name.upper() + ":Schedule Value [](Hourly:ON)")
            ach_file.iloc[:, column_to_edit_index] = ach_file.iloc[:, column_to_edit_index].apply(lambda x: x/factor)


    ach_file.to_csv(Path(temp_dir) / "schedule_runs" / "ach_schedules.csv", sep=";")
    try:
        os.makedirs(Path(temp_dir) / "schedule_runs_file")
    except FileExistsError:
        pass

    idf_editor.set_outputs(idf, [None], None, frequency="Hourly", reset=True)  # Re-add some outputs.


def set_stack_ventilation_std(idf, schedule):
    for o in list(set(idf.windows_orientations)):
        sc = idf.newidfobject("SCHEDULE:COMPACT")
        sch_name = f"{o}_" + schedule["Name"]
        set_fields(sc, data=schedule)
        set_fields(sc, Name=sch_name)

        for z in idf.idfobjects["ZONE"]:
            windows = [
                fsd
                for fsd in idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]
                if z.Name in fsd.Building_Surface_Name
                and (o - 22.5) < fsd.azimuth
                and fsd.azimuth < (o + 22.5)
                and len(fsd.Outside_Boundary_Condition_Object) == 0
            ]
            if len(windows) > 0:
                value = sum([x.area for x in windows])
                # if (o - 22.5) < fsd.azimuth and  fsd.azimuth < (o + 22.5):  # NOTE: Remove me.
                print("Added", z.Name, sc.Name, "wind and stack ventilation object")
                idf_editor.add_windandstack_nat_vent(idf, filter_by=z.Name, value=value, schedule=sc.Name)



def set_stack_ventilation(idf, add="auto", temp_dir="", set_std=False):
    if add == "auto":
        # No wind_and_stack -> Add it.
        if len(idf.idfobjects["ZoneVentilation:WindandStackOpenArea".upper()]) == 0:
            print("[+] Adding 'Wind and Stack Open Area' ventilation to the building!")
            add = 1

        # Outdoor air already present -> Do not add it.
        else:
            print("[i] 'Wind and Stack Open Area' ventilation system detected in the building.")
            add = 0

    # print("Creating wind and stack ventilation objects")
    if add:
        stack_df = pd.DataFrame()
        for o in list(set(idf.windows_orientations)):
            sf = idf.newidfobject("SCHEDULE:FILE")
            set_fields(sf, data={
                "Name": str(o) + "_stack_opening_factor",
                "Schedule Type Limits Name": "Any Number",
                "Rows to Skip at Top": 1,
                "Column Separator": "Semicolon",
                "File Name": str(Path(temp_dir) / "_stack.csv"),
            })
            stack_df[sf.Name] = [0 for _ in range(8760)]
            set_fields(sf, Column_Number=list(stack_df.columns).index(sf.Name) + 2)
            idf_editor.set_outputs(idf, ["Schedule Value"], sf.Name, frequency="Hourly", reset=False)

            for z in idf.idfobjects["ZONE"]:
                windows = [
                    fsd
                    for fsd in idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]
                    if z.Name in fsd.Building_Surface_Name
                    and (o - 22.5) < fsd.azimuth
                    and fsd.azimuth < (o + 22.5)
                    and len(fsd.Outside_Boundary_Condition_Object) == 0
                ]
                if len(windows) > 0:
                    value = sum([x.area for x in windows])
                    # if (o - 22.5) < fsd.azimuth and  fsd.azimuth < (o + 22.5):  # NOTE: Remove me.
                    print("Added", z.Name, sf.Name, "wind and stack ventilation object")
                    idf_editor.add_windandstack_nat_vent(idf, filter_by=z.Name, value=value, schedule=sf.Name)
                    for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
                        if sc.Name == sf.Name:
                            idf.removeidfobject(sc)
    elif not add:
        if set_std:
            schedule = {
                "Name": "stack_std",
                "Schedule_Type_Limits_Name": "Any Number",
                "Field_1": "Through: 12/31",
                "Field_2": "For: AllDays",
                "Field_3": "Until: 6:00",
                "Field_4": 0.3,
                "Field_5": "Until: 23:00",
                "Field_6": 0,
                "Field_7": "Until: 24:00",
                "Field_8": 0.3,
            }
            set_stack_ventilation_std(idf, schedule)

            schedule_names = []
            for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
                if "stack_std" in sc.Name.lower():
                    schedule_names.append(sc.Name)

            # Delete useless outputs that slow down the simulation.
            idf_editor.set_outputs(idf, [None], None, reset=True)

            # Add new schedules in the output variables to create a file that will
            # be used in new Schedule:File schedules later.
            for schedule in schedule_names:
                idf_editor.set_outputs(idf, ["Schedule Value"], schedule, frequency="Hourly", reset=False)  # O forse "Hourly"

            # Delete useless outputs that slow down the simulation.
            del idf.idfobjects["Output:EnvironmentalImpactFactors".upper()][:]
            del idf.idfobjects["Output:Meter".upper()][:]

            # Match first weekday of the year.
            # idf_editor.set_fields(idf.idfobjects["RUNPERIOD"][0], Day_of_Week_for_Start_Day=dow)  # NOTE: De-comment me

            try:
                os.makedirs(Path(temp_dir) / "stack_prel_run")
            except FileExistsError:
                pass

            print("Exporting stack ventilation schedules to CSV file")
            idf.run(output_directory=Path(temp_dir) / "stack_prel_run", readvars=True, verbose="q", weather="/home/pgrasso/work/predyce/test/EPW/ITA_Torino-Caselle.160590_IGDG.epw")
            print("Converting stack ventilation Schedule:Compact to Schedule:File objects")

            # Read eplusout.csv and use it as a CSV for Schedule:File
            res = pd.read_csv(Path(temp_dir) / "stack_prel_run" / "eplusout.csv")
            res.index = res["Date/Time"].apply(EnergyPlusKPI.convert_to_datetime)
            del res["Date/Time"]
            res.to_csv(Path(temp_dir) / "stack_prel_run" / "stack_schedules.csv", sep=";")
            res.to_csv("/home/pgrasso/work/predyce-24/original/stack_schedules.csv", sep=";")
            # res.to_csv("/home/pgrasso/work/predyce-24/final/ach_schedules.csv", sep=";")
            res = res.resample("1H").mean()
        
            stack_file = pd.read_csv(
                Path(temp_dir) / "stack_prel_run" / "stack_schedules.csv",
                sep=";",
                index_col=0,
            )
            columns = [c.replace("ON) ", "ON)") for c in list(stack_file.columns)]

            # Assign Stack Schedule:Files
            assigned_schedules = []
            for schedule in schedule_names:
                for zvwsoa in idf.idfobjects["ZoneVentilation:WindandStackOpenArea".upper()]:
                    if idf.main_block.lower() in zvwsoa.Zone_Name.lower() and zvwsoa.Opening_Area_Fraction_Schedule_Name == schedule:
                        if zvwsoa.Opening_Area_Fraction_Schedule_Name in assigned_schedules:
                            continue
                        assigned_schedules.append(zvwsoa.Opening_Area_Fraction_Schedule_Name)
                        # zvwsoa.Opening_Area_Fraction_Schedule_Name += ("_" + zvwsoa.Zone_Name)  # FIXME: What is this?
                        # Create Schedule:File
                        for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
                            if sc.Name == schedule:
                                idf.removeidfobject(sc)
                        sf = idf.newidfobject("SCHEDULE:FILE")
                        set_fields(sf, data={
                            "Name": zvwsoa.Opening_Area_Fraction_Schedule_Name,
                            "Schedule Type Limits Name": "Fraction",
                            "Rows to Skip at Top": 1,
                            "Column Separator": "Semicolon",
                            "File Name": str(Path(temp_dir) / "_stack.csv"),
                            "Column Number": columns.index(zvwsoa.Opening_Area_Fraction_Schedule_Name.upper() + ":Schedule Value [](Hourly:ON)") + 2,
                        })

            del idf.idfobjects["OUTPUT:VARIABLE"][:]  # Delete schedule reports.
            stack_file.to_csv(Path(temp_dir) / "_stack.csv", sep=";")


        else:
            schedule_names = []
            for zvwsoa in idf.idfobjects["ZoneVentilation:WindandStackOpenArea".upper()]:
                found_sf = 0
                for sf in idf.idfobjects["SCHEDULE:FILE"]:
                    if sf.Name == zvwsoa.Opening_Area_Fraction_Schedule_Name:
                        found_sf = 1
                        break
                if not found_sf:  # If the associated schedule was not already a schedule file.
                    raise NotImplementedError

            if len(schedule_names) > 0:  # Only if necessary.
                raise NotImplementedError

            else:
                print("[>] Schedule:File for OutdoorAir already set, copying the data to %s" % str(Path(temp_dir) / "_stack.csv"))
                print("[i] Warning: multiple csv files are not supported yet.")
                sleep(1)

                # Set IDF schedule files to CSVs.
                for sf in idf.idfobjects["SCHEDULE:FILE"]:
                    if any([s in sf.Name.lower() for s in ["stack", "open"]]):
                        stack_file_name = sf.File_Name
                        sf.File_Name = str(Path(temp_dir) / "_stack.csv")

                for zvwsoa in idf.idfobjects["ZoneVentilation:WindandStackOpenArea".upper()]:
                    for sf in idf.idfobjects["SCHEDULE:FILE"]:
                        if sf.Name.lower() == zvwsoa.Opening_Area_Fraction_Schedule_Name.lower():
                            sf.File_Name = str(Path(temp_dir) / "_stack.csv")
                            stack_df = pd.read_csv(stack_file_name, index_col=0, sep=";")
                            stack_df.to_csv(Path(temp_dir) / "_stack.csv", sep=";")

                # Get original mechanical ventilation CSV file.
                # print("[>] Exporting mechanical ventilation schedules to CSV file...")
            

            print("stack_df:\n", stack_df)
            stack_df.set_index(IX, inplace=True)
            stack_df.to_csv(Path(temp_dir) / "_stack.csv", sep=";")
            stack_df.to_csv("/home/pgrasso/work/predyce-24/original/_stack.csv", sep=";")
            # assert False


def set_calculated_ventilation(idf, temp_dir):
    opening_df = pd.DataFrame()
    for o in idf.windows_orientations:
        # Add schedule file.
        sf = idf.newidfobject("SCHEDULE:FILE")
        set_fields(sf, data={
            "Name": o + "_opening_factor",
            "Schedule Type Limits Name": "Any Number",
            "Rows to Skip at Top": 1,
            "Column Separator": "Semicolon",
            "File Name": str(Path(temp_dir) / "_opening_factor.csv"),
            "Column Number": 0,
        })
        opening_df[sf.Name] = [0 for _ in range(8760)]
        idf_editor.set_outputs(idf, ["Schedule Value"], sf.Name, frequency="Hourly", reset=False)
    
        # Add EMS sensor.
        sensor = idf.newidfobject("EnergyManagementSystem:Sensor".upper())
        idf_editor.set_fields(sensor, data={
            "Name": "OpeningFactorValue_" + o,
            "Output:Variable or Output:Meter Index Key Name": sf.Name,
            "Output:Variable or Output:Meter Name": "Schedule Value"
        })

        # # Add EMS actuator.
        # actuator = idf.newidfobject("EnergyManagementSystem:Actuator".upper())
        # idf_editor.set_fields(actuator, data={
        #     "Name": "MyOpenFactor",
        #     "Component Name": "x",
        #     "Component Type": "AirFlow Network Window/Door Opening",
        #     "Control Type": "Venting Opening Factor"
        # })

        # Add EMS program calling manager.
        pcm = idf.newidfobject("ENERGYMANAGEMENTSYSTEM:PROGRAMCALLINGMANAGER")
        idf_editor.set_fields(pcm, data={
            "Name": "Scheduled Open Factor",
            "EnergyPlus Model Calling Point": "BeginTimestepBeforePredictor", # TODO: Check calling point
            "Program Name 1": "ScheduleController"
        })

        # Add EMS program.
        program = idf.newidfobject("EnergyManagementSystem:Program".upper())
        idf_editor.set_fields(program, data={
            "Name": "ScheduleController",
            "Program Line 1": "SET MyOpenFactor = OpeningFactorValue"
        })

    opening_df.set_index(IX, inplace=True)
    opening_df.to_csv(str(Path(temp_dir) / "_opening_factor.csv"), sep=";")
    opening_df.to_csv("/home/pgrasso/work/predyce-24/original/_opening_factor.csv", sep=";")
    # opening_df.to_csv("/home/pgrasso/work/predyce-24/final/_opening_factor.csv", sep=";")