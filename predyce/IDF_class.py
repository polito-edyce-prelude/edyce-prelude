from geomeppy import IDF as geomeppy_idf
from predyce import idf_editor


class IDF(geomeppy_idf):
    """Add properties to IDF class."""

    def __init__(self, idfname=None, epw=None):
        """
        :param idfname: Path to the IDF file, defaults to None
        :type idfname: str, optional
        :param epw: Path to the EPW file, defaults to None
        :type epw: str, optional
        """
        super().__init__(idfname, epw)

    @property
    def area(self):
        """Area of the building, including only zones belonging to the building
        name.

        :return: Area value in m2
        :rtype: float
        """
        return self._area

    @area.setter
    def area(self, value):
        """Set area of the building.

        :param value: Area in m2
        :type value: float
        """
        self._area = value

    @property
    def volume(self):
        """Volume of the building, including only zones belonging to the
        building name.

        :return: Volume value in m3
        :rtype: float
        """
        return self._volume

    @volume.setter
    def volume(self, value):
        """Set volume of the building.

        :param value: Volume in m3
        :type value: float
        """
        self._volume = value

    @property
    def main_block(self):
        """Name of the main block of the building.

        :return: Name of the main block
        :rtype: str
        """
        return self._main_block

    @main_block.setter
    def main_block(self, value):
        """Set name of the main block of the building.

        :param value: Name of the main block
        :type value: str
        """
        self._main_block = value

    @property
    def heat_season(self):
        """Name of the building heating season schedule.

        :return: Name of the building heating season schedule
        :rtype: str
        """
        return self._heat_season

    @heat_season.setter
    def heat_season(self, value):
        """Set name of the building heating season schedule.

        :param value: Name of the building heating season schedule
        :type value: str
        """
        self._heat_season = value

    @property
    def cool_season(self):
        """Name of the building cooling season schedule.

        :return: Name of the building cooling season schedule
        :rtype: str
        """
        return self._cool_season

    @cool_season.setter
    def cool_season(self, value):
        """Set name of the building cooling season schedule.

        :param value: Name of the building cooling season schedule
        :type value: str
        """
        self._cool_season = value

    @property
    def epw_dir(self):
        """EPW directory.

        :return: EPW directory
        :rtype: Path or str
        """
        return self._epw_dir

    @epw_dir.setter
    def epw_dir(self, value):
        """Set EPW directory.

        :param value: EPW directory
        :type value: Path or str
        """
        self._epw_dir = value

    @property
    def mean_dt(self):
        """Mean delta temperature of ventilation flow rate.

        :return: Mean delta temperature
        :rtype: int
        """
        return self._mean_dt

    @mean_dt.setter
    def mean_dt(self, value):
        """Set mean delta temperature of ventilation flow rate.

        :param value: Mean delta temperature
        :type value: int
        """
        self._mean_dt = value

    @property
    def mean_ach(self):
        """Mean air changes per hour of ventilation flow rate.

        :return: Air changes per hour
        :rtype: float
        """
        return self._mean_ach

    @mean_ach.setter
    def mean_ach(self, value):
        """Set mean air changes per hour of ventilation flow rate.

        :param value: Air changes per hour
        :type value: int
        """
        self._mean_ach = value

    @property
    def soil_properties(self):
        """Soil properties.

        :return: Soil surface temperature in tuple form: (Average, Amplitude, Phase)
        :rtype: tuple
        """
        return self._soil_properties

    @soil_properties.setter
    def soil_properties(self, value):
        """Set soil surface temperature average, amplitude, and phase.

        :param value: Soil surface temperature in tuple form: (Average, Amplitude, Phase)
        :type value: tuple
        """
        self._soil_properties = value

    def set_block(self, building_name):
        """Set building block name and automatically compute and set area,
        volume, mean ACH and mean delta temperature.

        :param building_name: Building name which appears in the IDF
        :type building_name: str
        """
        self.main_block = building_name
        self.area = idf_editor.get_area(self, self.main_block)
        self.volume = idf_editor.get_volume(self, self.main_block)
        # self.mean_ach = idf_editor.get_mean_ach(self, self.main_block)
        self.mean_dt = idf_editor.get_mean_dt(self, self.main_block)
        self.set_windows_orientations()

    @property
    def windows_orientations(self):
        return self._windows_orientations


    @windows_orientations.setter
    def windows_orientations(self, value):
        self._windows_orientations = value


    def set_windows_orientations(self):
        possible_values = [0, 45, 90, 135, 180, 225, 270, 315, 360]
        orientations = [
            f.azimuth
            for f in self.idfobjects["FENESTRATIONSURFACE:DETAILED"]
            if self.main_block.lower() in f.Building_Surface_Name.lower()
            and (f.Surface_Type == "Window" or f.Surface_Type == "GlazedDoor")
            and len(f.Outside_Boundary_Condition_Object) == 0
        ]

        orientations = [
            possible_values[
                min(
                    range(len(possible_values)),
                    key=lambda i: abs(possible_values[i] - o),
                )
            ]
            for o in orientations
        ]

        self.windows_orientations = orientations