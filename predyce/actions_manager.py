#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Functions used to manage .json database"""

import json as js


class Actions:
    """Class to manage json actions reading and writing."""

    def __init__(self):
        """Init function"""
        self.actions = {}
        self.path = ""
        self.text = ""
        self.type = ""

    def load_file(self, path, subpart):
        """Load actions in local memory.
        :param path: actions path
        :type path: string
        :param subpart: specify wether interested in inputs or outputs actions
        :type subpart: string
        """
        self.path = path
        self.type = subpart
        f = open(path, "r")
        self.text = f.read()
        self.database = js.loads(self.text)[subpart]
        f.close()
        return

    def get_object(self):
        """Not yet implemented."""
        pass

    def post_object(self):
        """Not yet implemented."""
        pass

    def put_object(self):
        """Not yet implemented."""
        pass

    def delete_object(self):
        """Not yet implemented."""
        pass


class WebActions:

    exposed = True

    def __init__(self):
        """Init function"""
        self.actions = Actions()

    def GET(self, *uri, **params):
        """Not yet implemented."""
        return

    def POST(self, *uri, **params):
        """Not yet implemented."""
        return

    def PUT(self, *uri, **params):
        """Not yet implemented."""
        return

    def DELETE(self):
        """Not yet implemented."""
        return
