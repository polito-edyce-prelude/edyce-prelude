# -*- coding: utf-8 -*-
import os, sys, inspect
currentdir = os.path.dirname(
    os.path.abspath(inspect.getfile(inspect.currentframe()))
)
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
from pathlib import Path
from predyce import kpi
import pandas as pd
import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import copy
import numpy as np
import re

YEAR = 2021
WARM_UP_DAYS = 6  # These initial days are remove from calibration phase.


def mbe(data_true, data_predicted):
    return np.nanmean(data_predicted - data_true)


def rmse(data_true, data_predicted):
    return np.sqrt(np.nanmean((data_predicted - data_true) ** 2))


def rmse_mbe(data_true, data_predicted):
    return np.sqrt(
        rmse(data_true, data_predicted) ** 2
        + mbe(data_true, data_predicted) ** 2
    )


def calibration_signature(
    data_true, data_predicted, x_axis, plot_dir, graph=True
):
    where = (data_true - data_predicted).dropna().index
    plt.rcParams.update({"font.size": 45})
    num = (data_true.loc[where] - data_predicted.loc[where]).dropna()
    cal_sig = 100 * num / data_true.loc[where].max()
    if graph:
        fig, ax = plt.subplots(constrained_layout=True, figsize=(18, 15))
        # ax.plot(cal_sig, label=data_true.columns[0], lw=3)
        ax.plot(
            x_axis.loc[where], cal_sig, "o", label=data_true.columns[0], markersize=10
        )
        ax.set_title("Calibration Signature")
        ax.set_xlabel("Outside Air Temperature [°C]")
        ax.set_ylabel("Percent")
        ax.set_ylim([-15, 15])
        plt.axhline(y=0, color="r", linestyle="-")
        plt.grid()
        plot_dir = Path(plot_dir)
        plt.savefig(plot_dir / "calibration_signature.png")
        plt.savefig(plot_dir / "calibration_signature.eps")
        plt.close(fig)


def comparison(data_true, data_predicted, plot_dir, highligh_period=False):
    plt.rc('font',**{'family':'serif','serif':['Palatino']})
    plt.rc('text', usetex=True)
    where = (data_true - data_predicted).dropna().index
    plt.rcParams.update({"font.size": 45})

    # First plot.
    fig, ax = plt.subplots(constrained_layout=True, figsize=(18, 18))
    ax.plot(
        data_predicted.loc[where],
        data_true.loc[where],
        marker="o",
        ls="",
        color="indigo",
        markersize=10,
    )
    ax.set_title("Measured vs Simulated Indoor Drybulb Temp.")#\nAQ June")
    plt.axis('square')
    ax.set_xlabel(
        r"Simulated $T_{\mathrm{db}}^\mathrm{i}$ [°C]"
    )
    ax.set_ylabel(
        r"Measured $T_{\mathrm{db}}^\mathrm{i}$ [°C]"
    )
    plt.grid()
    plot_dir = Path(plot_dir)
    x = np.linspace(*ax.get_xlim())
    ax.plot(x, x, 'r')
    plt.savefig(plot_dir / "predicted_vs_true.png")
    plt.savefig(plot_dir / "predicted_vs_true.eps")
    plt.close(fig)

    labels = ['-'.join((str(d.day), str(d.month))) for d in data_predicted.loc[where].index]

    # Second plot: Data vs Time
    fig, ax = plt.subplots(constrained_layout=False, figsize=(40, 20))
    ax.plot(
        data_predicted.loc[where].index + pd.DateOffset(year=YEAR),
        data_predicted.loc[where],
        color="tab:cyan",
        label="Simulated",
        lw=5,
        alpha=.6
    )
    ax.plot(
        data_true.loc[where].index + pd.DateOffset(year=YEAR),
        data_true.loc[where],
        color="tab:blue",
        label="Measured",
        lw=5,
        alpha=.6
    )

    # ax.axvspan(dt.datetime.strptime("17 08 " + str(YEAR), "%d %m %Y"),
    #         dt.datetime.strptime("31 08 " + str(YEAR), "%d %m %Y"),
    #         color='green',
    #         alpha=0.1,
    #         label="Calibration period")

    # ax.axvspan(dt.datetime.strptime("15 07 " + str(YEAR), "%d %m %Y"),
    #         dt.datetime.strptime("15 08 " + str(YEAR), "%d %m %Y"),
    #         color='green',
    #         alpha=0.1,
    #         label="Calibration period")
    plt.gcf().autofmt_xdate()
    plt.legend(loc="upper right")
    plt.grid()
    plot_dir = Path(plot_dir)
    plt.tight_layout()
    plt.savefig(plot_dir / "predicted_vs_true_time.png")
    plt.savefig(plot_dir / "predicted_vs_true_time.eps")
    # plt.close(fig)

    # Third plot: Data vs Time (weekly) and min_max
    daily_predicted = data_predicted.resample("1D").mean()
    daily_true = data_true.resample("1D").mean()
    where = (daily_predicted - daily_true).dropna().index
    min_predicted = data_predicted.resample("1D").min()
    max_predicted = data_predicted.resample("1D").max()
    min_true = data_true.resample("1D").min()
    max_true = data_true.resample("1D").max()
    where_min_max = (max_predicted - max_true).dropna().index

    # fig, ax = plt.subplots(constrained_layout=False, figsize=(40, 20))
    ax.plot(
        daily_predicted.loc[where].index + pd.DateOffset(year=YEAR),
        daily_predicted.loc[where],
        color="tab:cyan",
        label="Daily Mean Simulated",
        lw=2,
        alpha=1,
        marker='o',
        markersize=10
    )
    ax.plot(
        daily_true.loc[where].index + pd.DateOffset(year=YEAR),
        daily_true.loc[where],
        color="tab:blue",
        label="Daily Mean Measured",
        lw=2,
        alpha=1,
        marker='o',
        markersize=10
    )

    ax.fill_between(
        min_predicted.loc[where_min_max].index + pd.DateOffset(year=YEAR),
        [y[0] for y in min_predicted.loc[where_min_max].values],
        [y[0] for y in max_predicted.loc[where_min_max].values],
        color="cyan",
        alpha=0.15
    )
    ax.fill_between(
        min_predicted.loc[where_min_max].index + pd.DateOffset(year=YEAR),
        [y[0] for y in min_true.loc[where_min_max].values],
        [y[0] for y in max_true.loc[where_min_max].values],
        color="blue",
        alpha=0.05
    )
    # ax.plot(
    #     daily_true.loc[where].index + pd.DateOffset(year=YEAR),
    #     daily_true.loc[where],
    #     color="tab:blue",
    #     label="Daily Mean Measured",
    #     ls='None',
    #     alpha=1,
    #     marker='_',
    #     markersize=10
    # )

    ax.set_title("Indoor Drybulb Temperature; Measured and Simulated")#\nAQ June")
    # ax.set_xlabel("Datetime")
    ax.set_ylabel(r"$T_{\mathrm{db}}^\mathrm{i}$ [°C]")
    plt.gcf().autofmt_xdate()
    plt.legend()
    plt.grid()
    plot_dir = Path(plot_dir)
    plt.savefig(plot_dir / "predicted_vs_true_time_weekly.png")
    plt.savefig(plot_dir / "predicted_vs_true_time_weekly.eps")
    plt.close(fig)


def plot_variables(epc, variables, plot_dir, method="mean"):
    for el in variables:
        sim = el["sim"]  # Simulated columns to be used.
        meas = el["meas"]  # Measured columns to be used.

        # Convert always to list.
        if not isinstance(sim, list):
            sim = [sim]
        if not isinstance(meas, list):
            meas = [meas]

        plt.rcParams.update({"font.size": 45})
        fig, ax = plt.subplots(constrained_layout=False, figsize=(40, 30))
        alphas = [.6, .6]

        # Retrieve true and predicted data.
        col_name = "T"
        for x in meas:
            if "co2" in x.lower():
                col_name = "CO2"
                break

        df_true, df_predicted, _  = epc.retrieve_data(col_name=col_name, true_column=meas, mean=False)
        df_predicted.index = df_predicted.index + pd.DateOffset(year=df_true.index[0].year)

        # Filter predicted data by using specified columns.
        cols = [[c for c in df_predicted.columns if s.lower() in c.lower()] for s in sim]
        cols = list(np.array(cols).flatten())
        where = (df_true.iloc[:, 0] - df_predicted.iloc[:, 0]).dropna().index

        # Aggregate data.
        try:
            if el["agg_sim"]:
                name = str([n[:n.find("MAC")] if "MAC" in n else n for n in cols])
                name = str(cols).replace("[", "").replace("]", "").replace("'", "").replace('"', '').replace(",", "\n")
                df_predicted = df_predicted[cols].mean(axis=1).to_frame(name=name)
                cols = df_predicted.columns
                # print("COLS(COLUMNS):", cols)
        except KeyError:
            pass
        # Plot simulated / predicted data.
        for c, p in enumerate(df_predicted[cols].columns):
            var_to_plot = df_predicted[p]
            ax.plot(
                var_to_plot.loc[where].index + pd.DateOffset(year=df_true.index[0].year),
                var_to_plot.loc[where],
                # label=var.replace("_", "\_") + " sim",
                label=p,
                lw=5,
                alpha=alphas[c]
            )
        # Aggregate data.
        try:
            if el["agg_meas"]:
                name = str([n[:n.find("MAC")] if "MAC" in n else n for n in meas])
                name = str(meas).replace("[", "").replace("]", "").replace("'", "").replace('"', '').replace(",", "\n")
                # print("MEAS NAME:", name)
                df_true = df_true[meas].mean(axis=1).to_frame(name=name)
                meas = df_true.columns
                # print("MEAS(COLUMNS):", meas)
        except KeyError:
            pass
        # Plot measured / true data.
        for t in df_true[meas].columns:
            var_to_plot2 = df_true[t]
            ax.plot(
                var_to_plot2.loc[where].index + pd.DateOffset(year=df_true.index[0].year),
                var_to_plot2.loc[where],
                # label=var.replace("_", "\_") + " true",
                label=t,
                lw=5,
                alpha=.6
            )
        # Plot stuff.
        ax.set_xlabel("Datetime")
        ax.set_ylabel(r"$T_{\mathrm{db}}^\mathrm{i}$ [°C]")
        ax.xaxis.set_tick_params(which="minor", length=10, width=2.5,
                                 color="grey", labelsize="xx-small",
                                 labelcolor="grey")

        ax.xaxis.set_minor_formatter(mdates.DateFormatter('%d')) 
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%b %d')) 
        ax.xaxis.set_minor_locator(mdates.DayLocator(interval=1))

        plt.gcf().autofmt_xdate()
        # plt.legend(loc="upper left")
        plt.legend(loc="best")
        plt.grid(visible=True, which='major', color='darkgrey', linestyle='-')
        plt.grid(visible=True, which='minor', color='grey', linestyle='--')
        plot_dir = Path(plot_dir)
        sim_name = str([n[:n.find("MAC")] if "MAC" in n else n for n in sim])
        if not isinstance(sim_name, list):
            sim_name = [sim_name]
        if not isinstance(meas, list):
            meas = [meas]
        name = list(np.array([sim_name, meas]).flatten())
        name = "_".join(tuple(str(n) for n in name))
        _, df_predicted, _  = epc.retrieve_data(col_name="Rad")
        _, df_predicted, _ = epc.retrieve_data(col_name="sch")
        df_predicted.index = df_predicted.index.map(lambda t: t.replace(year=YEAR))
        colors = ["red", "green", "blue"]
        for cnt, v in enumerate(df_predicted.columns):
            ax = ax.twinx()
            ax.plot(df_predicted[v], label=v, ls="-", lw=2, color=colors[cnt])
            plt.legend(bbox_to_anchor=(0, -cnt/16-1/10), loc="upper left")
            plt.ylim(-2, 3)
            plt.yticks([min(df_predicted[v]), max(df_predicted[v])])
        plt.tight_layout()
        name = name.replace(":", "").replace("'", "").replace('"', '').replace(",", "_").replace("[", "").replace("]", "").replace("'", "").replace('"', '').replace(",", "").replace("\\n", "") + ".png"
        plt.savefig(plot_dir / (name))
        plt.close(fig)


class EnergyPlusCalibration:
    def __init__(
        self, df, idf, plot_dir, data_true_dir, graph=False, time_bin="1H"
    ):
        self.df = df
        self.df.columns = [c.replace(":ON", "") for c in self.df.columns]
        self.building_name = idf.main_block
        self.area = idf.area
        self.volume = idf.volume
        self.idf = idf
        self.plot_dir = plot_dir
        self.data_true_dir = str(Path(data_true_dir))
        self.graph = graph
        self.time_bin = time_bin

    def retrieve_data(self, col_name, filter_by=[], true_column="T_db_i", mean=True):
        # Retrieve simulated data.
        df_sim = copy.deepcopy(self.df)
        df_sim["Date/Time"] = df_sim["Date/Time"].apply(
            kpi.EnergyPlusKPI.convert_to_datetime
        )
        df_sim = df_sim.set_index("Date/Time")
        df_sim = df_sim.resample(self.time_bin).mean()
    
        # Remove the first six days from simulated data.
        first_date = df_sim.index[0]
        df_sim = df_sim.loc[df_sim.index > first_date + dt.timedelta(days=WARM_UP_DAYS)]

        x_axis = df_sim.rename(
            columns={
                "Environment:Site Outdoor Air Drybulb Temperature [C](TimeStep)": "T_db_e[C]"
            }
        ).loc[:, "T_db_e[C]"]
        x_axis = x_axis.to_frame()

        if col_name == "T_db_i":
            if filter_by == []:
                df_sim = kpi.EnergyPlusKPI.indoor_mean_air_temp(
                    df_sim, self.building_name
                )
                df_sim = df_sim.loc[:, ["T_db_i[C]"]]
                data_sim = df_sim.loc[:, ["T_db_i[C]"]]
            else:
                df_new = df_sim.copy()
                for col in df_new.columns:
                    if (
                        "Zone Mean Air Temperature [C](TimeStep)" not in col
                        or self.building_name.upper() not in col
                    ):
                        df_new = df_new.drop(col, axis=1)
                df_final = pd.DataFrame()
                for el in filter_by:
                    for col in df_new.columns:
                        if el in col:
                            df_final[col] = df_new[col]
                df_sim["T_db_i[C]"] = df_final.mean(axis=1)
                data_sim = df_sim.loc[:, ["T_db_i[C]"]]

        elif col_name == "Q_c":
            df_sim = df_sim.rename(
                columns={
                    "DistrictCooling:Facility [J](TimeStep)": "Q_c[Wh/m2]"
                }
            )
            df_sim["Q_c[Wh/m2]"] = df_sim["Q_c[Wh/m2]"].apply(
                kpi.EnergyPlusKPI.convert_to_W_m2, args=(self.volume,)
            )
            data_sim = df_sim.loc[:, ["Q_c[Wh/m2]"]]

        elif col_name == "Q_h":
            df_sim = df_sim.rename(
                columns={
                    "DistrictCooling:Facility [J](TimeStep)": "Q_c[Wh/m2]"
                }
            )
            df_sim["Q_c[Wh/m2]"] = df_sim["Q_c[Wh/m2]"].apply(
                kpi.EnergyPlusKPI.convert_to_W_m2, args=(self.volume,)
            )
            data_sim = df_sim.loc[:, ["Q_c[Wh/m2]"]]

        elif col_name == "T":
            df_sim = kpi.EnergyPlusKPI.indoor_mean_air_temp(
                df_sim, self.building_name, keep_single=True
            )
            data_sim = df_sim.copy()

        elif col_name == "CO2":
            cols = [c for c in df_sim.columns if "co2" in c.lower()]
            data_sim = df_sim[cols]
        
        elif col_name == "Rad":
            cols = [c for c in df_sim.columns if "gain" in c.lower()]
            data_sim = df_sim[cols]
        
        elif col_name == "sch":
            cols = [c for c in df_sim.columns if "window" in c.lower()]
            data_sim = df_sim[cols]

        else:
            raise Exception("Invalid col_name")

        # Try to read monitored data in a specific format.
        try:
            # Support different Date/Time formats for measured data.
            formats = ["%d/%m/%Y %H:%M", "%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M:%S%z"]
            for f in formats:
                try:
                    df_true = pd.read_csv(
                        self.data_true_dir, index_col=0, sep=";", parse_dates=True,
                        date_parser=lambda x: dt.datetime.strptime(x, f)
                        )
                    df_true = df_true.resample(self.time_bin).mean()
                    break
                except ValueError:
                    pass
            df_true.index = df_true.index.tz_localize(None)

        except TypeError:
            # Read eplusout.csv format.
            df_true = pd.read_csv(self.data_true_dir, sep=",")
            df_true["Date/Time"] = df_true["Date/Time"].apply(
                kpi.EnergyPlusKPI.convert_to_datetime
            )
            df_true.set_index("Date/Time", inplace=True, drop=True)
            df_true = df_true.resample(self.time_bin).mean()

        df_new = df_true.copy()

        new_columns = []
        if not isinstance(true_column, list):
            true_column = [true_column]
        for col in df_new.columns:
            for tc in true_column:
                if tc.lower() == col.lower():
                    new_columns.append(col)
        df_final = df_new[new_columns]
        if isinstance(filter_by, list) and len(filter_by) > 0:
            df_final = pd.DataFrame()
            for el in filter_by:
                for col in df_new.columns:
                    if el in col:
                        df_final[col] = df_new[col]
        if mean:
            data_true = df_final.mean(axis=1).to_frame()
            data_true.columns = ["T_db_i[C]"]
        else:
            data_true = df_final

        return (data_true, data_sim, x_axis)
