#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Functions used to run EnergyPlus simulations"""
from predyce import idf_editor, calibration
import pandas as pd
import numpy as np
from pathlib import Path
import copy
import subprocess
import os
import uuid
from itertools import product, zip_longest
from multiprocessing import Pool, cpu_count
from predyce.kpi import EnergyPlusKPI
import math
import json
import time
from datetime import datetime, timedelta
from pyepw.epw import EPW
from eppy.runner import run_functions
import gc
import shutil
import warnings
warnings.filterwarnings("ignore", category=FutureWarning) 


def make_eplaunch_options(
    idf, readvars=True, expandobjects=True, verbose="q", output_directory="Out"
):
    """Make options for run, so that it runs like EPLaunch on Windows.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param readvars: Run ReadVarsESO after simulation to generate the CSV file
        of the output, defaults to `True`
    :type readvars: bool, optional
    :param expandobjects: Run ExpandObjects prior to simulation, defaults to
        `True`
    :type expandobjects: bool, optional
    :param verbose: Set verbosity of runtime messages, defaults to 'q'
    :type verbose: str, optional
    :param out_dir: Output directory of EnergyPlus.
    :type out_dir: path or str
    :return: Options for EnergyPlus
    :rtype: dict
    """
    options = {
        "readvars": readvars,
        "expandobjects": expandobjects,
        "verbose": verbose,
        "output_directory": output_directory,
    }
    return options


class Runner:
    """Class which run EnergyPlus simulations."""

    def __init__(
        self,
        idf,
        input_file,
        temp_dir,
        out_dir,
        epw_dir,
        plot_dir,
        aggregations=None,
        data_true_dir=None,
        num_of_cpus="auto",
        checkpoint_interval=100,
        checkpoint_data=None,
        graph=False,
        opt=False
    ):
        """
        :param idf: IDF object
        :type idf: class:`predyce.IDF_class.IDF`
        :param input_file: Input file with actions and outputs.
        :type input_file: dict
        :param temp_dir: Output directory of EnergyPlus simulationns.
        :type temp_dir: path or str
        :param out_dir: Output directory.
        :type out_dir: path or str
        :param num_of_cpus: Number of processes to be used. If set to `auto`, a
            number of processes equal to number of CPUs will be used, defaults
            to auto
        :type num_of_cpus: str or int, optional
        :param checkpoint_interval: [Description], defaults to 100
        :type checkpoint_interval: int, optional
        :param checkpoint_data: Checkpoint dataframe, defaults to None
        :type checkpoint_data: class:`pandas.core.frame.DataFrame`, optional
        """
        self.idf = copy.deepcopy(idf)
        self.input_file = input_file
        self.outputs = None
        self.temp_dir = Path(temp_dir)
        self.out_dir = Path(out_dir)
        self.plot_dir = Path(plot_dir)
        self.opt = opt

        try:
            self.output_list = input_file["outputs"]
        except KeyError:
            self.output_list = {}

        try:
            self.kpi_list = input_file["kpi"]
        except KeyError:
            self.kpi_list = {}

        try:
            self.cal_list = input_file["calibration"]
        except KeyError:
            self.cal_list = {}

        try:
            self.calibration_objectives = input_file["calibration_objectives"]
        except KeyError:
            self.calibration_objectives = {}

        self.data_true_dir = data_true_dir
        self.num_cpu = cpu_count()
        if num_of_cpus == 0:
            self.num_processes = self.num_cpu
        else:
            self.num_processes = num_of_cpus
        self.epw_dir = epw_dir
        self.checkpoint_interval = checkpoint_interval
        self.idf.epw_dir = Path(epw_dir)
        self.checkpoint_data = checkpoint_data
        self.graph = graph
        # Create output directory.
        os.makedirs(out_dir, exist_ok=True)
        self.preliminary_actions()
        self.idf.set_block(input_file["building_name"])
        try:
            self.aggregations = input_file["aggregations"]
        except KeyError:
            self.aggregations = None
        try:
            self.start_date = input_file["start_date"]
        except KeyError:
            self.start_date = None
        try:
            self.end_date = input_file["end_date"]
        except KeyError:
            self.end_date = None
        try:
            self.idf.heat_season = input_file["heating_season"].upper()
            idf_editor.set_outputs(idf=self.idf, variable_list=["Schedule Value"], key_value=self.idf.heat_season, schedule_name="On", frequency="Timestep")
        except KeyError:
            self.idf.heat_season = None
        try:
            self.idf.cool_season = input_file["cooling_season"].upper()
            idf_editor.set_outputs(idf=self.idf, variable_list=["Schedule Value"], key_value=self.idf.cool_season, schedule_name="On", frequency="Timestep")
        except KeyError:
            self.idf.cool_season = None
        try:
            self.data_true_columns = input_file["data_true_columns"]
        except KeyError:
            self.data_true_columns = ["T_db_i"]
        try:
            self.area = input_file["area"]
        except KeyError:
            self.area = None
        try:
            self.remove_sim_files = input_file["remove_sim_files"]
        except KeyError:
            self.remove_sim_files = True

    def preliminary_actions(self, save=False):
        """Execute preliminary actions on the building before running the
        multiple simulations.
        """
        try:
            prel_acts = self.input_file["preliminary_actions"]
            if prel_acts:
                print("Preliminary actions...")
        except KeyError:
            prel_acts = {}

        # Set general EPW file for all preliminary actions.
        try:
            epw = prel_acts["epw"]
            print("Setting EPW file for all preliminary actions.")
            self.idf.epw = epw
            del prel_acts["epw"]
        except KeyError:
            pass

        sizing = 0
        free = 0

        for act, values in prel_acts.items():
            print(act, values)
            if act == "simple_hvac_sizing":
                sizing = 1  # HVAC sizing will be run.
            if act == "free_running":
                free = 1 # Free running period will be set.
            else:
                # Set EPW file for a single preliminary actions.
                try:
                    epw = values["epw"]
                    print("Setting EPW file for %s" % act)
                    self.idf.epw = epw
                    del values["epw"]
                except KeyError:
                    pass
                call_method(self.idf, act, values)

        # Run sizing after all preliminary actions.
        if sizing:
            print("Running HVAC sizing.")
            # Set EPW file for a single preliminary actions.
            try:
                epw = prel_acts["simple_hvac_sizing"]["epw"]
                print("Setting EPW file for simple_hvac_sizing")
                self.idf.epw = epw
                del prel_acts["simple_hvac_sizing"]["epw"]
            except KeyError:
                pass

            simple_hvac_sizing(
                self.idf, self.temp_dir, prel_acts["simple_hvac_sizing"]
            )
            # Try to restore the runperiod specified in preliminary actions.
            try:
                call_method(
                    self.idf, "change_runperiod", prel_acts["change_runperiod"]
                )
            except KeyError:
                pass

        # Set free running after all preliminary actions.
        if free:
            print("Setting free running period...")
            free_running(self.idf, self.temp_dir, **prel_acts["free_running"])
            del prel_acts["free_running"]


        # Try to restore the runperiod specified in preliminary actions.
        try:
            call_method(
                self.idf, "change_runperiod", prel_acts["change_runperiod"]
            )
        except KeyError:
            pass

        # Save IDF with preliminary actions applied.
        if save:
            self.idf.saveas(self.out_dir / "idf_preliminary_actions.idf")

    def create_dataframe(self, input_file, include_original=False):
        """Create datafame of all combinations.

        :param input_file: Input file with actions and outputs.
        :type input_file: dict
        :param include_original: Whether to include the original building in
            the list of simulations, defaults to `False`
        :type include_original: bool, optional
        """
        # If no action is specified, add a dummy action.
        if "actions" not in input_file:
            input_file["actions"] = {"no_action": {}}
        if not input_file["actions"]:
            input_file["actions"] = {"no_action": {}}

        # If a directory was specified in the EPW field.
        if not hasattr(self.idf, "epw"):
            if "epw" not in input_file["actions"]:
                # if not input_file["actions"]["epw"]:
                input_file["actions"]["epw"] = {"epw_file": ["dir"]}

        keys, values = zip(*input_file["actions"].items())
        all_data = []
        for k, v in zip(keys, values):
            new_keys = []
            new_values = []
            for field, field_values in v.items():
                new_key = ".".join((k, field))
                new_keys.append(new_key)
                if k == "epw":
                    for epw_file in field_values:
                        if epw_file == "dir":
                            new_values.append(os.listdir(self.epw_dir))
                        else:
                            new_values.append([epw_file])
                else:
                    new_values.append(field_values)
            permutations_dicts = [
                dict(zip(new_keys, v)) for v in product(*new_values)
            ]
            df = pd.DataFrame()
            for el in permutations_dicts:
                df = df.append(el, ignore_index=True)
            if include_original:
                if "epw" not in k:
                    df = df.append(pd.Series(), ignore_index=True)
            all_data.append(df)

        nums = [list(range(len(d))) for d in all_data]
        #runner edited for optimization task
        #perms = product(*nums)
        if self.opt == True:
            perms = zip_longest(*nums,fillvalue=0)
        else:
            perms = product(*nums)
            
        col_list = []
        for d in all_data:
            for col in d.columns:
                col_list.append(col)

        df = pd.DataFrame(columns=col_list)
        #runner edited for optimization task
        #perms = product(*nums)
        
        if self.opt == True:
            perms = zip_longest(*nums)
        else:
            perms = product(*nums)

        for idx, tup in enumerate(perms):
            for i, pos in enumerate(tup):
                new_data = all_data[i].loc[pos]
                df.loc[idx, new_data.index] = new_data

        self.df = df
        if self.outputs == None:
            self.outputs = copy.deepcopy(self.df)


        os.makedirs(self.plot_dir, exist_ok=True)

        with open(self.plot_dir / "summary.html", "w") as fo:
            self.df.to_html(fo)

        with open(self.plot_dir / "info.txt", "w") as f_input:
            try:
                f_input.write(input_file["info"])
            except KeyError:
                pass
        
        with open(self.plot_dir / "input_json.json", "w") as f_json:
            json.dump(input_file, f_json, indent=4)


    def run(self):
        """Run every row of the DataFrame as a simulation."""
        if self.graph:
            os.makedirs(self.plot_dir, exist_ok=True)

        pool = Pool(self.num_processes)
        # result_list = []
        db = Database(out_dir=self.out_dir, interval=self.checkpoint_interval)
        if self.checkpoint_data is not None:
            db.add_data(self.checkpoint_data)

        def log_result(result):
            db.add_data(result.to_frame().T)
            db.checkpoint()
            # result_list.append(result.to_frame().T)

        self.start = time.time()
        if self.num_processes == 1:  # To make it work with Jupyter Notebook
            for i in range(len(self.df)):
                data_res = run(
                    self.df.loc[i],
                    self.idf,
                    self.temp_dir,
                    self.output_list,
                    self.kpi_list,
                    self.cal_list,
                    self.plot_dir,
                    self.data_true_dir,
                    self.aggregations,
                    len(self.df),
                    self.graph,
                    self.start_date,
                    self.end_date,
                    self.data_true_columns,
                    self.area,
                    self.remove_sim_files
                )
                db.add_data(data_res)

        else:
            sims = range(len(self.df))
            if self.checkpoint_data is not None:
                sims = [i for i in sims if i not in self.checkpoint_data.index]
                print("Resuming checkpoint...")

            for i in sims:
                pool.apply_async(
                    run,
                    args=(
                        self.df.loc[i],
                        self.idf,
                        self.temp_dir,
                        self.output_list,
                        self.kpi_list,
                        self.cal_list,
                        self.plot_dir,
                        self.data_true_dir,
                        self.aggregations,
                        len(self.df),
                        self.graph,
                        self.start_date,
                        self.end_date,
                        self.data_true_columns,
                    ),
                    callback=log_result,
                )

            pool.close()
            pool.join()

            os.makedirs(self.out_dir, exist_ok=True)

        self.end = time.time()
        self.results = db.df

        # NOTE: Use this to remove columns from final output.
        # db.clean_data(["epw.epw_file"])
        for c in ["location", "epw.epw_file", "no_action"]:
            try:
                if db.df[c].nunique() == 1:
                    db.clean_data([c])
            except KeyError:
                pass

        db.save_data(self.out_dir / "data_res.csv")
        db.save_data(self.plot_dir / "data_res.csv")

        with open(self.plot_dir / "summary_res.html", "w") as fo:
            self.results.to_html(fo)

    def print_summary(self, filename="summary.txt"):
        """Print summary

        :param filename: Filename of saved summary, defaults to "summary.txt"
        :type filename: str, optional
        """
        lines = [
            "Number of simulations: %d" % (len(self.outputs)),
            "Number of processes allocated in the pool: %d"
            % self.num_processes,
            "Number of CPUs: %d" % self.num_cpu,
            "Started at: %s"
            % datetime.utcfromtimestamp(self.start).strftime(
                "%Y-%m-%d %H:%M:%S"
            ),
            "Finished at: %s"
            % datetime.utcfromtimestamp(self.end).strftime(
                "%Y-%m-%d %H:%M:%S"
            ),
            "Elapsed time: %s" % str(timedelta(seconds=self.end - self.start)),
        ]
        with open(self.out_dir / filename, "w") as fo:
            fo.writelines([l + "\n" for l in lines])

        for l in lines:
            print(l)
        print(self.results)


def calc_soil_surf_temp_run(epw, soil_cond, soil_surf_cond):
    print("Executing CalcSoilSurfTemp")
    # Copy EPW file to current directory.
    cwd = Path.cwd()
    shutil.copyfile(epw, cwd / "in.epw")

    # Run CalcSoilSurfTemp software.
    ep_path = run_functions.paths_from_version("8-9-0")
    coil_path = (
        Path(ep_path[1]) / "PreProcess/CalcSoilSurfTemp/CalcSoilSurfTemp.exe"
    )
    pop = subprocess.Popen(
        str(coil_path),
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    pop.communicate(
        bytes(
            str(soil_cond) + "\n" + str(soil_surf_cond) + "\n",
            encoding="utf-8",
        )
    )
    pop.wait()

    # Read output.
    file2 = cwd / "CalcSoilSurfTemp.out"
    cc = os.path.exists(file2)
    cc = str(cc)

    if cc == "True":
        out = open("CalcSoilSurfTemp.out", "r").readlines()
        if len(out) == 4:
            v1 = out[1]
            var1 = v1[40:]
            var1 = var1.replace(" ", "")

            v2 = out[2]
            var2 = v2[39:]
            var2 = var2.replace(" ", "")

            v3 = out[3]
            var3 = v3[42:]
            var3 = var3.replace(" ", "")

        else:
            print("Invalid length in CalcSoilSurfTemp.out")

        return float(var1), float(var2), float(var3)


def simple_hvac_sizing(idf, temp_dir, values):
    """HVAC sizing according to yearly heating and cooling zonal max values, in
    sobstitution to autosize defined values: a yearly simulation is run and max
    found hourly capacity values are passed to idf_editor function.
    Eventually user-defined max values for capacity are distributed according to zonal system use in percentage
    and then passed to idf_editor function.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param epw: EPW directory
    :type epw: Path or str
    :param temp_dir: Output directory of EnergyPlus simulationns.
    :type temp_dir: Path or str
    :param values: parameters associated to simple_hvac_sizing idf_editor function
    :type values: dict
    """
    # change runperiod
    idf_editor.change_runperiod(idf, "01-01", "31-12")

    # Set needed outputs
    idf_editor.set_outputs(
        idf,
        ["Zone Ideal Loads Zone Sensible Heating Energy"],
        "*",
        frequency="Hourly",
    )
    idf_editor.set_outputs(
        idf,
        ["Zone Ideal Loads Zone Total Cooling Energy"],
        "*",
        frequency="Hourly",
    )

    # Set autosizing to NoLimits in order to take max yearly values
    hvac_sz = idf.idfobjects["ZoneHVAC:IdealLoadsAirSystem".upper()]
    for el in hvac_sz:
        el.Heating_Limit = "NoLimit"
        el.Cooling_Limit = "NoLimit"
    zone_names = [el.Name.split(" ")[0] for el in hvac_sz]

    os.makedirs(temp_dir / "simple_hvac_sizing", exist_ok=True)

    # NOTE: EPW file is embedded into the IDF. Do not specify it.
    idf.run(
        output_directory=temp_dir / "simple_hvac_sizing",
        readvars=True,
        verbose="q",
        expandobjects=True,
    )

    # Read eplusout.
    eplusout = pd.read_csv(temp_dir / "simple_hvac_sizing" / "eplusout.csv")
    eplusout["Date/Time"] = eplusout["Date/Time"].apply(
        EnergyPlusKPI.convert_to_datetime
    )
    eplusout.columns = [c.replace(":ON", "") for c in eplusout.columns]
    zone_max_heat_dict = {}
    zone_max_cool_dict = {}
    df_new = eplusout.copy()

    # Remove useless columns from eplusout.
    for col in df_new.columns:
        if (
            "Zone Ideal Loads Zone Sensible Heating Energy [J](Hourly"
            not in col
            and "Zone Ideal Loads Zone Total Cooling Energy [J](Hourly"
            not in col
        ):
            df_new.drop(col, axis=1, inplace=True)

    # Create zone specific dictionaries with associated max values.
    for z in zone_names:
        for col in df_new.columns:
            if (
                "Zone Ideal Loads Zone Sensible Heating Energy [J](Hourly"
                in col
                and z.lower() in col.lower()
            ):
                zone_max_heat_dict[z] = np.nanmax(df_new[col].values) / 3600
            elif (
                "Zone Ideal Loads Zone Total Cooling Energy [J](Hourly" in col
                and z.lower() in col.lower()
            ):
                zone_max_cool_dict[z] = np.nanmax(df_new[col].values) / 3600

    if not bool(zone_max_heat_dict) and not bool(zone_max_cool_dict):
        raise Exception("Problem in retrieving heating and cooling data.")

    if any(np.isnan(val) for val in zone_max_heat_dict.values()) or any(
        np.isnan(val) for val in zone_max_cool_dict.values()
    ):
        raise Exception("Problem in retrieving heating and cooling data.")

    # If user set its own max capacity values, recalibrate % of max value
    # available to each zone based on sim results.
    if "Maximum_Sensible_Heating_Capacity" in values.keys():
        max_heat = 0
        for x in zone_max_heat_dict.values():
            max_heat += x
        for k in zone_max_heat_dict.keys():
            zone_max_heat_dict[k] = (
                float(values["Maximum_Sensible_Heating_Capacity"])
                * zone_max_heat_dict[k]
                / max_heat
            )
        del values["Maximum_Sensible_Heating_Capacity"]
    elif "Maximum_Total_Cooling_Capacity" in values.keys():
        max_cool = 0
        for x in zone_max_cool_dict.values():
            max_cool += x
        for k in zone_max_cool_dict.keys():
            zone_max_cool_dict[k] = (
                float(values["Maximum_Total_Cooling_Capacity"])
                * zone_max_cool_dict[k]
                / max_cool
            )
        del values["Maximum_Total_Cooling_Capacity"]

    if "factor" in values.keys():
        factor = values["factor"]
        del values["factor"]
    else:
        factor = 1

    # Call idf_editor function
    idf_editor.simple_hvac_sizing(
        idf, zone_max_heat_dict, zone_max_cool_dict, factor, **values
    )
    remove_files(temp_dir / "simple_hvac_sizing", preserve_path=True)
    return idf


def free_running(idf, temp_dir, start, end, filter_by=""):
    idf2 = copy.deepcopy(idf)
    idf_editor.change_runperiod(idf2, "01-01", "31-12", fmt="%d-%m")
    filter_by = idf_editor.to_list(filter_by)

    schedules = []
    for zhec in idf2.idfobjects["ZoneHVAC:EquipmentConnections".upper()]:
        if any([f.lower() in zhec.Zone_Name.lower() for f in filter_by]):

            for zceln in idf2.idfobjects["ZoneHVAC:EquipmentList".upper()]:
                if zceln.Name == zhec.Zone_Conditioning_Equipment_List_Name:

                    for zhilas in idf2.idfobjects["ZoneHVAC:IdealLoadsAirSystem".upper()]:
                        if zhilas.Name == zceln["Zone_Equipment_{}_Name".format(1)]:
                            schedules.append(zhilas.Heating_Availability_Schedule_Name)
                            schedules.append(zhilas.Cooling_Availability_Schedule_Name)
                            break

    for s in schedules:
        idf_editor.set_outputs(idf2, ["Schedule Value"], s, frequency="Hourly", reset=False)

    os.makedirs(temp_dir / "free_running", exist_ok=True)

    idf2.run(
        output_directory=temp_dir / "free_running",
        readvars=True,
        verbose="q",
        expandobjects=True,
        weather=Path(os.path.realpath(__file__)).parent.parent / "test/EPW/ITA_Rome.162420_IWEC.epw"
    )
    eplusout = pd.read_csv(temp_dir / "free_running" / "eplusout.csv")
    eplusout["Date/Time"] = eplusout["Date/Time"].apply(EnergyPlusKPI.convert_to_datetime)
    eplusout.set_index("Date/Time", inplace=True, drop=True)
    columns = [c for c in eplusout.columns if "heating availability" in c.lower() or "cooling availability" in c.lower()]
    avail_df = eplusout[columns].resample("1H").sum().astype(int)
    avail_df = avail_df.loc["01-01-1970 00:00:00":"12-31-1970 23:59:00"]

    avail_path = temp_dir / "free_running" / "avail.csv"
    avail_df.to_csv(avail_path, sep=";")

    # Assign availability schedules to file.
    for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
        if sc.Name in schedules:

            for cnt, c in enumerate(avail_df.columns):
                if sc.Name.lower() in c.lower():
                    col_number = cnt + 2
                    break
            sf = idf.newidfobject("SCHEDULE:FILE")
            fields = {
                "Name": sc.Name,
                "File Name": str(avail_path),
                "Column Number": col_number,
                "Rows to Skip at Top": 1,
                "Number of Hours of Data": 8760,
                "Column Separator": "Semicolon",
                "Interpolate to Timestep": "No"
            }
            idf_editor.set_fields(sf, data=fields)
            # print(sf)
            sc.Name = "_deleteme"

    while True:
        found = 0
        for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
            if "deleteme" in sc.Name:
                idf.removeidfobject(sc)
                found = 1
        if not found:
            break
    

    # Set free-running period.
    avail_df.loc["1970-" + start:"1970-" + end] = 0
    avail_df.to_csv(avail_path, sep=";")
    print("Free running period:  ", "1970-" + start + " > " + "1970-" + end)


def call_method(building, function, values):
    """Call functions from idf_editor.

    :param building: IDF object
    :type building: class:`predyce.IDF_class.IDF`
    :param function: Name of the function which has to be called.
    :type function: str
    :param values: Dictionary where each key is a field and each value is its
        value.
    :type values: dict
    :return: Modified IDF object
    :rtype: class:`predyce.IDF_class.IDF`
    """
    for _, v in values.items():
        try:
            if math.isnan(v):
                return building
        except Exception:
            pass
    # NOTE: IMPORTANT: Be aware of the list order!
    if function == "no_action":
        pass
    if function == "add_external_insulation_walls":
        idf_editor.add_external_insulation_walls(building, **values)
    elif function == "add_internal_insulation_walls":
        idf_editor.add_internal_insulation_walls(building, **values)
    elif function == "add_roof_insulation_tilted_roof":
        idf_editor.add_roof_insulation_tilted_roof(building, **values)
    elif function == "add_ceiling_insulation_tilted_roof":
        idf_editor.add_ceiling_insulation_tilted_roof(building, **values)
    elif function == "add_insulation_flat_roof":
        idf_editor.add_insulation_flat_roof(building, **values)
    elif "add_external_insulation_floor" in function:
        idf_editor.add_external_insulation_floor(building, **values)
    elif "add_insulation" in function:
        idf_editor.add_insulation(building, **values)
    elif "change_ufactor_walls" in function:  # NOTE:
        idf_editor.change_ufactor_walls(building, **values)  # leave
    elif "change_ufactor_roofs" in function:  # in
        idf_editor.change_ufactor_roofs(building, **values)  # this
    elif "change_ufactor_windows" in function:  # order
        idf_editor.change_ufactor_windows(building, **values)  # .
    elif "change_ufactor" in function:  # .
        idf_editor.change_ufactor(building, **values)  # .
    elif function == "change_windows_system":
        idf_editor.change_windows_system(building, **values)
    elif function == "change_wwr":
        idf_editor.change_wwr(building, **values)
    elif function == "epw":
        building.epw = building.epw_dir / values["epw_file"]
    elif "change_occupancy" in function:
        idf_editor.change_occupancy(building, **values)
    elif function == "add_overhang_simple":
        idf_editor.add_overhangs_simple(building, **values)
    elif function == "add_fin_simple":
        idf_editor.add_fin_simple(building, **values)
    elif "change_ach" in function:
        idf_editor.change_ach(building, **values)
    elif "change_infiltration" in function:
        idf_editor.change_infiltration(building, **values)
    elif "change_outdoor_air_ach" in function:
        idf_editor.change_outdoor_air_ach(building, **values)
    elif "add_blind_v2" in function:
        idf_editor.add_blind_v2(building, **values)
    elif "add_blind" in function:
        idf_editor.add_blind(building, **values)
    elif function == "remove_shading":
        idf_editor.remove_shading(building, **values)
    elif function == "add_slat_angle_control":
        idf_editor.add_slat_angle_control(building, **values)
    elif function == "set_block_beam_solar":
        idf_editor.set_block_beam_solar(building, **values)
    elif "add_scheduled_nat_vent" in function:
        idf_editor.add_scheduled_nat_vent(building, **values)
    elif function == "add_mechanical_ventilation":
        idf_editor.add_mechanical_ventilation(building, **values)
    elif function == "remove_mechanical_ventilation":
        idf_editor.remove_mechanical_ventilation(building, **values)
    elif function == "activate_cooling":
        idf_editor.activate_cooling(building, **values)
    elif function == "activate_heating":
        idf_editor.activate_heating(building, **values)
    elif function == "deactivate_cooling":
        idf_editor.deactivate_cooling(building, **values)
    elif function == "deactivate_heating":
        idf_editor.deactivate_heating(building, **values)
    elif function == "deactivate_humidistat":
        idf_editor.deactivate_humidistat(building, **values)
    elif function == "change_runperiod":
        idf_editor.change_runperiod(building, **values)
    elif function == "add_custom_runperiod":
        idf_editor.add_custom_runperiod(building, **values)
    elif function == "set_first_day":
        idf_editor.set_first_day(building, **values)
    elif function == "change_window_opening":
        idf_editor.change_window_opening(building, **values)
    elif function == "add_hvac":
        idf_editor.add_hvac(building, **values)
    elif function == "set_daylight_saving":
        idf_editor.set_daylight_saving(building, **values)
    elif function == "set_ems_constants":
        idf_editor.set_ems_constants(building, **values)
    elif "set_simplified_windows" in function:
        idf_editor.set_simplified_windows(building, **values)
    elif "change_shgc" in function:
        idf_editor.change_shgc(building, **values)
    elif function == "change_visible_transmittance":
        idf_editor.change_visible_transmittance(building, **values)
    elif "add_internal_heat_gain" in function:
        idf_editor.add_internal_heat_gain(building, **values)
    elif function == "change_internal_heat_gain":
        idf_editor.change_internal_heat_gain(building, **values)
    elif function == "change_specific_heat_material":
        idf_editor.change_specific_heat_material(building, **values)
    elif function == "add_overhangs_simple":
        idf_editor.add_overhangs_simple(building, **values)
    elif function == "set_shading_control":
        idf_editor._set_shading_control(building, **values)
    elif "set_object_params" in function:
        idf_editor.set_object_params(building, **values)
    elif "set_category_params" in function:
        idf_editor.set_category_params(building, **values)
    elif "remove_objects" in function:
        idf_editor.remove_objects(building, **values)
    elif "add_internal_mass" in function:
        idf_editor.add_internal_mass(building, **values)
    elif "change_internal_mass" in function:
        idf_editor.change_internal_mass(building, **values)
    elif function == "set_ach_schedule":
        idf_editor.set_ach_schedule(building, **values)
    elif "set_outputs" in function:
        idf_editor.set_outputs(building, **values)
    elif "edit_csv_schedule" in function:
        idf_editor.edit_csv_schedule(building, **values)
    elif "set_outdoor_co2" in function:
        idf_editor.set_outdoor_co2(building, **values)
    elif "change_co2_prod_rate" in function:
        idf_editor.change_co2_prod_rate(building, **values)
    elif "add_windandstack_nat_vent" in function:
        idf_editor.add_windandstack_nat_vent(building, **values)
    elif "change_stack_opening" in function:
        idf_editor.change_stack_opening(building, **values)
    elif "assign_radiators_schedules" in function:
        idf_editor.assign_radiators_schedules(building, **values)
    elif "change_setpoint_thermostat" in function:
        idf_editor.change_setpoint_thermostat(building, **values)
    elif "change_setpoint" in function:
        idf_editor.change_setpoint(building, **values)
    elif "change_setback" in function:
        idf_editor.change_setback(building, **values)
    elif "activate_humidistat" in function:
        idf_editor.activate_humidistat(building, **values)
    elif "add_mechanical_ventilation" in function:
        idf_editor.add_mechanical_ventilation(building, **values)
    elif function == "assign_construction_copy":
        idf_editor.assign_construction_copy(building, **values)
    elif "change_availability_thermostat" in function:
        idf_editor.change_availability_thermostat(building, **values)
    elif function == "set_epw":
        idf_editor.set_epw(building, **values)
    elif function == "add_pdec":
        idf_editor.add_pdec(building, **values)
    elif function == "add_eahx":
        soil_cond = values["soil_cond"]
        soil_surf_cond = values["soil_surf_cond"]
        (
            av_soil_surf_temp,
            ampl_soil_surf_temp,
            phase_soil_surf_temp,
        ) = calc_soil_surf_temp_run(building.epw, soil_cond, soil_surf_cond)
        building.soil_properties = (
            av_soil_surf_temp,
            ampl_soil_surf_temp,
            phase_soil_surf_temp,
        )
        idf_editor.add_eahx(
            building,
            av_soil_surf_temp=av_soil_surf_temp,
            ampl_soil_surf_temp=ampl_soil_surf_temp,
            phase_soil_surf_temp=phase_soil_surf_temp,
            **values
        )
    else:
        raise Exception("Error: No edit. Requested edit was %s." % function)
    return building


def call_kpi(ekpi, function, params):
    func = function.lower()
    # NOTE: Write KPI strings in lowercase.
    if func == "pmv_ppd":
        kpi = ekpi.pmv_ppd(**params)
    elif func == "adaptive_comfort_model":
        kpi = ekpi.adaptive_comfort_model(**params)
    elif "energy_signature" in func:
        kpi = ekpi.energy_signature(**params)
    elif func == "cidh":
        kpi = ekpi.cidh(**params)
    elif func == "adaptive_residuals":
        kpi = ekpi.adaptive_residuals(**params)
    elif func == "cdd":
        kpi = ekpi.cdd(**params)
    elif func == "cdd_res":
        kpi = ekpi.cdd_res(**params)
    elif func == "cdh":
        kpi = ekpi.cdh(**params)
    elif func == "cdh_res_pdec":
        kpi = ekpi.cdh_res_pdec(**params)
    elif func == "hdh":
        kpi = ekpi.hdh(**params)
    elif func == "hdd":
        kpi = ekpi.hdd(**params)
    elif func == "hidh":
        kpi = ekpi.hidh(**params)
    elif func == "qach":
        kpi = ekpi.qach(**params)
    elif func == "qc_pdec":
        kpi = ekpi.qc_pdec(**params)
    elif func == "qc_eahx":
        kpi = ekpi.qc_eahx(**params)
    elif func == "qh_eahx":
        kpi = ekpi.qh_eahx(**params)
    elif func == "ccp":
        kpi = ekpi.ccp(**params)
    elif "f_q_c" in func:
        kpi = ekpi.f_Q_c(**params)
    elif "f_q_h" in func:
        kpi = ekpi.f_Q_h(**params)
    elif "q_c" in func:
        kpi = ekpi.Q_c(**params)
    elif "q_h" in func:
        kpi = ekpi.Q_h(**params)
    elif "q_i" in func:
        kpi = ekpi.Q_I(**params)
    elif func == "interior_lights":
        kpi = ekpi.interior_lights(**params)
    elif func == "n_h_kwh":
        kpi = ekpi.n_h_kwh(**params)
    elif "n_fr" in func:
        kpi = ekpi.n_fr(**params)
    elif "t_op" in func:
        kpi = ekpi.t_op(**params)
    elif func == "co2":
        kpi = ekpi.co2(**params)
    elif "n_co2_bi" in func:
        kpi = ekpi.n_co2_bI(**params)
    elif "ach_tot" in func:
        kpi = ekpi.ach_tot(**params)
    elif "n_co2_aiii" in func:
        kpi = ekpi.n_co2_aIII(**params)
    elif "wbd_averages" in func:
        kpi = ekpi.wbd_averages(**params)
    elif "carpet_plot" in func:
        kpi = ekpi.carpet_plot(**params)
    elif "carrier_plot" in func:
        kpi = ekpi.carrier_plot(**params)
    elif "correlations" in func:
        kpi = ekpi.correlations(**params)
    else:
        raise Exception("KPI not found. Requested was %s." % func)

    if isinstance(kpi, dict):
        return json.dumps(kpi, cls=NpEncoder)
    else:
        return kpi


def call_timeseries_kpi(ekpi, function, params):
    func = function.lower()
    if "timeseries_q_c" in func:
        kpi = ekpi.timeseries_Q_c(**params)
    elif "timeseries_q_h" in func:
        kpi = ekpi.timeseries_Q_h(**params)
    elif "timeseries_f_q_h" in func:
        kpi = ekpi.timeseries_f_Q_h(**params)
    elif "timeseries_n_fr" in func:
        kpi = ekpi.timeseries_n_fr(**params)
    elif "timeseries_t_db_i" in func:
        kpi = ekpi.timeseries_t_db_i(**params)
    elif "timeseries_t_db_o" in func:
        kpi = ekpi.timeseries_t_db_o(**params)
    elif "timeseries_co2" in func:
        kpi = ekpi.timeseries_co2(**params)
    elif "timeseries_n_co2_aiii" in func:
        kpi = ekpi.timeseries_n_co2_aIII(**params)
    elif "timeseries_n_co2_bi" in func:
        kpi = ekpi.timeseries_n_co2_bI(**params)
    elif "timeseries_acm_hourly_cat" in func:
        kpi = ekpi.timeseries_acm_hourly_cat(**params)
    elif "timeseries_wbt" in func:
        kpi = ekpi.timeseries_wbt(**params)
    elif "timeseries_mix_ratio_i" in func:
        kpi = ekpi.timeseries_mix_ratio_i(**params)
    elif "timeseries_mix_ratio_o" in func:
        kpi = ekpi.timeseries_mix_ratio_o(**params)
    elif "timeseries_mix_ratio_eahx" in func:
        kpi = ekpi.timeseries_mix_ratio_eahx(**params)
    elif "timeseries_wbd" in func:
        kpi = ekpi.timeseries_wbd(**params)
    elif "timeseries_pmv_ppd" in func:
        kpi = ekpi.timeseries_pmv_ppd(**params)
    elif "timeseries_t_op" in func:
        kpi = ekpi.timeseries_t_op(**params)
    else:
        raise Exception("Timeseries KPI not found. Requested was %s." % func)

    if isinstance(kpi, dict):
        return json.dumps(kpi, cls=NpEncoder)
    else:
        return kpi


def run(
    df,
    idf,
    temp_dir,
    output_list,
    kpi_list,
    cal_list,
    plot_dir,
    data_true_dir,
    aggregations,
    num_total="?",
    graph=False,
    start_date=None,
    end_date=None,
    true_column="",
    area=None,
    remove_sim_files=True
):
    """Create building, run EnergyPlus and return its output.

    :param df: DataFrame of simulations.
    :type df: class:`pandas.core.frame.DataFrame`
    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param temp_dir: Output directory of EnergyPlus simulationns.
    :type temp_dir: path
    :param output_list: EnergyPlus outputs which have to be saved into the
        results dataframe.
    :type output_list: list
    :param kpi_list: KPI which have to be calculated and saved into the results
        dataframe.
    :type kpi_list: list
    :param cal_list: [Description]
    :type cal_list: [Description]
    :param plot_dir: Path to directory where plots are saved.
    :type plot_dir: Path or str
    :param data_true_dir: [Description]
    :type data_true_dir: [Description]
    :param num_total: Total number of simulations, only for print purpose,
        defaults to "?"
    :type num_total: int, optional
    :param graph: Whether to save plots, defaults to `False`
    :type graph: bool, optional
    :raises Exception: If editing action has no effect on the building
    :return: Dataframe with added outputs.
    :rtype: class:`pandas.core.frame.DataFrame`
    """
    # Create and cd into a temporary directory.
    os.makedirs(temp_dir, exist_ok=True)

    temp_dir = generate_temp_folder(temp_dir, 25)
    os.mkdir(temp_dir)
    os.chdir(temp_dir)

    plot_idx = str(df.name)  # Name of the simulation

    # Create building to be simulated.
    transformation = df
    building = create_building(idf, transformation)

    # TODO: Gestire meglio.
    epwobj = EPW()
    epwobj.read(building.epw)
    location = {
        "city": epwobj.location.city,
        "longitude": epwobj.location.longitude,
        "latitude": epwobj.location.latitude,
    }
    df["location"] = json.dumps(location, cls=NpEncoder)  # Generates warning
    error_occurred = 0
    try:
        out = run_building(building, df, temp_dir, output_list, num_total)
    except KeyboardInterrupt:
        print("Forced interruption, perform cleaning...")
        remove_files(temp_dir)
        print("Done")
        quit()  # Terminate program
    except Exception as e:
        print(e)
        error_occurred = 1
        df["err"] = 1

    if not error_occurred:
        # TODO: Teoricamente da rimuovere se non abbiamo output standard.
        for o in output_list:
            if "RunPeriod" in o:
                df[o] = out[o].iloc[-1]
            elif ("Hourly" in o) or ("Monthly" in o) or ("Daily" in o):
                df[o] = out[o]
            else:
                pass

        ts_df = TimeSeriesDatabase()
        real_name = copy.deepcopy(building.main_block)  # A backup
        if aggregations is not None:
            for func, agg in aggregations.items():
                if "timeseries" in func:
                    for a in agg:
                        # building.main_block = a
                        building.set_block(a)
                        ekpi = EnergyPlusKPI(
                            out,
                            building,
                            plot_dir / plot_idx,
                            start_date,
                            end_date,
                            graph=graph,
                            area=area,
                        )
                        params = kpi_list[func]
                        pd_series = pd.Series(
                            call_timeseries_kpi(ekpi, func, params)
                        )
                        pd_series.name = "_".join((func, a))
                        ts_df.add_data(pd_series)
                else:
                    for a in agg:
                        # building.main_block = a
                        building.set_block(a)
                        ekpi = EnergyPlusKPI(
                            out,
                            building,
                            plot_dir / plot_idx,
                            start_date,
                            end_date,
                            graph=graph,
                            area=area,
                        )
                        params = kpi_list[func]
                        if func == "pmv_ppd":
                            kpi = ekpi.pmv_ppd(**params)
                        elif func == "adaptive_comfort_model":
                            kpi = ekpi.adaptive_comfort_model(**params)
                        elif func == "co2":
                            kpi = ekpi.co2(**params)
                        elif "t_op" in func:
                            kpi = ekpi.t_op(**params)
                        elif "n_co2_aIII" in func:
                            kpi = ekpi.n_co2_aIII(**params)
                        elif "n_co2_bI" in func:
                            kpi = ekpi.n_co2_bI(**params)
                        elif func == "Q_h":
                            kpi = ekpi.Q_h(**params)
                        elif func == "f_Q_h":
                            kpi = ekpi.f_Q_h(**params)
                        elif "n_fr" in func:
                            kpi = ekpi.n_fr(**params)
                        elif "energy_signature" in func:
                            kpi = ekpi.energy_signature(**params)
                        elif func == "Q_c":
                            kpi = ekpi.Q_c(**params)
                        else:
                            raise Exception(
                                "Aggregation KPI not found. Requested was %s."
                                % func
                            )

                        if isinstance(kpi, dict):
                            df["_".join((func, a))] = json.dumps(
                                kpi, cls=NpEncoder
                            )
                        else:
                            df["_".join((func, a))] = kpi

            building.set_block(real_name)

        ekpi = EnergyPlusKPI(
            out, building, plot_dir / plot_idx, start_date, end_date, graph=graph, area=area
        )
        for func, params in kpi_list.items():
            if "timeseries" in func:
                pd_series = pd.Series(call_timeseries_kpi(ekpi, func, params))
                pd_series.name = func
                ts_df.add_data(pd_series)
            elif func == "fict_cool":
                # TODO gestire due casi: casa con già hvac ma senza cooling, casa senza niente
                idf_editor.activate_cooling(building, **params)
                idf_editor.change_ach(building, 0, "ventilation")
                out2 = run_building(
                    building,
                    transformation,
                    temp_dir / ".kpi",
                    output_list,
                )
                kpi = 0
                kpi = ekpi.fict_cool(out2)
                if isinstance(kpi, dict):
                    df[func] = json.dumps(kpi, cls=NpEncoder)
                else:
                    df[func] = kpi

            elif func == "fict_heat":
                # TODO gestire due casi: casa con già hvac ma senza heating, casa senza niente
                # non mi ricordo come capisce da solo che deve leggere il district heating/cooling nell'eplusout
                idf_editor.activate_heating(building, **params)
                idf_editor.change_ach(building, 0, "ventilation")
                out2 = run_building(
                    building,
                    transformation,
                    temp_dir / ".kpi",
                    output_list,
                )
                kpi = 0
                kpi = ekpi.fict_heat(out2)
                if isinstance(kpi, dict):
                    df[func] = json.dumps(kpi, cls=NpEncoder)
                else:
                    df[func] = kpi

            elif func == "cdh_res_eahx":
                try:
                    v1, v2, v3 = building.soil_properties
                except AttributeError:
                    v1, v2, v3 = calc_soil_surf_temp_run(
                        building.epw,
                        params["soil_condition"],
                        params["soil_surf_cond"],
                    )
                kpi = ekpi.cdh_res_eahx(var1=v1, var2=v2, var3=v3, **params)
                if isinstance(kpi, dict):
                    df[func] = json.dumps(kpi, cls=NpEncoder)
                else:
                    df[func] = kpi

            elif func == "hdh_res_eahx":
                try:
                    v1, v2, v3 = building.soil_properties
                except AttributeError:
                    v1, v2, v3 = calc_soil_surf_temp_run(
                        building.epw, params["soil_cond"], params["soil_surf_cond"]
                    )
                kpi = ekpi.cdh_res_eahx(var1=v1, var2=v2, var3=v3, **params)
                if isinstance(kpi, dict):
                    df[func] = json.dumps(kpi, cls=NpEncoder)
                else:
                    df[func] = kpi

            elif func == "initial_cost_torino":
                #TODO non capisco come funziona il passaggio di sim_params in questo caso, qualcuno me lo può spiegare?
                kpi = ekpi.initial_cost_torino(df, **params)
                if isinstance(kpi, dict):
                    df[func] = json.dumps(kpi, cls=NpEncoder)
                else:
                    df[func] = kpi

            # Standard KPIs.
            else:
                df[func] = call_kpi(ekpi, func, params)  # Generates warning
                # df.loc[:, func] = call_kpi(ekpi, func, params)  # FIXME

        # Calibration.
        if len(cal_list) > 0:
            # TODO: Make a separate function which calls calibration functions.
            epc = calibration.EnergyPlusCalibration(
                out, building, plot_dir / plot_idx, data_true_dir
            )
            # TODO: Replace col_name calls and support multiple objectives.
            (x, y, ax) = epc.retrieve_data(
                col_name="T_db_i", true_column=true_column
            )
            x.index = x.index.map(lambda t: t.replace(year=1970))
            y.index = y.index.map(lambda t: t.replace(year=1970))
            ax.index = ax.index.map(lambda t: t.replace(year=1970))

            for func, params in cal_list.items():
                if func == "rmse":
                    met = calibration.rmse(x, y)
                if func == "mbe":
                    met = calibration.mbe(x, y)
                if func == "rmse_mbe":
                    met = calibration.rmse_mbe(x, y)
                if func == "calibration_signature":
                    met = calibration.calibration_signature(
                        x, y, ax, plot_dir / plot_idx
                    )
                if func == "comparison":
                    met = calibration.comparison(x, y, plot_dir / plot_idx, **params)
                if func == "plot_variables":
                    met = calibration.plot_variables(
                        epc, plot_dir=plot_dir / plot_idx, **params
                    )
                try:
                    df[func] = met
                except Exception:
                    df[func] = float("nan")

        ts_df.save_data(plot_dir / plot_idx / "timeseries_data_res.csv")

    os.makedirs(plot_dir / plot_idx, exist_ok=True)

    try:
        shutil.copyfile(temp_dir / "eplusout.err", plot_dir / plot_idx / "eplusout.err")
    except Exception as e:
        print(e)

    if remove_sim_files:
        remove_files(temp_dir)  # Remove e+ simulation files

    return df


def create_building(idf, transformation):
    """Apply edits on building before running EnergyPlus simulation.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param transformation: Dictionary of transformations. Each key corresponds
        to an action. Each value is a dictionary where each key is a field and
        each value is its value.
    :type transformation: dict
    :return: Modified IDF object
    :rtype: class:`predyce.IDF_class.IDF`
    """
    building = copy.deepcopy(idf)
    new_trans = {}
    for k, v in transformation.items():
        methods = k.split(".")[0]
        field = k.split(".")[1]
        if methods not in new_trans:
            data = {methods: {field: v}}
            new_trans.update(data)
        else:
            data = {field: v}
            new_trans[methods].update(data)
    transformation = new_trans

    for k, v in transformation.items():
        building = call_method(building, k, v)

    return building


def run_building(building, df, out_dir, output_list, num_total="?"):
    """Run EnergyPlus simulation for the specified building.

    :param building: IDF object
    :type building: class:`predyce.IDF_class.IDF`
    :param df: DataFrame where each row represent a simulation.
    :type df: class:`pandas.core.frame.DataFrame`
    :param out_dir: Output directory of EnergyPlus.
    :type out_dir: path or str
    :param output_list: EnergyPlus outputs which has to be saved into the
        dataframe.
    :type output_list: list
    :param num_total: Total number of simulations, only for print purpose,
        defaults to "?"
    :type num_total: int, optional
    :return: Output dataframe corresponding to the original dataframe with
        added outputs.
    :rtype: class:`pandas.core.frame.DataFrame`
    """
    print("Running... %s/%s" % (df.name, num_total))
    # print(out_dir, "- Running... %s/%s" % (df.name, num_total))
    out_file = out_dir / "eplusout.csv"
    options = make_eplaunch_options(building, output_directory=out_dir)
    building.run(**options)
    out = pd.read_csv(out_file)

    return out


def generate_temp_folder(directory, length=25):
    """Generate temp folder name.

    :param directory: Initial directory
    :type directory: path
    :param length: Number of characters of the alphanumeric part of the name,
        defaults to 25
    :type length: int, optional
    :return: Temporary directory
    :rtype: path
    """
    random_string = "_" + str(uuid.uuid4()).replace("-", "")[0:length]
    temp_dir = directory / random_string
    return temp_dir


def remove_files(dir_path, preserve_path=False):
    """Remove `mtr`, `htm`, `edd`, `eio`, `eso`, `edd`, `shd`, `dxf`, `mtd`,
    `rdd`, `tab`, `mdd`, `bnd`, `err`, `audit`, `rvaudit`, `end`, `dbg`, `csv`,
    `epw`, `out` files from specified folder. This function can be utilised to
    clean EnergyPlus simulation files.

    :param dir_path: Path of directory
    :type dir_path: Path or str
    :param preserve_path: If `True`, the current working directory is not
        changed during file removal, defaults to False
    :type preserve_path: Path or str, optional

    .. warning:: The eplusout.csv file which contains simulation results is
        also deleted.
    """
    dir_path = Path(dir_path)
    extensions = (
        ".mtr",
        ".htm",
        ".edd",
        ".eio",
        ".eso",
        ".edd",
        ".shd",
        ".dxf",
        ".mtd",
        ".rdd",
        ".tab",
        ".mdd",
        ".bnd",
        ".err",
        ".audit",
        ".rvaudit",
        ".end",
        ".dbg",
        ".csv",
        ".epw",
        ".out",
    )
    files = os.listdir(dir_path)
    for item in files:
        if item.endswith(extensions):
            try:
                os.remove(dir_path / item)
            except OSError as e:
                print("Error: %s : %s" % (dir_path / item, e.strerror))
    try:
        if not preserve_path:
            os.chdir("..")
        os.rmdir(dir_path)
    except OSError as e:
        print("Error: %s : %s" % (dir_path, e.strerror))


class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NpEncoder, self).default(obj)


class Database:
    """Class which stores results."""

    def __init__(self, out_dir, interval):
        """
        :param out_dir: Output directory
        :type out_dir: Path or str
        :param interval: Checkpoint interval
        :type interval: int
        """
        self.df = pd.DataFrame()
        self.out_dir = out_dir
        self.cnt = 0
        self.interval = interval

    def add_data(self, data):
        """Add data to the database

        :param data: DataFrame which contains simulation results.
        :type data: class:`pandas.core.frame.DataFrame`
        """
        self.df = self.df.append(data, ignore_index=False)
        self.cnt += 1

    def checkpoint(self):
        """Save a checkpoint as CSV file if the number of data in the database
        is a multiple of the checkpoint interval.
        """
        if self.cnt % self.interval == 0:
            gc.collect(2)
            self.save_data(
                self.out_dir
                / "checkpoint-{}.csv".format(
                    datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
                )
            )

    def clean_data(self, columns):
        """Delete some columns from the database.

        :param columns: List of columns
        :type columns: list of str
        """
        for c in columns:
            del self.df[c]

    def save_data(self, filename, sep=";"):
        """Save database to CSV file.

        :param filename: Filename
        :type filename: str
        :param sep: Separator, defaults to ";"
        :type sep: str, optional
        """
        self.df.to_csv(filename, sep=sep)


class TimeSeriesDatabase:
    def __init__(self):
        self.df = pd.DataFrame()

    def add_data(self, series):
        """Add data to the database

        :param series: Series that will be added as a column to the database
        :type series: class:`pandas.core.series.Series`
        """
        self.df[series.name] = series

    def save_data(self, filename, sep=";"):
        """Save database to CSV file.

        :param filename: Filename
        :type filename: str
        :param sep: Separator, defaults to ";"
        :type sep: str, optional
        """
        self.df.to_csv(filename, sep=sep)
