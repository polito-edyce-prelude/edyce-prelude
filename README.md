# PREDYCE - Python Realtime Energy DYnamics and Climate Evaluation
## Politecnico di Torino - Department DAD
This software has been created by the ETD research group, lead by prof. Chiesa, at Politecnico di Torino - Department DAD (Architecture & Design), under different implementation actions: i. *DYCE*, part of the EU H2020 project **E-DYCE**
(https://edyce.eu/) co-funded by the EU H2020 research and innovation programme under GA 893945, and ii. *PRE*, part of the EU H2020 project **PRELUDE** (https://prelude-project.eu/) co-funded by the EU H2020 research and innovation programme under GA 958345.

<img src="https://www.polito.it/themes/custom/polito/logo.svg" width="200" style="background-color:#FFFFFF;padding:10px;">

![Python Version](https://img.shields.io/badge/python-^3.8-informational?style=flat&logo=python&logoColor=white)
[![License: GPL v3](https://img.shields.io/badge/license-GNU%20GPLv3-orange.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Depencency management: Poetry](https://img.shields.io/badge/dependency%20management-Poetry-2f2f55.svg)](https://python-poetry.org/)
[![Documentation Status](https://readthedocs.org/projects/predyce/badge/?version=latest)](https://predyce.readthedocs.io/en/latest/?badge=latest)

## Projects
- [E-DYCE](https://edyce.eu/)
- [PRELUDE](https://prelude-project.eu/)

# Installation
## Tools
- Python 3.8 or higher
- Poetry

## Virtual environment with Poetry
### Install [Poetry](https://github.com/python-poetry/poetry#Installation):

Linux:
``` sh
curl -sSL https://install.python-poetry.org | python3 -
```
Windows PowerShell (Administrator):
``` powershell 
(Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py -
```
### It may be necessary to add the following path to Windows PATH:
``%AppData%\Roaming\Python\Scripts``

### Add Poetry to Linux Shell:
``` sh
export PATH=$PATH:$HOME/.poetry/bin
```
### [Recommended] Configure Poetry it in order to create a virtual environment in a ```.venv``` folder inside the project directory:
```sh
poetry config virtualenvs.in-project true
```
* cd in project directory.
* Create virtual environment, upgrade pip and install Python dependencies:
``` sh
poetry run pip install --upgrade pip
poetry install
```
### Activate the virtual environment:
Linux:
``` sh
source .venv/bin/activate
```
Windows:
``` powershell
.venv\Scripts\activate
```
Alternate way on Linux and Windows:
``` sh
poetry shell
```

# Documentation
Activate the virtual environment, ```cd``` in the **docs** folder and run the following command to generate HTML documentation:
``` sh
make html
```
If you have LaTeX installed, you can also generate PDF documentation:
```
make latexpdf
```
# Authors
- Giacomo Chiesa
- Francesca Fasano
- Paolo Grasso
# Additional co-authors
- Paolo Carrisi
# License
This project is licensed under the [Creative Commons BY-NC-ND 4.0](./LICENSE)

Please quote one or more of the following references:

- Chiesa G, Fasano F, Grasso P (2021) A New Tool for Building Energy Optimization: First Round of Successful Dynamic Model Simulations, Energies 14(19), 6429. https://doi.org/10.3390/en14196429

- Chiesa G, Fasano F, Grasso P (2023) Energy simulation platform supporting building design and management, Techne 25, pp. 134-142. https://dx.doi.org/10.36253/techne-13583 
- Chiesa G, Fasano F, Grasso P (2023) "Simulated Versus Monitored Building Behaviours: Sample Demo Applications of a Perfomance Gap Detection Tool in a Northern Italian Climate", In: Sayigh A (Ed) Towards Net Zero Carbon Emissions in the Building Industry, Srpinger, Cham, pp. 109-133. https://dx.doi.org/10.1007/978-3-031-15218-4_6
- Chiesa G, Fasano F, Grasso P (2022) "Thermal Comfort and Climatic Potential of Ventilative Cooling in Italian Climates", In: Sayigh A (Ed) Achieving Building Comfort by Natural Means, Springer, Cham, pp. 423-449. https://dx.doi.org/10.1007/978-3-031-04714-5_18
- Chiesa G, Grasso P, Fasano F (2023) Comparing different approaches to define shading control threshold via a new automatic building simulation platform, Journal of Physics: Conference Series 2600, 092008, CISBAT 2023. doi:10.1088/1742-6596/2600/9/092008 
- Chiesa G, Fasano F, Grasso P (2023) Impact of different thermal zone data simplification for model calibration on monitored-simulated performance gaps, Journal of Physics: Conference Series 2600, 092022, CISBAT 2023. doi:10.1088/1742-6596/2600/9/092022


